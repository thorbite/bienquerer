package com.jujuy.bienquerer

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.paging.ExperimentalPagingApi
import com.facebook.login.LoginManager
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.material.navigation.NavigationView
import com.google.firebase.auth.FirebaseAuth
import com.jujuy.bienquerer.api.network.Resource
import com.jujuy.bienquerer.base.BaseActivity
import com.jujuy.bienquerer.base.NavigationController
import com.jujuy.bienquerer.databinding.MainActivityBinding
import com.jujuy.bienquerer.extensions.gone
import com.jujuy.bienquerer.extensions.visible
import com.jujuy.bienquerer.model.entity.User
import com.jujuy.bienquerer.ui.appointment.calendar.CalendarActivity
import com.jujuy.bienquerer.ui.home.HomeFragment
import com.jujuy.bienquerer.ui.notifications.NotificationsActivity
import com.jujuy.bienquerer.ui.search.SearchActivity
import com.jujuy.bienquerer.utils.*
import com.jujuy.bienquerer.viewmodel.notifications.NotificationsViewModel
import com.jujuy.bienquerer.viewmodel.user.UserViewModel
import com.skydoves.androidbottombar.BottomMenuItem
import com.skydoves.androidbottombar.OnMenuItemSelectedListener
import com.skydoves.androidbottombar.animations.BadgeAnimation
import com.skydoves.androidbottombar.forms.badgeForm
import com.skydoves.androidbottombar.forms.iconForm
import com.skydoves.androidbottombar.forms.titleForm
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.android.ext.android.inject
import timber.log.Timber
import kotlin.system.exitProcess
import kotlin.time.ExperimentalTime


@ExperimentalCoroutinesApi
@ExperimentalTime
class MainActivity : BaseActivity(), NavigationView.OnNavigationItemSelectedListener {

    private val navigationController: NavigationController by inject()
    private val userViewModel: UserViewModel by inject()
    private val notificationsViewModel: NotificationsViewModel by inject()
    private val pref: SharedPreferences by inject()
    private val sessionManager: AppPreferences by inject()
    private lateinit var messageNotificationTextView: TextView
    private var loginUserId: String? = null
    private var selectedLocationId: String? = null
    private var selectedLocationName: String? = null
    private var selected_lat: String? = null
    private var selected_lng: String? = null
    private var menuId: Int = 0
    private val psDialogMsg: PSDialogMsg by lazy { PSDialogMsg(this, false) }
    private val loadingMsg: LoadingMsg by lazy { LoadingMsg(this, false) }

    private var notificationTextView: TextView? = null
    lateinit var notificationIconImageView: ImageView
    var notificationCount = "0"

    private lateinit var mGoogleSignInClient: GoogleSignInClient

    var user: User? = null
    lateinit var drawerToggle: ActionBarDrawerToggle

    lateinit var binding: MainActivityBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        //setTheme(R.style.Base_PSTheme)
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView<MainActivityBinding>(this, R.layout.main_activity)
        setSupportActionBar(binding.toolbarMain)
        initUIAndActions()
        initData()

    }

    private fun initUIAndActions() {
        initToolbar(binding.toolbarMain, "Inicio")

        initDrawerLayout()
        initNavigationView()
        navigationController.navigateToHome(this)

        setSelectedMenu(R.id.nav_home)

        getIntentData()

        if (!sessionManager.isLoggedIn()) {
            binding.androidBottomBar.gone()
        }

        //val bottomNavigationMenuView: BottomNavigationMenuView = binding.bottomNavigationView.getChildAt(0) as BottomNavigationMenuView
        //val bTMView = bottomNavigationMenuView.getChildAt(2)
        //val itemView: BottomNavigationItemView = bTMView as BottomNavigationItemView
        //val badgeView = LayoutInflater.from(this).inflate(R.layout.notification_badge, itemView, true)
        ///messageNotificationTextView = badgeView.findViewById(R.id.notifications_badge)
        //messageNotificationTextView.gone()



        val titleForm = titleForm(this) {
            setTitleColor(getColor(R.color.global__primary_dark))
            setTitleActiveColorRes(R.color.global__accent)
            setTitleSize(12f)
        }

        val iconForm = iconForm(this) {
            setIconSize(23)
        }

        val badgeForm = badgeForm(this) {
            setBadgeTextSize(9f)
            setBadgePaddingLeft(2)
            setBadgePaddingRight(2)
            setBadgeDuration(550)
        }

        binding.androidBottomBar.addBottomMenuItems(
            mutableListOf(
                BottomMenuItem(this)
                    .setTitleForm(titleForm)
                    .setIconForm(iconForm)
                    .setBadgeForm(badgeForm)
                    .setTitle("Inicio")
                    .setIcon(R.drawable.ic_icon_home)
                    .setIconColor(R.color.black)
                    .build(),

                BottomMenuItem(this)
                    .setTitleForm(titleForm)
                    .setIconForm(iconForm)
                    .setBadgeForm(badgeForm)
                    .setTitle("Trabajos")
                    .setIcon(R.drawable.ic_icon_suitcase)
                    .setIconColor(R.color.black)
                    .build(),


                BottomMenuItem(this)
                    .setTitleForm(titleForm)
                    .setIconForm(iconForm)
                    .setBadgeForm(badgeForm)
                    .setTitle("Mensajes")
                    .setBadgeText("⭐⭐⭐")
                    .setBadgeColorRes(R.color.red)
                    .setBadgeTextColorRes(R.color.black)
                    .setBadgeAnimation(BadgeAnimation.SCALE)
                    .setIcon(R.drawable.ic_icon_chat)
                    .setIconColor(R.color.black)
                    .build(),

                BottomMenuItem(this)
                    .setTitleForm(titleForm)
                    .setIconForm(iconForm)
                    .setBadgeForm(badgeForm)
                    .setTitle("Favoritos")
                    .setIcon(R.drawable.ic_icon_heart)
                    .setIconColor(R.color.black)
                    .build(),

                BottomMenuItem(this)
                    .setTitleForm(titleForm)
                    .setIconForm(iconForm)
                    .setBadgeForm(badgeForm)
                    .setTitle("Mi Perfil")
                    .setIcon(R.drawable.ic_icon_user)
                    .setIconColor(R.color.black)
                    .build()
            )
        )

        binding.androidBottomBar.onMenuItemSelectedListener = object : OnMenuItemSelectedListener {
            override fun onMenuItemSelected(
                index: Int,
                bottomMenuItem: BottomMenuItem,
                fromUser: Boolean
            ) {
                when (index) {
                    0 -> {
                        refreshPSCount()
                        //Utils.addToolbarScrollFlag(binding.toolbarMain)
                        navigationController.navigateToHome(this@MainActivity)
                        setToolbarText(binding.toolbarMain, "Inicio")
                    }
                    1 -> {
                        refreshPSCount()
                        navigationController.navigateToJobs(this@MainActivity)
                        setToolbarText(binding.toolbarMain, "Agenda")
                    }
                    2 -> {
                        refreshPSCount()
                        navigationController.navigateToMessage(this@MainActivity)
                        setToolbarText(binding.toolbarMain, "Mensajes")
                    }
                    3 -> {
                        refreshPSCount()
                        navigationController.navigateToFavorite(this@MainActivity)
                        setToolbarText(binding.toolbarMain, "Favoritos")
                    }
                    4 -> {
                        refreshPSCount()
                        navigationController.navigateToUserProfile(this@MainActivity)
                    }
                }

            }
        }


        val googleSignInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(resources.getString(R.string.google_id))
            .requestEmail()
            .build()

        mGoogleSignInClient = GoogleSignIn.getClient(this, googleSignInOptions)

    }


    private fun getIntentData() {
        loginUserId = pref.getString(Constants.USER_ID, Constants.EMPTY_STRING)
        Timber.e("USER ID: $loginUserId" )
        selectedLocationId = intent.getStringExtra(Constants.SELECTED_LOCATION_ID)
        selectedLocationName = intent.getStringExtra(Constants.SELECTED_LOCATION_NAME)
        selected_lat = intent.getStringExtra(Constants.LAT)
        selected_lng = intent.getStringExtra(Constants.LNG)
        val goToJob = intent.getStringExtra("jobs")
        Timber.e("TO JOBS: $goToJob")
        if (goToJob != null) {
            binding.androidBottomBar.setOnBottomMenuInitializedListener {
                setBottomNavigationMenu(1)
            }
        }

        pref.edit().putString(Constants.SELECTED_LOCATION_ID, selectedLocationId).apply()
        pref.edit().putString(Constants.SELECTED_LOCATION_NAME, selectedLocationName).apply()
        pref.edit().putString(Constants.LAT, selected_lat).apply()
        pref.edit().putString(Constants.LNG, selected_lng).apply()
    }

    private fun initDrawerLayout() {
        drawerToggle = object : ActionBarDrawerToggle(
            this,
            binding.drawerLayout,
            binding.toolbarMain,
            R.string.app_drawer_open,
            R.string.app_drawer_close
        ) {}

        drawerToggle.apply {
            isDrawerIndicatorEnabled = false
            setHomeAsUpIndicator(R.drawable.baseline_menu_white_24)
            setToolbarNavigationClickListener { binding.drawerLayout.openDrawer(GravityCompat.START) }
        }

        binding.drawerLayout.apply {
            addDrawerListener(drawerToggle)
            post(drawerToggle::syncState)
        }
    }

    private fun initNavigationView() {
        binding.navView?.let {
            val m = binding.navView.menu
            try {
                m?.let {
                    for (i in 0 until m.size()) {
                        val mi = m.getItem(i)
                        mi.title = mi.title
                    }
                }
            } catch (e: Exception) {
                Timber.e(e)
            }
            binding.navView.setNavigationItemSelectedListener { menuItem ->
                navigationMenuChanged(menuItem)
                return@setNavigationItemSelectedListener true
            }
        }

        /*binding.bottomNavigationView.whatIfNotNull {
            val m = binding.bottomNavigationView.menu
            try {
                for(i in 0..m.size()){
                    val mi = m.getItem(i)
                    mi.title = mi.title
                }
            }catch (e:Exception){
                Timber.e(e)
            }
            binding.navView.setNavigationItemSelectedListener { menuItem ->
                navigationMenuChanged(menuItem)
                return@setNavigationItemSelectedListener true
            }

        }*/
    }

    private fun navigationMenuChanged(menuItem: MenuItem) {
        openFragment(menuItem.itemId)
        if (menuItem.itemId != R.id.nav_logout_login) {
            menuItem.isChecked = true
            Handler().postDelayed({
                binding.drawerLayout.closeDrawers()
            }, 100)
        }

    }

    fun refreshUserData() {
        try {
            loginUserId = pref.getString(Constants.USER_ID, Constants.EMPTY_STRING)
        } catch (e: Exception) {
            Timber.e("ERROR: $e")
        }
    }

    private fun openFragment(menuId: Int) {
        this.menuId = menuId
        when (menuId) {
            R.id.nav_home, R.id.nav_home_login -> {
                refreshPSCount()
                setToolbarText(binding.toolbarMain, "Inicio")
                navigationController.navigateToHome(this)
            }
            R.id.nav_category, R.id.nav_category_login -> {
                setToolbarText(binding.toolbarMain, "Inicio")
                navigationController.navigateToCategory(this)
            }
            R.id.nav_profile_login -> {
                navigationController.navigateToUserProfile(this)
            }
            R.id.nav_favourite_news_login -> {
                Utils.addToolbarScrollFlag(binding.toolbarMain)
                navigationController.navigateToFavorite(this@MainActivity)
                setToolbarText(binding.toolbarMain, "Favoritos")
            }

            R.id.nav_professional_calendar -> {
                startActivity(Intent(this, CalendarActivity::class.java))
            }

            R.id.nav_logout_login -> {
                psDialogMsg.showConfirmDialog(
                    getString(R.string.edit_setting__logout_question),
                    getString(R.string.app__ok),
                    getString(R.string.app__cancel)
                )

                psDialogMsg.show()

                psDialogMsg.okButton.setOnClickListener {

                    psDialogMsg.cancel()

                    hideBottomNavigation()
                    this.menuId = 0
                    logoutServerFcm()
                }
                psDialogMsg.cancelButton.setOnClickListener { view -> psDialogMsg.cancel() }
            }
        }
    }

    private fun logoutServerFcm() {
        sessionManager.fetchTokenFirebase()?.let {
            userViewModel.logoutUser(it).observe(this, { resource ->
                when (resource.status) {
                    Resource.Status.LOADING -> {
                        loadingMsg.showloaderDialog()
                        loadingMsg.show()
                    }
                    Resource.Status.SUCCESS -> {
                        loadingMsg.cancel()
                        LoginManager.getInstance().logOut()
                        FirebaseAuth.getInstance().signOut()
                        sessionManager.signOut()
                        Utils.deleteUserLoginData(pref)
                        navigationController.navigateToUserLoginActivity(this)
                        finish()
                    }
                    Resource.Status.ERROR -> {
                        loadingMsg.cancel()
                    }
                }
            })
        }
    }


    fun initData() {

            updateMenu()
        if (!sessionManager.isLoggedIn()) {
            setToolbarText(binding.toolbarMain, getString(R.string.app_name))
            navigationController.navigateToHome(this@MainActivity)
            mGoogleSignInClient.revokeAccess()
        }


        notificationsViewModel.getNotificationCount().observe(this, { listResource ->
            when (listResource.status) {
                Resource.Status.LOADING -> {
                }
                Resource.Status.SUCCESS -> {
                    if (listResource.data != null) {
                        notificationCount = listResource.data.data.toString()
                        if (notificationTextView != null) {
                            if (notificationCount == "0") {
                                notificationTextView!!.visibility = View.GONE
                            } else {
                                notificationTextView!!.visibility = View.VISIBLE
                                val count = notificationCount.toInt()
                                if (count > 9) {
                                    notificationTextView!!.text = "9+"
                                } else {
                                    notificationTextView!!.text = count.toString()
                                }
                            }
                        }
                    }
                }
                Resource.Status.ERROR -> {

                }
            }
        })

    }

    fun hideBottomNavigation() {
        Utils.removeToolbarScrollFlag(binding.toolbarMain)
    }


    private fun updateMenu() {
        if (!sessionManager.isLoggedIn()) {
            binding.navView.menu.setGroupVisible(R.id.group_before_login, true)
            binding.navView.menu.setGroupVisible(R.id.group_after_login, false)
            setSelectedMenu(R.id.nav_home)
        } else {
            binding.navView.menu.setGroupVisible(R.id.group_before_login, false)
            binding.navView.menu.setGroupVisible(R.id.group_after_login, true)
            when (menuId) {
                //R.id.nav_profile -> setSelectedMenu(R.id.nav_profile_login)
                R.id.nav_profile_login -> setSelectedMenu(R.id.nav_profile_login)
                else -> setSelectedMenu(R.id.nav_home_login)
            }
        }

    }

    fun setCalendarItemVisibility(phoneVerified: Boolean){
        binding.navView.menu.findItem(R.id.nav_professional_calendar).isVisible = phoneVerified
    }


    fun setSelectedMenu(id: Int) {
        binding.navView.setCheckedItem(id)
    }

    fun setBottomNavigationMenu(id: Int) {
        Timber.e("Cambiar al fragment home $id")
        binding.androidBottomBar.getBottomMenuItemView(id)
            .onMenuItemClickListener.invoke(
                binding.androidBottomBar.getBottomMenuItemView(id).config,
                binding.androidBottomBar.getBottomMenuItemView(id)
            )
    }

    fun exitApp() { //To exit the application call this function from fragment
        this.finishAffinity()
    }

    override fun onBackPressed() {
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            binding.drawerLayout.closeDrawer(GravityCompat.START)
        }else{
            if (supportFragmentManager.fragments.last() is HomeFragment) {
                val message = baseContext.getString(R.string.message_want_to_quit)
                val okStr = baseContext.getString(R.string.message_ok_close)
                val cancelStr = baseContext.getString(R.string.message_cancel_close)
                psDialogMsg.showConfirmDialog(message, okStr, cancelStr)
                psDialogMsg.show()
                psDialogMsg.okButton.setOnClickListener {
                    psDialogMsg.cancel()
                    finishAffinity()
                    exitProcess(0)
                }
                psDialogMsg.cancelButton.setOnClickListener { psDialogMsg.cancel() }
            } else {
                super.onBackPressed()
            }
        }

    }

    override fun onResume() {
        super.onResume()
        refreshPSCount()

    }

    fun refreshPSCount() {
        if (loginUserId!!.isNotEmpty()) {
            notificationsViewModel.setNotificationCount()
        }
    }

    /*override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            val message = baseContext.getString(R.string.message_want_to_quit)
            val okStr = baseContext.getString(R.string.message_ok_close)
            val cancelStr = baseContext.getString(R.string.message_cancel_close)
            psDialogMsg.showConfirmDialog(message, okStr, cancelStr)
            psDialogMsg.show()
            psDialogMsg.okButton.setOnClickListener {
                psDialogMsg.cancel()
                finishAffinity()
                exitProcess(0)
            }
            psDialogMsg.cancelButton.setOnClickListener { psDialogMsg.cancel() }
        }
        return true
    }*/


    @ExperimentalPagingApi
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.toolbar_menu, menu)
        val itemView = menu!!.getItem(1).actionView

        if (!sessionManager.isLoggedIn()) {
            menu.getItem(1).isVisible = false
        }

        notificationTextView = itemView.findViewById(R.id.txtCount) as TextView
        notificationTextView?.setText(notificationCount)

        if (notificationCount == "0") {
            notificationTextView?.gone()
        } else {
            notificationTextView?.visible()
            val count: Int = Integer.valueOf(notificationCount)
            if (count > 9) {
                notificationTextView?.setText("9+")
            }
        }
        notificationIconImageView = itemView.findViewById(R.id.notiImageView)
        notificationIconImageView.setOnClickListener{
                startActivity(Intent(this, NotificationsActivity::class.java))
            }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.navigation_notifications -> {
                startActivity(Intent(this, NotificationsActivity::class.java))
            }
            R.id.navigation_search -> {
                startActivity(Intent(this, SearchActivity::class.java))
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {

            //R.id.navigation_search -> startActivity(Intent(this,SearchActivity::class.java))
        }
        binding.drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        super.onActivityResult(requestCode, resultCode, data)
        val fragment =
            supportFragmentManager.findFragmentById(R.id.content_frame)
        fragment?.onActivityResult(requestCode, resultCode, data)
    }


}
