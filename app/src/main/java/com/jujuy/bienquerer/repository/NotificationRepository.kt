package com.jujuy.bienquerer.repository

import androidx.paging.ExperimentalPagingApi
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.jujuy.bienquerer.api.network.Resource
import com.jujuy.bienquerer.api.service.NotificationService
import com.jujuy.bienquerer.model.network.BaseResponse
import com.jujuy.bienquerer.model.network.response.ListNotifications
import com.jujuy.bienquerer.room.AppDatabase
import com.jujuy.bienquerer.room.NotificationDao
import com.jujuy.bienquerer.room.NotificationKeysDao
import com.jujuy.bienquerer.ui.notifications.NotificationRemoteMediator
import com.jujuy.bienquerer.utils.Connectivity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class NotificationRepository (private val connectivity: Connectivity,
                              private val notificationDao: NotificationDao,
                              private val notificationService: NotificationService,
                              private val notificationKeysDao: NotificationKeysDao,
                              private val roomDatabase: AppDatabase){


    fun getNotificationCount() : Flow<Resource<BaseResponse<Int>>> = flow {
        notificationService.getNotificationsCount().collect{emit(it)}
    }

    /*@ExperimentalCoroutinesApi
    fun getNotificationList() : Flow<Resource<List<ListNotifications>>> {
        return object : com.jujuy.yonecesito.utils.NetworkBoundResource<List<ListNotifications>, BaseResponse<DataResponse>>(){

            override fun shouldFetchFromRemote(data: List<ListNotifications>)= connectivity.isConnected

            override fun fetchFromLocal(): Flow<List<ListNotifications>> {
                return notificationDao.getAllNotificationList()
            }

            override fun fetchFromRemote(): Flow<Resource<BaseResponse<DataResponse>>> {
                return notificationService.getNotificationsList()
            }

            override fun saveRemoteData(data: BaseResponse<DataResponse>) {
                notificationDao.deleteAllNotificationList()
                data.data?.list?.let { notificationDao.insertAllNotificationList(it) }
            }
        }.asFlow().flowOn(Dispatchers.IO)

    }*/

    @OptIn(ExperimentalPagingApi::class)
    fun getNotificationList(): Flow<PagingData<ListNotifications>> {
        val pagingSourceFactory = { notificationDao.getAllNotificationList() }
        return Pager(
            config = PagingConfig(pageSize = 10,
                enablePlaceholders = false,
                prefetchDistance = 1, initialLoadSize = 10),
            remoteMediator = NotificationRemoteMediator(roomDatabase,
                notificationService, notificationDao, notificationKeysDao),
            pagingSourceFactory = pagingSourceFactory
        ).flow
    }

    @ExperimentalCoroutinesApi
    fun getNotificationItem(id: String) : Flow<Resource<ListNotifications>> {
        return object : com.jujuy.bienquerer.utils.NetworkBoundResource<ListNotifications, BaseResponse<ListNotifications>>(){
            override fun fetchFromLocal(): Flow<ListNotifications> {
                return notificationDao.getNotificationById(id)
            }

            override fun fetchFromRemote(): Flow<Resource<BaseResponse<ListNotifications>>> {
                return notificationService.getNotificationsItem(id)
            }

            override fun saveRemoteData(data: BaseResponse<ListNotifications>) {
                notificationDao.deleteNotificationbyId(id)
                data.data?.let { notificationDao.insert(it) }
            }

            override fun shouldFetchFromRemote(data: ListNotifications): Boolean {
                return connectivity.isConnected
            }
        }.asFlow().flowOn(Dispatchers.IO)
    }

    fun deleteNotification(id: String): Flow<Resource<BaseResponse<Boolean>>> = flow{
        notificationService.deleteNotificationbyId(id).collect { emit(it) }
    }
}
