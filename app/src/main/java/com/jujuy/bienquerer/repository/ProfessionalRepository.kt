package com.jujuy.bienquerer.repository

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.google.gson.Gson
import com.jujuy.bienquerer.api.network.Resource
import com.jujuy.bienquerer.api.service.ProfessionalService
import com.jujuy.bienquerer.model.ItemParameterHolder
import com.jujuy.bienquerer.model.RegisterProfessionalRequest
import com.jujuy.bienquerer.model.SocialNetworks
import com.jujuy.bienquerer.model.entity.Comment
import com.jujuy.bienquerer.model.entity.ItemSearch
import com.jujuy.bienquerer.model.entity.Professional
import com.jujuy.bienquerer.model.entity.ProfessionalFav
import com.jujuy.bienquerer.model.network.*
import com.jujuy.bienquerer.model.network.request.MapListRequest
import com.jujuy.bienquerer.model.network.request.ScoreRequest
import com.jujuy.bienquerer.model.network.response.FavoriteListResponse
import com.jujuy.bienquerer.room.AppDatabase
import com.jujuy.bienquerer.room.ProfessionalDao
import com.jujuy.bienquerer.room.RemoteKeysDao
import com.jujuy.bienquerer.ui.favorite.FavoriteRemoteMediator
import com.jujuy.bienquerer.ui.notifications.NotificationRemoteMediator
import com.jujuy.bienquerer.ui.professionals.map.MapListPagingSource
import com.jujuy.bienquerer.ui.professionals.search.adapter.ProfessionalsPagingSource
import com.jujuy.bienquerer.utils.Connectivity
import com.jujuy.bienquerer.utils.NetworkBoundResource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONException
import timber.log.Timber
import java.io.File
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class ProfessionalRepository(
    private val connectivity: Connectivity,
    val professionalDao: ProfessionalDao,
    val professionalService: ProfessionalService,
    private val roomDatabase: AppDatabase
) {

    fun getItemListByKey(itemParameterHolder: ItemParameterHolder): Flow<PagingData<Professional>> {
        val pagingSourceFactory = { professionalDao.getAllProfessionalList() }
        return Pager(
            config = PagingConfig(
                pageSize = 10,
                enablePlaceholders = false,
                prefetchDistance = 1,
                initialLoadSize = 10
            ),
            /*remoteMediator = ProfessionalRemoteMediator(
                itemParameterHolder,
                professionalService, professionalDao, remoteKeysDao
            ),*/
            pagingSourceFactory = { ProfessionalsPagingSource(itemParameterHolder, professionalService) }
        ).flow
    }

    fun getProfessionalDetail(professionalId: String): Flow<Resource<BaseResponse<Professional>>> = flow{
        professionalService.getProfessionalDetail(professionalId).collect {
            emit(it)
        }
    }

    fun getAllSocialNetworks(): Flow<Resource<BaseResponse<List<SocialNetworks>>>> = flow{
        professionalService.getAllSocialNetworks().collect {
            emit(it)
        }

    }

    fun updateUserLocation(itemParameterHolder: ItemParameterHolder): Flow<Resource<Professional>> = flow {
        professionalService.updateUserLocation(
            itemParameterHolder.userId,
            itemParameterHolder.address,
            itemParameterHolder.lat,
            itemParameterHolder.lng
        ).collect {
            when(it.status){
                Resource.Status.LOADING ->{ emit(Resource.loading(null))}
                Resource.Status.SUCCESS ->{
                    if (it.data?.code == 200){
                        professionalDao.update(it.data.data!!)
                    }
                    emit(Resource.success(professionalDao.getProfessionalById(it.data?.data?.id.toString())))
                }
                Resource.Status.ERROR ->{
                    emit(Resource.error(it.message!!, null))
                }
            }
        }
    }


    fun postSaveProfession(userId: String, professionId: String): Flow<Resource<BaseResponse<Boolean>>> = flow {
        professionalService.saveProfessionById(userId, professionId).collect {
            emit(it)
        }
    }

    /*suspend fun postSaveProfession(userId: String, professionId: String): LiveData<Resource<BaseResponse<Boolean>>> = withContext(Dispatchers.IO) {
        val statusLiveData = MutableLiveData<Resource<BaseResponse<Boolean>>>()
        professionalService.saveProfessionById(userId, professionId).request{
            it.onSuccess {
                if(data != null){
                    statusLiveData.postValue(Resource.success(data))
                }
            }.onError {
                statusLiveData.postValue(Resource.error(this.response.message(), null))
            }.onException {
                statusLiveData.postValue(Resource.error(this.message!!, null))
            }
        }
        return@withContext statusLiveData
    }*/

    fun deleteProfession(userId: String, professionId: String): Flow<Resource<BaseResponse<Boolean>>> = flow {
        professionalService.deleteProfessionById(userId, professionId).collect {
            emit(it)
        }
    }

    fun uploadFavouritePostToServer(professionalId: String): Flow<Resource<BaseResponse<Boolean>>> = flow{
        professionalService.setPostFavourite(professionalId).collect{
            emit(it)
        }
    }

    fun verifyFavoriteProfessional(professionalId: String): Flow<Resource<BaseResponse<Boolean>>> = flow {
        professionalService.getItemFavorite(professionalId).collect {
           emit(it)
        }
        /*return object : com.jujuy.bienquerer.utils.NetworkBoundResource<Boolean, BaseResponse<Boolean>>(){
            override fun saveRemoteData(data: BaseResponse<Boolean>) {
                professionalDao.updateProfessionalFavById(professionalId, data.data!!)
            }

            override fun fetchFromLocal(): Flow<Boolean> {
                return professionalDao.getIsFavProfessional(professionalId)
            }

            override fun fetchFromRemote(): Flow<Resource<BaseResponse<Boolean>>> {
                return professionalService.getItemFavorite(professionalId)
            }

            override fun shouldFetchFromRemote(data: Boolean) = connectivity.isConnected

        }.asFlow().flowOn(Dispatchers.IO)*/
    }

    fun removeFavoriteProfessional(professionalId: String): Flow<Resource<BaseResponse<Boolean>>> = flow{
        professionalService.deleteFavoriteById(professionalId).collect{
            emit(it)
        }

    }

    fun getListFavoriteProfessional(): Flow<PagingData<ProfessionalFav>>{
        val pagingSourceFactory = { professionalDao.getListFavourite() }
        return Pager(
            config = PagingConfig(pageSize = 10,
                enablePlaceholders = false,
                prefetchDistance = 1, initialLoadSize = 10),
            remoteMediator = FavoriteRemoteMediator(roomDatabase, professionalService),
            pagingSourceFactory = pagingSourceFactory
        ).flow

        /*return object : com.jujuy.bienquerer.utils.NetworkBoundResource<List<ProfessionalFav>, BaseResponse<FavoriteListResponse>>(){
            override fun fetchFromLocal(): Flow<List<ProfessionalFav>> {
                return professionalDao.getListFavourite()
            }

            override fun fetchFromRemote(): Flow<Resource<BaseResponse<FavoriteListResponse>>> {
                return professionalService.getListFavorite()
            }

            override fun saveRemoteData(data: BaseResponse<FavoriteListResponse>) {
                professionalDao.deleteAllFavouriteItems()
                data.data?.list?.let { professionalDao.insertAllFavourites(it) }
            }

            override fun shouldFetchFromRemote(data: List<ProfessionalFav>): Boolean {
                return connectivity.isConnected
            }

        }.asFlow().flowOn(Dispatchers.IO)*/
    }

    fun putAvailableProfessional(): Flow<Resource<BaseResponse<Boolean>>> = flow {
        professionalService.putProfessionalAvailable().collect{
            emit(it)
        }
    }

    fun putSocialProfessional(): Flow<Resource<BaseResponse<Boolean>>> = flow {
        professionalService.putProfessionalSocial().collect { emit(it) }
    }

    fun registerProfessional(profileImagePath: String,
                             bannerImagePath: String,
                             registerProfessionalRequest: RegisterProfessionalRequest): Flow<Resource<BaseResponse<Boolean>>> = flow{

        val profileFile = File(profileImagePath)
        val profileRequestFile: RequestBody = profileFile.asRequestBody("multipart/form-data".toMediaTypeOrNull())
        val profilebody: MultipartBody.Part = MultipartBody.Part.createFormData("profilePhoto",profileFile.name, profileRequestFile)

        val bannerFile = File(bannerImagePath)
        val bannerRequestFile: RequestBody = bannerFile.asRequestBody("multipart/form-data".toMediaTypeOrNull())
        val bannerbody: MultipartBody.Part = MultipartBody.Part.createFormData("coverPhoto",bannerFile.name, bannerRequestFile)

        val dataJson = Gson().toJson(registerProfessionalRequest)
        var draBody: RequestBody? = null
        try {
            draBody = dataJson.toRequestBody("text/plain".toMediaTypeOrNull())
            Timber.e("requestUploadSurvey: RequestBody : " + draBody)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        professionalService.registerProfessional(profilebody, bannerbody, draBody).collect{
            emit(it)
        }
    }

    fun getItemSearch(itemParameterHolder: ItemParameterHolder): Flow<Resource<BaseResponse<List<ItemSearch>>>> = flow{
        professionalService.getItemSearch(itemParameterHolder.keyword).collect{emit(it)}
    }

    fun putScoreToProfessional(score: ScoreRequest): Flow<Resource<BaseResponse<Boolean>>> = flow{
        professionalService.putProfessionalScore(score).collect{emit(it)}
    }

    fun getListComments(professionalId: String): Flow<Resource<BaseResponse<List<Comment>>>> = flow {
        professionalService.getListComments(professionalId).collect{emit(it)}
    }

    fun getMapListFilter(lat: String, lng: String, categoryId: String, professionId: String?, radio: String): Flow<Resource<BaseResponse<List<Professional>>>> = flow {
        professionalService.getMapListFilter(MapListRequest(lat,lng,radio,categoryId,professionId)).collect{emit(it)}
    }

    fun getMapListPage(itemParameterHolder: ItemParameterHolder): Flow<PagingData<Professional>> {
        val pagingSourceFactory = { professionalDao.getAllProfessionalList() }
        return Pager(
            config = PagingConfig(
                pageSize = 100000,
                enablePlaceholders = false,
                prefetchDistance = 1,
                initialLoadSize = 10
            ),
            /*remoteMediator = ProfessionalRemoteMediator(
                itemParameterHolder,
                professionalService, professionalDao, remoteKeysDao
            ),*/
            pagingSourceFactory = { MapListPagingSource(itemParameterHolder, professionalService) }
        ).flow
    }


}