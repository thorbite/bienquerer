package com.jujuy.bienquerer.repository

import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingState
import androidx.paging.RemoteMediator
import com.jujuy.bienquerer.api.service.ProfessionalService
import com.jujuy.bienquerer.model.ItemParameterHolder
import com.jujuy.bienquerer.model.entity.Professional
import com.jujuy.bienquerer.model.entity.RemoteKeys
import com.jujuy.bienquerer.model.network.request.ListProfessionalRequest
import com.jujuy.bienquerer.room.ProfessionalDao
import com.jujuy.bienquerer.room.RemoteKeysDao
import retrofit2.HttpException
import timber.log.Timber
import java.io.IOException
import java.io.InvalidObjectException

private const val STARTING_PAGE_INDEX = 1

@OptIn(ExperimentalPagingApi::class)
class ProfessionalRemoteMediator(
    private val itemParameterHolder: ItemParameterHolder,
    private val professionalService: ProfessionalService,
    private val professionalDao: ProfessionalDao,
    private val remoteKeysDao: RemoteKeysDao
): RemoteMediator<Int, Professional>() {
    override suspend fun load(loadType: LoadType, state: PagingState<Int, Professional>): MediatorResult {
        val page = when (loadType) {
            LoadType.REFRESH -> {
                val remoteKeys = getRemoteKeyClosestToCurrentPosition(state)
                Timber.d("REFRESH-> ${remoteKeys?.prevKey} -> ${remoteKeys?.nextKey} ")
                remoteKeys?.nextKey?.minus(1) ?: STARTING_PAGE_INDEX
            }
            LoadType.PREPEND -> {
                return MediatorResult.Success(endOfPaginationReached = true)
            }
            LoadType.APPEND -> {
                val remoteKeys = getRemoteKeyForLastItem(state)
                if (remoteKeys?.nextKey == null) {
                    return MediatorResult.Success(endOfPaginationReached = true)
                }
                Timber.d("APPEND-> ${remoteKeys.prevKey} -> ${remoteKeys.nextKey} ")
                remoteKeys.nextKey
            }

        }

        try {
            Timber.d("Page= $page, LoadType= $loadType")
            val apiResponse = professionalService.getProfessionalsBySubCatId(
                ListProfessionalRequest(
                itemParameterHolder.cat_id,
                itemParameterHolder.sub_cat_id
            ), page)

            val repos = apiResponse.data?.list ?: emptyList()
            val endOfPaginationReached = repos.isEmpty()
                // clear all tables in the database
                if (loadType == LoadType.REFRESH) {
                    remoteKeysDao.clearRemoteKeys()
                    professionalDao.deleteAllProfessionalList()
                }
                val prevKey = if (page == STARTING_PAGE_INDEX) null else page - 1
                val nextKey = if (endOfPaginationReached) null else page + 1
            Timber.d("PrevKey =$prevKey, NextKey=$nextKey endOfPaginationReached= $endOfPaginationReached")
            Timber.d("Page= $page, LoadType= $loadType")
                val keys = repos.map {
                    RemoteKeys(repoId = it.id.toLong(), prevKey = prevKey, nextKey = nextKey)
                }
                remoteKeysDao.insertAll(keys)
            Timber.e("KEYS= ${remoteKeysDao.getAllKeys()}")
                professionalDao.insertAllProfessionalList(repos)
            return MediatorResult.Success(endOfPaginationReached = endOfPaginationReached)
        } catch (exception: IOException) {
            return MediatorResult.Error(exception)
        } catch (exception: HttpException) {
            return MediatorResult.Error(exception)
        }
    }

    private suspend fun getRemoteKeyForLastItem(state: PagingState<Int, Professional>): RemoteKeys? {
        // Get the last page that was retrieved, that contained items.
        // From that last page, get the last item
        return state.pages.lastOrNull() { it.data.isNotEmpty() }?.data?.lastOrNull()
            ?.let { repo ->
                // Get the remote keys of the last item retrieved
                remoteKeysDao.remoteKeysRepoId(repo.id.toLong())
            }
    }

    private suspend fun getRemoteKeyForFirstItem(state: PagingState<Int, Professional>): RemoteKeys? {
        // Get the first page that was retrieved, that contained items.
        // From that first page, get the first item
        return state.pages.firstOrNull { it.data.isNotEmpty() }?.data?.firstOrNull()
            ?.let { repo ->
                // Get the remote keys of the first items retrieved
                remoteKeysDao.remoteKeysRepoId(repo.id.toLong())
            }
    }

    private suspend fun getRemoteKeyClosestToCurrentPosition(
        state: PagingState<Int, Professional>
    ): RemoteKeys? {
        // The paging library is trying to load data after the anchor position
        // Get the item closest to the anchor position
        return state.anchorPosition?.let { position ->
            state.closestItemToPosition(position)?.id?.let { repoId ->
                remoteKeysDao.remoteKeysRepoId(repoId.toLong())
            }
        }
    }
}