package com.jujuy.bienquerer.repository

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.filter
import com.jujuy.bienquerer.api.network.Resource
import com.jujuy.bienquerer.api.service.AppointmentService
import com.jujuy.bienquerer.model.entity.ProfessionalFav
import com.jujuy.bienquerer.model.network.BaseResponse
import com.jujuy.bienquerer.model.network.request.AppointmentRequest
import com.jujuy.bienquerer.model.network.response.Job
import com.jujuy.bienquerer.model.network.response.JobsListResponse
import com.jujuy.bienquerer.room.AppoinmentDao
import com.jujuy.bienquerer.room.JobDao
import com.jujuy.bienquerer.model.network.response.Appointment
import com.jujuy.bienquerer.model.network.response.ListAppoinments
import com.jujuy.bienquerer.room.AppDatabase
import com.jujuy.bienquerer.ui.appointment.professional.JobProfessionalRemoteMediator
import com.jujuy.bienquerer.ui.appointment.user.AppointmentUserRemoteMediator
import com.jujuy.bienquerer.ui.favorite.FavoriteRemoteMediator
import com.jujuy.bienquerer.utils.Connectivity
import com.jujuy.bienquerer.utils.NetworkBoundResource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.*
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class JobRepository (val appointmentService: AppointmentService,
                     private val connectivity: Connectivity,
                     val appoinmentDao: AppoinmentDao,
                     val jobDao: JobDao, private val roomDatabase: AppDatabase){

    fun sendRequestAppointent(appointmentRequest: AppointmentRequest): Flow<Resource<BaseResponse<Boolean>>> = flow{
        appointmentService.addNewAppointment(appointmentRequest).collect {emit(it) }
    }

    fun getListAppointmentPaged(): Flow<PagingData<Appointment>> {
        val pagingSourceFactory = { appoinmentDao.getAppointmentListPaged() }
        return Pager(
            config = PagingConfig(
                pageSize = 10,
                enablePlaceholders = false,
                prefetchDistance = 1, initialLoadSize = 10
            ),
            remoteMediator = AppointmentUserRemoteMediator(roomDatabase, appointmentService),
            pagingSourceFactory = pagingSourceFactory
        ).flow
    }

    fun getListAppointmentPagedFilterStatus(id: Int): Flow<PagingData<Appointment>> {
        val queryFlow = MutableStateFlow("init_query")
        val pagingSourceFactory = { appoinmentDao.getAppointmentListPaged() }
        return Pager(
            config = PagingConfig(
                pageSize = 10,
                enablePlaceholders = false,
                prefetchDistance = 1, initialLoadSize = 10
            ),
            remoteMediator = AppointmentUserRemoteMediator(roomDatabase, appointmentService),
            pagingSourceFactory = pagingSourceFactory
        ).flow.combine(queryFlow){ pagingData, _ -> pagingData.filter { it.status.id == id } }
    }

    fun getListAppointent(status: String): Flow<Resource<List<Appointment>>> {
        return object : NetworkBoundResource<List<Appointment>, BaseResponse<ListAppoinments>>(){
            override fun fetchFromLocal(): Flow<List<Appointment>> {
                return appoinmentDao.getAllAppointmentList()
            }

            override fun fetchFromRemote(): Flow<Resource<BaseResponse<ListAppoinments>>> {
                return appointmentService.getListAppointments()
            }

            override fun saveRemoteData(data: BaseResponse<ListAppoinments>) {
                appoinmentDao.deleteAllAppointmentList()
                data.data?.list?.let { appoinmentDao.insertAllAppointmentList(it) }
            }

            override fun shouldFetchFromRemote(data: List<Appointment>): Boolean {
                return connectivity.isConnected
            }

        }.asFlow().flowOn(Dispatchers.IO)
    }

    fun getListjobPaged(): Flow<PagingData<Job>> {
        val pagingSourceFactory = { jobDao.getJobListPaged() }
        return Pager(
            config = PagingConfig(
                pageSize = 10,
                enablePlaceholders = false,
                prefetchDistance = 1, initialLoadSize = 10
            ),
            remoteMediator = JobProfessionalRemoteMediator(roomDatabase, appointmentService),
            pagingSourceFactory = pagingSourceFactory
        ).flow
    }

    fun getListJob(status: String): Flow<Resource<List<Job>>> {
        return object : NetworkBoundResource<List<Job>, BaseResponse<JobsListResponse>>(){
            override fun fetchFromLocal(): Flow<List<Job>> {
                return jobDao.getAllJobList()
            }

            override fun fetchFromRemote(): Flow<Resource<BaseResponse<JobsListResponse>>> {
                return appointmentService.getListJobs()
            }

            override fun saveRemoteData(data: BaseResponse<JobsListResponse>) {
                jobDao.deleteAllJobList()
                data.data?.list?.let { jobDao.insertAllJobList(it) }
            }

            override fun shouldFetchFromRemote(data: List<Job>): Boolean {
                return connectivity.isConnected
            }

        }.asFlow().flowOn(Dispatchers.IO)
    }

    fun getJobDataById(id: String): Flow<Job> = jobDao.getJobById(id)

    fun getAppointmentDataById(id: String): Flow<Appointment> = appoinmentDao.getAppointmentById(id)

    fun changeStatusJob(comment: String?, jobId: String, statusId: String): Flow<Resource<BaseResponse<Any>>> = flow{
        appointmentService.changeStatusJob(comment, jobId, statusId).collect {
           emit(it)
        }
    }

}