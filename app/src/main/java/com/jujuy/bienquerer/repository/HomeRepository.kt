package com.jujuy.bienquerer.repository

import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.jujuy.bienquerer.api.service.CategoryService
import com.jujuy.bienquerer.model.entity.Category
import com.jujuy.bienquerer.model.entity.Provider
import com.jujuy.bienquerer.room.CategoryDao
import com.jujuy.bienquerer.room.ProviderDao
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

import java.util.ArrayList

class HomeRepository constructor(private val categoryService: CategoryService,
    val categoryDao: CategoryDao, val providerDao: ProviderDao) {

    /*suspend fun loadCategory(page: Int, error: (String) -> Unit) = withContext(Dispatchers.IO) {
        val liveDate = MutableLiveData<List<Category>>()
        var category = categoryDao.getAllCategoryList()
        if (category.isEmpty()) {
            categoryService.getAllCategories().request { response ->
                response.onSuccess {
                    category = data?.data!!
                    liveDate.postValue(category)
                    categoryDao.insertAllCategoryList(category)
                }.onError {
                    error(message())
                }.onException {
                    error(message())
                }
            }
        }
        liveDate.apply { postValue(category) }
    }
*/
    suspend fun loadRecentProvider(page: Int, error: (String) -> Unit) = withContext(Dispatchers.IO){
        val liveData = MutableLiveData<List<Provider>>()
        var provider= providerDao.getAllProviderList()
        if(provider.isEmpty()){
            provider = recentProviderList
            liveData.postValue(provider)
            providerDao.insertAllProviderList(provider)
        }
        liveData.apply { postValue(provider) }
    }

    val categoryList: ArrayList<Category>
        get() = Gson().fromJson<ArrayList<Category>>(categoryJson, object : TypeToken<ArrayList<Category>>() {
        }.type)

    val recentProviderList: ArrayList<Provider>
        get() = Gson().fromJson<ArrayList<Provider>>(RecentProviderJson, object : TypeToken<ArrayList<Provider>>() {

        }.type)


    private val categoryJson = "[{\n" +
            "  \"id\": \"cat1\",\n" +
            "  \"icon\": \"ic_home_white\",\n" +
            "  \"name\": \"Hogar\",\n" +
            "  \"photo\": \"c2\",\n" +
            "  \"page\": \"1\"\n" +
            "},\n" +
            "  {\n" +
            "    \"id\": \"cat2\",\n" +
            "    \"icon\": \"ic_health\",\n" +
            "    \"name\": \"Salud\",\n" +
            "    \"photo\": \"c8\",\n" +
            "    \"page\": \"1\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"id\": \"cat3\",\n" +
            "    \"icon\": \"ic_education\",\n" +
            "    \"name\": \"Educacion\",\n" +
            "    \"photo\": \"c12\",\n" +
            "    \"page\": \"1\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"id\": \"cat4\",\n" +
            "    \"icon\": \"ic_events\",\n" +
            "    \"name\": \"Eventos\",\n" +
            "    \"photo\": \"c11\",\n" +
            "    \"page\": \"1\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"id\": \"cat5\",\n" +
            "    \"icon\": \"ic_car\",\n" +
            "    \"name\": \"Vehiculos\",\n" +
            "    \"photo\": \"c5\",\n" +
            "    \"page\": \"1\"\n" +
            "  },\n" +
            "  {\n" +
            "  \"id\": \"cat6\",\n" +
            "  \"icon\": \"ic_plumber\",\n" +
            "  \"name\": \"Plomeria\",\n" +
            "  \"photo\": \"c3\",\n" +
            "  \"page\": \"1\"\n" +
            "}\n" +
            "]"

    private val RecentProviderJson = "[{\n" +
            "  \"id\": \"1\",\n" +
            "  \"firstName\": \"Jose\",\n" +
            "  \"lastName\": \"Plumber\",\n" +
            "  \"qualification\": \"2.5\",\n" +
            "  \"profilePhoto\": \"c2\"\n" +
            "},\n" +
            "  {\n" +
            "    \"id\": \"2\",\n" +
            "    \"firstName\": \"Hector\",\n" +
            "    \"lastName\": \"Bitancur\",\n" +
            "    \"qualification\": \"2.0\",\n" +
            "    \"profilePhoto\": \"c5\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"id\": \"3\",\n" +
            "    \"firstName\": \"Peter\",\n" +
            "    \"lastName\": \"Parker\",\n" +
            "    \"qualification\": \"4.2\",\n" +
            "    \"profilePhoto\": \"c6\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"id\": \"4\",\n" +
            "    \"firstName\": \"sg_clarke_quay\",\n" +
            "    \"lastName\": \"Sebas Electricista\",\n" +
            "    \"qualification\": \"1.3\",\n" +
            "    \"profilePhoto\": \"c8\"\n" +
            "  }\n" +
            "]"

}
