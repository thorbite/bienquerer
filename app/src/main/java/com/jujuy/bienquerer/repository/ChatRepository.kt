package com.jujuy.bienquerer.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.jujuy.bienquerer.api.network.Resource
import com.jujuy.bienquerer.model.Message
import com.jujuy.bienquerer.model.entity.Chat
import com.jujuy.bienquerer.model.entity.UserChat
import com.jujuy.bienquerer.room.MessageDao
import com.jujuy.bienquerer.utils.Utils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.util.*
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class ChatRepository (private val messageDao: MessageDao) {
    private lateinit var firebaseAuth: FirebaseAuth
    lateinit var refUsers: DatabaseReference
    private lateinit var chatList: List<Chat>
    private var lastChildTimeStamp: Long = 0
    private var loadMore = true
    private var deleteAll = true
    private var keyRepo: String = ""

    fun registerUserToFireBase(email: String, password: String): LiveData<Resource<Boolean>> {
        val statusLiveData = MutableLiveData<Resource<Boolean>>()
        val firebaseAuth: FirebaseAuth = FirebaseAuth.getInstance()
        firebaseAuth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener { value ->
                if (value.isSuccessful) {
                    statusLiveData.postValue(Resource.success(true))
                } else {
                    if (value.exception != null) {
                        try {
                            statusLiveData.postValue(
                                Resource.error(
                                    value.exception!!.message!!,
                                    false
                                )
                            )
                        } catch (e: Exception) {
                            Timber.e("ERROR")
                        }
                    }
                }
            }

        return statusLiveData
    }

    fun loginUserToFireBase(email: String, password: String): LiveData<Resource<Boolean>> {
        val statusLiveData = MutableLiveData<Resource<Boolean>>()
        val firebaseAuth: FirebaseAuth = FirebaseAuth.getInstance()
        firebaseAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener { value ->
            if (value.isSuccessful) {
                statusLiveData.postValue(Resource.success(true))
            } else {
                if (value.exception != null) {
                    try {
                        statusLiveData.postValue(Resource.error(value.exception!!.message!!, false))
                    } catch (e: Exception) {
                        Timber.e("ERROR")
                    }
                }
            }
        }

        return statusLiveData
    }

    fun saveUserToFirebase(userChat: UserChat): LiveData<Resource<Boolean>> {
        val statusLiveData = MutableLiveData<Resource<Boolean>>()
        val firebaseRef = FirebaseDatabase.getInstance().reference
        refUsers = firebaseRef.child("users").child(userChat.id)
        refUsers.setValue(userChat).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                statusLiveData.postValue(Resource.success(true))
            }else {
                if (task.exception != null) {
                    statusLiveData.postValue(Resource.error(task.exception!!.message!!, null))
                    Timber.e(task.exception!!.message)
                }
            }
        }

        return statusLiveData
    }

    suspend fun saveMessagesToFirebase(message: Message): LiveData<Resource<Boolean>> = withContext(Dispatchers.IO){
        val statusLiveData = MediatorLiveData<Resource<Boolean>>()
        val reference = FirebaseDatabase.getInstance().reference
        val pushedPostRef = reference.child("messages").child(message.sessionId).push()
        val key = pushedPostRef.key

        if (key != null) {
            message.id = key
            pushedPostRef.setValue(message).addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        //messageDao.insert(message)
                        /*loadMore = true
                        lastChildTimeStamp = 0L*/
                        statusLiveData.postValue(Resource.success(true))
                    } else {
                        if (task.exception != null) {
                            statusLiveData.postValue(
                                Resource.error(
                                    task.exception!!.message!!,
                                    null
                                )
                            )
                            Timber.e(task.exception!!.message)
                        }
                    }
                }
        } else {
            statusLiveData.postValue(Resource.error("", null))
        }

        return@withContext statusLiveData
    }

    suspend fun saveUserChatRoomToFirebase(chat: Chat, senderId: String, receiverId: String): LiveData<Resource<Boolean>> = withContext(Dispatchers.IO){
        val statusLiveData = MediatorLiveData<Resource<Boolean>>()
        val ref = FirebaseDatabase.getInstance().reference.child("userChatrooms")
        val keyChat: String = Utils.generateKeyForChatHeadId(senderId, receiverId)
        val map: HashMap<String, Any> = HashMap()
        //val map2: HashMap<String, Any> = HashMap()
        map[keyChat] = chat
        /*map2[senderId] = map
        map2[receiverId] = map*/
        ref.child(senderId).updateChildren(map).addOnCompleteListener { task ->
            if (task.isSuccessful){
                ref.child(receiverId).updateChildren(map).addOnCompleteListener { task ->
                    statusLiveData.postValue(Resource.success(true))
                }
            }else {
                if (task.exception != null) {
                    statusLiveData.postValue(task.exception!!.message?.let {
                        Resource.error(it, null) })
                    Timber.e(task.exception!!.message)
                }
            }
        }
        return@withContext statusLiveData
    }

    fun getMessagesFromSpecificNode(
        senderId: String,
        receiverId: String
    ): LiveData<Resource<Boolean>> {
        var statusLiveData = MutableLiveData<Resource<Boolean>>()
        val key: String = Utils.generateKeyForChatHeadId(senderId, receiverId)
        if (key != keyRepo){
            lastChildTimeStamp = 0
            loadMore = true
            deleteAll = true
            statusLiveData = checkRepository(senderId, receiverId, key)
        }else{
            keyRepo = key
            statusLiveData = checkRepository(senderId, receiverId, key)
        }
        Timber.e("LOADMORE : $loadMore")

        return statusLiveData
    }

    fun checkRepository(senderId: String, receiverId: String, key: String): MutableLiveData<Resource<Boolean>>{
        val liveData = MutableLiveData<Resource<Boolean>>()
        keyRepo = key
        if (loadMore) {
            val query: Query
            val messageList: MutableList<Message?> = ArrayList()
            val databaseReference = FirebaseDatabase.getInstance().reference
            query = if (lastChildTimeStamp == 0L) {
                databaseReference.child("messages")
                    .child(key)
                    .orderByChild("addedDate")
                    .limitToLast(10)
            } else {
                databaseReference.child("messages")
                    .child(key)
                    .orderByChild("addedDate")
                    .limitToLast(10)
                    .endAt(lastChildTimeStamp.toDouble())
            }
            query.addValueEventListener(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    messageList.clear()
                    for (snapshot in dataSnapshot.children) {
                        val message = snapshot.getValue(Message::class.java)
                        messageList.add(message)
                    }
                    Timber.e("lastChildTimeStamp : $lastChildTimeStamp")
                    Timber.e("loadMore : $loadMore")
                    Timber.e("MESSAGES : $messageList")
                    if (messageList.size > 0) {
                        if (lastChildTimeStamp != messageList[0]?.addedDate || messageList.size != 1) {
                            if(deleteAll){
                                messageDao.deleteAll(Utils.generateKeyForChatHeadId(senderId, receiverId))
                                deleteAll = false
                            }
                            messageDao.insertAll(messageList)
                            Timber.e("MESSAGES LOCAL : ${messageList.toString()}")
                            lastChildTimeStamp = messageList[0]?.addedDate!!
                        } else {
                            loadMore = false

                        }
                    }
                    liveData.postValue(Resource.success(true))
                }

                override fun onCancelled(databaseError: DatabaseError) {
                    liveData.postValue(Resource.error("error", null))
                }
            })
        } else {
            Timber.e("ERROR chatrepository")
            liveData.postValue(Resource.error("error", null))
        }
        return liveData
    }

    fun getMessagesFromDatabase(
        senderId: String,
        receiverId: String
    ): LiveData<List<Message>> {
        return messageDao.getMessages(Utils.generateKeyForChatHeadId(senderId, receiverId))
    }


    fun getUserChatrooms(userId: String): LiveData<List<Chat>>{
        val statusLiveData = MediatorLiveData<List<Chat>>()
        chatList = ArrayList()
        val dbReference = FirebaseDatabase.getInstance().reference
            .child("userChatrooms")
            .child(userId)

        dbReference.addValueEventListener(object : ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {
                (chatList as ArrayList).clear()
                for (data in snapshot.children){
                    val list = data.getValue(Chat::class.java)
                    (chatList as ArrayList).add(list!!)
                }
                statusLiveData.postValue(chatList)
            }
            override fun onCancelled(error: DatabaseError) {}

        })
        statusLiveData.postValue(chatList)
        return statusLiveData
    }

    fun saveUnSeenCountToFirebase(key: String, receiver: String): LiveData<Resource<Boolean>> {
        val statusLiveData = MutableLiveData<Resource<Boolean>>()
        val dbRef = FirebaseDatabase.getInstance().reference.child("unseenMsgCountData")
            .child(key).child(receiver).child("unseenMsgCount")

        dbRef.runTransaction(object : Transaction.Handler {
            override fun doTransaction(mutableData: MutableData): Transaction.Result {
                val currentValue = mutableData.getValue(Int::class.java)
                if (currentValue == null) {
                    mutableData.value = 1
                } else {
                    mutableData.value = currentValue + 1
                }
                return Transaction.success(mutableData)
            }

            override fun onComplete(
                databaseError: DatabaseError?,
                committed: Boolean,
                dataSnapshot: DataSnapshot?
            ) {
                statusLiveData.postValue(Resource.success(true))
            }
        })

        return statusLiveData
    }

    fun resetUnSeenCountToFirebase(key: String, userId: String): LiveData<Resource<Boolean>> {
        val statusLiveData = MutableLiveData<Resource<Boolean>>()
        val dbRef = FirebaseDatabase.getInstance().reference.child("unseenMsgCountData")
            .child(key).child(userId).child("unseenMsgCount")

        dbRef.runTransaction(object : Transaction.Handler {
            override fun doTransaction(mutableData: MutableData): Transaction.Result {
                val currentValue = mutableData.getValue(Int::class.java)
                mutableData.value = 0
                return Transaction.success(mutableData)
            }

            override fun onComplete(
                databaseError: DatabaseError?,
                committed: Boolean,
                dataSnapshot: DataSnapshot?
            ) {
                statusLiveData.postValue(Resource.success(true))
            }
        })

        return statusLiveData
    }

    /*fun saveChatToFirebase(
        chat: Message,
        senderId: String,
        receiverId: String
    ): LiveData<Resource<Boolean>> {
        val statusLiveData = MediatorLiveData<Resource<Boolean>>()
        val reference = FirebaseDatabase.getInstance().reference

        val key = reference.push().key

        if (key != null) {
            chat.messageId = key
            reference.child("Chats").child(key).setValue(chat)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        val chatListReference = FirebaseDatabase.getInstance()
                            .reference.child("ChatList").child(senderId).child(receiverId)

                        chatListReference.addListenerForSingleValueEvent(object : ValueEventListener {
                            override fun onCancelled(error: DatabaseError) {
                            }

                            override fun onDataChange(snapshot: DataSnapshot) {
                                if (!snapshot.exists()){
                                    chatListReference.child("id").setValue(receiverId)
                                }
                                val chatListReceiverRef = FirebaseDatabase.getInstance()
                                    .reference.child("ChatList").child(receiverId).child(senderId)
                                chatListReceiverRef.child("id").setValue(senderId)
                            }

                        })



                        val reference = FirebaseDatabase.getInstance().reference.child("Users").child(senderId)
                        statusLiveData.postValue(Resource.success(true))
                    } else {
                        if (task.exception != null) {
                            statusLiveData.postValue(task.exception!!.message?.let {
                                Resource.error(
                                    it,
                                    null
                                )
                            })
                            Timber.e(task.exception!!.message)
                        }
                    }
                }
        }

        */
    /*reference.child("ChatLists").child("id").setValue(senderId)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    statusLiveData.postValue(Resource.success(true))
                } else {
                    if (task.exception != null) {
                        statusLiveData.postValue(task.exception!!.message?.let {
                            Resource.error(
                                it,
                                null
                            )
                        })
                        Timber.e(task.exception!!.message)
                    }
                }
            }*//*
        return statusLiveData;
    }*/


}