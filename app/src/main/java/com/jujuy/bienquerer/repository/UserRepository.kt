package com.jujuy.bienquerer.repository

import androidx.annotation.MainThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jujuy.bienquerer.api.network.*
import com.jujuy.bienquerer.api.service.AuthService
import com.jujuy.bienquerer.model.entity.*
import com.jujuy.bienquerer.model.network.*
import com.jujuy.bienquerer.model.network.request.FacebookRequest
import com.jujuy.bienquerer.model.network.request.GoogleRequest
import com.jujuy.bienquerer.model.network.request.LoginRequest
import com.jujuy.bienquerer.model.network.request.RegisterRequest
import com.jujuy.bienquerer.model.network.response.LoginResponse
import com.jujuy.bienquerer.room.UserDao
import com.jujuy.bienquerer.utils.Connectivity
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import timber.log.Timber
import java.io.File

@ExperimentalCoroutinesApi
class UserRepository constructor(
    private val connectivity: Connectivity,
    private val appExecutors: AppExecutors,
    private val authService: AuthService,
    private val userDao: UserDao
) {


    fun doLogin(loginRequest: LoginRequest): Flow<Resource<LoginResponse>> = flow {
        authService.postUserLogin(loginRequest).collect {
            emit(it)
        }
    }

    fun getLoginUser() = userDao.getUserLoginData()

    fun getDetailUser(userId: String): Flow<Resource<BaseResponse<User>>> = flow {
        authService.getUser(userId).collect {
            emit(it)
        }
    }

    @MainThread
    fun getUserLocal(userId: String): Flow<User> = userDao.getUserData(userId)

    fun getLoginUserRemote(): Flow<Resource<User>> = flow {
        authService.getUserDetail().collect {
            when (it.status) {
                Resource.Status.LOADING -> {
                    emit(Resource.loading(null))
                }
                Resource.Status.SUCCESS -> {
                    val user = it.data?.data
                    Timber.e("getLoginUser " + user.toString())
                    userDao.deleteUserLogin()
                    user?.let { data -> userDao.insert(data) }
                    emit(Resource.success(user))
                }
                Resource.Status.ERROR -> {
                    emit(Resource.error(it.message!!, null))
                }
            }
        }
    }

    fun postGoogleLogin(googleRequest: GoogleRequest): Flow<Resource<LoginResponse>> = flow {
        authService.postUserGoogle(googleRequest).collect { emit(it) }
    }

    fun postFacebookLogin(facebookRequest: FacebookRequest): Flow<Resource<LoginResponse>> = flow {
        authService.postRegisterFacebook(facebookRequest).collect { emit(it) }
    }

    fun registerUser(registerRequest: RegisterRequest): Flow<Resource<LoginResponse>> = flow {
        authService.postUser(registerRequest).collect {
            when (it.status) {
                Resource.Status.LOADING -> {
                    emit(Resource.loading(null))
                }
                Resource.Status.SUCCESS -> {
                    if (it.data != null) {
                        userDao.deleteUserLogin()
                        emit(Resource.success(it.data))
                    }
                }
                Resource.Status.ERROR -> {
                    emit(Resource.error(it.message!!,null))
                }
            }
        }
    }


    fun delete(user: User): LiveData<Resource<Boolean>> {
        val statusLiveData = MutableLiveData<Resource<Boolean>>()
        CoroutineScope(Dispatchers.IO).launch {
            userDao.deleteUserLogin()
            statusLiveData.postValue(Resource.success(true))
        }
        return statusLiveData
    }

    fun postSendSms(userId: String): Flow<Resource<LoginResponse>> = flow {
        authService.sendSmsforCode(userId).collect {
            emit(it)
        }
    }

    fun sendSmsPhone(phone: String): Flow<Resource<BaseResponse<Any>>> = flow {
        authService.sendSms(phone).collect {
            emit(it)
        }
    }


    fun updateUser(user: User?): Flow<Resource<BaseResponse<Any>>> = flow {
        var userId = ""
        var resultsDb: BaseResponse<Any>? = null

        authService.putUser(
            user?.id!!,
            user.person?.name,
            user.person?.lastname,
            user.username!!,
            user.email!!,
            user.phone,
            user.person?.address,
            "",
            null,
            user.rol?.get(0)?.id
        ).collect {
            when (it.status) {
                Resource.Status.LOADING -> {
                    emit(Resource.loading(null))
                }
                Resource.Status.SUCCESS -> {
                    if (it.data?.code == 200) {
                        userId = user.id
                        userDao.update(user)
                        val userLogin = UserLogin(userId, true, user)
                        userDao.update(userLogin)
                    }

                    emit(Resource.success(it.data))
                }
                Resource.Status.ERROR -> {
                    emit(Resource.error(it.message!!, null))
                }
            }
        }
    }

    fun uploadImage(filePath: String): Flow<Resource<User>> = flow {
        val file = File(filePath)
        val requestFile: RequestBody =
            file.asRequestBody("multipart/form-data".toMediaTypeOrNull())
        val body: MultipartBody.Part =
            MultipartBody.Part.createFormData("file", file.name, requestFile)
        // add another part within the multipart request
        val fullName = file.name.toRequestBody("multipart/form-data".toMediaTypeOrNull())
        //val platformRB: RequestBody = RequestBody.create(parse.parse("multipart/form-data"), platform)
        //val useIdRB = userId.toRequestBody("multipart/form-data".toMediaTypeOrNull())
        authService.doUploadImage(fullName, body).collect {
            when (it.status) {
                Resource.Status.LOADING -> {
                    emit(Resource.loading(null))
                }
                Resource.Status.SUCCESS -> {
                    var userId = ""
                    val datas = it.data?.data
                    userId = datas?.id.toString()
                    Timber.e("USER Antes" + userDao.getUser(userId))
                    val person = Person(
                        name = datas?.name,
                        lastname = datas?.lastname,
                        address = datas?.address,
                        photoProfileUrl = datas?.photo_profile_url
                    )
                    val user = User(
                        id = datas?.id.toString(), email = datas?.email, person = person
                    )

                    userDao.updateImageProfile(userId, datas?.photo_profile_url!!)

                    //val userLogin = UserLogin(userId, true, user)
                    userDao.updateImageProfileUserLogin(userId, datas.photo_profile_url!!)
                    Timber.e("USER Despues" + userDao.getUser(userId))
                    val resultsDb = userDao.getUser(userId)
                    emit(Resource.success(resultsDb))
                }
                Resource.Status.ERROR -> {
                    emit(Resource.error(it.message!!, null))
                }
            }
        }
    }

    fun uploadCoverImage(filePath: String): Flow<Resource<User>> = flow {
        val file = File(filePath)
        val requestFile: RequestBody =
            file.asRequestBody("multipart/form-data".toMediaTypeOrNull())
        val body: MultipartBody.Part =
            MultipartBody.Part.createFormData("file", file.name, requestFile)
        // add another part within the multipart request
        val fullName = file.name.toRequestBody("multipart/form-data".toMediaTypeOrNull())
        //val platformRB: RequestBody = RequestBody.create(parse.parse("multipart/form-data"), platform)
        //val useIdRB = userId.toRequestBody("multipart/form-data".toMediaTypeOrNull())
        authService.doUploadCoverImage(fullName, body).collect {
            when (it.status) {
                Resource.Status.LOADING -> {
                    emit(Resource.loading(null))
                }
                Resource.Status.SUCCESS -> {
                    var userId = ""
                    val datas = it.data?.data
                    userId = datas?.id.toString()
                    Timber.e("USER Antes" + userDao.getUser(userId))

                    userDao.updateImageCoverProfile(userId, datas?.photo_cover_url!!)

                    userDao.updateImageCoverProfileUserLogin(userId, datas.photo_cover_url!!)
                    Timber.e("USER Despues" + userDao.getUser(userId))
                    val resultsDb = userDao.getUser(userId)
                    emit(Resource.success(resultsDb))
                }
                Resource.Status.ERROR -> {
                    emit(Resource.error(it.message!!, null))
                }
            }
        }
    }

    fun logoutUser(fcmKey: String): Flow<Resource<BaseResponse<Any>>> = flow {
        authService.logoutUser(fcmKey).collect {
            emit(it)
        }
    }
}