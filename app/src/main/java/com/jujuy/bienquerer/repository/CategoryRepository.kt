package com.jujuy.bienquerer.repository

import android.content.SharedPreferences
import androidx.annotation.MainThread
import androidx.core.content.edit
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jujuy.bienquerer.api.network.ApiResponse
import com.jujuy.bienquerer.api.network.AppExecutors
import com.jujuy.bienquerer.api.network.NetworkBoundResource
import com.jujuy.bienquerer.api.network.Resource
import com.jujuy.bienquerer.api.service.CategoryService
import com.jujuy.bienquerer.api.service.SubCategoryService
import com.jujuy.bienquerer.model.entity.Category
import com.jujuy.bienquerer.model.entity.SubCategory
import com.jujuy.bienquerer.model.network.*
import com.jujuy.bienquerer.room.CategoryDao
import com.jujuy.bienquerer.room.SubCategoryDao
import com.jujuy.bienquerer.utils.Connectivity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.withContext
import timber.log.Timber
import kotlin.time.ExperimentalTime
import kotlin.time.hours
@ExperimentalTime
@ExperimentalCoroutinesApi
class CategoryRepository constructor(private val connectivity: Connectivity,
                                     private val appExecutors: AppExecutors,
                                     private val categoryDao: CategoryDao,
                                     private val subCategoryDao: SubCategoryDao,
                                     private val categoryService: CategoryService,
                                     private val subCategoryService: SubCategoryService,
                                     private val sharedPref: SharedPreferences) {

    companion object {
        @ExperimentalTime
        private val EXPIRY_IN_MILLIS = 1.hours.inMilliseconds.toLong()
        private const val KEY_LAST_SYNCED = "last_synced"
    }

    suspend fun getAllItemSubCategoryList(): LiveData<Resource<List<SubCategory>>> = withContext(Dispatchers.IO){
        return@withContext object : NetworkBoundResource<List<SubCategory>, BaseResponse<List<SubCategory>>>(appExecutors){
            override fun saveCallResult(item: BaseResponse<List<SubCategory>>) {
                subCategoryDao.deleteAllSubCategory()
                val list = item.data
                if (list != null) {
                    subCategoryDao.insertAll(list)
                    for (i in list.indices){
                        subCategoryDao.insert(SubCategory(list[i].idSubCategory,list[i].nameSubCategory,list[i].createdAtSubCategory,list[i].updatedAtSubCategory,list[i].categorySubCategory, list[i].imageBackgroundUrl))
                    }
                }
                Timber.e("HILO!!!!!!!!!!  : I'm working in thread ${Thread.currentThread().name}")

            }

            override fun shouldFetch(data: List<SubCategory>?) = connectivity.isConnected

            override fun loadFromDb(): LiveData<List<SubCategory>> = subCategoryDao.getAllSubCategory()


            override fun createCall(): LiveData<ApiResponse<BaseResponse<List<SubCategory>>>> {
                Timber.e("HILO!!!!!!!!!!  : I'm working in thread ${Thread.currentThread().name}")
                return subCategoryService.getAllSubCategories()
            }

        }.asLiveData()
    }

    @MainThread
    fun getSubCategoryList(idCat: String): Flow<List<SubCategory>> = subCategoryDao.getSubCategoryList(idCat.toInt())

   /* fun getSubCategoryList(idCat: String): Flow<Resource<List<SubCategory>>>{
        return object :
            NetworkBoundResource<List<SubCategory>, BaseResponse<List<SubCategory>>>(appExecutors) {
            override fun saveCallResult(item: BaseResponse<List<SubCategory>>) {
                subCategoryDao.deleteAllSubCategory()
                val list = item.data?.toList()
                if (list != null) {
                    try {
                        subCategoryDao.insertAll(list)
                        Timber.d("DEBUG Repository LIST: %s", list)
                        for (i in list.indices) {
                            subCategoryDao.insert(
                                SubCategory(
                                    list[i].idSubCategory,
                                    list[i].nameSubCategory,
                                    list[i].createdAtSubCategory,
                                    list[i].updatedAtSubCategory,
                                    list[i].categorySubCategory,
                                    list[i].imageBackgroundUrl
                                )
                            )
                        }
                    }catch (e:Exception){
                        Timber.e("Error en : $e ")
                    }

                }
            }

            override fun shouldFetch(data: List<SubCategory>?) = connectivity.isConnected

            override fun loadFromDb(): LiveData<List<SubCategory>> =
                subCategoryDao.getSubCategoryList(idCat.toInt())

            override fun createCall(): LiveData<ApiResponse<BaseResponse<List<SubCategory>>>> {
                return subCategoryService.getAllSubCategoriesByCatId(idCat)
            }
        }.asLiveData()

    }*/


    fun loadCategoryList(): Flow<Resource<List<Category>>> {
        return object :
            com.jujuy.bienquerer.utils.NetworkBoundResource<List<Category>, BaseResponse<List<Category>>>() {
            override fun fetchFromLocal(): Flow<List<Category>> {
                return categoryDao.getAllCategoryById()
            }

            override fun fetchFromRemote(): Flow<Resource<BaseResponse<List<Category>>>> {
                return categoryService.getAllCategories()
            }

            override fun saveRemoteData(data: BaseResponse<List<Category>>) {
                categoryDao.deleteAllCategoryList()
                val list = data.data
                if (list != null) {
                    for (i in list.indices) {
                        categoryDao.insert(
                            Category(
                                i + 1,
                                list[i].idCategory,
                                list[i].nameCategory,
                                list[i].created_atCategory,
                                list[i].updated_atCategory,
                                list[i].imageUrl,
                                list[i].imageBackgroundUrl
                            )
                        )
                    }
                }
                sharedPref.edit {
                    putLong(KEY_LAST_SYNCED, System.currentTimeMillis())
                }
            }

            override fun shouldFetchFromRemote(data: List<Category>) : Boolean {
                val lastSynced = sharedPref.getLong(KEY_LAST_SYNCED, -1)
                return lastSynced == -1L ||
                        data.isNullOrEmpty() ||
                        isExpired(lastSynced)
            }

        }.asFlow().flowOn(Dispatchers.IO)
    }


    fun getAllProfessions(): Flow<Resource<List<SubCategory>>>{
        return object : com.jujuy.bienquerer.utils.NetworkBoundResource<List<SubCategory>, BaseResponse<List<SubCategory>>>(){
            override fun fetchFromLocal(): Flow<List<SubCategory>> {
               return subCategoryDao.getAllProfessions()
            }

            override fun fetchFromRemote(): Flow<Resource<BaseResponse<List<SubCategory>>>> {
                return subCategoryService.getAllProfessions()
            }

            override fun saveRemoteData(data: BaseResponse<List<SubCategory>>) {
                subCategoryDao.deleteAllSubCategory()
                subCategoryDao.insertAll(data.data!!)
            }

            override fun shouldFetchFromRemote(data: List<SubCategory>) = connectivity.isConnected
        }.asFlow().flowOn(Dispatchers.IO)
    }


    private fun isExpired(lastSynced: Long): Boolean {
        val currentTime = System.currentTimeMillis()
        return (currentTime - lastSynced) >= EXPIRY_IN_MILLIS
    }

}
