@file:Suppress("unused")

package com.jujuy.bienquerer.extensions

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.ViewAnimationUtils
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.jujuy.bienquerer.R

fun checkIsMaterialVersion() = true

fun Activity.circularRevealedAtCenter(view: View) {
  val cx = (view.left + view.right) / 2
  val cy = (view.top + view.bottom) / 2
  val finalRadius = view.width.coerceAtLeast(view.height)

  if (checkIsMaterialVersion() && view.isAttachedToWindow) {
    ViewAnimationUtils.createCircularReveal(view, cx, cy, 0f, finalRadius.toFloat())
      .apply {
        view.visible()
        view.setBackgroundColor(ContextCompat.getColor(baseContext, R.color.background))
        duration = 550
        start()
      }
  }
}

fun Activity.requestGlideListener(view: View): RequestListener<Drawable> {
  return object : RequestListener<Drawable> {
    override fun onLoadFailed(
      e: GlideException?,
      model: Any?,
      target: Target<Drawable>?,
      isFirstResource: Boolean
    ): Boolean {
      return false
    }

    override fun onResourceReady(
      resource: Drawable?,
      model: Any?,
      target: Target<Drawable>?,
      dataSource: DataSource?,
      isFirstResource: Boolean
    ): Boolean {
      circularRevealedAtCenter(view)
      return false
    }
  }
}

fun AppCompatActivity.simpleToolbarWithHome(toolbar: Toolbar, title_: String = "") {
  setSupportActionBar(toolbar)
  supportActionBar?.run {
    setDisplayHomeAsUpEnabled(true)
    setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)
    title = title_
  }
}

fun AppCompatActivity.applyToolbarMargin(toolbar: Toolbar) {
  if (checkIsMaterialVersion()) {
    toolbar.layoutParams = (toolbar.layoutParams as CollapsingToolbarLayout.LayoutParams).apply {
      topMargin = getStatusBarSize()
    }
  }
}

fun AppCompatActivity.getStatusBarSize(): Int {
  val idStatusBarHeight = resources.getIdentifier("status_bar_height", "dimen", "android")
  return if (idStatusBarHeight > 0) {
    resources.getDimensionPixelSize(idStatusBarHeight)
  } else 0
}

fun Activity.setSoftInputMode(mode: Int) {
  window.setSoftInputMode(mode)
}

inline fun <reified T : Any> Activity.launchActivity (
  requestCode: Int = -1,
  options: Bundle? = null,
  noinline init: Intent.() -> Unit = {})
{
  val intent = newIntent<T>(this)
  intent.init()
  startActivityForResult(intent, requestCode, options)
}

inline fun <reified T : Any> Context.launchActivity (
  options: Bundle? = null,
  noinline init: Intent.() -> Unit = {})
{
  val intent = newIntent<T>(this)
  intent.init()
  startActivity(intent, options)
}

inline fun <reified T : Any> newIntent(context: Context): Intent =
  Intent(context, T::class.java)

inline fun FragmentManager.transaction(func: FragmentTransaction.() -> FragmentTransaction) {
  beginTransaction().func().commitAllowingStateLoss()
}

fun AppCompatActivity.replace(fragment: Fragment, container: Int) {
  supportFragmentManager.transaction { replace(container, fragment) } }
