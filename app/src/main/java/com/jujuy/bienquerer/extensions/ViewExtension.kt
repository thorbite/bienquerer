package com.jujuy.bienquerer.extensions

import android.content.Context
import android.text.TextUtils
import android.util.DisplayMetrics
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.core.view.isGone
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import androidx.core.view.updatePadding
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.jujuy.bienquerer.R
import kotlin.math.roundToInt

fun View.visible() {
  isVisible = true
}

fun View.inVisible() {
  isInvisible = true
}

fun View.gone() {
  isGone = true
}

fun View.showShortToast(message: CharSequence): Toast {
  return context.showShortToast(message)
}


fun View.showLongToast(message: CharSequence): Toast {
  return context.showLongToast(message)
}

fun Context.showShortToast(message: CharSequence): Toast {
  return showToast(message, duration = Toast.LENGTH_SHORT)
}


fun Context.showLongToast(message: CharSequence): Toast {
  return showToast(message, duration = Toast.LENGTH_LONG)
}


fun Context.showToast(message: CharSequence, duration: Int = Toast.LENGTH_SHORT): Toast {
  return Toast.makeText(this, message, duration)
    .apply { show() }
}


fun Fragment.showShortToast(message: CharSequence): Toast {
  return requireContext().showShortToast(message)
}


fun Fragment.showLongToast(message: CharSequence): Toast {
  return requireContext().showLongToast(message)
}

fun Int.dpToPx(context: Context): Int {
  return toFloat().dpToPx(context).roundToInt()
}

fun Float.dpToPx(context: Context): Float {
  return (this * context.displayMetrics.density)
}

val Context.displayMetrics: DisplayMetrics
  get() = resources.displayMetrics

fun String.isEmailValid(): Boolean {
  return !TextUtils.isEmpty(this) && android.util.Patterns.EMAIL_ADDRESS.matcher(this).matches()
}

fun View.snackbar(message: String){
  Snackbar.make(this, message, Snackbar.LENGTH_LONG).also { snackbar ->
    snackbar.setAction("OK") {
      snackbar.dismiss()
    }
  }.show()
}


fun EditText.placeCursorToEnd() {
  this.setSelection(this.text.length)
}

internal fun View.setVisibilityMarker(isVisible: Boolean) {
  setTag(R.id.visibility_marker, isVisible)
}


internal fun View.getVisibilityMarker(): Boolean {
  return ((getTag(R.id.visibility_marker) as? Boolean) ?: isVisible)
}


internal fun View.setAnimationMarker(marker: Any) {
  setTag(R.id.animation_marker, marker)
}


@Suppress("UNCHECKED_CAST")
internal fun <T> View.getAnimationMarker(): T? {
  return (getTag(R.id.animation_marker) as T?)
}


internal fun View.cancelAllAnimations() {
  this.clearAnimation()
  this.animate().cancel()
}

fun View.clearPadding() {
  updatePadding( 0, 0, 0, 0)
}

fun View.onClick(action: (View) -> Unit) {
  setOnClickListener(action)
}

fun View.disable() {
  alpha = 0.5f
  isClickable = false
  isEnabled = false
}

fun View.enable() {
  alpha = 1f
  isClickable = true
  isEnabled = true
}
