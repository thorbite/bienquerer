package com.jujuy.bienquerer.ui.professionals.map

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.base.BaseActivity
import com.jujuy.bienquerer.base.NavigationController
import com.jujuy.bienquerer.databinding.MapFilteringActivityBinding
import com.jujuy.bienquerer.utils.Constants
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.android.ext.android.inject
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class MapFilteringActivity : BaseActivity() {
    @ExperimentalCoroutinesApi
    @ExperimentalTime
    val navigationController: NavigationController by inject()
    val binding : MapFilteringActivityBinding by lazy {
        DataBindingUtil.setContentView<MapFilteringActivityBinding>(this, R.layout.map_filtering_activity) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initUI(binding)
    }

    private fun initUI(binding: MapFilteringActivityBinding) {

        initToolbar(binding.toolbar, resources.getString(R.string.map_filter__map_title))
        setupFragment(MapFilteringFragment())
    }

    override fun onBackPressed() {
        setResult(Constants.RESULT_CODE__FROM_MAP_VIEW)
        super.onBackPressed()
        //navigationController.navigateBackToSearchFromMapFiltering(this, null)
    }
}