package com.jujuy.bienquerer.ui.appointment.jobDetailUser

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.api.network.Resource
import com.jujuy.bienquerer.base.BaseFragment
import com.jujuy.bienquerer.databinding.FragmentJobDetailUserBinding
import com.jujuy.bienquerer.extensions.gone
import com.jujuy.bienquerer.extensions.visible
import com.jujuy.bienquerer.utils.Constants
import com.jujuy.bienquerer.utils.LoadingMsg
import com.jujuy.bienquerer.utils.autoCleared
import com.jujuy.bienquerer.viewmodel.jobs.AppointmentViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class JobDetailUserFragment : BaseFragment() {

    private var binding by autoCleared<FragmentJobDetailUserBinding>()
    private val loadingMsg by lazy { LoadingMsg(requireActivity(), false) }
    private val appointmentViewModel by viewModel<AppointmentViewModel>()

    private lateinit var appointmentId: String
    companion object {
        fun newInstance() = JobDetailUserFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentJobDetailUserBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun initUIAndActions() {
        getIntentData()
        binding.acceptJob.setOnClickListener {
            navigationController.navigateToItemDetailActivity(requireActivity(),
                appointmentViewModel.appointmentContainer?.professional?.id.toString(),
                appointmentViewModel.appointmentContainer?.professional?.name.toString())
        }
        binding.rejectJob.setOnClickListener {
            if (appointmentViewModel.appointmentContainer?.status?.id == 1){
                appointmentViewModel.setChangeStatusJob(null, appointmentId, "5")
            }
        }

        binding.messageWithProfessional.setOnClickListener {
            navigationController.navigateToChatActivity(
                requireActivity(),
                appointmentViewModel.appointmentContainer?.professional?.id.toString(),
                appointmentViewModel.appointmentContainer?.professional?.name,
                appointmentViewModel.appointmentContainer?.professional?.lastname,
                appointmentViewModel.appointmentContainer?.professional?.photoProfileUrl,
                0
            )
        }

        binding.scoringJob.setOnClickListener {
            navigationController.navigateToScoringJob(requireActivity(),
                appointmentViewModel.appointmentContainer?.professional,
                appointmentViewModel.appointmentContainer?.profession?.nameSubCategory, appointmentViewModel.appointmentContainer?.id.toString())
        }
    }

    private fun getIntentData() {
        if (requireActivity().intent.extras != null) {
            appointmentId = requireActivity().intent.extras!!.getString(Constants.APPOINTMENT_ID)!!
        }
    }

    private fun checkforButtons() {
        when(appointmentViewModel.appointmentContainer?.status?.id){
            2, 3, 5 ->{
                binding.rejectJob.gone()
            }
            4 ->{
                binding.rejectJob.gone()
                binding.scoringJob.visible()
            }
        }
    }

    override fun initAdapters() {

    }

    override fun initData() {
        getResultsData()
        loadDataJob()
    }

    private fun loadDataJob() {
        appointmentViewModel.getAppointmentData(appointmentId)?.observe(this, Observer { job ->
            job?.let {
                Timber.d("JOB: $it")
                binding.appointment = it
                appointmentViewModel.appointmentContainer = it
                getJobStatus(it.status.id.toString())
                checkforButtons()
            }
        })
    }

    private fun getResultsData() {
        appointmentViewModel.getChangeStatusJob().observe(this, Observer {
            when(it.status){
                Resource.Status.LOADING ->{
                    loadingMsg.showloaderDialog()
                    loadingMsg.show()
                }
                Resource.Status.SUCCESS -> {
                    //loadingMsg.cancel()
                    getListUpdated()
                    Timber.d(it.data.toString())

                }
                Resource.Status.ERROR ->{
                    loadingMsg.cancel()
                    Timber.d(it.message)
                }
            }
        })
    }

    private fun getListUpdated() {
        appointmentViewModel.setListAppointmentObj()
        appointmentViewModel.getListAppoinmentData().observe(this, Observer {
            when(it.status){
                Resource.Status.LOADING ->{}
                Resource.Status.SUCCESS ->{
                    loadingMsg.cancel()
                    loadDataJob()
                }
                Resource.Status.ERROR ->{}
            }
        })
    }

    private fun getJobStatus(jobStatus: String) {
        val alfa = 0.5f
        when (jobStatus) {
            "1" -> {
                setStatusPending(alfa)
            }
            "2" -> {
                setStatusAcepted(alfa)
            }
            "4" -> {
                setStatusComplete(alfa)
            }
            "3" -> {
                setStatusReject(alfa)
            }
            "5" ->{
                setStatusCanceled(alfa)
            }
        }
    }

    private fun setStatusPending(alfa: Float) {
        binding.viewOrderPlaced.background = ContextCompat.getDrawable(requireContext(), R.drawable.shape_status_completed)
        binding.viewOrderConfirmed.background = ContextCompat.getDrawable(requireContext(), R.drawable.shape_status_current)
        binding.viewOrderProcessed.background = ContextCompat.getDrawable(requireContext(), R.drawable.shape_status_current)
        binding.conDivider.background = ContextCompat.getDrawable(requireContext(), R.drawable.shape_status_current)
        binding.placedDivider.background = ContextCompat.getDrawable(requireContext(), R.drawable.shape_status_current)
        binding.placedDivider.alpha = alfa
        binding.conDivider.alpha = alfa
        binding.viewOrderConfirmed.alpha = alfa
        binding.viewOrderProcessed.alpha = alfa
        binding.imgJobAcepted.alpha = alfa
        binding.textJobAcepted.alpha = alfa
        binding.descJobAcepted.alpha = alfa
        binding.imgJobComplete.alpha = alfa
        binding.textJobComplete.alpha = alfa
        binding.descJobComplete.alpha = alfa
    }

    private fun setStatusAcepted(alfa: Float) {
        binding.viewOrderPlaced.background = ContextCompat.getDrawable(requireContext(), R.drawable.shape_status_completed)
        binding.viewOrderConfirmed.background = ContextCompat.getDrawable(requireContext(), R.drawable.shape_status_completed)
        binding.viewOrderProcessed.background = ContextCompat.getDrawable(requireContext(), R.drawable.shape_status_current)
        binding.conDivider.background = ContextCompat.getDrawable(requireContext(), R.drawable.shape_status_current)
        binding.placedDivider.background = ContextCompat.getDrawable(requireContext(), R.drawable.shape_status_completed)
        binding.placedDivider.alpha = 1f
        binding.viewOrderConfirmed.alpha = 1f
        binding.imgJobAcepted.alpha = 1f
        binding.textJobAcepted.alpha = 1f
        binding.descJobAcepted.alpha = 1f
        binding.conDivider.alpha = alfa
        binding.imgJobComplete.alpha = alfa
        binding.textJobComplete.alpha = alfa
        binding.descJobComplete.alpha = alfa
    }

    private fun setStatusComplete(alfa: Float) {
        binding.viewOrderPlaced.background = ContextCompat.getDrawable(requireContext(), R.drawable.shape_status_completed)
        binding.viewOrderConfirmed.background = ContextCompat.getDrawable(requireContext(), R.drawable.shape_status_completed)
        binding.viewOrderProcessed.background = ContextCompat.getDrawable(requireContext(), R.drawable.shape_status_completed)
        binding.conDivider.background = ContextCompat.getDrawable(requireContext(), R.drawable.shape_status_completed)
        binding.placedDivider.background = ContextCompat.getDrawable(requireContext(), R.drawable.shape_status_completed)
        binding.placedDivider.alpha = 1f
        binding.viewOrderConfirmed.alpha = 1f
        binding.imgJobAcepted.alpha = 1f
        binding.textJobAcepted.alpha = 1f
        binding.descJobAcepted.alpha = 1f
        binding.conDivider.alpha = 1f
        binding.imgJobComplete.alpha = 1f
        binding.textJobComplete.alpha = 1f
        binding.descJobComplete.alpha = 1f
    }

    private fun setStatusReject(alfa: Float){
        binding.imgJobPending.setImageResource(R.drawable.ic_icon_reject)
        binding.textJobPending.text = "Rechazado"

        binding.viewOrderPlaced.background = ContextCompat.getDrawable(requireContext(), R.drawable.shape_status_completed)
        binding.viewOrderConfirmed.background = ContextCompat.getDrawable(requireContext(), R.drawable.shape_status_current)
        binding.viewOrderProcessed.background = ContextCompat.getDrawable(requireContext(), R.drawable.shape_status_current)
        binding.conDivider.background = ContextCompat.getDrawable(requireContext(), R.drawable.shape_status_current)
        binding.placedDivider.background = ContextCompat.getDrawable(requireContext(), R.drawable.shape_status_current)
        binding.placedDivider.alpha = alfa
        binding.conDivider.alpha = alfa
        binding.viewOrderConfirmed.alpha = alfa
        binding.viewOrderProcessed.alpha = alfa
        binding.imgJobAcepted.alpha = alfa
        binding.descJobAcepted.alpha = alfa
        binding.textJobAcepted.alpha = alfa
        binding.imgJobComplete.alpha = alfa
        binding.textJobComplete.alpha = alfa
        binding.descJobComplete.alpha = alfa
    }

    private fun setStatusCanceled(alfa: Float){
        binding.imgJobPending.setImageResource(R.drawable.ic_icon_reject)
        binding.textJobPending.text = "Cancelado"

        binding.viewOrderPlaced.background = ContextCompat.getDrawable(requireContext(), R.drawable.shape_status_completed)
        binding.viewOrderConfirmed.background = ContextCompat.getDrawable(requireContext(), R.drawable.shape_status_current)
        binding.viewOrderProcessed.background = ContextCompat.getDrawable(requireContext(), R.drawable.shape_status_current)
        binding.conDivider.background = ContextCompat.getDrawable(requireContext(), R.drawable.shape_status_current)
        binding.placedDivider.background = ContextCompat.getDrawable(requireContext(), R.drawable.shape_status_current)
        binding.placedDivider.alpha = alfa
        binding.conDivider.alpha = alfa
        binding.viewOrderConfirmed.alpha = alfa
        binding.viewOrderProcessed.alpha = alfa
        binding.imgJobAcepted.alpha = alfa
        binding.descJobAcepted.alpha = alfa
        binding.textJobAcepted.alpha = alfa
        binding.imgJobComplete.alpha = alfa
        binding.textJobComplete.alpha = alfa
        binding.descJobComplete.alpha = alfa
    }
}