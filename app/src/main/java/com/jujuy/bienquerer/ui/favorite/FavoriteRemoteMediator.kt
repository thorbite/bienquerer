package com.jujuy.bienquerer.ui.favorite

import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingState
import androidx.paging.RemoteMediator
import androidx.room.withTransaction
import com.jujuy.bienquerer.api.service.NotificationService
import com.jujuy.bienquerer.api.service.ProfessionalService
import com.jujuy.bienquerer.model.entity.ProfessionalFav
import com.jujuy.bienquerer.model.network.response.ListNotifications
import com.jujuy.bienquerer.room.AppDatabase
import com.jujuy.bienquerer.ui.notifications.NotificationKeys
import kotlinx.coroutines.ExperimentalCoroutinesApi
import retrofit2.HttpException
import timber.log.Timber
import java.io.IOException
import java.io.InvalidObjectException
import kotlin.time.ExperimentalTime

private const val STARTING_PAGE_INDEX = 1

@ExperimentalTime
@ExperimentalCoroutinesApi
@OptIn(ExperimentalPagingApi::class)
class FavoriteRemoteMediator(
    private val roomDatabase: AppDatabase,
    private val favService: ProfessionalService
): RemoteMediator<Int, ProfessionalFav>() {
    override suspend fun load(
        loadType: LoadType,
        state: PagingState<Int, ProfessionalFav>
    ): MediatorResult {
        val page = when (loadType) {
            LoadType.REFRESH -> {
                val remoteKeys = getRemoteKeyClosestToCurrentPosition(state)
                remoteKeys?.nextKey?.minus(1) ?: STARTING_PAGE_INDEX
            }
            LoadType.PREPEND -> {
                return MediatorResult.Success(endOfPaginationReached = true)
            }
            LoadType.APPEND -> {
                val remoteKeys = getRemoteKeyForLastItem(state)
                if (remoteKeys?.nextKey == null) {
                    return MediatorResult.Success(endOfPaginationReached = true)
                }
                remoteKeys.nextKey
            }

        }
        try {
            val apiResponse = favService.getListFavorite(page)
            val favorites = apiResponse.data?.list ?: emptyList()
            val endOfPaginationReached = favorites.isEmpty()
            roomDatabase.withTransaction {
                if (loadType == LoadType.REFRESH) {
                    roomDatabase.favoriteKeysDao().clearRemoteKeys()
                    roomDatabase.professionalDao().deleteAllFavouriteItems()
                }
                val prevKey = if (page == STARTING_PAGE_INDEX) null else page - 1
                val nextKey = if (endOfPaginationReached) null else page + 1
                val keys = favorites.map {
                    FavoriteKeys(favId = it.id, prevKey = prevKey, nextKey = nextKey)
                }
                roomDatabase.favoriteKeysDao().insertAll(keys)
                roomDatabase.professionalDao().insertAllFavourites(favorites)
            }
            return MediatorResult.Success(endOfPaginationReached = endOfPaginationReached)
        } catch (exception: IOException) {
            return MediatorResult.Error(exception)
        } catch (exception: HttpException) {
            return MediatorResult.Error(exception)
        }
    }

    private suspend fun getRemoteKeyForLastItem(state: PagingState<Int, ProfessionalFav>): FavoriteKeys? {
        // Get the last page that was retrieved, that contained items.
        // From that last page, get the last item
        return state.pages.lastOrNull() {
            it.data.isNotEmpty() }?.data?.lastOrNull()
            ?.let { repo ->
                // Get the remote keys of the last item retrieved
                Timber.e("LAST ITEM= ${repo.id}")
                roomDatabase.favoriteKeysDao().remoteKeysRepoId(repo.id)
            }
    }

    private suspend fun getRemoteKeyForFirstItem(state: PagingState<Int, ProfessionalFav>): FavoriteKeys? {
        // Get the first page that was retrieved, that contained items.
        // From that first page, get the first item
        return state.pages.firstOrNull { it.data.isNotEmpty() }?.data?.firstOrNull()
            ?.let { repo ->
                // Get the remote keys of the first items retrieved
                Timber.e("FIRST ITEM= ${repo.id}")
                roomDatabase.favoriteKeysDao().remoteKeysRepoId(repo.id)
            }
    }

    private suspend fun getRemoteKeyClosestToCurrentPosition(
        state: PagingState<Int, ProfessionalFav>
    ): FavoriteKeys? {
        // The paging library is trying to load data after the anchor position
        // Get the item closest to the anchor position
        return state.anchorPosition?.let { position ->
            state.closestItemToPosition(position)?.id?.let { repoId ->
                roomDatabase.favoriteKeysDao().remoteKeysRepoId(repoId)
            }
        }
    }
}