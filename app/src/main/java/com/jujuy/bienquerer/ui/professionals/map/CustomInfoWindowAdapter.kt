package com.jujuy.bienquerer.ui.professionals.map

import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.model.entity.Professional
import timber.log.Timber


class CustomInfoWindowAdapter(private var inflater: LayoutInflater?, professional: Professional) : GoogleMap.InfoWindowAdapter {
    private val TAG = "CustomInfoWindowAdapter"
    private var professional: Professional? = professional

    override fun getInfoWindow(p0: Marker): View? {
        return null
    }

    override fun getInfoContents(m: Marker): View? {
        Timber.e("en el adapter de getinfowindow ${professional}")
        val v: View? = inflater?.inflate(R.layout.info_window_layout, null)
        if (v != null) {
            (v.findViewById<View>(R.id.info_window_nombre) as TextView).text = professional?.name
            (v.findViewById<View>(R.id.info_window_placas) as TextView).text = professional?.lastname
            (v.findViewById<View>(R.id.info_window_estado) as TextView).text = professional?.profession?.get(0)?.nameSubCategory
        }

        return v
    }
}