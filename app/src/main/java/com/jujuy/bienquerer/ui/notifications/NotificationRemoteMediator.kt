package com.jujuy.bienquerer.ui.notifications

import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingState
import androidx.paging.RemoteMediator
import androidx.room.withTransaction
import com.jujuy.bienquerer.api.service.NotificationService
import com.jujuy.bienquerer.model.network.response.ListNotifications
import com.jujuy.bienquerer.room.AppDatabase
import com.jujuy.bienquerer.room.NotificationDao
import com.jujuy.bienquerer.room.NotificationKeysDao
import kotlinx.coroutines.ExperimentalCoroutinesApi
import retrofit2.HttpException
import timber.log.Timber
import java.io.IOException
import java.io.InvalidObjectException
import kotlin.time.ExperimentalTime

private const val STARTING_PAGE_INDEX = 1

@ExperimentalTime
@ExperimentalCoroutinesApi
@OptIn(ExperimentalPagingApi::class)
class NotificationRemoteMediator (
    private val roomDatabase: AppDatabase,
    private val notiService: NotificationService,
    private val notiDao: NotificationDao,
    private val notiKeysDao: NotificationKeysDao): RemoteMediator<Int, ListNotifications>() {
        override suspend fun load(loadType: LoadType, state: PagingState<Int, ListNotifications>): MediatorResult {
            val page = when (loadType) {
                LoadType.REFRESH -> {
                    val remoteKeys = getRemoteKeyClosestToCurrentPosition(state)
                    Timber.d("REFRESH-> ${remoteKeys?.prevKey} -> ${remoteKeys?.nextKey} ")
                    remoteKeys?.nextKey?.minus(1) ?: STARTING_PAGE_INDEX
                }
                LoadType.PREPEND -> {
                    val remoteKeys = getRemoteKeyForFirstItem(state)
                        ?: throw InvalidObjectException("Invalid state, key should not be null")
                    remoteKeys.prevKey ?: return MediatorResult.Success(endOfPaginationReached = true)
                    remoteKeys.prevKey
                }
                LoadType.APPEND -> {
                    val remoteKeys = getRemoteKeyForLastItem(state)
                    if (remoteKeys?.nextKey == null) {
                        throw InvalidObjectException("Remote key should not be null for $loadType")
                    }
                    Timber.d("APPEND-> ${remoteKeys.prevKey} -> ${remoteKeys.nextKey} ")
                    remoteKeys.nextKey
                }

            }

            try {
                Timber.d("Page= $page, LoadType= $loadType")
                val apiResponse = notiService.getNotificationsList(page)
                val notifications = apiResponse.data?.list ?: emptyList()
                val endOfPaginationReached = notifications.isEmpty()
                roomDatabase.withTransaction {
                    if (loadType == LoadType.REFRESH) {
                        roomDatabase.notificationKeysDao().clearRemoteKeys()
                        roomDatabase.notificationDao().deleteAllNotificationList()
                    }
                    val prevKey = if (page == STARTING_PAGE_INDEX) null else page - 1
                    val nextKey = if (endOfPaginationReached) null else page + 1
                    Timber.d("PrevKey =$prevKey, NextKey=$nextKey")
                    val keys = notifications.map {
                        NotificationKeys(repoId = it.id, prevKey = prevKey, nextKey = nextKey)
                    }
                    roomDatabase.notificationKeysDao().insertAll(keys)
                    Timber.e("KEYS= ${notiKeysDao.getAllKeys()}")
                    roomDatabase.notificationDao().insertAllNotificationList(notifications)
                    Timber.e("NOTIFICATIONS = ${notiDao.getAllNotification()}")
                }
                return MediatorResult.Success(endOfPaginationReached = endOfPaginationReached)
            } catch (exception: IOException) {
                return MediatorResult.Error(exception)
            } catch (exception: HttpException) {
                return MediatorResult.Error(exception)
            }
        }



        private suspend fun getRemoteKeyForLastItem(state: PagingState<Int, ListNotifications>): NotificationKeys? {
            // Get the last page that was retrieved, that contained items.
            // From that last page, get the last item
            return state.pages.lastOrNull() {
                it.data.isNotEmpty() }?.data?.lastOrNull()
                ?.let { repo ->
                    // Get the remote keys of the last item retrieved
                    Timber.e("LAST ITEM= ${repo.id}")
                    notiKeysDao.remoteKeysRepoId(repo.id)
                }
        }

        private suspend fun getRemoteKeyForFirstItem(state: PagingState<Int, ListNotifications>): NotificationKeys? {
            // Get the first page that was retrieved, that contained items.
            // From that first page, get the first item
            return state.pages.firstOrNull { it.data.isNotEmpty() }?.data?.firstOrNull()
                ?.let { repo ->
                    // Get the remote keys of the first items retrieved
                    Timber.e("FIRST ITEM= ${repo.id}")
                    notiKeysDao.remoteKeysRepoId(repo.id)
                }
        }

        private suspend fun getRemoteKeyClosestToCurrentPosition(
            state: PagingState<Int, ListNotifications>
        ): NotificationKeys? {
            // The paging library is trying to load data after the anchor position
            // Get the item closest to the anchor position
            return state.anchorPosition?.let { position ->
                state.closestItemToPosition(position)?.id?.let { repoId ->
                    notiKeysDao.remoteKeysRepoId(repoId)
                }
            }
        }
    }