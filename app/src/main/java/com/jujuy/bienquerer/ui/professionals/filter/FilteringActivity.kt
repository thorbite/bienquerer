package com.jujuy.bienquerer.ui.professionals.filter

import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.base.BaseActivity
import com.jujuy.bienquerer.databinding.FilteringActivityBinding
import com.jujuy.bienquerer.ui.category.categoryfilter.CategoryFilterFragment
import com.jujuy.bienquerer.utils.Constants
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class FilteringActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: FilteringActivityBinding = DataBindingUtil.setContentView(this,R.layout.filtering_activity)
        initUI(binding)
    }

    private fun initUI(binding: FilteringActivityBinding) {
        val name = intent.getStringExtra(Constants.FILTERING_FILTER_NAME)
        if (name == Constants.FILTERING_TYPE_FILTER){
            initToolbar(binding.toolbar, resources.getString(R.string.Feature_UI_Type__Button))
            setupFragment(CategoryFilterFragment())
        }
    }

    //endregion
    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        super.onActivityResult(requestCode, resultCode, data)
        val fragment =
            supportFragmentManager.findFragmentById(R.id.content_frame)
        fragment?.onActivityResult(requestCode, resultCode, data)
    }

    override fun onBackPressed() {
        setResult(Constants.RESULT_CODE__CATEGORY_FILTER)
        super.onBackPressed()
        //navigationController.navigateBackToSearchFromMapFiltering(this, null)
    }
}