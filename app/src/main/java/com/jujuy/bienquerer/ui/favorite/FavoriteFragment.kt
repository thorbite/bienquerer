package com.jujuy.bienquerer.ui.favorite

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import com.jujuy.bienquerer.MainActivity
import com.jujuy.bienquerer.base.BaseFragment
import com.jujuy.bienquerer.databinding.FavoriteFragmentBinding
import com.jujuy.bienquerer.extensions.setSoftInputMode
import com.jujuy.bienquerer.model.entity.ProfessionalFav
import com.jujuy.bienquerer.ui.notifications.LoadingAdapter
import com.jujuy.bienquerer.utils.LoadingMsg
import com.jujuy.bienquerer.utils.autoCleared
import com.jujuy.bienquerer.viewmodel.favorite.FavoriteViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class FavoriteFragment : BaseFragment(), FavoriteAdapter.OnItemClick {

    companion object {
        fun newInstance() = FavoriteFragment()
    }

    private var searchJob: Job? = null
    private val notiAdapter = FavoriteAdapter(this)

    private val favoriteViewModel by viewModel<FavoriteViewModel>()
    private var binding by autoCleared<FavoriteFragmentBinding>()
    private val loading: LoadingMsg by lazy { LoadingMsg(requireActivity(), false) }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FavoriteFragmentBinding.inflate(inflater, container, false)
        activity?.setSoftInputMode(SOFT_INPUT_ADJUST_PAN)
        setHasOptionsMenu(true)
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun initUIAndActions() {

    }

    override fun initAdapters() {
        binding.favoriteListRv.adapter = notiAdapter.withLoadStateHeaderAndFooter(
            header = LoadingAdapter { notiAdapter.retry() },
            footer = LoadingAdapter { notiAdapter.retry() }
        )

        notiAdapter.addLoadStateListener { loadState ->
            binding.progressBar.isVisible = loadState.source.refresh is LoadState.Loading
            binding.retryButton.isVisible = loadState.source.refresh is LoadState.Error

            // Toast on any error, regardless of whether it came from RemoteMediator or PagingSource
            val errorState = loadState.source.append as? LoadState.Error
                ?: loadState.source.prepend as? LoadState.Error
                ?: loadState.append as? LoadState.Error
                ?: loadState.prepend as? LoadState.Error
            errorState?.let {
                Toast.makeText(
                    requireContext(),
                    "\uD83D\uDE28 Wooops ${it.error}",
                    Toast.LENGTH_LONG
                ).show()
            }

            if (loadState.source.refresh is LoadState.NotLoading && notiAdapter.itemCount < 1) {
                Timber.e("Ocultar lista ${notiAdapter.itemCount < 1} ${loadState.refresh.endOfPaginationReached} ${loadState.source.refresh is LoadState.NotLoading}")
                binding.favoriteListRv.isVisible = false
                binding.noItemConstraintLayout.isVisible = true
            } else {
                Timber.e("Mostrar lista ${notiAdapter.itemCount < 1} ${loadState.refresh.endOfPaginationReached} ${loadState.source.refresh is LoadState.NotLoading}")
                binding.favoriteListRv.isVisible = true
                binding.noItemConstraintLayout.isVisible = false
            }
        }
    }

    override fun initData() {
        fetchFavorites()
    }

    private fun fetchFavorites() {
        searchJob?.cancel()
        searchJob = lifecycleScope.launch {
            favoriteViewModel.getListFavoritesData().collectLatest {
                Timber.e("Fragment list dataaaa")
                notiAdapter.submitData(it)
            }
        }
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                (activity as MainActivity).setBottomNavigationMenu(0)
            }
        })
    }

    override fun onItemClick(item: ProfessionalFav, position: Int) {
        navigationController.navigateToItemDetailActivity(
            requireActivity(),
            item.favorite.id.toString(),
            item.favorite.name.toString()
        )
    }
}
