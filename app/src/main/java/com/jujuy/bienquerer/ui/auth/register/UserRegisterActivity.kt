package com.jujuy.bienquerer.ui.auth.register

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.jujuy.bienquerer.R
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class UserRegisterActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.user_register_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, UserRegisterFragment.newInstance())
                .commitNow()
        }
    }

    override fun onBackPressed() {
        finish()
    }
}
