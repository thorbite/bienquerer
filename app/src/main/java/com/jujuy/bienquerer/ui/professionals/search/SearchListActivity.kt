package com.jujuy.bienquerer.ui.professionals.search

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.databinding.DataBindingUtil
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.base.BaseActivity
import com.jujuy.bienquerer.databinding.SearchListActivityBinding
import com.jujuy.bienquerer.utils.Constants
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class SearchListActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: SearchListActivityBinding = DataBindingUtil.setContentView(this,R.layout.search_list_activity)
        initUI(binding)
    }

    private fun initUI(binding: SearchListActivityBinding) {
        val title : String? = intent.getStringExtra(Constants.ITEM_NAME)
        if(title != null){
            initToolbar(binding.toolbar, title.capitalize())
        }else{
            initToolbar(binding.toolbar, "Lista de Profesionales")
        }

        setupFragment(SearchListFragment())
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val fragment = supportFragmentManager.findFragmentById(R.id.content_frame)
        fragment?.onActivityResult(requestCode, resultCode, data)
    }

    override fun onResume() {
        Log.e("SearchListActivyt", intent.getStringExtra(Constants.SUBCATEGORY_NAME) ?: "")
        super.onResume()
    }
}