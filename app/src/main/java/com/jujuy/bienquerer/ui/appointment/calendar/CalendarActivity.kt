package com.jujuy.bienquerer.ui.appointment.calendar

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.sundeepk.compactcalendarview.CompactCalendarView
import com.github.sundeepk.compactcalendarview.CompactCalendarView.CompactCalendarViewListener
import com.github.sundeepk.compactcalendarview.domain.Event
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.api.network.Resource
import com.jujuy.bienquerer.base.BaseActivity
import com.jujuy.bienquerer.model.network.response.Job
import com.jujuy.bienquerer.utils.Constants
import com.jujuy.bienquerer.utils.Utils
import com.jujuy.bienquerer.viewmodel.jobs.AppointmentViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class CalendarActivity : BaseActivity() {
    var compactCalendarView: CompactCalendarView? = null
    private val simpleDateFormat: SimpleDateFormat = SimpleDateFormat("MM-yyyy", Locale.getDefault())
    private val DateFormat: SimpleDateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
    var sdf: SimpleDateFormat? = null
    var tx_date: TextView? = null
    var tx_today: TextView? = null
    var ly_detail: LinearLayout? = null
    var ly_left: LinearLayout? = null
    var ly_right: LinearLayout? = null
    lateinit var myCalendar: Calendar
    var im_back: ImageView? = null
    //var webService: WebService? = null
    var progressDialog: ProgressDialog? = null
    var user_type: String? = null
    var id = 0
    var c: Date? = null
    var df: SimpleDateFormat? = null
    var formattedDate: String? = null
    var dates = arrayOfNulls<String>(0)
    var recyclerView: RecyclerView? = null
    var tx_item: TextView? = null
    var adapter: CalendarAdapter? = null

    private val jobViewModel by viewModel<AppointmentViewModel>()
    private var data: List<Job>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calendar)

        init()
        valid()
        calendarlistener()
        Setdate()
        initData()
        //EventListAsy().execute("" + id, formattedDate, user_type)
        ///EventViewAsy().execute("" + id, formattedDate, user_type)
        tx_item!!.text = "$formattedDate No hay eventos disponibles en este dia"

        ly_right!!.setOnClickListener {
            compactCalendarView!!.showCalendarWithAnimation()
            compactCalendarView!!.scrollLeft()

        }

        ly_left!!.setOnClickListener {
            compactCalendarView!!.showCalendarWithAnimation()
            compactCalendarView!!.scrollRight()
        }

        tx_today!!.setOnClickListener {
            val intent = Intent(this@CalendarActivity, CalendarActivity::class.java)
            startActivity(intent)
            finish()
        }

        im_back!!.setOnClickListener {
            finish()
        }
    }

    fun init() {
        compactCalendarView = findViewById(R.id.compactcalendar_view);
        tx_date = findViewById(R.id.text);
        ly_left = findViewById(R.id.layout_left);
        ly_right = findViewById(R.id.layout_right);
        im_back = findViewById(R.id.image_back);
        tx_today = findViewById(R.id.text_today);
        ly_detail = findViewById(R.id.layout_detail);

        recyclerView = findViewById(R.id.list_recycleView);
        tx_item = findViewById(R.id.text_item);


    }

    fun initData() {
        jobViewModel.setListJobObj()
        jobViewModel.getListJobData().observe(this, androidx.lifecycle.Observer { response ->
            Timber.e(response.toString())
                when(response.status){
                    Resource.Status.LOADING ->{}
                    Resource.Status.SUCCESS ->{
                        compactCalendarView?.setUseThreeLetterAbbreviation(true)
                        compactCalendarView?.shouldDrawIndicatorsBelowSelectedDays(true)
                        sdf = SimpleDateFormat("MMMM yyyy")
                        tx_date?.setText(sdf!!.format(compactCalendarView?.getFirstDayOfCurrentMonth()))
                        myCalendar = Calendar.getInstance()
                        data = response.data
                        Timber.e("LIST: $data")
                        for (j in 0 until data?.size!!) {
                            val s1: String = Utils.ParseDate(data!![j].requestDate)
                            dates = s1.split("-".toRegex()).toTypedArray()
                            Timber.e("DIA ${dates[2]}")
                            val mon: Int = dates[1]?.toInt() ?: 1
                            dates[0]?.toInt()?.let { myCalendar.set(Calendar.YEAR, it) }
                            myCalendar.set(Calendar.MONTH, mon - 1)
                            dates[2]?.toInt()?.let { myCalendar.set(Calendar.DAY_OF_MONTH, it) }
                            Timber.e("TIME ${myCalendar.timeInMillis}")
                            val event = Event(Color.RED, myCalendar.timeInMillis, data!![j])
                            compactCalendarView?.addEvent(event,false)
                            compactCalendarView!!.showCalendarWithAnimation()
                            //compactCalendarView?.setEventIndicatorStyle(1)

                        }
                    }
                    Resource.Status.ERROR -> {
                        Timber.e("ERROR: ${response.message}")
                    }
                }
        })
    }

    fun valid() {
        //id = getUserID()
        //user_type = getUserType()
        progressDialog = ProgressDialog(this@CalendarActivity, R.style.ThemeOverlay_AppCompat_Dialog)
        progressDialog!!.setCanceledOnTouchOutside(false)
    }

    private fun calendarlistener() {
        compactCalendarView!!.setListener(object : CompactCalendarViewListener {
            override fun onDayClick(dateClicked: Date?) {
                //EventViewAsy().execute("" + id, DateFormat.format(dateClicked), user_type)
                val events: List<Event> = dateClicked?.time?.let { compactCalendarView!!.getEvents(it) } as List<Event>
                if (events.isEmpty()) {
                    tx_item?.visibility = View.VISIBLE
                    recyclerView?.visibility = View.GONE
                } else {
                    tx_item?.visibility = View.GONE
                    recyclerView?.visibility = View.VISIBLE
                }
                adapter = CalendarAdapter(events, this@CalendarActivity)
                val mLayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(applicationContext)
                recyclerView?.layoutManager = mLayoutManager
                recyclerView?.itemAnimator = DefaultItemAnimator()
                recyclerView?.adapter = adapter
                tx_item!!.text = DateFormat.format(dateClicked).toString() + " No hay eventos disponibles en este dia"
            }

            override fun onMonthScroll(firstDayOfNewMonth: Date?) {
                //compactCalendarView!!.removeAllEvents()
                tx_date?.setText(sdf!!.format(firstDayOfNewMonth))
                //tx_date!!.text = simpleDateFormat.format(firstDayOfNewMonth)
                //EventListAsy().execute("" + id, DateFormat.format(firstDayOfNewMonth), user_type)
                //EventViewAsy().execute("" + id, DateFormat.format(firstDayOfNewMonth), user_type)
                tx_item!!.text = DateFormat.format(firstDayOfNewMonth).toString() + " No hay eventos disponibles en este dia"
            }
        })
    }

    private fun Setdate() {
        c = Calendar.getInstance().time
        df = SimpleDateFormat("dd-MM-yyyy")
        formattedDate = df!!.format(c)
    }

    /*inner class EventListAsy : AsyncTask<String?, Void?, ArrayList<Appointment>>() {
        override fun onPreExecute() {
            super.onPreExecute()
            progressDialog.show()
            webService = WebService()
        }

       override fun doInBackground(vararg params: String?): ArrayList<Appointment> {
            return webService.getEventList(params[0], params[1], params[2])
        }

        override fun onPostExecute(list: ArrayList<Appointment>) {
            super.onPostExecute(list)
            progressDialog?.dismiss()
            compactCalendarView?.setUseThreeLetterAbbreviation(true)
            sdf = SimpleDateFormat("MMMM yyyy")
            tx_date?.setText(sdf!!.format(compactCalendarView.getFirstDayOfCurrentMonth()))
            myCalendar = Calendar.getInstance()
            for (j in 0 until list.size) {
                val s1: String = list[j].requestDate
                dates = s1.split("-".toRegex()).toTypedArray()
                val mon: Int = dates.get(1).toInt()
                myCalendar.set(Calendar.YEAR, dates.get(0).toInt())
                myCalendar.set(Calendar.MONTH, mon - 1)
                myCalendar.set(Calendar.DAY_OF_MONTH, dates.get(2).toInt())
                val event = Event(Color.RED, myCalendar.getTimeInMillis(), "test")
                compactCalendarView?.addEvent(event)
            }
        }
    }

    inner class EventViewAsy : AsyncTask<String?, Void?, ArrayList<Appointment>>() {
        override fun onPreExecute() {
            super.onPreExecute()
            progressDialog?.show()
            webService = WebService()
        }

        override fun doInBackground(vararg params: String?): ArrayList<Appointment> {
            return webService.viewEvent(params[0], params[1], params[2])
        }

        override fun onPostExecute(list: ArrayList<Appointment>) {
            super.onPostExecute(list)
            progressDialog?.dismiss()
            if (list.size == 0) {
                tx_item?.setVisibility(View.VISIBLE)
                recyclerView?.setVisibility(View.GONE)
            } else {
                tx_item?.setVisibility(View.GONE)
                recyclerView?.setVisibility(View.VISIBLE)
            }
            adapter = CalendarAdapter(list, this@CalendarActivity)
            val mLayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(applicationContext)
            recyclerView?.setLayoutManager(mLayoutManager)
            recyclerView?.setItemAnimator(DefaultItemAnimator())
            recyclerView?.setAdapter(adapter)
        }
    }*/

    private fun getUserID(): Int {
        val prefs = getSharedPreferences(Constants.USER_ID, Context.MODE_PRIVATE)
        return prefs.getInt("User_id", 0)
    }

    private fun getUserType(): String? {
        val prefs = getSharedPreferences(Constants.USER_ID, Context.MODE_PRIVATE)
        return prefs.getString("User_type", null)
    }
}