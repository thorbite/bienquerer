package com.jujuy.bienquerer.ui.category.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.jujuy.bienquerer.MainActivity
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.api.network.Resource
import com.jujuy.bienquerer.base.BaseFragment
import com.jujuy.bienquerer.databinding.FragmentCategoryBinding
import com.jujuy.bienquerer.model.entity.Category
import com.jujuy.bienquerer.ui.category.adapter.ListCategoryAdapter
import com.jujuy.bienquerer.viewmodel.category.CategoryViewModel
import com.jujuy.bienquerer.utils.Utils
import com.jujuy.bienquerer.utils.autoCleared
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.androidx.viewmodel.ext.android.viewModel
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class CategoryFragment : BaseFragment() {
    companion object {
        fun newInstance() =
            CategoryFragment()
    }

    private val categoryViewModel by viewModel<CategoryViewModel>()
    private var binding by autoCleared<FragmentCategoryBinding>()
    private var adapterCategory by autoCleared<ListCategoryAdapter>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentCategoryBinding.inflate(inflater, container, false)
        activity?.let {
            it.window?.setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN
            )
        }
        binding.loadingMore = connectivity.isConnected
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun initUIAndActions() {
        if (activity is MainActivity) {
            (activity as MainActivity).setToolbarText((activity as MainActivity).binding.toolbarMain, "Categorias")
        }
        binding.swipeRefresh.setColorSchemeColors(ContextCompat.getColor(requireContext(), R.color.white))
        binding.swipeRefresh.setProgressBackgroundColorSchemeColor(ContextCompat.getColor(requireContext(), R.color.global__primary))
        binding.swipeRefresh.setOnRefreshListener {
            categoryViewModel.loadingDirection = Utils.LoadingDirection.top
            categoryViewModel.offset = 0
            categoryViewModel.forceEndLoading = false
            categoryViewModel.setCategoryListObj("10", "0")
        }
    }

    override fun initAdapters() {
        val nvAdapter = ListCategoryAdapter(appExecutors, object :
            ListCategoryAdapter.CategoryClickCallback {
            override fun onClick(category: Category) {
                navigationController.navigateToSubCategoryActivity(
                    requireActivity(),
                    category.idCategory,
                    category.nameCategory
                )
            }
        })

        this.adapterCategory = nvAdapter
        binding.categoryList.adapter = nvAdapter
    }

    override fun initData() {
        loadCategory()
    }

    private fun loadCategory() {
        categoryViewModel.setCategoryListObj("10","0")
        categoryViewModel.getCategoryListData()?.observe(this, { listResource ->
            if (listResource != null){
                when(listResource.status){
                    Resource.Status.LOADING -> {
                        if (listResource.data != null){
                            fadeIn(binding.root)
                            replaceData(listResource.data)
                        }
                    }
                    Resource.Status.SUCCESS -> {
                        if (listResource.data != null) {
                           replaceData(listResource.data)
                        }
                        categoryViewModel.setLoadingState(false)
                    }
                    Resource.Status.ERROR -> categoryViewModel.setLoadingState(false)
                }
            }else{
                if (categoryViewModel.offset > 1){
                    categoryViewModel.forceEndLoading = true
                }
            }
        })

        categoryViewModel.getLoadingState()?.observe(this, Observer { loadingState ->
            binding.loadingMore = categoryViewModel.isLoading
            if (loadingState != null && !loadingState){
                binding.swipeRefresh.isRefreshing = false
            }
        })
    }

    private fun replaceData(categoryList: List<Category>) {
        adapterCategory.submitList(categoryList)
        binding.executePendingBindings()
    }

}
