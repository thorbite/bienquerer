package com.jujuy.bienquerer.ui.splashScreen

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.google.android.gms.location.*
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.InstanceIdResult
import com.google.firebase.messaging.FirebaseMessaging
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.base.BaseFragment
import com.jujuy.bienquerer.extensions.gone
import com.jujuy.bienquerer.extensions.putAny
import com.jujuy.bienquerer.utils.Constants
import com.jujuy.bienquerer.utils.PSDialogMsg
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import kotlinx.coroutines.ExperimentalCoroutinesApi
import timber.log.Timber
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class SplashFragment : BaseFragment() {

    lateinit var loading: ImageView

    private lateinit var r: Runnable
    var h: Handler = Handler()

    private lateinit var mFusedLocationClient: FusedLocationProviderClient


    lateinit var psDialogMsg: PSDialogMsg

    companion object {
        fun newInstance() = SplashFragment()
        private const val PERMISSION_ID = 1234
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())
        //checkAllPermissions()
        getLastLocation()
        return inflater.inflate(R.layout.fragment_splash, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loading = view.findViewById(R.id.loadingSplash)
        loading.startAnimation(AnimationUtils.loadAnimation(requireContext(), R.anim.loading))
    }


    override fun initUIAndActions() {
        psDialogMsg = PSDialogMsg(activity, false)
        selected_location_id = "1"
        selected_location_name = "Mi Ubicacion"
        //goOn()
        Timber.d("LATITUD $selectedLat")
        Timber.d("LONGITUD $selectedLng")
        r = Runnable {
            loading.gone()
            if (loginUserId == Constants.EMPTY_STRING) {
                navigationController.navigateToUserLoginActivity(requireActivity())
                requireActivity().finish()
                h.removeCallbacks(r)
            } else {
                navigationController.navigateToMainActivity(
                    requireActivity(),
                    selected_location_id,
                    selected_location_name,
                    selectedLat,
                    selectedLng
                )
                requireActivity().finish()
                h.removeCallbacks(r)
            }
        }

        if (!checkPermissions()) {
            checkAllPermissions()
        } else {
            getLastLocation()
            h.postDelayed(r, 4500)
        }

    }

    override fun initAdapters() {
    }

    override fun initData() {

    }

    @SuppressLint("MissingPermission")
    private fun getLastLocation() {
        if (checkPermissions()) {
            if (isLocationEnabled()) {

                mFusedLocationClient.lastLocation.addOnCompleteListener(requireActivity()) { task ->
                    val location: Location? = task.result
                    if (location == null) {
                        requestNewLocationData()
                    } else {
                        Timber.d("ES $selectedLat LATITUD")
                        selectedLat = location.latitude.toString()
                        selectedLng = location.longitude.toString()

                        pref.putAny(Constants.LAT, selectedLat)
                        pref.putAny(Constants.LNG, selectedLng)
                    }
                }
            } else {
                Toast.makeText(requireContext(), "Turn on location", Toast.LENGTH_LONG).show()
                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(intent)
            }
        } else {
            checkAllPermissions()
        }
    }


    @SuppressLint("MissingPermission")
    private fun requestNewLocationData() {
        val mLocationRequest = LocationRequest()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.interval = 0
        mLocationRequest.fastestInterval = 0
        mLocationRequest.numUpdates = 1

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())
        mFusedLocationClient!!.requestLocationUpdates(
            mLocationRequest, mLocationCallback, Looper.myLooper()
        )
    }

    private val mLocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            val mLastLocation: Location? = locationResult.lastLocation
            if (mLastLocation != null) {
                selectedLat = mLastLocation.latitude.toString()
                selectedLng = mLastLocation.longitude.toString()
                pref.putAny(Constants.LAT, selectedLat)
                pref.putAny(Constants.LNG, selectedLng)
            }
        }
    }

    private fun isLocationEnabled(): Boolean {
        val locationManager: LocationManager =
            requireActivity().getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
            LocationManager.NETWORK_PROVIDER
        )
    }

    private fun checkPermissions(): Boolean {
        return ContextCompat.checkSelfPermission(
            requireActivity(),
            Manifest.permission.ACCESS_COARSE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(
                    requireActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED

    }

    private fun requestPermissions() {
        requestPermissions(
            arrayOf(
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
            ),
            PERMISSION_ID
        )
    }


    /*override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray): Unit {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        Timber.e("CODIGO: ${requestCode.toString()}")
        if (requestCode == PERMISSION_ID ) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                h.postDelayed(r,1500)
                getLastLocation()
            }else if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_DENIED){
                Timber.e("Grant empty!!")
                Toast.makeText(
                    context,
                    "Permisos rechazados.. la aplicacion podria no funcionar correctamente",
                    Toast.LENGTH_LONG
                ).show()
                activity?.finish()
                //h.postDelayed(r,2500)
            }
        }else{
            Timber.e("mal codigo!!")
        }
    }*/

    override fun onDestroy() {
        super.onDestroy()
        h.removeCallbacks(r)
    }

    private fun checkAllPermissions() {
        Dexter.withContext(requireContext())
            .withPermissions(
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.RECEIVE_SMS,
                Manifest.permission.READ_SMS,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                    report?.let {
                        if (report.areAllPermissionsGranted()) {
                            Timber.e("PERMISOS ACEPTADOS!!")
                            getLastLocation()
                            h.postDelayed(r, 2500)
                        } else {
                            activity?.finish()
                        }
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: MutableList<PermissionRequest>?,
                    token: PermissionToken?
                ) {
                    token?.continuePermissionRequest()
                }
            })
            .withErrorListener {
                Toast.makeText(requireContext(), it.name, Toast.LENGTH_SHORT).show()
            }
            .check()
    }

}