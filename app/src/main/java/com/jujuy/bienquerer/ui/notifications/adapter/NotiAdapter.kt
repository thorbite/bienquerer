package com.jujuy.bienquerer.ui.notifications.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.model.network.response.ListNotifications
import com.jujuy.bienquerer.utils.Utils
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class NotiAdapter(private val listener: OnItemClick):
    PagingDataAdapter<ListNotifications, NotiAdapter.NotiViewHolder>(UIMODEL_COMPARATOR) {

    var item: MutableList<ListNotifications>? = null


    interface OnItemClick{
        fun onItemClick(item: ListNotifications, position: Int)
    }

    override fun onBindViewHolder(holder: NotiViewHolder, position: Int) {
        getItem(position)?.let {
            item?.add(it)
            holder.bind(it, holder.itemView)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotiViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_notification, parent, false)
        return NotiViewHolder(view)
    }

    fun deleteItem(pos: Int) {
        notifyItemRemoved(pos)
    }

    fun getNotificationAt(position: Int): ListNotifications?{
        return getItem(position)
    }


    @SuppressLint("ClickableViewAccessibility")
    inner class NotiViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val titleTxt: TextView = view.findViewById(R.id.notiTitle)
        private val bodyTxt: TextView = view.findViewById(R.id.messageTextView)
        private val date: TextView = view.findViewById(R.id.dateNotification)
        private val itemBackground: ConstraintLayout = view.findViewById(R.id.notiConstraintLayout)

        init {
            itemView.setOnClickListener {
                val position = bindingAdapterPosition
                if (position != RecyclerView.NO_POSITION){
                    val item = getItem(position)
                    if (item != null) {
                        listener.onItemClick(item, position)
                    }
                }
            }
        }


        fun bind(item: ListNotifications, itemView: View) {
            with(item) {
                titleTxt.text = title.toString()
                bodyTxt.text = body.toString()
                date.text = Utils.parseDateCorrectForm(item.createdAt.toString())
            }

            if (item.isNew == true){
                itemBackground.setBackgroundColor(ContextCompat.getColor(itemView.context,R.color.md_grey_300))
            }else{
                itemBackground.setBackgroundColor(ContextCompat.getColor(itemView.context, R.color.md_white_1000))
            }
        }
    }

    companion object {
        private val UIMODEL_COMPARATOR = object : DiffUtil.ItemCallback<ListNotifications>() {
            override fun areItemsTheSame(oldItem: ListNotifications, newItem: ListNotifications): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: ListNotifications, newItem: ListNotifications): Boolean =
                oldItem == newItem
        }
    }
}