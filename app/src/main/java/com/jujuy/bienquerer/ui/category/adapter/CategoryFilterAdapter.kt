package com.jujuy.bienquerer.ui.category.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.databinding.ItemCategoryFilterBinding
import com.jujuy.bienquerer.databinding.ItemSubCategoryFilterBinding
import com.jujuy.bienquerer.model.entity.Category
import com.jujuy.bienquerer.model.entity.SubCategory
import com.jujuy.bienquerer.ui.category.categoryfilter.Grouping
import com.jujuy.bienquerer.utils.Constants
import com.jujuy.bienquerer.utils.Utils
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class CategoryFilterAdapter(
    val callback: FilteringClickCallback,
    var catId: String,
    var subCatId: String
) : BaseExpandableListAdapter() {
    var categoryList: List<Category> = ArrayList()
    var subCategoryList: List<SubCategory> = ArrayList()
    var map: LinkedHashMap<Category, List<SubCategory>> = LinkedHashMap()
    private val grouping: Grouping = Grouping()
    private var selectedView: TextView? = null

    interface FilteringClickCallback {
        fun onClick(catId: String?, subCatId: String?, nameSubcategory: String?)
    }

    fun replaceCategory(categoryList: List<Category>?) {
        this.categoryList = categoryList!!
        if (categoryList != null && categoryList.isNotEmpty()) {
            map = grouping.group(categoryList, subCategoryList)
        }
    }

    fun replaceSubCategory(subCategoryList: List<SubCategory>) {
        this.subCategoryList = subCategoryList
        if (categoryList.isNotEmpty() && subCategoryList.isNotEmpty()) {
            map = grouping.group(categoryList, subCategoryList)
        }
    }

    override fun getGroup(p0: Int): Any {
        return categoryList[p0]
    }

    override fun isChildSelectable(p0: Int, p1: Int): Boolean {
        return true
    }

    override fun hasStableIds(): Boolean {
        return true
    }

    override fun getGroupView(
        i: Int,
        isExpanded: Boolean,
        view: View?,
        viewGroup: ViewGroup?
    ): View {
        val binding: ItemCategoryFilterBinding = DataBindingUtil.inflate(
            LayoutInflater.from(viewGroup?.context),
            R.layout.item_category_filter,
            viewGroup,
            false
        )
        binding.category = categoryList[i]
        if (categoryList[i].idCategory.toString() == catId) {
            binding.groupview.setBackgroundColor(
                ContextCompat.getColor(
                    viewGroup?.context!!,
                    R.color.md_green_50
                )
            )
        }

        if (isExpanded) {
            Utils.toggleUporDown(binding.dropdownimage)
        }
        return binding.root
    }

    override fun getChildrenCount(i: Int): Int {
        val sub: List<SubCategory>?
        if (categoryList != null) {
            sub = map[categoryList[i]]
            if (sub != null) {
                return sub.size
            }
        }

        return 0
    }

    override fun getChild(i: Int, i1: Int): SubCategory? {
        val subCategoryList: List<SubCategory>? = map[categoryList[i]]
        if (subCategoryList != null) {
            return subCategoryList[i1]
        }
        return null
    }

    override fun getGroupId(p0: Int): Long {
        return p0.toLong()
    }

    override fun getChildView(
        i: Int,
        childPosition: Int,
        b: Boolean,
        view: View?,
        viewGroup: ViewGroup?
    ): View {
        val subCategoryItemBinding: ItemSubCategoryFilterBinding = DataBindingUtil.inflate(
            LayoutInflater.from(viewGroup?.context),
            R.layout.item_sub_category_filter,
            viewGroup,
            false
        )
        val subCategory: SubCategory? = getChild(i, childPosition)
        subCategoryItemBinding.subcategory = subCategory

        if (categoryList[i].idCategory.toString() == catId && subCategory?.idSubCategory == subCatId.toInt()) {
            subCategoryItemBinding.subcategoryItem.setCompoundDrawablesWithIntrinsicBounds(
                null,
                null,
                ContextCompat.getDrawable(viewGroup!!.context, R.drawable.baseline_check_green_24),
                null
            )
            selectedView = subCategoryItemBinding.subcategoryItem
        }


        subCategoryItemBinding.root.setOnClickListener { view1 ->
            if (selectedView != null) {
                selectedView!!.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null)
            }
            subCategoryItemBinding.subcategoryItem.setCompoundDrawablesWithIntrinsicBounds(
                null,
                null,
                ContextCompat.getDrawable(viewGroup!!.context, R.drawable.baseline_check_green_24),
                null
            )
            catId = categoryList[i].idCategory.toString()

            subCatId = if (childPosition != 0) {
                subCategory?.idSubCategory.toString()
            } else {
                Constants.ZERO
            }
            callback.onClick(catId, subCatId, subCategory?.nameSubCategory)

            (viewGroup.context as Activity).finish()
        }
        return subCategoryItemBinding.root
    }

    override fun getChildId(p0: Int, childPosition: Int): Long {
        return childPosition.toLong()
    }

    override fun getGroupCount(): Int {
        return if (categoryList == null) {
            0
        } else categoryList.size
    }
}