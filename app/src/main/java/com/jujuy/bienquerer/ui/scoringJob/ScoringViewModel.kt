package com.jujuy.bienquerer.ui.scoringJob

import androidx.lifecycle.*
import com.jujuy.bienquerer.api.network.Resource
import com.jujuy.bienquerer.base.BaseViewModel
import com.jujuy.bienquerer.model.network.BaseResponse
import com.jujuy.bienquerer.model.network.request.AppointmentRequest
import com.jujuy.bienquerer.model.network.request.Ratings
import com.jujuy.bienquerer.model.network.request.ScoreRequest
import com.jujuy.bienquerer.model.network.response.Client
import com.jujuy.bienquerer.repository.ProfessionalRepository
import com.jujuy.bienquerer.viewmodel.jobs.AppointmentViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.map
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class ScoringViewModel(val professionalRepository: ProfessionalRepository) : BaseViewModel() {
    var professionalContainer: Client? = null
    var professionOfProfessional: String = ""
    var jobId: String = ""

    private var saveProfessionalScoreData: LiveData<Resource<BaseResponse<Boolean>>>? = null
    private var saveProfessionalScoreObj = MutableLiveData<TmpDataHolder>()

    init {
        saveProfessionalScoreData = saveProfessionalScoreObj.switchMap { obj ->
            professionalRepository.putScoreToProfessional(
                ScoreRequest(
                    obj.jobID,
                    Ratings(obj.puntuality, obj.result, obj.price),
                    obj.comment
                )
            ).map {
                when(it.status){
                    Resource.Status.LOADING ->{
                        Resource.loading()
                    }
                    Resource.Status.SUCCESS ->{
                        Resource.success(it.data)
                    }
                    Resource.Status.ERROR -> {
                        Resource.error(it.message!!)
                    }
                }
            }.asLiveData(viewModelScope.coroutineContext)
        }
    }

    fun setSaveScoreObj(jobId: String, puntuality: String, result: String, price: String, comment: String?) {
            val tmpDataHolder = TmpDataHolder()
            tmpDataHolder.jobID = jobId
            tmpDataHolder.puntuality = puntuality
            tmpDataHolder.result = result
            tmpDataHolder.price = price
            tmpDataHolder.comment = comment ?: ""

            saveProfessionalScoreObj.value = tmpDataHolder
    }
    fun getSaveScoreData(): LiveData<Resource<BaseResponse<Boolean>>>? = saveProfessionalScoreData


    internal class TmpDataHolder {
        var jobID = ""
        var puntuality = ""
        var result = ""
        var price = ""
        var comment: String = ""
    }

}