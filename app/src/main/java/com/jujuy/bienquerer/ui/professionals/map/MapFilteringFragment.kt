package com.jujuy.bienquerer.ui.professionals.map

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.*
import android.widget.SeekBar
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.model.*
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.api.network.Resource
import com.jujuy.bienquerer.base.BaseFragment
import com.jujuy.bienquerer.databinding.MapFilteringFragmentBinding
import com.jujuy.bienquerer.model.ItemParameterHolder
import com.jujuy.bienquerer.model.entity.Professional
import com.jujuy.bienquerer.utils.Constants
import com.jujuy.bienquerer.utils.LoadingMsg
import com.jujuy.bienquerer.utils.Utils
import com.jujuy.bienquerer.utils.autoCleared
import com.jujuy.bienquerer.viewmodel.professional.ProfessionalViewModel
import com.skydoves.whatif.whatIfNotNull
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import kotlin.math.ln
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class MapFilteringFragment : BaseFragment(), GoogleMap.OnInfoWindowClickListener {

    companion object {
        fun newInstance() =
            MapFilteringFragment()
    }

    private val PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1
    lateinit var itemParameterHolder: ItemParameterHolder
    lateinit var map: GoogleMap
    var markerOptions: MarkerOptions? = null
    var circle: Circle? = null
    var bundle: Bundle? = null
    var mHashMap: HashMap<Marker, Array<String>> = HashMap()
    private val seekBarValues = arrayOf("0.5", "1", "2.5", "5", "10", "25", "50", "100", "200", "500", "Todo")

    private var currentIndex = 1

    private var binding by autoCleared<MapFilteringFragmentBinding>()

    private val markerOp =  MarkerOptions()
    private val markerOp2 =  MarkerOptions()
    var marker: List<Marker>? =null
    var marker2: Marker? =null
    private val distance = FloatArray(2)
    private val distance2 = FloatArray(2)
    private val professionalViewModel by viewModel<ProfessionalViewModel>()
    private val loading : LoadingMsg by lazy { LoadingMsg(requireActivity(), false) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = MapFilteringFragmentBinding.inflate(inflater, container, false)
        activity?.let {
            it.window?.setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN
            )
        }
        setHasOptionsMenu(true)
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_map_filtering, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
       when(item.itemId) {
           R.id.aceptFilterMap -> {
              activity.whatIfNotNull {
                  if (currentIndex != seekBarValues.size - 1) {
                      //itemParameterHolder.mapMiles = String.valueOf(Utils.roundDouble(Float.parseFloat(seekBarValues[currentIndex]) * 0.621371, 3));
                      itemParameterHolder.mapMiles = circle?.radius.toString() //seekBarValues[currentIndex]
                  } else {
                      resetTheValues()
                  }
                  navigationController.navigateBackToSearchFromMapFiltering(requireActivity(), itemParameterHolder)
              }
           }
           R.id.applyFilterMap -> {
               //resetTheValues()
               getNearbyMarkers(itemParameterHolder.lat.toDouble(), itemParameterHolder.lng.toDouble(), itemParameterHolder.mapMiles)
           }
       }
        return super.onOptionsItemSelected(item)
    }

    private fun getMiles(kmValue: Float): String? {
        return (Utils.roundDouble(kmValue * 0.621371, 3).toString())
    }

    private fun  initializeMap(savedInstanceState: Bundle?){
        activity.whatIfNotNull {
            itemParameterHolder = requireActivity().intent.getSerializableExtra(Constants.ITEM_HOLDER) as ItemParameterHolder

            if (itemParameterHolder.lat != "" && itemParameterHolder.lng != "") {
                itemParameterHolder.mapMiles = getMiles(2.5f)
            }
        }
        try {
            MapsInitializer.initialize(this.activity)
            Timber.e("MAPA CREADO")
        }catch (e:Exception){
            Timber.e("ERROR $e")
        }

        binding.mapView.onCreate(savedInstanceState)
        binding.mapView.onResume()

        binding.mapView.getMapAsync {
            map = it
            Timber.e(map.toString())
            updateUI(itemParameterHolder)
            checkToDisableSeekBar()

            map.setOnMapClickListener {latLng ->
                if (markerOptions == null){
                    markerOptions = MarkerOptions()
                }
                checkToDisableSeekBar()

                markerOptions?.position(latLng)
                markerOptions?.title(latLng.latitude.toString() + ":"+ latLng.longitude.toString())
                markerOptions?.draggable(false)

                itemParameterHolder.lat = latLng.latitude.toString()
                itemParameterHolder.lng = latLng.longitude.toString()

                map.clear()
                var zoomlevel = 15
                if (circle != null){
                    zoomlevel = getZoomLevel(circle)
                }

                // Animating to the touched position
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoomlevel.toFloat()))
                map.addMarker(markerOptions)

                if (currentIndex == seekBarValues.size - 1) {
                    currentIndex = seekBarValues.size - 2
                    binding.seekBar.progress = seekBarValues.size - 2
                }

                drawCircle(latLng.latitude, latLng.longitude, seekBarValues[currentIndex])
            }

            map.setOnMarkerDragListener(object : GoogleMap.OnMarkerDragListener{
                override fun onMarkerDragEnd(marker: Marker?) {
                    itemParameterHolder.lat = marker!!.position.latitude.toString()
                    itemParameterHolder.lng = marker.position.longitude.toString()

                    if (currentIndex == seekBarValues.size - 1) {
                        currentIndex = seekBarValues.size - 2
                        binding.seekBar.progress = seekBarValues.size - 2
                    }

                    if (itemParameterHolder.lat.isNotEmpty() && itemParameterHolder.lng.isNotEmpty()) {
                        drawCircle(
                            marker.position.latitude,
                            marker.position.longitude,
                            seekBarValues[currentIndex]
                        )
                    }
                }

                override fun onMarkerDragStart(p0: Marker?) {}

                override fun onMarkerDrag(p0: Marker?) {}

            })
            getMyLocation()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }

    /*override fun onDestroy() {
        if (binding != null) {
            binding.mapView.onDestroy()
            map.clear()
        }
        super.onDestroy()
    }*/

    override fun onLowMemory() {
        binding.mapView.onLowMemory()
        super.onLowMemory()
    }

    override fun onPause() {
        binding.mapView.onPause()
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
        binding.mapView.onResume()
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getMyLocation()
            } else {
                if (activity != null) {
                    this.requireActivity().finish()
                }
            }
        }
    }

    private fun drawCircle(lat: Double, lng: Double, radius: String) {
        try {
            val temp = radius.toDouble()
            circle = if (circle != null) {
                circle!!.remove()
                map.addCircle(
                    CircleOptions()
                        .center(LatLng(lat, lng))
                        .radius(temp * 1000)
                        .strokeColor(ContextCompat.getColor(requireContext(),R.color.md_blue_50))
                        .fillColor(0x220000b2)
                )
            } else {
                map.addCircle(CircleOptions()
                        .center(LatLng(lat, lng))
                        .radius(temp * 1000)
                        .strokeColor(ContextCompat.getColor(requireContext(),R.color.md_blue_50))
                        .fillColor(0x220000b2)
                )
            }

            //getNearbyMarkers(lat, lng, radius)
            //addSecondMarker()
        } catch (e: NumberFormatException) {
            if (circle != null) {
                circle!!.remove()
            }
            Timber.e(e)
        }
    }

    private fun getZoomLevel(circle: Circle?): Int {
            var zoomLevel = 0
            if (circle != null) {
                val radius = circle.radius
                val scale = radius / 500
                zoomLevel = (16 - ln(scale) / ln(2.0)).toInt()
            }
            return zoomLevel
    }

    private fun getNearbyMarkers(lat: Double, lng: Double, radius: String) {
        Timber.e("getNearbyMarkers")
        marker?.forEach { it.remove()}

        professionalViewModel.setGetMapList(
            lat.toString(),
            lng.toString(),
            itemParameterHolder.cat_id.toString(),
            itemParameterHolder.sub_cat_id.toString(),
            circle?.radius.toString())

        /*Location.distanceBetween(
            itemParameterHolder.lat.toDouble() + 0.1,
            itemParameterHolder.lng.toDouble() + 0.1,
            circle!!.center.latitude,
            circle!!.center.longitude,
            distance
        )

        if (distance[0] <= circle!!.radius) {
            markerOp.title("Profesional 1")
            markerOp.icon(BitmapDescriptorFactory.fromBitmap(generateSmallIcon(requireContext())))
            markerOp.snippet("Soy un profesional")
            markerOp.position(LatLng(itemParameterHolder.lat.toDouble() + 0.1, itemParameterHolder.lng.toDouble() + 0.1))
            marker = listOf(map.addMarker(markerOp))
            marker?.forEach { it.showInfoWindow() }
        } else {
            marker?.forEach{it.remove()}
        }*/

    }

    /*private fun addSecondMarker() {
        marker2?.remove()
        Location.distanceBetween(
            itemParameterHolder.lat.toDouble() - 0.1,
            itemParameterHolder.lng.toDouble() - 0.1,
            circle!!.center.latitude,
            circle!!.center.longitude,
            distance2
        )

        if (distance2[0] <= circle!!.radius) {
            markerOp2.title("Profesional 2")
            markerOp2.icon(BitmapDescriptorFactory.fromBitmap(generateSmallIcon(requireContext())))
            markerOp2.snippet("Soy otro profesional")
            markerOp2.position(LatLng(itemParameterHolder.lat.toDouble() - 0.1, itemParameterHolder.lng.toDouble() - 0.1))
            marker2 = map.addMarker(markerOp2)
            marker2?.showInfoWindow()
        } else {
            marker2?.remove()
        }

    }*/

    override fun initUIAndActions() {
        itemParameterHolder = ItemParameterHolder()

        binding.startPointTextView.text = String.format("%s km", seekBarValues[0])

        binding.seekBar.max = 10

        binding.seekBar.progress = currentIndex

        binding.seekBarValueTextView.text = String.format("%s km", seekBarValues[currentIndex])

        binding.seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener{
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                currentIndex = progress
                if (progress != seekBarValues.size -1){
                    binding.seekBarValueTextView.text = String.format("%s km", seekBarValues[progress])
                    if (markerOptions == null) {
                        markerOptions = MarkerOptions()
                    }
                    if (itemParameterHolder.lat.isNotEmpty() && itemParameterHolder.lng.isNotEmpty()) {
                        map.addMarker(markerOptions!!
                                .position(LatLng(itemParameterHolder.lat.toDouble(),itemParameterHolder.lng.toDouble()))
                                .title("City Name")
                                .draggable(false)
                        )
                    }
                }else{
                    binding.seekBarValueTextView.text = seekBarValues[progress]

                    if (map != null) {
                        map.clear()
                        markerOptions = null
                    }
                }

                if (itemParameterHolder.lat.isNotEmpty() && itemParameterHolder.lng.isNotEmpty()) {
                    drawCircle(itemParameterHolder.lat.toDouble(), itemParameterHolder.lng.toDouble(), seekBarValues[currentIndex])
                    var zoomlevel = 15
                    if (circle != null) {
                        zoomlevel = getZoomLevel(circle)
                    }

                    map.animateCamera(
                        CameraUpdateFactory.newLatLngZoom(
                            LatLng(
                                itemParameterHolder.lat.toDouble(),
                                itemParameterHolder.lng.toDouble()
                            ),
                            zoomlevel.toFloat()
                        )
                    )
                }
            }

            override fun onStartTrackingTouch(p0: SeekBar?) {}

            override fun onStopTrackingTouch(p0: SeekBar?) {}

        })
        initializeMap(bundle)
    }

    override fun initAdapters() {}

    override fun initData() {
        Timber.e("initdata")

        professionalViewModel.getMapList()?.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Resource.Status.LOADING -> {
                    loading.showloaderDialog()
                    loading.show()

                }
                Resource.Status.SUCCESS -> {
                    loading.cancel()
                    Timber.e("${it.data}")
                    professionalViewModel.listItem = it.data?.data!!
                    setMarkers()
                }
                Resource.Status.ERROR -> {
                    loading.cancel()
                    Timber.e(it.message)
                }
            }
        })
    }

    private fun setMarkers(){
        if (professionalViewModel.listItem != null) {
            for (item in professionalViewModel.listItem!!) {
                markerOp.title("${item.name} ${item.lastname}")
                markerOp.icon(BitmapDescriptorFactory.fromBitmap(generateSmallIcon(requireContext())))
                markerOp.snippet("${item.profession?.get(0)?.nameSubCategory}")
                markerOp.position(LatLng(item.latitude!!.toDouble(), item.longitude!!.toDouble()))
                map.setInfoWindowAdapter(CustomInfoWindowAdapter(LayoutInflater.from(requireContext()), item))
                map.setOnInfoWindowClickListener(this)
                val markerTemporal = map.addMarker(markerOp)
                mHashMap[markerTemporal] = arrayOf(item.id.toString(), "${item.name} ${item.lastname}")
                marker = listOf(markerTemporal)
                marker?.forEach {
                    it.showInfoWindow()
                }
            }
        }
    }

    private fun getMyLocation(){
        Timber.e("getMyLocation")
        if (this.activity != null) {
            if (ContextCompat.checkSelfPermission(this.requireActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
                    this.requireActivity(),
                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION)
            } else {
                if (map != null) {
                    map.isMyLocationEnabled = true
                }
            }
        }
    }

    private fun resetTheValues() {
        initUIAndActions()
    }

    private fun checkToDisableSeekBar() {
        binding.seekBar.isEnabled = markerOptions != null
    }

    private fun updateUI(itemParameterHolder: ItemParameterHolder) {
        Timber.e("updateUI")
        if (itemParameterHolder.lat.isNotEmpty() && itemParameterHolder.lng.isNotEmpty() && itemParameterHolder.mapMiles.isNotEmpty()) {
            if (map != null) {
                map.moveCamera(
                    CameraUpdateFactory.newCameraPosition(
                        CameraPosition.Builder().target(
                            LatLng(itemParameterHolder.lat.toDouble(), itemParameterHolder.lng.toDouble()
                            )
                        ).zoom(10f).bearing(10f).tilt(10f).build()
                    )
                )
                if (markerOptions == null) {
                    markerOptions = MarkerOptions()
                }
                map.addMarker(
                    markerOptions!!
                        .position(
                            LatLng(itemParameterHolder.lat.toDouble(), itemParameterHolder.lng.toDouble())
                        )
                        //.icon(BitmapDescriptorFactory.fromBitmap(generateSmallIcon(requireContext())))
                        .title("City Name")
                        .draggable(false)
                )
                currentIndex = findTheIndexOfTheValue(itemParameterHolder.mapMiles)
                drawCircle(itemParameterHolder.lat.toDouble(), itemParameterHolder.lng.toDouble(),
                    seekBarValues[currentIndex]
                )
                binding.seekBar.progress = currentIndex
            }
        } else {
            binding.seekBar.progress = seekBarValues.size
            currentIndex = seekBarValues.size - 1
        }
    }

    private fun findTheIndexOfTheValue(value: String): Int {
        var index = 0
        for (i in 0 until seekBarValues.size - 1) {
            if (value != "All") {
                //if (String.valueOf(Utils.roundDouble(Float.parseFloat(seekBarValues[i]) * 0.621371, 3)).equals(value)) {
                if (getMiles(seekBarValues[i].toFloat()) == value) {
                    index = i
                }
            } else {
                index = seekBarValues.size - 1
            }
        }
        return index
    }


    private fun generateSmallIcon(context: Context): Bitmap {
        val height = 100
        val width = 100
        val bitmap = BitmapFactory.decodeResource(context.resources, R.drawable.finger_marker)
        return Bitmap.createScaledBitmap(bitmap, width, height, false)
    }

    override fun onInfoWindowClick(p0: Marker) {
        val pos = mHashMap[p0]
        navigationController.navigateToItemDetailActivity(requireActivity(), pos?.get(0).toString(), pos?.get(1).toString() )
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                Timber.e("On back pressed!!!!")
                navigationController.navigateBackToFiltering(requireActivity(), itemParameterHolder.cat_id, itemParameterHolder.sub_cat_id)
            }
        })
    }
}