package com.jujuy.bienquerer.ui.appointment.bookAppointment

import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.api.network.Resource
import com.jujuy.bienquerer.base.BaseFragment
import com.jujuy.bienquerer.databinding.FragmentBookAppointmentDetailBinding
import com.jujuy.bienquerer.model.entity.Professional
import com.jujuy.bienquerer.ui.appointment.selectProfessionItem.SelectableItemActivity
import com.jujuy.bienquerer.utils.*
import com.jujuy.bienquerer.utils.Constants.EMPTY_STRING
import com.jujuy.bienquerer.utils.Constants.RESULT_OK
import com.jujuy.bienquerer.viewmodel.jobs.AppointmentViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import java.util.*
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class BookAppointmentDetailFragment : BaseFragment() {
    lateinit var c: Calendar
    private var year : Int? = null
    private var month : Int? = null
    private var day : Int? = null

    private var binding by autoCleared<FragmentBookAppointmentDetailBinding>()
    private val appointmentViewModel by viewModel<AppointmentViewModel>()
    private val psDialogMsg: PSDialogMsg by lazy { PSDialogMsg(activity, false)}
    private val loading: LoadingMsg by lazy { LoadingMsg(requireActivity(), false) }

    companion object {
        const val REQUEST_CODE_SERVICE = 100
        fun newInstance() = BookAppointmentDetailFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentBookAppointmentDetailBinding.inflate(inflater, container, false)
        activity?.let { it.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN) }
        setHasOptionsMenu(true)
        c = Calendar.getInstance()
         year = c.get(Calendar.YEAR)
         month = c.get(Calendar.MONTH)
         day = c.get(Calendar.DAY_OF_MONTH)
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }


    override fun initUIAndActions() {
        if (activity != null) {
            appointmentViewModel.professionalContainer = requireActivity().intent.getParcelableExtra<Professional>(Constants.PROFESSIONAL) as Professional

            Timber.e(appointmentViewModel.professionalContainer.toString())

            appointmentViewModel.name = requireActivity().intent.getStringExtra(Constants.USER_NAME)
            appointmentViewModel.lastname =
                requireActivity().intent.getStringExtra(Constants.USER_LASTNAME)
            appointmentViewModel.email =
                requireActivity().intent.getStringExtra(Constants.USER_EMAIL)
            appointmentViewModel.phone =
                requireActivity().intent.getStringExtra(Constants.USER_PHONE)
        }
        binding.appointmentUserName.setText("Nombre: ${appointmentViewModel.professionalContainer?.name} ${appointmentViewModel.professionalContainer?.lastname}")
        binding.appointmentUserEmail.setText("Email: ${appointmentViewModel.professionalContainer?.email}")
        binding.appointmentUserPhone.setText("Telefono: ${appointmentViewModel.professionalContainer?.phone ?: "-"}")

        binding.appointmentService.setOnClickListener {
            val newIntent = Intent(requireContext(), SelectableItemActivity::class.java)
            val categories = ArrayList<String>()
            appointmentViewModel.professionalContainer?.profession?.let {
                for (services in it) {
                    categories.add(services.nameSubCategory)
                }
            }
            newIntent.putStringArrayListExtra(Constants.DATA_PROFESSION, categories)
            newIntent.putExtra(Constants.TITLE_PROFESSION, "Servicios")
            newIntent.putExtra(
                Constants.SELECTED_ITEM,
                binding.appointmentService.text.toString()
            )
            startActivityForResult(newIntent, REQUEST_CODE_SERVICE)
        }

        binding.appointmentDate.setOnClickListener {
            year?.let { it1 ->
                month?.let { it2 ->
                    day?.let { it3 ->
                        DatePickerDialog(
                            requireContext(),
                            DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                                // Display Selected date in TextView
                                binding.appointmentDate.setText("$dayOfMonth-${monthOfYear + 1}-$year")
                            },
                            it1,
                            it2,
                            it3
                        )
                    }
                }
            }?.show()
        }

        binding.appointmentConfirm.setOnClickListener {
            var professionId = ""
            for (i in appointmentViewModel.professionalContainer?.profession!!.indices) {
                if (appointmentViewModel.professionalContainer?.profession!![i].nameSubCategory == binding.appointmentService.text) {
                    professionId =
                        appointmentViewModel.professionalContainer!!.profession?.get(i)?.idSubCategory.toString()
                }
            }
            appointmentViewModel.setSaveAppointmentObj(
                appointmentViewModel.professionalContainer?.id.toString(),
                professionId,
                binding.appointmentDate.text.toString(),
                binding.appointmentDescription.text.toString()
            )
        }
    }

    override fun initAdapters() {

    }

    override fun initData() {
        appointmentViewModel.getSaveAppointmentData()?.observe(this, androidx.lifecycle.Observer { response ->
             when(response.status){
                 Resource.Status.LOADING ->{
                 }
                 Resource.Status.SUCCESS -> {
                     if (response.data?.code == 200){
                         psDialogMsg.showSuccessDialog(response.data.message, getString(R.string.app__ok))
                         psDialogMsg.okButton.setOnClickListener {
                             psDialogMsg.cancel()
                             //activity?.finish()
                             navigationController.navigateToJobList(requireActivity())
                             (activity as BookAppointmentDetailActivity).changeTextToolbar("Lista de trabajos")
                         }
                         psDialogMsg.show()
                         //Toast.makeText(requireContext(), response.data.message, Toast.LENGTH_SHORT).show()
                     }
                 }
                 Resource.Status.ERROR -> {
                     psDialogMsg.showErrorDialog(response.message, getString(R.string.app__ok))
                     psDialogMsg.show()
                 }
             }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE_SERVICE) {
            if (resultCode == RESULT_OK) {
                val categoryList: List<*>? = data!!.getStringArrayListExtra(Constants.DATA_PROFESSION)
                if (categoryList != null && categoryList.isNotEmpty()) {
                    binding.appointmentService.text = categoryList[0].toString()
                    binding.appointmentService.error = null

                } else {
                    binding.appointmentService.text = EMPTY_STRING
                }
            }
        }
    }

}