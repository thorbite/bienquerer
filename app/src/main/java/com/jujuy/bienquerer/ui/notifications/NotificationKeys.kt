package com.jujuy.bienquerer.ui.notifications

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "notification_keys")
data class NotificationKeys (
    @PrimaryKey
    val repoId: Int,
    val prevKey: Int?,
    val nextKey: Int?
)