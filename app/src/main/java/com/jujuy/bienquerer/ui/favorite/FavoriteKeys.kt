package com.jujuy.bienquerer.ui.favorite

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "favorite_keys")
data class FavoriteKeys (
    @PrimaryKey
    val favId: Int,
    val prevKey: Int?,
    val nextKey: Int?
)