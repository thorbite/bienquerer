package com.jujuy.bienquerer.ui.professionals.comments

import androidx.lifecycle.*
import com.jujuy.bienquerer.api.network.Resource
import com.jujuy.bienquerer.base.BaseViewModel
import com.jujuy.bienquerer.model.entity.Category
import com.jujuy.bienquerer.model.entity.Comment
import com.jujuy.bienquerer.model.network.BaseResponse
import com.jujuy.bienquerer.repository.ProfessionalRepository
import com.jujuy.bienquerer.viewmodel.category.CategoryViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.map
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class CommentsViewModel(private val professionalRepository: ProfessionalRepository) : BaseViewModel() {
    var professionalId: String? = ""
    private var categoryListObj: MutableLiveData<TmpDataHolder> = MutableLiveData<TmpDataHolder>()


    // category list
    fun setCategoryListObj(limit: String, offset: String) {
        if (!isLoading) {
            val tmpDataHolder = TmpDataHolder()
            tmpDataHolder.offset = offset
            tmpDataHolder.limit = limit
            categoryListObj.value = tmpDataHolder

            // start loading
            setLoadingState(true)
        }
    }
    @ExperimentalTime
    @ExperimentalCoroutinesApi
    fun getCategoryListData(professionalId: String): LiveData<Resource<BaseResponse<List<Comment>>>> = categoryListObj.switchMap { obj ->
        professionalRepository.getListComments(professionalId).map {
            when(it.status){
                Resource.Status.LOADING -> {
                    Resource.loading()
                }
                Resource.Status.SUCCESS -> {
                    val user = it.data
                    Resource.success(user)
                }
                Resource.Status.ERROR -> {
                    Resource.error(it.message!!)
                }
            }
        }.asLiveData(viewModelScope.coroutineContext)
    }
    internal class TmpDataHolder {
        var limit = ""
        var offset = ""
    }

}