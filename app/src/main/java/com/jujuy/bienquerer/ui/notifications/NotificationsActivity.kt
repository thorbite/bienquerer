package com.jujuy.bienquerer.ui.notifications

import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.base.BaseActivity
import com.jujuy.bienquerer.databinding.ActivityNotificationsBinding
import com.skydoves.whatif.whatIfNotNull
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class NotificationsActivity :  BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityNotificationsBinding = DataBindingUtil.setContentView(this,R.layout.activity_notifications)
        initUI(binding)
    }


    private fun initUI(binding: ActivityNotificationsBinding) {

        initToolbar(binding.toolbar, "Notificaciones")
        setupFragment(NotificationListFragment())
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val fragment = supportFragmentManager.findFragmentById(R.id.content_frame)
        fragment.whatIfNotNull {
            it.onActivityResult(requestCode,resultCode,data)
        }
    }

}
