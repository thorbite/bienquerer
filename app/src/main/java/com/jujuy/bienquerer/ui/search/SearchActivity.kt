package com.jujuy.bienquerer.ui.search

import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.view.WindowManager
import android.view.animation.LinearInterpolator
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import coil.api.load
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.api.network.AppExecutors
import com.jujuy.bienquerer.api.network.Resource
import com.jujuy.bienquerer.base.BaseActivity
import com.jujuy.bienquerer.base.NavigationController
import com.jujuy.bienquerer.databinding.ActivitySearchBinding
import com.jujuy.bienquerer.extensions.dpToPx
import com.jujuy.bienquerer.extensions.gone
import com.jujuy.bienquerer.extensions.visible
import com.jujuy.bienquerer.model.ItemParameterHolder
import com.jujuy.bienquerer.model.entity.ItemSearch
import com.jujuy.bienquerer.ui.search.adapter.ProfessionalAdapter
import com.jujuy.bienquerer.utils.Constants
import com.jujuy.bienquerer.viewmodel.professional.ProfessionalViewModel
import com.paulrybitskyi.persistentsearchview.adapters.model.SuggestionItem
import com.paulrybitskyi.persistentsearchview.listeners.OnSearchConfirmedListener
import com.paulrybitskyi.persistentsearchview.listeners.OnSearchQueryChangeListener
import com.paulrybitskyi.persistentsearchview.listeners.OnSuggestionChangeListener
import com.paulrybitskyi.persistentsearchview.utils.SuggestionCreationUtil
import com.paulrybitskyi.persistentsearchview.utils.VoiceRecognitionDelegate
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import kotlin.time.ExperimentalTime

@ExperimentalTime
@ExperimentalCoroutinesApi
class SearchActivity : BaseActivity(), View.OnClickListener,
    ProfessionalAdapter.ProfClickCallback {
    private val searchViewModel by viewModel<ProfessionalViewModel>()
    private val dataProvider: DataProvider by inject()
    private var items: MutableList<ItemSearch> = mutableListOf()
    private lateinit var adapter: ProfessionalAdapter
    private val appExecutors by inject<AppExecutors>()
    private val navigationController: NavigationController by inject()
    private var itemParameterHolder = ItemParameterHolder()
    lateinit var binding: ActivitySearchBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_search)
        init()
    }

    private fun init() {
        initProgressBar()
        initSearchView()
        initEmptyView()
        initRecyclerView()
        getData()
    }

    private fun initProgressBar() {
        binding.progressBar.gone()
    }

    private fun initSearchView() = with(binding.persistentSearchView) {
        setOnLeftBtnClickListener(this@SearchActivity)
        setOnClearInputBtnClickListener(this@SearchActivity)
        setOnRightBtnClickListener(this@SearchActivity)
        hideRightButton()
        setVoiceRecognitionDelegate(VoiceRecognitionDelegate(this@SearchActivity))
        setOnSearchConfirmedListener(mOnSearchConfirmedListener)
        setOnSearchQueryChangeListener(mOnSearchQueryChangeListener)
        setOnSuggestionChangeListener(mOnSuggestionChangeListener)
        setDismissOnTouchOutside(true)
        setDimBackground(true)
        isProgressBarEnabled = true
        isVoiceInputButtonEnabled = true
        isClearInputButtonEnabled = true
        setSuggestionsDisabled(false)
        setQueryInputGravity(Gravity.START or Gravity.CENTER)
    }

    private fun initEmptyView() {
        binding.emptyViewLl.isVisible = items.isEmpty()
    }

    private fun initRecyclerView() = with(binding.recyclerView) {
        layoutManager = LinearLayoutManager(this@SearchActivity)
        adapter = initAdapter()
        addItemDecoration(initVerticalSpacingItemDecorator())
        addOnScrollListener(initHeaderedRecyclerViewListener())
    }

    private fun initAdapter(): ProfessionalAdapter {
        return ProfessionalAdapter(appExecutors, this).also { adapter = it }
    }

    override fun onClick(professional: ItemSearch) {
        if (professional.entity == "professional") {
            navigationController.navigateToItemDetailActivity(
                this@SearchActivity, professional.id!!, professional.name.orEmpty()
            )
        } else {
            itemParameterHolder.sub_cat_id = professional.id.toString()
            navigationController.navigateToHomeFilteringActivity(
                this@SearchActivity, itemParameterHolder, professional.name!!,
                Constants.MAP_MILES
            )
        }
        finish()
    }

    private fun initVerticalSpacingItemDecorator(): VerticalSpacingItemDecorator {
        return VerticalSpacingItemDecorator(
            verticalSpacing = 2.dpToPx(this),
            verticalSpacingCompensation = 2.dpToPx(this)
        )
    }

    private fun initHeaderedRecyclerViewListener(): HeaderedRecyclerViewListener {
        return object : HeaderedRecyclerViewListener(this@SearchActivity) {
            override fun showHeader() {
                AnimationUtils.showHeader(binding.persistentSearchView)
            }

            override fun hideHeader() {
                AnimationUtils.hideHeader(binding.persistentSearchView)
            }

        }
    }

    private fun loadInitialDataIfNecessary() {
        val searchQueries = if (binding.persistentSearchView.isInputQueryEmpty) {
            dataProvider.getInitialSearchQueries()
        } else {
            dataProvider.getSuggestionsForQuery(binding.persistentSearchView.inputQuery)
        }
        setSuggestions(searchQueries, false)
    }

    private fun performSearch(query: String) {
        binding.apply {
            emptyViewLl.gone()
            recyclerView.alpha = 0f
            progressBar.visible()
            searchInProfessional(query)
            persistentSearchView.hideLeftButton(false)
            persistentSearchView.showProgressBar()
        }
    }

    private fun getData() {
        searchViewModel.getProfessionalListByKeyData()?.observe(this, { listResource ->
            if (listResource != null) {
                when (listResource.status) {
                    Resource.Status.LOADING -> {
                        showSearchLoading()
                    }
                    Resource.Status.SUCCESS -> {
                        setupSearchAdapter(listResource.data?.data)
                    }
                    Resource.Status.ERROR -> {
                        handleSearchError(listResource.message)
                    }
                }
            }
        })
    }

    private fun showSearchLoading() {

    }

    private fun setupSearchAdapter(professionalList: List<ItemSearch>?) {
        Timber.e(professionalList.toString())
        if (professionalList.isNullOrEmpty()) {
            setSearchViewsVisibility(false)
        } else {
            setSearchViewsVisibility(true)
            replaceData(professionalList)
            items = professionalList.toMutableList()
        }
    }

    private fun setSearchViewsVisibility(dataAvailable: Boolean) {
        binding.apply {
            progressBar.gone()
            persistentSearchView.hideProgressBar(false)
            persistentSearchView.showLeftButton(true)
            if (!dataAvailable) {
                recyclerView.gone()
                emptyViewLl.visible()
                iconIv.load(R.drawable.baseline_empty_item_grey_24)
                titleTv.text = "Error"
                descriptionTv.text = "No se encontraron datos"
            } else {
                emptyViewLl.gone()
                recyclerView.visible()
                recyclerView.animate()
                    .alpha(1f)
                    .setInterpolator(LinearInterpolator())
                    .setDuration(300L)
                    .start()
            }
        }
    }

    private fun replaceData(professionalList: List<ItemSearch>) {
        adapter.submitList(professionalList)
    }

    private fun handleSearchError(message: String?) {
        setSearchViewsVisibility(false)
    }


    override fun onClick(view: View) {
        when (view.id) {
            R.id.leftBtnIv -> onLeftButtonClicked()
            R.id.clearInputBtnIv -> onClearInputButtonClicked()
        }
    }

    private fun onLeftButtonClicked() {
        onBackPressed()
    }

    private fun onClearInputButtonClicked() {
        //
    }

    private val mOnSearchConfirmedListener = OnSearchConfirmedListener { searchView, query ->
        saveSearchQueryIfNecessary(query)
        searchView.collapse()
        performSearch(query)
    }

    private val mOnSearchQueryChangeListener =
        OnSearchQueryChangeListener { searchView, oldQuery, newQuery ->
            setSuggestions(
                if (newQuery.isBlank()) {
                    dataProvider.getInitialSearchQueries()
                } else {
                    if (newQuery.trim { it <= ' ' }.length >= 3) {
                        searchInProfessional(newQuery)
                    }
                    dataProvider.getSuggestionsForQuery(newQuery)
                },
                true
            )
        }


    private val mOnSuggestionChangeListener = object : OnSuggestionChangeListener {
        override fun onSuggestionPicked(suggestion: SuggestionItem) {
            val query = suggestion.itemModel.text
            saveSearchQueryIfNecessary(query)
            setSuggestions(dataProvider.getSuggestionsForQuery(query), false)
            performSearch(query)
        }

        override fun onSuggestionRemoved(suggestion: SuggestionItem) {
            dataProvider.removeSearchQuery(suggestion.itemModel.text)
        }
    }

    private fun searchInProfessional(query: String) {
        searchViewModel.holder.keyword = query.trim()
        searchViewModel.setProfessionalListByKey(searchViewModel.holder)
    }

    private fun saveSearchQueryIfNecessary(query: String) {
        dataProvider.saveSearchQuery(query)
    }

    private fun setSuggestions(queries: List<String>, expandIfNecessary: Boolean) {
        val suggestions: List<SuggestionItem> =
            SuggestionCreationUtil.asRecentSearchSuggestions(queries)
        binding.persistentSearchView.setSuggestions(suggestions, expandIfNecessary)
    }

    override fun onResume() {
        super.onResume()
        loadInitialDataIfNecessary()
        if (shouldExpandSearchView()) {
            binding.persistentSearchView.expand(false)
            window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE or WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING)
        } else {
            window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN or WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING)
        }
    }

    private fun shouldExpandSearchView(): Boolean {
        return (
                (binding.persistentSearchView.isInputQueryEmpty && (adapter.itemCount == 0)) ||
                        binding.persistentSearchView.isExpanded
                )
    }

    override fun onBackPressed() {
        if (binding.persistentSearchView.isExpanded) {
            binding.persistentSearchView.collapse()
            return
        }
        super.onBackPressed()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        VoiceRecognitionDelegate.handleResult(binding.persistentSearchView, requestCode, resultCode, data)
    }
}