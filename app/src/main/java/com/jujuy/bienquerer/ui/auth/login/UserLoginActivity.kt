package com.jujuy.bienquerer.ui.auth.login

import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.base.BaseActivity
import com.jujuy.bienquerer.databinding.UserLoginActivityBinding
import com.skydoves.whatif.whatIfNotNull
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class UserLoginActivity : BaseActivity() {
    val binding : UserLoginActivityBinding by lazy {
        DataBindingUtil.setContentView<UserLoginActivityBinding>(this,R.layout.user_login_activity) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initUI(binding)
    }

    private fun initUI(binding: UserLoginActivityBinding) {
        //initToolbar(binding.toolbar, "Ingresar")
        setupFragment(UserLoginFragment())
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val fragment = supportFragmentManager.findFragmentById(R.id.content_frame)
        fragment.whatIfNotNull {
            it.onActivityResult(requestCode,resultCode,data)
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}
