package com.jujuy.bienquerer.ui.appointment.selectProfessionItem

import com.jujuy.bienquerer.model.network.response.Appointment
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
interface OnListInteractionListener {
    fun onAppointmentInteraction(item: Appointment?, pos: Int)
}