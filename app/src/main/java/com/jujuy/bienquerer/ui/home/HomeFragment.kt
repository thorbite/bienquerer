package com.jujuy.bienquerer.ui.home

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import com.jujuy.bienquerer.MainActivity
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.api.network.Resource
import com.jujuy.bienquerer.base.BaseFragment
import com.jujuy.bienquerer.databinding.TestConstraintBinding
import com.jujuy.bienquerer.extensions.*
import com.jujuy.bienquerer.model.entity.Category
import com.jujuy.bienquerer.model.entity.Professional
import com.jujuy.bienquerer.model.entity.User
import com.jujuy.bienquerer.viewmodel.category.CategoryViewModel
import com.jujuy.bienquerer.ui.category.list.CategoryActivity
import com.jujuy.bienquerer.ui.home.adapter.CategoryHomeAdapter
import com.jujuy.bienquerer.ui.home.adapter.ReposAdapter
import com.jujuy.bienquerer.ui.home.adapter.SliderImageAdapter
import com.jujuy.bienquerer.ui.notifications.LoadingAdapter
import com.jujuy.bienquerer.utils.*
import com.jujuy.bienquerer.viewmodel.professional.ProfessionalViewModel
import com.jujuy.bienquerer.viewmodel.user.UserViewModel
import com.smarteist.autoimageslider.IndicatorAnimations
import com.smarteist.autoimageslider.SliderAnimations
import com.smarteist.autoimageslider.SliderView
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class HomeFragment : BaseFragment(), ReposAdapter.ListenerClick,
    CategoryHomeAdapter.HomeCategoryClickCallback {
    companion object {
        fun newInstance() = HomeFragment()
    }

    private val itemCategoryViewModel by viewModel<CategoryViewModel>()
    private val professionalViewModel by viewModel<ProfessionalViewModel>()
    private val userViewModel by viewModel<UserViewModel>()
    private var categoryAdapter by autoCleared<CategoryHomeAdapter>()
    private var binding by autoCleared<TestConstraintBinding>()
    private val adapter = ReposAdapter(this)
    private val psDialogMsg: PSDialogMsg by lazy { PSDialogMsg(activity, false) }
    private val socialDialogMsg: PSDialogMsg by lazy { PSDialogMsg(activity, false) }
    private val loading: LoadingMsg by lazy { LoadingMsg(requireActivity(), false) }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = TestConstraintBinding.inflate(inflater, container, false)
        activity?.setSoftInputMode(SOFT_INPUT_ADJUST_PAN)
        setHasOptionsMenu(true)
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root

    }

    override fun initUIAndActions() {
        if (!sessionManager.isLoggedIn()) {
            binding.navDrawer.clearPadding()
            binding.buttonsHome.gone()
        }

        setUpSlider(binding.root)
        binding.textCategoryViewAll.onClick { goToCategoryListActivity() }

        binding.beProfesionalUser.onClick {
            navigationController.navigateToRegisterProfessional(requireActivity())
        }

        binding.beProfessionalSocial.onClick {
            if (!professionalViewModel.isSocial!!) {
                psDialogMsg.showConfirmDialog("¿Sentis el llamado de ayudar?",
                    "Ver más...",
                    getString(R.string.message__cancel_close)
                )
            }

            psDialogMsg.show()

            psDialogMsg.okButton.onClick {
                psDialogMsg.cancel()
                socialDialogMsg.showConfirmDialog("Te vas a convertir en un profesional social que ayuda a las personas que mas lo necesitan sin cobro", getString(R.string.app__ok), getString(R.string.message__cancel_close))
                socialDialogMsg.show()
                socialDialogMsg.okButton.onClick {
                    professionalViewModel.setProfessionalSocial()
                    socialDialogMsg.cancel()
                }
                socialDialogMsg.cancelButton.onClick { socialDialogMsg.cancel() }

                //professionalViewModel.isSocial = !itemViewModel.isSocial!!

            }
            psDialogMsg.cancelButton.onClick {
                psDialogMsg.cancel()
            }
        }
    }

    override fun initAdapters() {
        val categoryAdapter = CategoryHomeAdapter(appExecutors, this)
        this.categoryAdapter = categoryAdapter
        binding.rvCategory.adapter = categoryAdapter


        binding.rvRecentProfessional.adapter = adapter.withLoadStateHeaderAndFooter(
            header = LoadingAdapter { adapter.retry() },
            footer = LoadingAdapter { adapter.retry() }
        )
        adapter.addLoadStateListener { loadState ->
            // Only show the list if refresh succeeds.
            //binding.swipeRefresh.isRefreshing = false
            //binding.notificationList.isVisible = loadState.source.refresh is LoadState.NotLoading
            // Show loading spinner during initial load or refresh.
            //binding.loadMoreBar.isVisible = loadState.source.refresh is LoadState.Loading
            // Show the retry state if initial load or refresh fails.
            //binding.retryButton.isVisible = loadState.source.refresh is LoadState.Error

            // Toast on any error, regardless of whether it came from RemoteMediator or PagingSource
            val errorState = loadState.source.append as? LoadState.Error
                ?: loadState.source.prepend as? LoadState.Error
                ?: loadState.append as? LoadState.Error
                ?: loadState.prepend as? LoadState.Error
            errorState?.let {
                Toast.makeText(
                    requireContext(),
                    "\uD83D\uDE28 Wooops ${it.error}",
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }

    override fun initData() {
        loadUser()
        loadCategories()
        loadProfessionals()
        getDataProfessionalUser()
        observeSocialProfessional()
    }

    private fun loadUser() {
        userViewModel.getUserData()?.observe(this, { listResource ->
            when (listResource.status) {
                Resource.Status.LOADING -> {
                }
                Resource.Status.SUCCESS -> {
                    if (listResource.data != null) {
                        val user = listResource.data
                        saveToLocalData(user)
                        Timber.e("HOME: $user")
                        checkForButtons(user)
                    }
                }
                Resource.Status.ERROR -> {
                }
            }
        })
    }

    private fun checkForButtons(user: User) {
        if (user.phoneVerified == false && (user.person?.social == false || user.person?.social == null)) {
            with(binding) {
                buttonsHome.visible()
                beProfessionalSocial.visible()
                beProfesionalUser.visible()
            }
        } else {
            binding.buttonsHome.gone()
        }
        if (user.phoneVerified == true && (user.person?.social == false || user.person?.social == null)) {
            with(binding) {
                beProfessionalSocial.visible()
                beProfesionalUser.gone()
                buttonsHome.visible()
            }
        }
        if (user.phoneVerified == false && user.person?.social == true) {
            with(binding) {
                beProfessionalSocial.gone()
                beProfesionalUser.visible()
                buttonsHome.visible()
            }
        }
        user.phoneVerified?.let { (activity as MainActivity).setCalendarItemVisibility(it) }
    }

    private fun loadCategories() {
        itemCategoryViewModel.setCategoryListObj("10", Constants.ZERO)
        itemCategoryViewModel.getCategoryListData()?.observe(this, Observer { listResource ->
            when (listResource.status) {
                Resource.Status.LOADING -> {
                    if (listResource.data != null) {
                        if (listResource.data.isNotEmpty()) {
                            replaceCategory(listResource.data)
                        }
                    }
                }
                Resource.Status.SUCCESS -> {
                    if (listResource.data != null) {
                        if (listResource.data.isNotEmpty()) {
                            replaceCategory(listResource.data)
                        }
                    }
                    itemCategoryViewModel.setLoadingState(false)

                }
                Resource.Status.ERROR -> {
                    itemCategoryViewModel.setLoadingState(false)
                }
            }
        })
    }

    private fun replaceCategory(categories: List<Category>) {
        categoryAdapter.submitList(categories)
        binding.executePendingBindings()
    }

    private fun getListProfessionals() {
        lifecycleScope.launch {
            professionalViewModel.getListProfessionals(professionalViewModel.holder).collectLatest {
                adapter.submitData(it)
            }
        }
    }

    private fun loadProfessionals() {
        getListProfessionals()
    }

    private fun getDataProfessionalUser() {
        professionalViewModel.setProfessionalListDetailObj(loginUserId)
        professionalViewModel.getProfessionalListDetailData()
            ?.observe(this, Observer { professional ->
                when (professional.status) {
                    Resource.Status.LOADING -> {
                    }
                    Resource.Status.SUCCESS -> {
                        Timber.e("DATA PROFESSIONAL " + professional.toString())
                        sessionManager.setIsProfessional(professional.data?.social ?: false)
                        professionalViewModel.isSocial = professional.data?.social ?: false
                        professional.data?.social?.let {
                            if (it) binding.beProfessionalSocial.gone() else binding.beProfessionalSocial.visible()
                        }
                    }
                    Resource.Status.ERROR -> {
                    }
                }
            })
    }

    private fun observeSocialProfessional() {
        professionalViewModel.getProfessionalSocial()?.observe(this, Observer { response ->
            when (response.status) {
                Resource.Status.LOADING -> {
                }
                Resource.Status.SUCCESS -> {
                    response.data?.let {
                        if (it.code == 200) {
                            binding.beProfessionalSocial.gone()
                            Utils.showSnackBarSuccess(
                                requireActivity(),
                                "Ahora eres un profesional social"
                            )
                            Timber.e("Social " + it.message)
                        }
                    }
                }
                Resource.Status.ERROR -> {
                    Utils.showSnackBarError(requireActivity(), "Error! No se pudo actualizar!")
                    Timber.e("ERROR " + response.message)
                }
            }
        })
    }

    override fun onResume() {
        super.onResume()
        loadUser()
    }

    private fun saveToLocalData(user: User) {
        Timber.e("user: $user")
        user.userCode?.let { it -> sessionManager.saveUserCode(it) }
        Utils.updateUserLoginData(pref, user)
    }

    private fun goToCategoryListActivity() {
        requireActivity().launchActivity<CategoryActivity>()
    }

    private fun setUpSlider(root: View) {
        val slider = root.findViewById(R.id.imageSlider) as SliderView
        val sliderItem = DataImgSlider.createDataSet()
        val imageAdapter = SliderImageAdapter(sliderItem, root.context)
        slider.apply {
            setSliderAdapter(imageAdapter)
            setIndicatorAnimation(IndicatorAnimations.WORM); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
            setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
            autoCycleDirection = SliderView.AUTO_CYCLE_DIRECTION_RIGHT;
            indicatorSelectedColor = Color.WHITE;
            indicatorUnselectedColor = Color.GRAY;
            scrollTimeInSec = 3;
            startAutoCycle()
        }
    }

    override fun onItemClick(prof: Professional) {
        navigationController.navigateToItemDetailActivity(
            requireActivity(),
            prof.id.toString(),
            prof.name.toString()
        )
    }

    override fun onClick(category: Category?) {
        navigationController.navigateToSubCategoryActivity(
            requireActivity(),
            category?.idCategory,
            category?.nameCategory
        )
    }
}
