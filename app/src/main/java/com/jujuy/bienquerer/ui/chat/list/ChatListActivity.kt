package com.jujuy.bienquerer.ui.chat.list

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.base.BaseActivity
import com.jujuy.bienquerer.databinding.ActivityChatListBinding
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlin.time.ExperimentalTime

@ExperimentalTime
@ExperimentalCoroutinesApi
class ChatListActivity : BaseActivity() {
    lateinit var binding: ActivityChatListBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_chat_list)
        initUI(binding)
    }

    private fun initUI(binding: ActivityChatListBinding) {
        setupFragment(ChatListFragment())
    }

    fun changeText(text: String?) {
        initToolbar(binding.toolbar, text!!)
    }
}