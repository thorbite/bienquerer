package com.jujuy.bienquerer.ui.beProfessional

import android.util.TypedValue
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jujuy.bienquerer.model.MySpinnerItem
import com.skydoves.powerspinner.OnSpinnerItemSelectedListener
import com.skydoves.powerspinner.PowerSpinnerInterface
import com.skydoves.powerspinner.PowerSpinnerView
import com.skydoves.powerspinner.databinding.ItemDefaultBinding

class SocialMediaAdapter(powerSpinnerView: PowerSpinnerView) : RecyclerView.Adapter<SocialMediaAdapter.MySpinnerViewHolder>(),
    PowerSpinnerInterface<MySpinnerItem> {
    override val spinnerView: PowerSpinnerView = powerSpinnerView
    override var onSpinnerItemSelectedListener: OnSpinnerItemSelectedListener<MySpinnerItem>? = null

    private val compoundPadding: Int = 12
    private val spinnerItems: MutableList<MySpinnerItem> = arrayListOf()

    init {
        this.spinnerView.compoundDrawablePadding = compoundPadding
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MySpinnerViewHolder {
        val binding = ItemDefaultBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MySpinnerViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MySpinnerViewHolder, position: Int) {
        val item = this.spinnerItems[position]
        holder.bind(item, spinnerView)
        holder.itemView.setOnClickListener { notifyItemSelected(position) }
    }

    override fun setItems(itemList: List<MySpinnerItem>) {
        this.spinnerItems.clear()
        this.spinnerItems.addAll(itemList)
        notifyDataSetChanged()
    }

    override fun notifyItemSelected(index: Int) {
        this.spinnerView.setCompoundDrawablesWithIntrinsicBounds(this.spinnerItems[index].icon, null,
            spinnerView.arrowDrawable, null)
        this.spinnerView.notifyItemSelected(index, this.spinnerItems[index].text)
        this.onSpinnerItemSelectedListener?.onItemSelected(index, this.spinnerItems[index])
    }

    override fun getItemCount() = this.spinnerItems.size

    class MySpinnerViewHolder(private val binding: ItemDefaultBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: MySpinnerItem, spinnerView: PowerSpinnerView) {
            itemView.apply {
                binding.itemDefaultText.apply {
                    text = item.text
                    typeface = spinnerView.typeface
                    gravity = spinnerView.gravity
                    setTextSize(TypedValue.COMPLEX_UNIT_PX, spinnerView.textSize)
                    setTextColor(spinnerView.currentTextColor)
                    compoundDrawablePadding = spinnerView.compoundDrawablePadding
                    setCompoundDrawablesWithIntrinsicBounds(item.icon, null, null, null)
                }
                setPadding(spinnerView.paddingLeft, spinnerView.paddingTop, spinnerView.paddingRight,
                    spinnerView.paddingBottom)
            }
        }
    }
}
