package com.jujuy.bienquerer.ui.professionals.map

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.*
import androidx.databinding.DataBindingUtil
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.base.BaseFragment
import com.jujuy.bienquerer.databinding.FragmentPickMapBinding
import com.jujuy.bienquerer.utils.AutoClearedValue
import com.jujuy.bienquerer.utils.Constants
import com.skydoves.whatif.whatIfNotNull
import kotlinx.coroutines.ExperimentalCoroutinesApi
import timber.log.Timber
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class PickMapFragment : BaseFragment() {
    companion object {
        fun newInstance() = PickMapFragment()
    }
    private lateinit var binding: AutoClearedValue<FragmentPickMapBinding>
    private var map : GoogleMap? = null
    private var markerOptions = MarkerOptions()
    var bundle: Bundle? = null

    private var latValue: String? = "48.856452647178386"
    private var lngValue: String? = "2.3523519560694695"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val dataBinding: FragmentPickMapBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_pick_map, container, false
        )
        binding = AutoClearedValue(this, dataBinding)
        activity.whatIfNotNull {
            activity?.window?.setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN
            )
        }
        setHasOptionsMenu(true)
        binding.get().lifecycleOwner = viewLifecycleOwner
        return binding.get().root
    }

    private fun initializeMap(savedInstanceState: Bundle?) {
        try {
            if (this.activity != null) {
                MapsInitializer.initialize(this.activity)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        binding.get().mapView.onCreate(savedInstanceState)
        binding.get().mapView.onResume()
        binding.get().mapView.getMapAsync { googleMap ->
            map = googleMap
            map?.addMarker(
                MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromBitmap(generateSmallIcon(requireContext())))
                    .position(LatLng(latValue!!.toDouble(), lngValue!!.toDouble()))
                    .title("City Name")
            )

            //zoom
            if (!latValue!!.isEmpty() && !lngValue!!.isEmpty()) {
                val zoomlevel = 15
                // Animating to the touched position
                map?.animateCamera(
                    CameraUpdateFactory.newLatLngZoom(
                        LatLng(
                            latValue!!.toDouble(),
                            lngValue!!.toDouble()
                        ), zoomlevel.toFloat()
                    )
                )
            }
            map?.setOnMapClickListener(GoogleMap.OnMapClickListener { latLng: LatLng ->
                // Creating a marker
                // Setting the position for the marker
                map?.clear()
                markerOptions.position(latLng)
                markerOptions.icon(BitmapDescriptorFactory.fromBitmap(generateSmallIcon(requireContext())))

                // Setting the title for the marker.
                // This will be displayed on taping the marker
                markerOptions.title(latLng.latitude.toString() + " : " + latLng.longitude)
                markerOptions.draggable(true)
                latValue = latLng.latitude.toString()
                lngValue = latLng.longitude.toString()

                // Clears the previously touched position
                map?.clear()

                // Animating to the touched position
                map?.animateCamera(CameraUpdateFactory.newLatLng(latLng))

                // Placing a marker
                // the touched position
                map?.addMarker(markerOptions)
            })
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_pick_location, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.pickButton) {
            Timber.e("I am here for ok Button")
            navigationController.navigateBackFromMapView(requireActivity(), latValue, lngValue)
            if (activity != null) {
                requireActivity().finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun initUIAndActions() {
        initializeMap(bundle)

    }

    override fun initAdapters() {
    }

    override fun initData() {
        Timber.d("LATITUD ${requireActivity().intent.getStringExtra(Constants.LAT)}")
        Timber.d("LONGITUD ${requireActivity().intent.getStringExtra(Constants.LNG)}")
        if (activity != null) {
            latValue = requireActivity().intent.getStringExtra(Constants.LAT)
            lngValue = requireActivity().intent.getStringExtra(Constants.LNG)
        }
    }
    private fun generateSmallIcon(context: Context): Bitmap {
        val height = 100
        val width = 100
        val bitmap = BitmapFactory.decodeResource(context.resources, R.drawable.finger_marker)
        return Bitmap.createScaledBitmap(bitmap, width, height, false)
    }



}