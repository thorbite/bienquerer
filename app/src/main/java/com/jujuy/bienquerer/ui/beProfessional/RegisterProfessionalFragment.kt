package com.jujuy.bienquerer.ui.beProfessional

import android.content.Context
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.AdapterView
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import bov.vitali.smsinterceptor.OnMessageListener
import bov.vitali.smsinterceptor.SmsInterceptor
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.android.flexbox.FlexboxLayout
import com.google.android.material.chip.Chip
import com.jujuy.bienquerer.MainActivity
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.api.network.Resource
import com.jujuy.bienquerer.base.BaseFragment
import com.jujuy.bienquerer.databinding.FragmentRegisterProfessionalBinding
import com.jujuy.bienquerer.model.MySpinnerItem
import com.jujuy.bienquerer.model.RegisterProfessionalRequest
import com.jujuy.bienquerer.model.entity.*
import com.jujuy.bienquerer.ui.profile.adapter.AutoCompleteProfessionAdapter
import com.jujuy.bienquerer.utils.*
import com.jujuy.bienquerer.viewmodel.professional.ProfessionalViewModel
import com.jujuy.bienquerer.viewmodel.subcategory.SubCategoryViewModel
import com.jujuy.bienquerer.viewmodel.user.UserViewModel
import com.sucho.placepicker.AddressData
import com.sucho.placepicker.MapType
import com.sucho.placepicker.PlacePicker
import kotlinx.android.synthetic.main.fragment_register_professional.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import java.io.File
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class RegisterProfessionalFragment : BaseFragment(), OnMessageListener {

    private val userViewModel by viewModel<UserViewModel>()
    private val socialMediaViewModel by sharedViewModel<SocialMediaViewModel>()
    private val itemViewModel by viewModel<ProfessionalViewModel>()
    private val professionalViewModel by viewModel<SubCategoryViewModel>()
    private val phoneBalloon by lazy {
        BalloonUtils.getPhoneBalloon(
            requireContext(),
            this,
            "Se enviara un sms a este numero para verificar tu identidad"
        )
    }
    private val descriptionBalloon by lazy {
        BalloonUtils.getPhoneBalloon(
            requireContext(),
            this,
            "Escribe aqui tu informacion adicional"
        )
    }
    private val socialMediaBalloon by lazy {
        BalloonUtils.getPhoneBalloon(
            requireContext(),
            this,
            "Agrega aqui tus redes sociales"
        )
    }
    private val professionsBalloon by lazy {
        BalloonUtils.getPhoneBalloon(
            requireContext(),
            this,
            "Agrega al menos una profesion"
        )
    }
    private val addressBalloon by lazy {
        BalloonUtils.getPhoneBalloon(
            requireContext(),
            this,
            "Elige tu ubicacion"
        )
    }
    private val psDialogMsg: PSDialogMsg by lazy { PSDialogMsg(activity, false) }
    private var binding by autoCleared<FragmentRegisterProfessionalBinding>()
    private val loadingMsg by lazy { LoadingMsg(requireActivity(), false) }
    var phoneNumber: String? = null
    var phone: String = ""
    var userId: String? = null
    var smsInterceptor: SmsInterceptor? = null
    var professionList: List<SubCategory> = ArrayList()
    lateinit var adapter: AutoCompleteProfessionAdapter
    var showTooltipPhone: Boolean = true
    var showTooltipDescription: Boolean = true
    var showTooltipSocialMedia: Boolean = true
    var showTooltipProfessions: Boolean = true
    var showTooltipAddress: Boolean = true

    val listSocialMedia: MutableList<Social_network> = ArrayList()
    val listProfessions: MutableList<Int> = ArrayList()
    var dataRegisterToSend: RegisterProfessionalRequest? = null

    companion object {
        fun newInstance() = RegisterProfessionalFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentRegisterProfessionalBinding.inflate(inflater, container, false)
        activity?.let {
            it.window?.setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN
            )
        }
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }


    override fun initUIAndActions() {

        socialMediaViewModel.selected.observe(this, Observer { itemSpinner ->
            Timber.e("DESDE EL OBSERVER....")
            if (itemSpinner != null) {
                addNewChip(itemSpinner, chip_group)
                listSocialMedia.add(Social_network(itemSpinner.id.toInt(), itemSpinner.url!!))
            }
        })

        binding.addSocialMediaBtn.setOnClickListener {
            if (showTooltipSocialMedia) {
                socialMediaBalloon.showAlignTop(it)
                showTooltipSocialMedia = false
            } else {
                socialMediaBalloon.dismiss()
                childFragmentManager.let { it1 ->
                    val dialogInstance = SocialMediaFragment()
                    dialogInstance.show(it1, "SocialMediaFragment")
                }
            }

        }

        binding.profileImageView.setOnClickListener {
            if (connectivity.isConnected) {
                try {
                    if (Utils.isStoragePermissionGranted(requireActivity())) {
                        ImagePicker.with(this)
                            .cropSquare()
                            .compress(1024)
                            .galleryMimeTypes(
                                mimeTypes = arrayOf(
                                    "image/png",
                                    "image/jpg",
                                    "image/jpeg"
                                )
                            ).start(Constants.RESULT_LOAD_IMAGE_PROFILE)
                        //val i = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                        //startActivityForResult(i, Constants.RESULT_LOAD_IMAGE)
                    }
                } catch (e: Exception) {
                    Timber.e("ERROR $e")
                }
            } else {
                psDialogMsg.showWarningDialog(
                    getString(R.string.no_internet_error),
                    getString(R.string.app__ok)
                )
                psDialogMsg.show()
            }
        }

        binding.userCoverBlurImageView.setOnClickListener {
            try {
                if (Utils.isStoragePermissionGranted(requireActivity())) {
                    ImagePicker.with(this)
                        .crop(16f, 9f)                   //Crop image(Optional), Check Customization for more option
                        .compress(1024)            //Final image size will be less than 1 MB(Optional)
                        .galleryMimeTypes(  //Exclude gif images
                            mimeTypes = arrayOf(
                                "image/png",
                                "image/jpg",
                                "image/jpeg"
                            )
                        )           //Final image size will be less than 1 MB(Optional)
                        //Final image resolution will be less than 1080 x 1080(Optional)
                        .start(Constants.RESULT_LOAD_IMAGE_BANNER)
                    //val i = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                    //startActivityForResult(i, Constants.RESULT_LOAD_IMAGE)
                }
            } catch (e: Exception) {
                Timber.e("ERROR $e")
            }

        }

        binding.saveButton.setOnClickListener {
            val description = descriptionProfessionalET.text.toString()
            val code = binding.enterCodeET.otp

            if (itemViewModel.address.isEmpty()) {
                binding.addressEditET.error = "Debe asignar una direccion"
                binding.addressEditET.requestFocus()
                Toast.makeText(requireContext(), "Debe asignar una direccion", Toast.LENGTH_SHORT)
                    .show()
            } else if (description.isEmpty()) {
                binding.descriptionProfessionalET.error = "Debe escribir una description"
                binding.descriptionProfessionalET.requestFocus()
                Toast.makeText(
                    requireContext(),
                    "Debe escribir una description",
                    Toast.LENGTH_SHORT
                ).show()
            } else if (itemViewModel.profileImagePath == Constants.EMPTY_STRING){
                Toast.makeText(
                    requireContext(),
                    "Debe elegir una foto de perfil",
                    Toast.LENGTH_SHORT
                ).show()
            } else if (listProfessions.isEmpty()) {
                Toast.makeText(
                    requireContext(),
                    "Debe asignar al menos una profesion",
                    Toast.LENGTH_SHORT
                ).show()
            } else if (listSocialMedia.isEmpty()) {
                Toast.makeText(
                    requireContext(),
                    "Debe asignar al menos una red social",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                itemViewModel.setLoadingState(true)
                dataRegisterToSend = RegisterProfessionalRequest(
                    itemViewModel.address,
                    itemViewModel.latValue, itemViewModel.lngValue, phoneNumber, description,
                    code, listProfessions.toIntArray(), listSocialMedia.toList()
                )

                itemViewModel.setRegisterProfessional(
                    itemViewModel.profileImagePath,
                    itemViewModel.bannerImagePath,
                    dataRegisterToSend!!
                )
            }
        }

        binding.sendSmsButton.setOnClickListener {
            phone = binding.codeEditText.text.toString().trim() + binding.numberEditText.text.toString().trim()
            if (phone.isNotEmpty()) {
                validateNumberPhone(phone)
            }
        }

        binding.tooltipPhone.setOnClickListener {
            phoneBalloon.showAlignTop(it)
        }

        binding.tooltipSocial.setOnClickListener {
            socialMediaBalloon.showAlignTop(it)
        }

        binding.tooltipDescription.setOnClickListener {
           descriptionBalloon.showAlignTop(it)
        }

        binding.tooltipAddress.setOnClickListener {
            addressBalloon.showAlignTop(it)
        }

        binding.tooltipProfessions.setOnClickListener {
            professionsBalloon.showAlignTop(it)
        }


        binding.addressEditET.setOnClickListener {
                val intent = PlacePicker.IntentBuilder()
                    .setLatLong(
                        selectedLat.toDouble(),
                        selectedLng.toDouble()
                    )  // Initial Latitude and Longitude the Map will load into
                    .showLatLong(true)  // Show Coordinates in the Activity
                    .setMapZoom(12.0f)  // Map Zoom Level. Default: 14.0
                    .setAddressRequired(true) // Set If return only Coordinates if cannot fetch Address for the coordinates. Default: True
                    .setMarkerDrawable(R.drawable.ic_marker_finger3)
                    .setFabColor(R.color.global__primary)
                    //Set Map Style (https://mapstyle.withgoogle.com/)
                    .setMapType(MapType.NORMAL)
                    .setPlaceSearchBar(
                        true,
                        getString(R.string.google_map_api_key)
                    ) //Activate GooglePlace Search Bar. Default is false/not activated. SearchBar is a chargeable feature by Google
                    //.onlyCoordinates(true)  //Get only Coordinates from Place Picker
                    .build(requireActivity())
                startActivityForResult(intent, Constants.PLACE_PICKER_REQUEST)

        }
    }


    private fun setupSearchAdapter(data: List<SubCategory>?) {
        professionList = data!!
        Timber.e("LISTA DE PROFESSIONES $professionList")
        adapter =
            AutoCompleteProfessionAdapter(
                requireContext(),
                professionList
            )
        binding.professionsInputET.setAdapter(adapter)

        binding.professionsInputET.onItemClickListener =
            AdapterView.OnItemClickListener { adapterView, view, i, p3 ->
                val profession: SubCategory = adapterView.getItemAtPosition(i) as SubCategory
                addNewProfessionChip(profession, chip_group_professions)
                binding.professionsInputET.setText("")
                listProfessions.add(profession.idSubCategory)
                Timber.e(listProfessions.toString())
            }
    }


    override fun initAdapters() {

    }

    override fun initData() {
        loadUser()

        userViewModel.getUpdateUserData()?.observe(this, Observer { listResource ->
            when (listResource.status) {
                Resource.Status.LOADING -> {
                    loadingMsg.showloaderDialog()
                    loadingMsg.show()
                }
                Resource.Status.SUCCESS -> {
                    loadingMsg.cancel()
                    if (listResource.data?.code == 200) {
                        //validateNumberPhone()
                        userViewModel.setPhoneLoginUser(userId)
                        loadUser()
                    } else {
                        Timber.e(userViewModel.isLoading.toString())
                        psDialogMsg.showErrorDialog(
                            listResource.data?.message,
                            getString(R.string.message_ok_close)
                        )
                        psDialogMsg.show()
                    }

                }
                Resource.Status.ERROR -> {
                    loadingMsg.cancel()
                    psDialogMsg.showErrorDialog(
                        listResource.message,
                        getString(R.string.app__ok)
                    )
                    psDialogMsg.show()
                }
            }
        })

        userViewModel.getPhoneLoginData()?.observe(this, Observer { listResources ->
            if (listResources != null) {
                when (listResources.status) {
                    Resource.Status.LOADING -> {
                        loadingMsg.showloaderDialog()
                        loadingMsg.show()
                    }
                    Resource.Status.SUCCESS -> {
                        loadingMsg.cancel()
                        if (listResources.data?.statusCode == 200) {
                            Toast.makeText(context, listResources.data.message, Toast.LENGTH_LONG)
                                .show()
                            Timber.d(listResources.data.message)
                        }
                    }
                    Resource.Status.ERROR -> {
                        loadingMsg.cancel()
                        Toast.makeText(context, listResources.data?.message, Toast.LENGTH_LONG)
                            .show()
                    }
                }
            }
        })

        userViewModel.getUserPhoneData()?.observe(this, Observer { listResources ->
            when (listResources.status) {
                Resource.Status.LOADING -> {
                    loadingMsg.showloaderDialog()
                    loadingMsg.show()
                }
                Resource.Status.SUCCESS -> {
                    loadingMsg.cancel()
                    if (listResources.data?.code == 200) {
                        Toast.makeText(context, listResources.data.message, Toast.LENGTH_LONG)
                            .show()
                        Timber.d(listResources.data.message)
                    } else {
                        psDialogMsg.showErrorDialog(listResources.data?.message, getString(R.string.app__ok))
                        psDialogMsg.show()
                    }
                }
                Resource.Status.ERROR -> {
                    loadingMsg.cancel()
                    psDialogMsg.showErrorDialog(listResources.message, getString(R.string.app__ok))
                    psDialogMsg.show()
                }
            }
        })

        //professionalViewModel.setProfessionListObj("")
        professionalViewModel.getProfessionListData()?.observe(this, Observer { listResources ->
            when (listResources.status) {
                Resource.Status.LOADING -> {
                }
                Resource.Status.SUCCESS -> {
                    if (listResources.data != null) {
                        Timber.e("DATOS: " + listResources.data.toString())
                        setupSearchAdapter(listResources.data)
                    }
                }
                Resource.Status.ERROR -> {
                }
            }
        })

        itemViewModel.getRegisterProfessional()?.observe(this, Observer { listResources ->
            when (listResources.status) {
                Resource.Status.LOADING -> {
                    loadingMsg.showloaderDialog()
                    loadingMsg.show()
                }
                Resource.Status.SUCCESS -> {
                    loadingMsg.cancel()
                    if (listResources.data?.code == 200) {
                        psDialogMsg.showSuccessDialog(
                            "Felicitaciones ahora eres un profesional y puedes brindar servicios",
                            getString(
                                R.string.app__ok
                            )
                        )
                        psDialogMsg.okButton.setOnClickListener { requireActivity().finish() }
                        psDialogMsg.show()
                    }else{
                        psDialogMsg.showErrorDialog(listResources.data?.message!!, getString(R.string.message_ok_close))
                        psDialogMsg.show()
                    }
                }
                Resource.Status.ERROR -> {
                    loadingMsg.cancel()
                    psDialogMsg.showErrorDialog(
                        listResources.message!!,
                        getString(R.string.message_ok_close)
                    )
                    psDialogMsg.show()
                }
            }
        })

    }

    private fun addNewChip(item: MySpinnerItem, chipGroup: FlexboxLayout) {
        val chip = Chip(chipGroup.context)
        chip.text = item.text
        //chip.chipIcon = ContextCompat.getDrawable(requireContext(), R.mipmap.ic_launcher)
        chip.isCloseIconVisible = false
        chip.isClickable = true
        chip.isCheckable = false
        chip.chipBackgroundColor = ColorStateList.valueOf(
            ContextCompat.getColor(
                requireActivity(),
                R.color.global__primary
            )
        )
        chip.setTextColor(ContextCompat.getColor(requireActivity(), R.color.white))
        chipGroup.addView(chip as View, chipGroup.childCount - 1)
        /*chip.setOnCloseIconClickListener {
            chipGroup.removeView(chip as View)
            //listSocialMedia.removeAt(item.id.toInt())
            //deleteProfession(person.idSubCategory.toString())
        }*/
    }

    private fun addNewProfessionChip(person: SubCategory, chipGroup: FlexboxLayout) {
        val chip = Chip(chipGroup.context)
        chip.text = person.nameSubCategory
        chip.chipIcon = ContextCompat.getDrawable(requireContext(), R.mipmap.ic_launcher_round)
        chip.isCloseIconVisible = false
        chip.isClickable = true
        chip.isCheckable = false
        chip.chipBackgroundColor = ColorStateList.valueOf(
            ContextCompat.getColor(
                requireActivity(),
                R.color.global__primary
            )
        )
        chip.setTextColor(ContextCompat.getColor(requireActivity(), R.color.white))
        chipGroup.addView(chip as View, chipGroup.childCount - 1)
        /*chip.setOnCloseIconClickListener {
            chipGroup.removeView(chip as View)
            //listSocialMedia.removeAt(item.id.toInt())
            //deleteProfession(person.idSubCategory.toString())
        }*/
    }

    private fun contextDrawable(@DrawableRes resource: Int): Drawable? {
        return ContextCompat.getDrawable(requireContext(), resource)
    }

    private fun loadUser() {
        userViewModel.setUserObj(loginUserId)
        userViewModel.getUserData()?.observe(this, Observer { listResource ->
            when (listResource.status) {
                Resource.Status.LOADING -> {
                }
                Resource.Status.SUCCESS -> {
                    if (listResource.data != null) {
                        Timber.e(listResource.data.toString())
                        val user = listResource.data
                        binding.user = user
                        userViewModel.user = user
                        phoneNumber = listResource.data.phone
                        binding.codeEditText.setText(phoneNumber.toString())
                        // TODO() cambiar esto cuando se tenga separados los strings de telefono
                        binding.numberEditText.setText(phoneNumber.toString())
                        sessionManager.addUserLoginData(user)
                    }
                }
                Resource.Status.ERROR -> {
                }
            }

        })
    }

    fun updateDataUser(phone: String) {
        if (!connectivity.isConnected) {
            psDialogMsg.showWarningDialog(
                getString(R.string.no_internet_error),
                getString(R.string.app__ok)
            )
            psDialogMsg.show()
            return
        }

        userViewModel.setUserPhoneObj(phone)
    }

    private fun validateNumberPhone(phone: String) {
        if (phone.isNotEmpty()) {
            psDialogMsg.showConfirmDialog(
                "Para convertirte en profesional se enviará un sms al numero $phone para confimar tu identidad",
                getString(R.string.app_ok),
                getString(R.string.message__cancel_close)
            )
            psDialogMsg.show()

            psDialogMsg.okButton.setOnClickListener {
                updateDataUser(phone)
                //userViewModel.setPhoneLoginUser(userId)
                psDialogMsg.cancel()
            }
            psDialogMsg.cancelButton.setOnClickListener {
                psDialogMsg.cancel()
            }

        } else {
            psDialogMsg.showErrorDialog(
                "Ingresa un telefono!",
                getString(R.string.app_ok)
            )
            psDialogMsg.show()

            psDialogMsg.okButton.setOnClickListener {
                //userViewModel.setDeleteUserObj(loginUserId)
                psDialogMsg.cancel()
            }
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        try {
            if (requestCode == Constants.RESULT_LOAD_IMAGE_PROFILE && resultCode == Constants.RESULT_OK) {
                //Image Uri will not be null for RESULT_OK
                val fileUri = data?.data
                binding.profileImageView.setImageURI(fileUri)

                //You can get File object from intent
                val file: File? = ImagePicker.getFile(data)

                //You can also get File Path from intent
                val filePath: String? = ImagePicker.getFilePath(data)
                itemViewModel.profileImagePath = filePath.toString()
            } /*else if (resultCode == ImagePicker.RESULT_ERROR) {
                Toast.makeText(activity, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(activity, "Task Cancelled", Toast.LENGTH_SHORT).show()
            }*/
            if (requestCode == Constants.RESULT_LOAD_IMAGE_BANNER && resultCode == Constants.RESULT_OK) {
                val fileUri = data?.data
                binding.userCoverBlurImageView.setImageURI(fileUri)
                val file: File? = ImagePicker.getFile(data)
                val filePath: String? = ImagePicker.getFilePath(data)
                itemViewModel.bannerImagePath = filePath.toString()
            }

            if (requestCode == Constants.PLACE_PICKER_REQUEST) {
                if (resultCode == Constants.RESULT_OK) {
                    val addressData =
                        data?.getParcelableExtra<AddressData>(Constants.ADDRESS_INTENT)
                    itemViewModel.latValue = addressData?.latitude.toString()
                    itemViewModel.lngValue = addressData?.longitude.toString()
                    itemViewModel.address =
                        addressData?.addressList?.get(0)?.getAddressLine(0) ?: "Desconocido"

                    Timber.d("LATITUD ${itemViewModel.latValue}")
                    Timber.d("LONGITUD ${itemViewModel.lngValue}")
                    Timber.e(
                        "ADDRESS ${
                            addressData?.addressList?.get(0)?.getAddressLine(0) ?: "Desconocido"
                        }"
                    )

                    binding.addressEditET.text = itemViewModel.address
                }
            }

        } catch (e: Exception) {
            Timber.e("ERROR $e")
        }
    }

    private fun uploadImage() {
        userViewModel.setLoadingState(true)
        userViewModel.uploadImage(userViewModel.profileImagePath)
            .observe(this, Observer { listResource ->
                if (listResource?.data != null) {
                    Timber.d(listResource.data.toString())
                    userViewModel.setLoadingState(false)
                    Timber.d("Got Data" + listResource.message + listResource.toString())
                    if (listResource.message != null && listResource.message != "") {

                        //initData()
                    } else {
                        // Update the data
                        //initData()

                    }
                } else if (listResource?.message != null) {
                    Timber.d("Message from server.")
                    psDialogMsg.showInfoDialog(listResource.message, getString(R.string.app__ok))
                    psDialogMsg.show()
                    userViewModel.setLoadingState(false)
                } else {
                    userViewModel.setLoadingState(false)
                    Timber.e("Empty Data")
                }
            })
    }

    override fun messageReceived(message: String?) {
        val numsStr = message?.replace("[^0-9]".toRegex(), "")
        binding.enterCodeET.otp = numsStr!!
    }

    override fun onStart() {
        super.onStart()
        smsInterceptor = SmsInterceptor(requireActivity(), this)
    }

    override fun onResume() {
        super.onResume()
        smsInterceptor?.register()
    }

    override fun onPause() {
        super.onPause()
        smsInterceptor?.unregister()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        smsInterceptor?.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                psDialogMsg.showConfirmDialog("¿Estas seguro de cancelar el registro?", getString(R.string.app__ok), getString(R.string.app__cancel))
                psDialogMsg.okButton.setOnClickListener { requireActivity().finish() }
                psDialogMsg.cancelButton.setOnClickListener { psDialogMsg.cancel() }
                psDialogMsg.show()
            }
        })
    }

}