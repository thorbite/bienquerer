package com.jujuy.bienquerer.ui.category.categoryfilter

import android.os.Bundle
import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.api.network.Resource
import com.jujuy.bienquerer.base.BaseFragment
import com.jujuy.bienquerer.databinding.FragmentCategoryFilterBinding
import com.jujuy.bienquerer.model.entity.Category
import com.jujuy.bienquerer.model.entity.SubCategory
import com.jujuy.bienquerer.viewmodel.category.CategoryViewModel
import com.jujuy.bienquerer.ui.category.adapter.CategoryFilterAdapter
import com.jujuy.bienquerer.ui.common.DataBoundListAdapter
import com.jujuy.bienquerer.viewmodel.subcategory.SubCategoryViewModel
import com.jujuy.bienquerer.utils.AutoClearedValue
import com.jujuy.bienquerer.utils.Constants
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class CategoryFilterFragment : BaseFragment(), DataBoundListAdapter.DiffUtilDispatchedInterface {
    companion object {
        fun newInstance() = CategoryFilterFragment()
    }

    private val itemCategoryViewModel by viewModel<CategoryViewModel>()
    private val itemSubCategoryViewModel by viewModel<SubCategoryViewModel>()
    lateinit var binding: AutoClearedValue<FragmentCategoryFilterBinding>
    lateinit var adapter: AutoClearedValue<CategoryFilterAdapter>
    private var lastCategoryData: AutoClearedValue<List<Category>>? = null
    var catId: String? = ""
    var subCatId: String? = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val dataBinding: FragmentCategoryFilterBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_category_filter, container, false)
        binding = AutoClearedValue(this, dataBinding)
        binding.get().loadingMore = connectivity.isConnected
        binding.get().lifecycleOwner = viewLifecycleOwner
        setHasOptionsMenu(true)
        return binding.get().root
    }

    override fun initUIAndActions() {}

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.clear_button, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.clear) {
            catId = ""
            subCatId = ""
            initializeAdapter()
            initData()
            if (activity != null) {
                navigationController.navigateBackToHomeFeaturedFragment(
                    requireActivity(),
                    catId,
                    subCatId, null
                )
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun initAdapters() {
        try {
            if (activity != null) {
                catId = requireActivity().intent.getStringExtra(Constants.CATEGORY_ID)
                subCatId = requireActivity().intent.getStringExtra(Constants.SUBCATEGORY_ID)
            }
        } catch (e: Exception) {
            Timber.e("ERROR $e")
        }
        initializeAdapter()
    }

    private fun initializeAdapter() {
        val nvAdapter = CategoryFilterAdapter(object : CategoryFilterAdapter.FilteringClickCallback{
            override fun onClick(catId: String?, subCatId: String?, nameSubcategory: String?) {
                assignCategoryId(catId, subCatId)
                if (activity != null){
                    navigationController.navigateBackToHomeFeaturedFragment(
                        requireActivity(), catId, subCatId, nameSubcategory)
                    activity!!.finish()
                }
            }

        },this.catId!!,this.subCatId!!)
        this.adapter = AutoClearedValue(this, nvAdapter)
        binding.get().CategoryList.setAdapter(nvAdapter)
    }

    private fun assignCategoryId(catId: String?, subCatId: String?) {
        this.catId = catId
        this.subCatId = subCatId
    }

    override fun initData() {
       itemCategoryViewModel.setCategoryListObj("10", itemCategoryViewModel.offset.toString())
       itemSubCategoryViewModel.setAllSubCategoryListObj()
        val subCategories: LiveData<Resource<List<SubCategory>>>? = itemSubCategoryViewModel.getAllSubCategoryListData()
        itemCategoryViewModel.getCategoryListData()?.observe(this, Observer { listResource ->
            if(listResource != null){
                if (listResource.data != null && listResource.data.isNotEmpty()){
                    lastCategoryData = AutoClearedValue(this, listResource.data)
                    replaceCategory(lastCategoryData!!.get())
                }
            }else{
                if (itemCategoryViewModel.offset > 1) {
                    itemCategoryViewModel.forceEndLoading = true
                }
            }
        })

        subCategories?.observe(this, Observer { listResource  ->
                if (listResource != null) {
                    if (listResource.data != null && listResource.data.isNotEmpty()) {
                        replaceSubCategory(listResource.data)
                    }
                } else {
                    if (itemSubCategoryViewModel.offset > 1) {
                        itemSubCategoryViewModel.forceEndLoading = true
                    }
                }
            }
        )
    }

    override fun onDispatched() {}

    private fun replaceCategory(CategoryList: List<Category>) {
        adapter.get().replaceCategory(CategoryList)
        adapter.get().notifyDataSetChanged()
        binding.get().executePendingBindings()
    }

    private fun replaceSubCategory(subCategoryList: List<SubCategory>) {
        adapter.get().replaceSubCategory(subCategoryList)
        adapter.get().notifyDataSetChanged()
        binding.get().executePendingBindings()
    }

}