package com.jujuy.bienquerer.ui.professionals.search

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import com.jujuy.bienquerer.MainActivity
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.api.network.Resource
import com.jujuy.bienquerer.base.BaseFragment
import com.jujuy.bienquerer.databinding.SearchListFragmentBinding
import com.jujuy.bienquerer.extensions.gone
import com.jujuy.bienquerer.model.ItemParameterHolder
import com.jujuy.bienquerer.model.entity.Professional
import com.jujuy.bienquerer.ui.home.adapter.ReposAdapter
import com.jujuy.bienquerer.ui.notifications.LoadingAdapter
import com.jujuy.bienquerer.utils.Constants
import com.jujuy.bienquerer.utils.LoadingMsg
import com.jujuy.bienquerer.utils.autoCleared
import com.jujuy.bienquerer.viewmodel.professional.ProfessionalViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import java.util.*
import kotlin.time.ExperimentalTime


@ExperimentalCoroutinesApi
@ExperimentalTime
class SearchListFragment : BaseFragment(), ReposAdapter.ListenerClick {

    companion object {
        fun newInstance() =
            SearchListFragment()
    }

    private var typeClicked = false
    private var filterClicked = false
    private var mapFilterClicked = false
    private var clearRecyclerView: List<Professional> = emptyList()

    private var binding by autoCleared<SearchListFragmentBinding>()
    private val itemViewModel by viewModel<ProfessionalViewModel>()
    private val loading : LoadingMsg by lazy { LoadingMsg(requireActivity(), false)}
    private val adapter = ReposAdapter(this)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = SearchListFragmentBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        Timber.e("CREADOO.........")
        return binding.root
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        binding.newsList.gone()

        if (requestCode == Constants.REQUEST_CODE__MAP_FILTERING && resultCode == Constants.RESULT_CODE__FROM_MAP_VIEW) {
            val catId = data?.getStringExtra(Constants.CATEGORY_ID)
            if (catId != null) {
                itemViewModel.holder.cat_id = catId
            }
            val subCatId = data?.getStringExtra(Constants.SUBCATEGORY_ID)
            if (subCatId != null) {
                itemViewModel.holder.sub_cat_id = subCatId
            }
            typeClicked = (itemViewModel.holder.cat_id != null && itemViewModel.holder.cat_id != ""
                    || itemViewModel.holder.sub_cat_id != null && itemViewModel.holder.sub_cat_id != "")

            typeButtonClicked(typeClicked)
            Timber.e("Entrando a la activity de vuelta a filtrador")
            loadItemList()
        } else if (requestCode == Constants.REQUEST_CODE__ITEM_LIST_FRAGMENT && resultCode == Constants.RESULT_CODE__CATEGORY_FILTER) {
            val catId = data?.getStringExtra(Constants.CATEGORY_ID)
            if (catId != null) {
                itemViewModel.holder.cat_id = catId
            }
            val subCatId = data?.getStringExtra(Constants.SUBCATEGORY_ID)
            if (subCatId != null) {
                itemViewModel.holder.sub_cat_id = subCatId
            }
            typeClicked = (itemViewModel.holder.cat_id != null && itemViewModel.holder.cat_id != ""
                    || itemViewModel.holder.sub_cat_id != null && itemViewModel.holder.sub_cat_id != "")

            typeButtonClicked(typeClicked)
            val nameSubcat = data?.getStringExtra(Constants.SUBCATEGORY_NAME)
            (activity as AppCompatActivity?)!!.supportActionBar?.title =
                nameSubcat?.capitalize(Locale.ROOT) ?: "Lista de Profesionales"
            loadItemList()
        }
        else if (requestCode == Constants.REQUEST_CODE__ITEM_LIST_FRAGMENT && resultCode == Constants.RESULT_CODE__SPECIAL_FILTER) {
            if (data?.getSerializableExtra(Constants.FILTERING_HOLDER) != null) {
                itemViewModel.holder =
                    data.getSerializableExtra(Constants.FILTERING_HOLDER) as ItemParameterHolder
            }

            loadItemList()
        } else if (requestCode == Constants.REQUEST_CODE__MAP_FILTERING && resultCode == Constants.RESULT_CODE__MAP_FILTERING) {
            if (data?.getSerializableExtra(Constants.ITEM_HOLDER) != null) {
                itemViewModel.holder = data.getSerializableExtra(Constants.ITEM_HOLDER) as ItemParameterHolder
                Timber.e("Capturando el holder ${itemViewModel.holder.mapMiles} ${itemViewModel.holder.cat_id} ${itemViewModel.holder.sub_cat_id}")
            }
            mapFilterClicked = itemViewModel.holder.lat != "" || itemViewModel.holder.lng != "" ||
                    itemViewModel.holder.mapMiles != ""

            mapFilterButtonClicked(mapFilterClicked)
            loadNewItemList()
        }
    }

    override fun initUIAndActions() {
        if (activity is MainActivity) {
            requireActivity().window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
            (this.activity as MainActivity).binding.toolbarMain.setBackgroundColor(
                ContextCompat.getColor(
                    requireContext(),
                    R.color.global__primary
                )
            )
        }
        binding.typeButton.setOnClickListener { v -> ButtonClick(v) }
        binding.sortButton.setOnClickListener { v -> ButtonClick(v) }
        binding.swipeRefresh.setColorSchemeColors(
            ContextCompat.getColor(
                requireContext(),
                R.color.md_grey_200
            )
        )
        binding.swipeRefresh.setProgressBackgroundColorSchemeColor(
            ContextCompat.getColor(
                requireContext(),
                R.color.global__primary
            )
        )

        binding.swipeRefresh.setOnRefreshListener {
            if (itemViewModel.holder.mapMiles.isNotEmpty()) {
                loadNewItemList()
            }
            adapter.refresh()
        }

        binding.retryButton.setOnClickListener {
            adapter.retry()
        }

        binding.newsList.gone()
        loading.showloaderDialog()
        loading.show()
    }

    private fun ButtonClick(v: View?) {
        when (v?.id) {
            R.id.typeButton -> {
                navigationController.navigateToTypeFilterFragment(
                    requireActivity(),
                    itemViewModel.holder.cat_id,
                    itemViewModel.holder.sub_cat_id,
                    itemViewModel.holder,
                    Constants.FILTERING_TYPE_FILTER
                )
                typeButtonClicked(typeClicked)
            }
            R.id.sortButton -> {
                itemViewModel.holder.cat_id
                itemViewModel.holder.sub_cat_id
                itemViewModel.holder.location_id = selected_location_id
                itemViewModel.holder.location_name = selected_location_name
                itemViewModel.holder.lat = selectedLat
                itemViewModel.holder.lng = selectedLng
                navigationController.navigateToMapFiltering(requireActivity(), itemViewModel.holder)
            }
        }
    }

    private fun typeButtonClicked(b: Boolean) {
        if (b) {
            binding.typeButton.setCompoundDrawablesWithIntrinsicBounds(
                null,
                ContextCompat.getDrawable(
                    requireContext(),
                    R.drawable.baseline_list_with_check_orange_24
                ),
                null,
                null
            )
        } else {
            binding.typeButton.setCompoundDrawablesWithIntrinsicBounds(
                null,
                ContextCompat.getDrawable(requireContext(), R.drawable.baseline_list_orange_24),
                null,
                null
            )
        }
    }

    private fun mapFilterButtonClicked(b: Boolean) {
        if (b) {
            binding.sortButton.setCompoundDrawablesWithIntrinsicBounds(
                null,
                ContextCompat.getDrawable(
                    requireContext(),
                    R.drawable.baseline_sort_with_check_orange_24
                ),
                null,
                null
            )
        } else {
            binding.sortButton.setCompoundDrawablesWithIntrinsicBounds(
                null,
                ContextCompat.getDrawable(requireContext(), R.drawable.baseline_sort_orange_24),
                null,
                null
            )
        }
    }


    override fun initAdapters() {
        binding.newsList.adapter = adapter.withLoadStateHeaderAndFooter(
            header = LoadingAdapter { adapter.retry() },
            footer = LoadingAdapter { adapter.retry() }
        )
        adapter.addLoadStateListener { loadState ->
            // Only show the list if refresh succeeds.
            binding.swipeRefresh.isRefreshing = false
            loading.cancel()
            // Show loading spinner during initial load or refresh.
            binding.loadMoreBar.isVisible = loadState.source.refresh is LoadState.Loading
            // Show the retry state if initial load or refresh fails.
            binding.retryButton.isVisible = loadState.source.refresh is LoadState.Error

            // Toast on any error, regardless of whether it came from RemoteMediator or PagingSource
            val errorState = loadState.source.append as? LoadState.Error
                ?: loadState.source.prepend as? LoadState.Error
                ?: loadState.append as? LoadState.Error
                ?: loadState.prepend as? LoadState.Error
            errorState?.let {
                Toast.makeText(
                    requireContext(),
                    "\uD83D\uDE28 Wooops ${it.error}",
                    Toast.LENGTH_LONG
                ).show()
            }

            if (loadState.source.refresh is LoadState.NotLoading && adapter.itemCount < 1) {
                    Timber.e("Ocultar lista ${adapter.itemCount < 1} ${loadState.refresh.endOfPaginationReached} ${loadState.source.refresh is LoadState.NotLoading }")
                    binding.newsList.isVisible = false
                    binding.noItemConstraintLayout.isVisible = true
                } else {
                    Timber.e("Mostrar lista ${adapter.itemCount < 1} ${loadState.refresh.endOfPaginationReached} ${loadState.source.refresh is LoadState.NotLoading }")
                    binding.newsList.isVisible = true
                    binding.noItemConstraintLayout.isVisible = false
                }
            }
    }

    override fun initData() {
        if (activity != null) {
            itemViewModel.holder =
                requireActivity().intent.getSerializableExtra(Constants.ITEM_PARAM_HOLDER_KEY) as ItemParameterHolder
            if (arguments != null) {
                itemViewModel.holder =
                    requireArguments().getSerializable(Constants.ITEM_PARAM_HOLDER_KEY) as ItemParameterHolder
            }
            if (itemViewModel.holder != null) {
                binding.buttonLayout.visibility = View.VISIBLE
            }
            loadItemList()

            filterClicked = !itemViewModel.holder.keyword.equals(Constants.FILTERING_INACTIVE) ||
                    !itemViewModel.holder.order_by.equals(Constants.FILTERING_INACTIVE) ||
                    !itemViewModel.holder.order_type.equals(Constants.FILTERING_INACTIVE)

            typeClicked =
                itemViewModel.holder.cat_id != Constants.FILTERING_INACTIVE || itemViewModel.holder.sub_cat_id != Constants.FILTERING_INACTIVE

            mapFilterClicked =
                itemViewModel.holder.lat != "" || itemViewModel.holder.lng != "" || itemViewModel.holder.mapMiles != ""

            typeButtonClicked(typeClicked)

            mapFilterButtonClicked(mapFilterClicked)


        }
    }

    private fun loadDataFromMap() {
        itemViewModel.getMapList()?.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Resource.Status.LOADING -> {
                    loading.showloaderDialog()
                    loading.show()
                }
                Resource.Status.SUCCESS -> {
                    loading.cancel()
                    Timber.e("${it.data}")
                    //if (it.data?.data != null) {
                        itemViewModel.listItem = it.data?.data!!
                        loadDataListFromMap()
                    //}
                }
                Resource.Status.ERROR -> {
                    loading.cancel()
                    Timber.e(it.message)
                }
            }
        })
    }


    private fun loadItemList() {

        lifecycleScope.launch {
            itemViewModel.getListProfessionals(itemViewModel.holder).collectLatest {
                adapter.submitData(it)
            }
        }
    }

    private fun loadDataListFromMap() {
        Timber.e("Load data from map ${itemViewModel.listItem.size}")
        lifecycleScope.launch {
           /* adapter.loadStateFlow.collectLatest {

                if (it.refresh is LoadState.NotLoading){
                    binding.noItemConstraintLayout.isVisible = adapter.itemCount < 1
                    binding.swipeRefresh.isRefreshing = false
                    adapter.submitData(PagingData.from(itemViewModel.listItem))
                }
            }*/
            //adapter.submitData(PagingData.empty())
            itemViewModel.getListProfessionalsMap(itemViewModel.holder).collectLatest {
                adapter.submitData(it)
            }
        }
       /* if (itemViewModel.listItem.isNotEmpty()) {
            lifecycleScope.launch {
                adapter.submitData(PagingData.from(itemViewModel.listItem))
            }
        } else {
            lifecycleScope.launch {
                adapter.loadStateFlow.collectLatest {
                    if(it.refresh is LoadState.NotLoading){
                        binding.newsList.isVisible = adapter.itemCount < 1
                    }
                }
                //adapter.submitData(PagingData.empty())
            }
        }*/

    }

    private fun loadNewItemList() {
        /*itemViewModel.setGetMapList(
            itemViewModel.holder.lat.toString(),
            itemViewModel.holder.lng.toString(),
            itemViewModel.holder.cat_id.toString(),
            itemViewModel.holder.sub_cat_id.toString(),
            itemViewModel.holder.mapMiles.toString())
        loadDataFromMap()*/
        loadDataListFromMap()
    }

    override fun onItemClick(prof: Professional) {
        navigationController.navigateToItemDetailActivity(
            requireActivity(),
            prof.id.toString(),
            prof.name.toString()
        )
    }
}