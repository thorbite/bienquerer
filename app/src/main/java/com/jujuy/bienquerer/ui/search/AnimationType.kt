package com.jujuy.bienquerer.ui.search

internal enum class AnimationType {

    ENTER,
    EXIT,
    NONE

}