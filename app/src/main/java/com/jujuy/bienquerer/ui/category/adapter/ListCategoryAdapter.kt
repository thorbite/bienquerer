package com.jujuy.bienquerer.ui.category.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.api.network.AppExecutors
import com.jujuy.bienquerer.databinding.ItemCategoryAdapterBinding
import com.jujuy.bienquerer.model.entity.Category
import com.jujuy.bienquerer.ui.common.custom.DataBoundViewHolder
import com.jujuy.bienquerer.utils.DataBoundListAdapter

class ListCategoryAdapter(appExecutors: AppExecutors, val callback: CategoryClickCallback
) : DataBoundListAdapter<Category, ItemCategoryAdapterBinding>(
    appExecutors= appExecutors,
    diffCallback = object : DiffUtil.ItemCallback<Category>() {
        override fun areItemsTheSame(oldItem: Category, newItem: Category): Boolean {
            return oldItem.idCategory == newItem.idCategory
                    && oldItem.nameCategory == newItem.nameCategory
        }

        override fun areContentsTheSame(oldItem: Category, newItem: Category): Boolean {
            return oldItem.idCategory == newItem.idCategory
                    && oldItem.nameCategory == newItem.nameCategory
        }
    }
) {

    interface CategoryClickCallback {
        fun onClick(category: Category);
    }

    private var lastPosition = -1

    override fun createBinding(parent: ViewGroup): ItemCategoryAdapterBinding {
        val binding: ItemCategoryAdapterBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context)
            ,R.layout.item_category_adapter,parent,false)
        binding.root.setOnClickListener{
            val category: Category= binding.category!!
            callback.onClick(category)
        }
        return binding
    }

    override fun bindView(holder: DataBoundViewHolder<ItemCategoryAdapterBinding>, position: Int) {
        super.bindView(holder, position)
        setAnimation(holder.itemView,position)
    }

    private fun setAnimation(viewToAnimate: View?, position: Int) {
        if (position > lastPosition)  {
            val animation = android.view.animation.AnimationUtils.loadAnimation(
                viewToAnimate?.context,
                R.anim.slide_in_bottom
            )
            viewToAnimate?.startAnimation(animation)
            lastPosition = position
        }
    }


    override fun bind(binding: ItemCategoryAdapterBinding, item: Category) {
        binding.category = item
    }
}
