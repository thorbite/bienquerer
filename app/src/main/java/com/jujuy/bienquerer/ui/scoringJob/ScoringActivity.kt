package com.jujuy.bienquerer.ui.scoringJob

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.base.BaseActivity
import com.jujuy.bienquerer.databinding.ScoringActivityBinding
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlin.time.ExperimentalTime

@ExperimentalTime
@ExperimentalCoroutinesApi
class ScoringActivity : BaseActivity() {

    val binding : ScoringActivityBinding by lazy {
        DataBindingUtil.setContentView<ScoringActivityBinding>(this, R.layout.scoring_activity) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initUI(binding)
    }

    private fun initUI(binding: ScoringActivityBinding) {
        initToolbar(binding.toolbar, "Puntuación")
        setupFragment(ScoringFragment())
    }
}