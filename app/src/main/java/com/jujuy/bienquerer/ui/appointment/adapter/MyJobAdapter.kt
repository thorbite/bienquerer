package com.jujuy.bienquerer.ui.appointment.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.model.network.response.Job
import com.jujuy.bienquerer.ui.appointment.professional.AppointmentProfessionalFragment
import com.jujuy.bienquerer.utils.Utils
import com.perfomer.blitz.setTimeAgo
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class MyJobAdapter(val listenerClick: OnListInteractionListener):
    PagingDataAdapter<Job, MyJobAdapter.JobProfessionalViewHolder>(
        UIMODEL_COMPARATOR
    ) {

    var item: MutableList<Job>? = null


    interface OnListInteractionListener {
        fun onClickJob(item: Job);
    }

    override fun onBindViewHolder(holder: JobProfessionalViewHolder, position: Int) {
        getItem(position)?.let {
            item?.add(it)
            holder.bind(it, holder.itemView)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): JobProfessionalViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.fragment_item_job, parent, false)
        return JobProfessionalViewHolder(view)
    }

    @SuppressLint("ClickableViewAccessibility")
    inner class JobProfessionalViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val name: TextView = view.findViewById(R.id.title)
        private val service: TextView = view.findViewById(R.id.service)
        private val date: TextView = view.findViewById(R.id.date)
        private val statusJob: TextView = view.findViewById(R.id.status)

        init {
            itemView.setOnClickListener {
                val position = bindingAdapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    val item = getItem(position)
                    if (item != null) {
                        listenerClick.onClickJob(item)
                    }
                }
            }

        }


        fun bind(item: Job, itemView: View) {
            with(item) {
                name.text = professional.name.toString() + professional.lastname.toString()
                date.setTimeAgo(Utils.toMilliseconds(createdAt), showSeconds = true, autoUpdate = true)
                statusJob.text = status.name ?: ""
                service.text = profession?.nameSubCategory ?: "No especificado"
                when(status.id){
                    1 -> statusJob.setBackgroundColor(ContextCompat.getColor(itemView.context, R.color.md_light_blue_300))
                    2 -> statusJob.setBackgroundColor(ContextCompat.getColor(itemView.context, R.color.md_green_800))
                    3 -> statusJob.setBackgroundColor(ContextCompat.getColor(itemView.context, R.color.md_red_900))
                    4 -> statusJob.setBackgroundColor(ContextCompat.getColor(itemView.context, R.color.com_facebook_blue))
                    5 -> statusJob.setBackgroundColor(ContextCompat.getColor(itemView.context, R.color.md_grey_700))
                }
            }

        }
    }

    companion object {
        private val UIMODEL_COMPARATOR = object : DiffUtil.ItemCallback<Job>() {
            override fun areItemsTheSame(
                oldItem: Job,
                newItem: Job
            ): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(
                oldItem: Job,
                newItem: Job
            ): Boolean =
                oldItem == newItem
        }
    }
}

   /* DataBoundListAdapter<Job, FragmentItemJobBinding>(
        appExecutors = appExecutors,
        diffCallback = object : DiffUtil.ItemCallback<Job>() {
            override fun areItemsTheSame(oldItem: Job, newItem: Job): Boolean {
                return oldItem.id == newItem.id
                        && oldItem.comment == newItem.comment
            }

            override fun areContentsTheSame(oldItem: Job, newItem: Job): Boolean {
                return oldItem == newItem
            }
        }
    ) {

    interface OnListInteractionListener {
        fun onAppointmentInteraction(item: Job)
    }

    override fun bindView(holder: DataBoundViewHolder<FragmentItemJobBinding>, position: Int) {
        super.bindView(holder, position)
        when(getItem(position).status.id){
            1 -> holder.binding.status.setBackgroundColor(ContextCompat.getColor(holder.binding.root.context, R.color.md_light_blue_300))
            2 -> holder.binding.status.setBackgroundColor(ContextCompat.getColor(holder.binding.root.context, R.color.md_green_800))
            3 -> holder.binding.status.setBackgroundColor(ContextCompat.getColor(holder.binding.root.context, R.color.md_red_900))
            4 -> holder.binding.status.setBackgroundColor(ContextCompat.getColor(holder.binding.root.context, R.color.com_facebook_blue))
            5 -> holder.binding.status.setBackgroundColor(ContextCompat.getColor(holder.binding.root.context, R.color.md_grey_700))
        }

        holder.binding.date.setTimeAgo(Utils.toMilliseconds(getItem(position).createdAt), showSeconds = true, autoUpdate = true)
    }

    override fun createBinding(parent: ViewGroup): FragmentItemJobBinding {
        val binding: FragmentItemJobBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.fragment_item_job, parent, false)
        binding.root.setOnClickListener {
            val job: Job = binding.job!!
            listenerClick.onAppointmentInteraction(job)
        }
        return binding
    }

    override fun bind(binding: FragmentItemJobBinding, item: Job) {
        binding.job = item
    }
}*/