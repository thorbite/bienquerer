package com.jujuy.bienquerer.ui.appointment.selectProfessionItem

data class SelectableItem (
    var isSelected: Boolean? = false,
    var name: String? = null,
    var key: String? = null
)