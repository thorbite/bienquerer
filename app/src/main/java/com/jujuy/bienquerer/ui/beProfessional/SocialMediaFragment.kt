package com.jujuy.bienquerer.ui.beProfessional

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.EditText
import android.widget.PopupWindow
import android.widget.Toast
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.api.network.Resource
import com.jujuy.bienquerer.extensions.placeCursorToEnd
import com.jujuy.bienquerer.model.MySpinnerItem
import com.jujuy.bienquerer.model.SocialNetworks
import kotlinx.android.synthetic.main.social_media_fragment.*
import kotlinx.android.synthetic.main.social_media_fragment.view.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import timber.log.Timber
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class SocialMediaFragment : DialogFragment() {

    companion object {
        fun newInstance() = SocialMediaFragment()
    }

    private var socialMediaAdapter: SocialMediaAdapter? = null
    private val socialMediaViewModel by sharedViewModel<SocialMediaViewModel>()
    private var popupWindow : PopupWindow? = null
    private var itemSelected: MySpinnerItem? = null
    lateinit var urlEditPlace: EditText

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.social_media_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupClickListeners(view)
        urlEditPlace = view.findViewById<EditText>(R.id.urlEdit)
        getListSocialNetwork()
    }

    private fun getListSocialNetwork() {
        socialMediaViewModel.getAllSocialNetworks()?.observe(this, Observer {
            when(it.status){
                Resource.Status.LOADING ->{}
                Resource.Status.SUCCESS ->{
                    Timber.e(it.data.toString())
                    it.data?.let {  list -> setSpinnerData(list)}
                }
                Resource.Status.ERROR ->{
                    Timber.e(it.message.toString())
                }
            }
        })
    }

    private fun setSpinnerData(list: List<SocialNetworks>) {
        val spinnerList = mutableListOf<MySpinnerItem>()
        for (i in list.indices){
            when(list[i].name){
                "FACEBOOK" ->{
                    spinnerList.add(MySpinnerItem("1",contextDrawable(R.drawable.ic_facebook), "Facebook", list[i].base_url))
                }
                "INSTAGRAM" ->{
                    spinnerList.add(MySpinnerItem("2", contextDrawable(R.drawable.ic_instagram), "Instagram", list[i].base_url))
                }
                "LINKEDIN" ->{
                    spinnerList.add(MySpinnerItem("3",contextDrawable(R.drawable.ic_linkedin), "Linkedin", list[i].base_url))
                }
                "TWITTER" ->{
                    spinnerList.add(MySpinnerItem("3",contextDrawable(R.drawable.ic_gorjeo), "Twitter", list[i].base_url))
                }
                "YOUTUBE" ->{
                    spinnerList.add(MySpinnerItem("3",contextDrawable(R.drawable.ic_youtube), "Youtube", list[i].base_url))
                }
            }
        }

        socialMediaSpinner.apply {
            setSpinnerAdapter(SocialMediaAdapter(this))
            setItems(spinnerList)
            setOnSpinnerItemSelectedListener<MySpinnerItem> { _, item ->
                itemSelected = item
                urlEditPlace.setText(item.url)
                urlEditPlace.placeCursorToEnd()

                Toast.makeText(activity?.applicationContext, item.id, Toast.LENGTH_SHORT).show()
            }
            getSpinnerRecyclerView().layoutManager = GridLayoutManager(requireContext(), 2)
            //selectItemByIndex(0)
            preferenceName = "Red Social"
            lifecycleOwner = this@SocialMediaFragment
        }
    }


    private fun setupClickListeners(view: View) {

        view.btnPositive.setOnClickListener {
            Timber.e(itemSelected?.text.toString())
            itemSelected?.let { item ->
                socialMediaViewModel.setSpinnerItem(MySpinnerItem(item.id, item.icon, item.text, urlEdit.text.toString()))
            }
            dismiss()
        }

        view.btnNegative.setOnClickListener{
            dismiss()
        }


        /*socialMediaSpinner.setOnClickListener {
            popupWindow?.dismiss()
            if (popupWindow == null) {
                provideCountryPopupWindow(it)
            }
            popupWindow!!.showAsDropDown(it, 0, -it.height)
        }*/

    }

    private fun contextDrawable(@DrawableRes resource: Int): Drawable? {
        return ContextCompat.getDrawable(requireContext(), resource)
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
    }
}