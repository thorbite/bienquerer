package com.jujuy.bienquerer.ui.beProfessional

import com.jujuy.bienquerer.R

enum class SocialMedia (val redSocialCode: String, val icon: Int) {
    FACEBOOK("Facebook", R.drawable.ic_facebook),
    INSTAGRAM("Instagram", R.drawable.ic_instagram),
    LINKEDIN("Linkedin", R.drawable.ic_linkedin),
    YOUTUBE("Youtube", R.drawable.ic_youtube),
    TWITTER("Twitter", R.drawable.ic_gorjeo),
}