package com.jujuy.bienquerer.ui.appointment.user

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "appointment_user_keys")
data class AppointmentUserKeys (
    @PrimaryKey
    val jobUserId: Int,
    val prevKey: Int?,
    val nextKey: Int?
)