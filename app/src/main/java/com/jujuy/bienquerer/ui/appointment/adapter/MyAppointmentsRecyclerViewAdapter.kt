package com.jujuy.bienquerer.ui.appointment.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.api.ApiConstants
import com.jujuy.bienquerer.api.network.AppExecutors
import com.jujuy.bienquerer.databinding.FragmentItemAppointmentBinding
import com.jujuy.bienquerer.databinding.ItemCategoryAdapterBinding
import com.jujuy.bienquerer.model.entity.ProfessionalFav
import com.jujuy.bienquerer.model.network.response.Appointment
import com.jujuy.bienquerer.ui.common.custom.DataBoundViewHolder
import com.jujuy.bienquerer.ui.favorite.FavoriteAdapter
import com.jujuy.bienquerer.utils.DataBoundListAdapter
import com.jujuy.bienquerer.utils.Utils
import com.perfomer.blitz.setTimeAgo
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class MyAppointmentsRecyclerViewAdapter(val listenerClick: AppoinmentClickCallback):
    PagingDataAdapter<Appointment, MyAppointmentsRecyclerViewAdapter.AppointmentUserViewHolder>(
    UIMODEL_COMPARATOR
) {

    var item: MutableList<Appointment>? = null


    interface AppoinmentClickCallback {
        fun onClick(category: Appointment);
    }

    override fun onBindViewHolder(holder: AppointmentUserViewHolder, position: Int) {
        getItem(position)?.let {
            item?.add(it)
            holder.bind(it, holder.itemView)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AppointmentUserViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.fragment_item_appointment, parent, false)
        return AppointmentUserViewHolder(view)
    }

    @SuppressLint("ClickableViewAccessibility")
    inner class AppointmentUserViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val name: TextView = view.findViewById(R.id.title)
        private val service: TextView = view.findViewById(R.id.service)
        private val date: TextView = view.findViewById(R.id.date)
        private val statusname: TextView = view.findViewById(R.id.status)

        init {
            itemView.setOnClickListener {
                val position = bindingAdapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    val item = getItem(position)
                    if (item != null) {
                        listenerClick.onClick(item)
                    }
                }
            }

        }


        fun bind(item: Appointment, itemView: View) {
            with(item) {
                name.text = professional.name.toString() + professional.lastname.toString()
                date.setTimeAgo(Utils.toMilliseconds(createdAt), showSeconds = true, autoUpdate = true)
                statusname.text = status.name ?: ""
                service.text = profession?.nameSubCategory ?: "No especificado"
                when(status.id){
                    1 -> statusname.setBackgroundColor(ContextCompat.getColor(itemView.context, R.color.md_light_blue_300))
                    2 -> statusname.setBackgroundColor(ContextCompat.getColor(itemView.context, R.color.md_green_800))
                    3 -> statusname.setBackgroundColor(ContextCompat.getColor(itemView.context, R.color.md_red_900))
                    4 -> statusname.setBackgroundColor(ContextCompat.getColor(itemView.context, R.color.com_facebook_blue))
                    5 -> statusname.setBackgroundColor(ContextCompat.getColor(itemView.context, R.color.md_grey_700))
                }
            }

        }
    }

    companion object {
        private val UIMODEL_COMPARATOR = object : DiffUtil.ItemCallback<Appointment>() {
            override fun areItemsTheSame(
                oldItem: Appointment,
                newItem: Appointment
            ): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(
                oldItem: Appointment,
                newItem: Appointment
            ): Boolean =
                oldItem == newItem
        }
    }
}