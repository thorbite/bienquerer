package com.jujuy.bienquerer.ui.notifications

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.api.network.Resource
import com.jujuy.bienquerer.base.BaseFragment
import com.jujuy.bienquerer.databinding.FragmentNotificationListBinding
import com.jujuy.bienquerer.model.network.response.ListNotifications
import com.jujuy.bienquerer.ui.notifications.adapter.NotiAdapter
import com.jujuy.bienquerer.utils.Constants
import com.jujuy.bienquerer.utils.autoCleared
import com.jujuy.bienquerer.viewmodel.notifications.NotificationsViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class NotificationListFragment : BaseFragment(), NotiAdapter.OnItemClick {
    private var binding by autoCleared<FragmentNotificationListBinding>()
    private val notificationViewModel by viewModel<NotificationsViewModel>()
    private val notiAdapter = NotiAdapter(this)
    private var searchJob: Job? = null

    companion object {
        fun newInstance() = NotificationListFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentNotificationListBinding.inflate(inflater, container, false)
        activity?.let {
            it.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
        }
        setHasOptionsMenu(true)
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun initUIAndActions() {
        binding.swipeRefresh.setColorSchemeColors(
            ContextCompat.getColor(
                requireContext(),
                R.color.white
            )
        )
        binding.swipeRefresh.setProgressBackgroundColorSchemeColor(
            ContextCompat.getColor(
                requireContext(),
                R.color.global__primary
            )
        )
        binding.swipeRefresh.setOnRefreshListener {
            notiAdapter.refresh()
        }
        binding.retryButton.setOnClickListener { notiAdapter.retry() }

        val itemTouchHelper = ItemTouchHelper(simpleCallback)
        itemTouchHelper.attachToRecyclerView(binding.notificationList)
    }

    override fun initAdapters() {
        binding.notificationList.adapter = notiAdapter.withLoadStateHeaderAndFooter(
            header = LoadingAdapter { notiAdapter.retry() },
            footer = LoadingAdapter { notiAdapter.retry() }
        )

        notiAdapter.addLoadStateListener { loadState ->
            // Only show the list if refresh succeeds.
            binding.swipeRefresh.isRefreshing = false
            // Show loading spinner during initial load or refresh.
            binding.progressBar.isVisible = loadState.source.refresh is LoadState.Loading
            // Show the retry state if initial load or refresh fails.
            binding.retryButton.isVisible = loadState.source.refresh is LoadState.Error

            // Toast on any error, regardless of whether it came from RemoteMediator or PagingSource
            val errorState = loadState.source.append as? LoadState.Error
                ?: loadState.source.prepend as? LoadState.Error
                ?: loadState.append as? LoadState.Error
                ?: loadState.prepend as? LoadState.Error
            errorState?.let {
                Toast.makeText(
                    requireContext(),
                    "\uD83D\uDE28 Wooops ${it.error}",
                    Toast.LENGTH_LONG
                ).show()
            }

            if (loadState.source.refresh is LoadState.NotLoading && notiAdapter.itemCount < 1) {
                Timber.e("Ocultar lista ${notiAdapter.itemCount < 1} ${loadState.refresh.endOfPaginationReached} ${loadState.source.refresh is LoadState.NotLoading}")
                binding.notificationList.isVisible = false
                binding.emptyConstraintLayout.isVisible = true
            } else {
                Timber.e("Mostrar lista ${notiAdapter.itemCount < 1} ${loadState.refresh.endOfPaginationReached} ${loadState.source.refresh is LoadState.NotLoading}")
                binding.notificationList.isVisible = true
                binding.emptyConstraintLayout.isVisible = false
            }
        }
    }

    override fun initData() {
        fetchNotifications()

        notificationViewModel.getDeleteNotificationItem()?.observe(this, Observer {
            when(it.status){
                Resource.Status.LOADING ->{}
                Resource.Status.SUCCESS ->{
                    Timber.e(it.message.toString())
                }
                Resource.Status.ERROR ->{
                    Timber.e(it.message.toString())
                    notiAdapter.notifyDataSetChanged()
                }
            }
        })
    }

    private fun fetchNotifications() {
        searchJob?.cancel()
        searchJob = lifecycleScope.launch {
            notificationViewModel.getListNotification().collectLatest {
                Timber.e("Fragment list dataaaa")
                notiAdapter.submitData(it)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        fetchNotifications()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Constants.REQUEST_CODE__NOTIFICATION_LIST_FRAGMENT
            && resultCode == Constants.RESULT_CODE__REFRESH_NOTIFICATION
        ) {
            notificationViewModel.notiId = data!!.getStringExtra(Constants.NOTI_HEADER_ID)
        }
    }

    override fun onItemClick(item: ListNotifications, position: Int) {
        navigationController.navigateToNotificationDetail(
            requireActivity(),
            item,
            "notificationViewModel.token"
        )
    }

    var simpleCallback: ItemTouchHelper.SimpleCallback =
        object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                viewHolder1: RecyclerView.ViewHolder
            ): Boolean {
                Toast.makeText(requireContext(), "Move", Toast.LENGTH_SHORT).show()
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, i: Int) {
                val position = viewHolder.bindingAdapterPosition
                if (position != RecyclerView.NO_POSITION){
                    val item = notiAdapter.getNotificationAt(position)
                    if (item != null) {
                        notificationViewModel.setDeleteNotificationItem(item.id.toString())
                        notiAdapter.deleteItem(position)
                    }
                }
            }
        }
}