package com.jujuy.bienquerer.ui.professionals.map

import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.base.BaseActivity
import com.jujuy.bienquerer.databinding.ActivityMapBinding
import com.jujuy.bienquerer.utils.Constants
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class MapActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityMapBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_map)

        initUi(binding)
    }

    private fun initUi(binding: ActivityMapBinding) {
        val intent = intent

        when (intent.getStringExtra(Constants.MAP_FLAG)) {
            Constants.MAP -> {
                initToolbar(
                    binding.toolbar,
                    resources.getString(R.string.map_filter__map_title)
                )
            }
            Constants.MAP_PICK -> {
                setupFragment(PickMapFragment())
                initToolbar(
                    binding.toolbar,
                    resources.getString(R.string.map_filter__map_title)
                )
            }
        }    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val fragment = supportFragmentManager.findFragmentById(R.id.content_frame)
        fragment?.onActivityResult(requestCode, resultCode, data)
    }
}