package com.jujuy.bienquerer.ui.professionals.comments

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.jujuy.bienquerer.MainActivity
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.api.network.Resource
import com.jujuy.bienquerer.base.BaseFragment
import com.jujuy.bienquerer.databinding.CommentsFragmentBinding
import com.jujuy.bienquerer.extensions.visible
import com.jujuy.bienquerer.model.entity.Category
import com.jujuy.bienquerer.model.entity.Comment
import com.jujuy.bienquerer.utils.Utils
import com.jujuy.bienquerer.utils.autoCleared
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.android.ext.android.bind
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class CommentsFragment : BaseFragment() {

    companion object {
        fun newInstance() = CommentsFragment()
    }

    private val commentViewModel by viewModel<CommentsViewModel>()
    private var binding by autoCleared<CommentsFragmentBinding>()
    private var adapterComment by autoCleared<ListCommentsAdapter>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        binding = CommentsFragmentBinding.inflate(inflater, container, false)
        activity?.let {
            it.window?.setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN
            )
        }
        binding.loadingMore = connectivity.isConnected
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun initUIAndActions() {
        if (activity is MainActivity) {
            (activity as MainActivity).setToolbarText((activity as MainActivity).binding.toolbarMain, "Categorias")
        }
        commentViewModel.professionalId = requireActivity().intent.extras!!.getString("idProf")
        binding.swipeRefresh.setColorSchemeColors(ContextCompat.getColor(requireContext(), R.color.white))
        binding.swipeRefresh.setProgressBackgroundColorSchemeColor(ContextCompat.getColor(requireContext(), R.color.global__primary))
        binding.swipeRefresh.setOnRefreshListener {
            commentViewModel.loadingDirection = Utils.LoadingDirection.top
            commentViewModel.offset = 0
            commentViewModel.forceEndLoading = false
            commentViewModel.setCategoryListObj("10", "0")
        }
    }

    override fun initAdapters() {
        val nvAdapter = ListCommentsAdapter(appExecutors)
        this.adapterComment = nvAdapter
        binding.commentsList.adapter = nvAdapter
    }

    override fun initData() {
        loadCategory()
    }

    private fun loadCategory() {
        commentViewModel.setCategoryListObj("10","0")
        commentViewModel.getCategoryListData(commentViewModel.professionalId!!).observe(this, { listResource ->
            if (listResource != null){
                when(listResource.status){
                    Resource.Status.LOADING -> {
                        if (listResource.data != null){
                            fadeIn(binding.root)
                            replaceData(listResource.data.data!!)
                        }
                    }
                    Resource.Status.SUCCESS -> {
                        if (!listResource.data?.data.isNullOrEmpty()) {
                            replaceData(listResource.data?.data!!)
                        }else{
                            Timber.e("Estoy mostrando el error no hay datos")
                            binding.noItemConstraintLayout.visible()
                        }
                        commentViewModel.setLoadingState(false)
                    }
                    Resource.Status.ERROR -> commentViewModel.setLoadingState(false)
                }
            }else{
                if (commentViewModel.offset > 1){
                    commentViewModel.forceEndLoading = true
                }
            }
        })

        commentViewModel.getLoadingState()?.observe(this, Observer { loadingState ->
            binding.loadingMore = commentViewModel.isLoading
            if (loadingState != null && !loadingState){
                binding.swipeRefresh.isRefreshing = false
            }
        })
    }

    private fun replaceData(categoryList: List<Comment>) {
        adapterComment.submitList(categoryList)
        binding.executePendingBindings()
    }

}