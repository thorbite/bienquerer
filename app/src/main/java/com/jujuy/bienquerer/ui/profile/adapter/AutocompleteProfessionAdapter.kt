package com.jujuy.bienquerer.ui.profile.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Filter
import android.widget.TextView
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.model.entity.SubCategory


class AutoCompleteProfessionAdapter(context: Context, professionList: List<SubCategory>) :
    ArrayAdapter<SubCategory>(context, 0, professionList) {

    private val professionListFull: List<SubCategory> = ArrayList(professionList)


    override fun getFilter(): Filter {
        return countryFilter
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var convertView1: View? = convertView
        if (convertView1 == null) {
            convertView1 = LayoutInflater.from(context).inflate(R.layout.profession_autocomplete_row, parent, false)
        }
        val textViewName: TextView = convertView1!!.findViewById(R.id.text_view_name_profession)
        val textViewNameCategory: TextView = convertView1.findViewById(R.id.text_view_name_category)

        val professionItem: SubCategory? = getItem(position)
        if (professionItem != null) {
            textViewName.text = professionItem.nameSubCategory.capitalize()
            textViewNameCategory.text = professionItem.categorySubCategory?.nameCategory?.toLowerCase()
                ?.capitalize() ?: ""

        }
        return convertView1
    }

    private val countryFilter: Filter = object : Filter() {
        override fun performFiltering(constraint: CharSequence?): FilterResults {
            val results = FilterResults()
            val suggestions: MutableList<SubCategory> = ArrayList()
            if (constraint == null || constraint.isEmpty()) {
                suggestions.addAll(professionListFull)
            } else {
                val filterPattern =
                    constraint.toString().toLowerCase().trim { it <= ' ' }
                for (item in professionListFull) {
                    if (item.nameSubCategory.toLowerCase().contains(filterPattern)) {
                        suggestions.add(item)
                    }
                }
            }
            results.values = suggestions
            results.count = suggestions.size
            return results
        }

        override fun publishResults(constraint: CharSequence?, results: FilterResults) {
            clear()
            addAll(results.values as List<SubCategory>)
            notifyDataSetChanged()
        }

        override fun convertResultToString(resultValue: Any): CharSequence {
            return (resultValue as SubCategory).nameSubCategory
        }
    }

}