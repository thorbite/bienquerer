package com.jujuy.bienquerer.ui.appointment.professional

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Toast
import androidx.core.util.Pair
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.datepicker.MaterialDatePicker
import com.jujuy.bienquerer.base.BaseFragment
import com.jujuy.bienquerer.databinding.FragmentAppointmentProfessionalBinding
import com.jujuy.bienquerer.model.network.response.Job
import com.jujuy.bienquerer.ui.appointment.adapter.MyJobAdapter
import com.jujuy.bienquerer.ui.notifications.LoadingAdapter
import com.jujuy.bienquerer.utils.autoCleared
import com.jujuy.bienquerer.viewmodel.jobs.AppointmentViewModel
import kotlinx.android.synthetic.main.fragment_appointment_professional.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class AppointmentProfessionalFragment : BaseFragment(), MyJobAdapter.OnListInteractionListener {


    private var jobProfessionalList: kotlinx.coroutines.Job? = null
    private val appointmentViewModel by viewModel<AppointmentViewModel>()
    private var binding by autoCleared<FragmentAppointmentProfessionalBinding>()
    private var adapter = MyJobAdapter(this)
    private var data: List<Job>? = null

    companion object {
        fun newInstance() = AppointmentProfessionalFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentAppointmentProfessionalBinding.inflate(inflater, container, false)
        setHasOptionsMenu(true)
        return binding.root
    }


    override fun initUIAndActions() {
        appointmentViewModel.setListJobObj()

        binding.appointmentSort.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

            override fun onItemSelected(
                parentView: AdapterView<*>?,
                selectedItemView: View?,
                position: Int,
                id: Long
            ) {
                sortByStatus(position)
            }
        }
        binding.aptFilterDate.setOnClickListener {
            val builder: MaterialDatePicker.Builder<Pair<Long, Long>> = MaterialDatePicker.Builder.dateRangePicker()
            builder.setTitleText("Elige un rango de fechas")
            val picker = builder.build()
            picker.show(childFragmentManager, picker.toString())
            picker.addOnPositiveButtonClickListener {pair ->
                lifecycleScope.launch{
                    appointmentViewModel.getJobFilteredByDate(pair.first!!, pair.second!!).collectLatest {
                        adapter.submitData(it)
                    }

                }
            }
            picker.addOnNegativeButtonClickListener {
                Timber.e("Salir del picker")
            }
        }



    }

    override fun initAdapters() {
        binding.listJobsRv.layoutManager = LinearLayoutManager(requireContext())
        binding.listJobsRv.adapter = adapter.withLoadStateHeaderAndFooter(
            header = LoadingAdapter { adapter.retry() },
            footer = LoadingAdapter { adapter.retry() }
        )
        adapter.addLoadStateListener { loadState ->
            binding.progressBar.isVisible = loadState.source.refresh is LoadState.Loading
            binding.retryButton.isVisible = loadState.source.refresh is LoadState.Error

            // Toast on any error, regardless of whether it came from RemoteMediator or PagingSource
            val errorState = loadState.source.append as? LoadState.Error
                ?: loadState.source.prepend as? LoadState.Error
                ?: loadState.append as? LoadState.Error
                ?: loadState.prepend as? LoadState.Error
            errorState?.let {
                Toast.makeText(
                    requireContext(),
                    "\uD83D\uDE28 Wooops ${it.error}",
                    Toast.LENGTH_LONG
                ).show()
            }

            if (loadState.source.refresh is LoadState.NotLoading && adapter.itemCount < 1) {
                Timber.e("Ocultar lista ${adapter.itemCount < 1} ${loadState.refresh.endOfPaginationReached} ${loadState.source.refresh is LoadState.NotLoading}")
                binding.listJobsRv.isVisible = false
                binding.noItemConstraintLayout.isVisible = true
            } else {
                Timber.e("Mostrar lista ${adapter.itemCount < 1} ${loadState.refresh.endOfPaginationReached} ${loadState.source.refresh is LoadState.NotLoading}")
                binding.listJobsRv.isVisible = true
                binding.noItemConstraintLayout.isVisible = false
            }
        }
    }

    override fun initData() {
        lifecycleScope.launch {
            appointmentViewModel.getJobListData().collectLatest {
                Timber.e("Fragment list dataaaa")
                adapter.submitData(it)
            }
        }

        /*appointmentViewModel.getListJobData().observe(this, Observer { response ->
            when (response.status) {
                Resource.Status.LOADING -> {
                }
                Resource.Status.SUCCESS -> {
                    if (response.data?.isNotEmpty()!!) {
                        noItemConstraintLayout.gone()
                        containerJobs.visible()
                        data = response.data
                        adapter.submitList(data!!.reversed())
                    } else {
                        noItemConstraintLayout.visible()
                        containerJobs.gone()
                    }
                }
                Resource.Status.ERROR -> {
                }
            }
        })*/
    }


    fun sortByStatus(id: Int) {
        if (id == 0){
            initData()
        }else{
            lifecycleScope.launch {
                appointmentViewModel.getJobFilteredByStatus(id).collectLatest {
                    Timber.e("Fragment list dataaaa")
                    adapter.submitData(it)
                }
            }
        }
    }

    override fun onClickJob(item: Job) {
        navigationController.navigateToDetailJob(requireActivity(), item)
    }

}