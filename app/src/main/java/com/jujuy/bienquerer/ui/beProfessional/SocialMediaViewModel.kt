package com.jujuy.bienquerer.ui.beProfessional

import androidx.lifecycle.*
import com.jujuy.bienquerer.api.network.Resource
import com.jujuy.bienquerer.model.MySpinnerItem
import com.jujuy.bienquerer.model.SocialNetworks
import com.jujuy.bienquerer.repository.ProfessionalRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.map
import timber.log.Timber
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class SocialMediaViewModel
constructor(private val professionalRepository: ProfessionalRepository) : ViewModel() {
    private var listSocialNetworksData: LiveData<Resource<List<SocialNetworks>>>? = null
    private var listSocialNetworksObj = MutableLiveData<String>()
    val selected = MutableLiveData<MySpinnerItem>()

    init {
        listSocialNetworksObj.value = ""
    }

    fun setSpinnerItem(item: MySpinnerItem?) {
        Timber.e(item?.text.toString())
        selected.postValue(item)
    }

    fun getAllSocialNetworks(): LiveData<Resource<List<SocialNetworks>>>? = listSocialNetworksObj.switchMap {
        professionalRepository.getAllSocialNetworks().map {
            when(it.status){
                Resource.Status.LOADING ->{
                    Resource.loading()
                }
                Resource.Status.SUCCESS ->{
                    Resource.success(it.data?.data)
                }
                Resource.Status.ERROR -> {
                    Resource.error(it.message!!)
                }
            }
        }.asLiveData(viewModelScope.coroutineContext)
    }
}