package com.jujuy.bienquerer.ui.professionals.comments

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.api.network.AppExecutors
import com.jujuy.bienquerer.databinding.ItemCommentListBinding
import com.jujuy.bienquerer.model.entity.Comment
import com.jujuy.bienquerer.utils.DataBoundListAdapter
import com.jujuy.bienquerer.ui.common.custom.DataBoundViewHolder
import com.jujuy.bienquerer.utils.Utils
import com.perfomer.blitz.setTimeAgo
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlin.time.ExperimentalTime

class ListCommentsAdapter(appExecutors: AppExecutors):
    DataBoundListAdapter<Comment, ItemCommentListBinding>(
        appExecutors = appExecutors,
        diffCallback = object : DiffUtil.ItemCallback<Comment>(){
            override fun areItemsTheSame(oldItem: Comment, newItem: Comment): Boolean {
                return oldItem.id == newItem.id && oldItem.comment == newItem.comment
            }

            override fun areContentsTheSame(oldItem: Comment, newItem: Comment): Boolean {
                return oldItem.id == newItem.id && oldItem.comment == newItem.comment
            }
        }

) {
    private var lastPosition = -1
    override fun createBinding(parent: ViewGroup): ItemCommentListBinding {
        return DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.item_comment_list, parent, false)
    }

    override fun bind(binding: ItemCommentListBinding, item: Comment) {
        binding.comment = item
    }

    @ExperimentalCoroutinesApi
    @ExperimentalTime
    override fun bindView(holder: DataBoundViewHolder<ItemCommentListBinding>, position: Int) {
        super.bindView(holder, position)
        setAnimation(holder.itemView, position)
        holder.binding.dateCommentTxt.setTimeAgo(Utils.toMilliseconds(getItem(position).createdAt), showSeconds = true, autoUpdate = true)
    }

    private fun setAnimation(viewToAnimate: View?, position: Int) {
        if (position > lastPosition)  {
            val animation = android.view.animation.AnimationUtils.loadAnimation(
                viewToAnimate?.context,
                R.anim.slide_in_bottom
            )
            viewToAnimate?.startAnimation(animation)
            lastPosition = position
        }
    }

}