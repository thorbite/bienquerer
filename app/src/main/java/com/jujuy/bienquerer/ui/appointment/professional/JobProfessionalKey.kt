package com.jujuy.bienquerer.ui.appointment.professional

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "job_professional_keys")
class JobProfessionalKey (
    @PrimaryKey
    val jobProfessionalId: Int,
    val prevKey: Int?,
    val nextKey: Int?
    )