package com.jujuy.bienquerer.ui.notifications.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.lifecycle.Observer
import com.jujuy.bienquerer.api.network.Resource
import com.jujuy.bienquerer.base.BaseFragment
import com.jujuy.bienquerer.databinding.FragmentNotificationDetailBinding
import com.jujuy.bienquerer.model.network.response.ListNotifications
import com.jujuy.bienquerer.ui.appointment.bookAppointment.BookAppointmentDetailActivity
import com.jujuy.bienquerer.utils.Constants
import com.jujuy.bienquerer.utils.autoCleared
import com.jujuy.bienquerer.viewmodel.notifications.NotificationsViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class NotificationDetailFragment : BaseFragment() {
    private var binding by autoCleared<FragmentNotificationDetailBinding>()
    private val notificationViewModel by viewModel<NotificationsViewModel>()
    private var notiId: String? = null

    companion object {
        fun newInstance() = NotificationDetailFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentNotificationDetailBinding.inflate(inflater, container, false)
        activity?.let {
            it.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
        }
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun initUIAndActions() {
        try {
            if (activity != null) {
                if (requireActivity().intent.extras != null) {
                    val NOTI_ID_KEY: String = Constants.NOTI_ID
                    notiId = requireActivity().intent.extras!!.getString(NOTI_ID_KEY)
                }
            }
        } catch (e: Exception) {
            Timber.e(e)
        }

        binding.description.setOnClickListener {
            navigationController.navigateToJobList(requireActivity())
            (activity as NotificationDetailActivity).changeTextToolbar("Lista de trabajos")
        }

        notiId?.let { notificationViewModel.setNotificationItem(it) }
        notificationViewModel.getNotificationItem()?.observe(this, Observer { item ->
                when(item.status){
                    Resource.Status.LOADING ->{
                        if (item.data != null) {
                            fadeIn(binding.root)
                            replaceNotificationDetailData(item.data)
                        }
                    }
                    Resource.Status.SUCCESS ->{
                        if (item.data != null) {
                            replaceNotificationDetailData(item.data)
                        }
                        notificationViewModel.setLoadingState(false)
                    }
                    Resource.Status.ERROR ->{notificationViewModel.setLoadingState(false)}
                }
        })
    }

    override fun initAdapters() {

    }

    override fun initData() {

    }

    private fun replaceNotificationDetailData(noti: ListNotifications) {
        binding.notification = noti
    }

    override fun onDestroy() {
        navigationController.navigateBackFromNotiList(requireActivity())
        super.onDestroy()
    }

}