package com.jujuy.bienquerer.ui.home

import com.jujuy.bienquerer.model.ItemImageSlider

class DataImgSlider{
    companion object {
        fun createDataSet() : ArrayList<ItemImageSlider>{
            val list = ArrayList<ItemImageSlider>()
            list.add(ItemImageSlider("Salud", "https://www.clickdoctors.es/wp-content/uploads/2017/08/clickdoctors-equipo-medico.jpg"))
            list.add(ItemImageSlider ("Eventos", "https://www.newstatesman.com/sites/all/themes/creative-responsive-theme/images/new_statesman_events.jpg"))
            list.add(ItemImageSlider ("Plomeria", "https://www.guide-artisan-centre.fr/img/actualites/les-differents-domaines-de-la-plomberie.jpg"))
            return list
        }
    }
}