package com.jujuy.bienquerer.ui.appointment.jobDetailProfessional

import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.base.BaseActivity
import com.jujuy.bienquerer.databinding.JobDetailProfessionalActivityBinding
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlin.time.ExperimentalTime
@ExperimentalCoroutinesApi
@ExperimentalTime
class JobDetailProfessionalActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: JobDetailProfessionalActivityBinding = DataBindingUtil.setContentView(this,R.layout.job_detail_professional_activity)
        initUI(binding)
    }


    private fun initUI(binding: JobDetailProfessionalActivityBinding) {
        initToolbar(binding.toolbar, "Detalles")
        setupFragment(JobDetailProfessionalFragment())
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val fragment = supportFragmentManager.findFragmentById(R.id.content_frame)
        fragment?.let {
            it.onActivityResult(requestCode,resultCode,data)
        }
    }
}