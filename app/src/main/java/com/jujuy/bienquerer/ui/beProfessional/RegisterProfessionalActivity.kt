package com.jujuy.bienquerer.ui.beProfessional

import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.base.BaseActivity
import com.jujuy.bienquerer.databinding.ActivityRegisterProfessionalBinding
import com.skydoves.whatif.whatIfNotNull
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class RegisterProfessionalActivity : BaseActivity() {

    val binding :  ActivityRegisterProfessionalBinding by lazy {
        DataBindingUtil.setContentView<ActivityRegisterProfessionalBinding>(this,R.layout.activity_register_professional) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       iniUI(binding)
    }

    private fun iniUI(binding: ActivityRegisterProfessionalBinding) {
        initToolbar(binding.toolbar, "Ser Profesional")
        setupFragment(RegisterProfessionalFragment())
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val fragment = supportFragmentManager.findFragmentById(R.id.content_frame)
        fragment.whatIfNotNull {
            it.onActivityResult(requestCode,resultCode,data)
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }
}