package com.jujuy.bienquerer.ui.professionals.map

import androidx.paging.PagingSource
import com.jujuy.bienquerer.api.service.ProfessionalService
import com.jujuy.bienquerer.model.ItemParameterHolder
import com.jujuy.bienquerer.model.entity.Professional
import com.jujuy.bienquerer.model.network.request.MapListRequest
import retrofit2.HttpException
import java.io.IOException

private const val STARTING_PAGE_INDEX = 1
class MapListPagingSource(private val itemParameterHolder: ItemParameterHolder,
                                private val professionalService: ProfessionalService
): PagingSource<Int, Professional>() {
    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Professional> {
        val position = STARTING_PAGE_INDEX
        return try {
            val response = professionalService.getMapList(
                MapListRequest(
                    itemParameterHolder.lat,
                    itemParameterHolder.lng,
                    itemParameterHolder.mapMiles,
                    itemParameterHolder.cat_id,
                    itemParameterHolder.sub_cat_id
                ))
            val repos = response.data ?: emptyList()
            LoadResult.Page(
                data = repos,
                prevKey =  null,
                nextKey = null
            )
        } catch (exception: IOException) {
            LoadResult.Error(exception)
        } catch (exception: HttpException) {
            LoadResult.Error(exception)
        }
    }

}