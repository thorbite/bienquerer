package com.jujuy.bienquerer.ui.chat.detail

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.base.BaseActivity
import com.jujuy.bienquerer.databinding.ActivityChatDetailBinding
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class ChatDetailActivity : BaseActivity() {
    lateinit var binding: ActivityChatDetailBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_chat_detail)
        initUI(binding)
    }

    private fun initUI(binding: ActivityChatDetailBinding) {
        setupFragment(ChatDetailFragment())
    }

    fun changeText(text: String?) {
        initToolbar(binding.toolbar, text!!)
    }
}