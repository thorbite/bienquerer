package com.jujuy.bienquerer.ui.editprofile

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.base.BaseActivity
import com.jujuy.bienquerer.databinding.ProfileEditActivityBinding
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class ProfileEditActivity : BaseActivity() {
    val binding : ProfileEditActivityBinding by lazy {
        DataBindingUtil.setContentView<ProfileEditActivityBinding>(this, R.layout.profile_edit_activity) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initUI(binding)
    }

    private fun initUI(binding: ProfileEditActivityBinding) {
        initToolbar(binding.toolbar, "Editar Perfil")
        setupFragment(ProfileEditFragment())
    }
}