package com.jujuy.bienquerer.ui.appointment.selectProfessionItem

import android.graphics.Color
import android.view.View
import android.widget.CheckedTextView
import androidx.recyclerview.widget.RecyclerView
import com.jujuy.bienquerer.R

class SelectableViewHolder(itemView: View, listener: OnItemSelectedListener) : RecyclerView.ViewHolder(itemView) {
    companion object {
        const val MULTI_SELECTION = 2
        const val SINGLE_SELECTION = 1
    }
    var textView: CheckedTextView = itemView.findViewById(R.id.checked_text_item)
    var mItem: SelectableItem? = null
    private var itemSelectedListener = listener

    init {
        textView.setOnClickListener{
            if (mItem?.isSelected!! && itemViewType == MULTI_SELECTION) {
                setChecked(false)
            } else {
                setChecked(true)
            }
            itemSelectedListener.onItemSelected(mItem)
        }
    }

    fun setChecked(value: Boolean) {
        if (value) {
            textView.setBackgroundColor(Color.LTGRAY)
        } else {
            textView.background = null
        }
        mItem?.isSelected = value
        textView.isChecked = value
    }


    interface OnItemSelectedListener {
        fun onItemSelected(item: SelectableItem?)
    }
}