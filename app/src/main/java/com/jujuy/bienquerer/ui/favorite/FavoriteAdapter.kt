package com.jujuy.bienquerer.ui.favorite

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.api.ApiConstants
import com.jujuy.bienquerer.model.entity.ProfessionalFav
import com.jujuy.bienquerer.model.network.response.ListNotifications
import com.jujuy.bienquerer.ui.notifications.adapter.NotiAdapter
import com.jujuy.bienquerer.utils.Utils

class FavoriteAdapter(private val listener: OnItemClick): PagingDataAdapter<ProfessionalFav, FavoriteAdapter.FavViewHolder>(UIMODEL_COMPARATOR) {

    var item: MutableList<ProfessionalFav>? = null


    interface OnItemClick {
        fun onItemClick(item: ProfessionalFav, position: Int)
    }

    override fun onBindViewHolder(holder: FavViewHolder, position: Int) {
        getItem(position)?.let {
            item?.add(it)
            holder.bind(it, holder.itemView)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_favorite_list, parent, false)
        return FavViewHolder(view)
    }

    fun deleteItem(pos: Int) {
        notifyItemRemoved(pos)
    }

    fun getNotificationAt(position: Int): ProfessionalFav? {
        return getItem(position)
    }


    @SuppressLint("ClickableViewAccessibility")
    inner class FavViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val name: TextView = view.findViewById(R.id.professional_name)
        private val professions: TextView = view.findViewById(R.id.professional_category)
        private val profileImage: ImageView = view.findViewById(R.id.professional_thumbail)

        init {
            itemView.setOnClickListener {
                val position = bindingAdapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    val item = getItem(position)
                    if (item != null) {
                        listener.onItemClick(item, position)
                    }
                }
            }

        }


        fun bind(item: ProfessionalFav, itemView: View) {
            with(item) {
                name.text = favorite.name.toString() + favorite.lastname.toString()
                val sb = StringBuilder()
                for (i in favorite.profession?.indices!!){
                    sb.append(favorite.profession!![i].nameSubCategory + "| ")
                }
                professions.text = sb

                if (favorite.photo_profile_url.isNullOrEmpty()){
                    profileImage.load(R.drawable.default_image)
                }else{
                    val urlComplete = ApiConstants.APP_PROFILE_IMAGES + favorite.photo_profile_url
                    profileImage.load(urlComplete) {
                        crossfade(true)
                        placeholder(R.drawable.default_image)
                    }
                }

            }

        }
    }

    companion object {
        private val UIMODEL_COMPARATOR = object : DiffUtil.ItemCallback<ProfessionalFav>() {
            override fun areItemsTheSame(
                oldItem: ProfessionalFav,
                newItem: ProfessionalFav
            ): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(
                oldItem: ProfessionalFav,
                newItem: ProfessionalFav
            ): Boolean =
                oldItem == newItem
        }
    }
}