package com.jujuy.bienquerer.ui.auth.login

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Toast
import androidx.lifecycle.Observer
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.GraphRequest
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.messaging.FirebaseMessaging
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.api.network.Resource
import com.jujuy.bienquerer.base.BaseFragment
import com.jujuy.bienquerer.databinding.UserLoginFragmentBinding
import com.jujuy.bienquerer.model.entity.User
import com.jujuy.bienquerer.utils.*
import com.jujuy.bienquerer.viewmodel.user.UserViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.json.JSONException
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class UserLoginFragment : BaseFragment() {
    private val userViewModel by viewModel<UserViewModel>()
    private var binding by autoCleared<UserLoginFragmentBinding>()
    private lateinit var userEmail: String
    private lateinit var userPassword: String
    private lateinit var callbackManager: CallbackManager
    var first_name = ""
    var last_name = ""
    var email = ""
    var id = ""
    var imageURL = ""
    private val fcmToken by lazy{ sessionManager.fetchTokenFirebase()}

    private val psDialogMsg: PSDialogMsg by lazy { PSDialogMsg(activity, false) }
    private val loadingMsg: LoadingMsg by lazy { LoadingMsg(requireActivity(),false) }

    private val gso: GoogleSignInOptions by lazy{ GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
        .requestIdToken(resources.getString(R.string.google_id))
        .requestEmail()
        .build() }
    private val mGoogleSignInClient: GoogleSignInClient by lazy { GoogleSignIn.getClient(requireActivity(), gso) }

    companion object {
        fun newInstance() = UserLoginFragment()
        private const val GOOGLE_SIGN = 123
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = UserLoginFragmentBinding.inflate(inflater, container, false)
        activity?.let {
            it.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
        }
        if (!sessionManager.isLoggedIn()){
            signOut()
        }
        return binding.root
    }

    override fun initUIAndActions() {
        Timber.d("LATITUD $selectedLat")
        Timber.d("LONGITUD $selectedLng")
        getTokenNotifications()

        callbackManager = CallbackManager.Factory.create()

        fadeIn(binding.root)

        binding.jumpNotLogin.setOnClickListener {
            navigationController.navigateToMainActivity(requireActivity())
        }

        binding.emailSignInButton.setOnClickListener { view ->
            Utils.hideKeyboard(requireActivity())
            if (connectivity.isConnected) {
                userEmail = binding.email.text.toString().trim()
                userPassword = binding.password.text.toString().trim()
                if (userEmail == "") {
                    psDialogMsg.showWarningDialog(
                        getString(R.string.error_message_blank_email),
                        getString(R.string.app_ok)
                    )
                    psDialogMsg.show()
                    return@setOnClickListener
                }
                if (userPassword == "") {
                    psDialogMsg.showWarningDialog(
                        getString(R.string.error_message_blank_password),
                        getString(R.string.app_ok)
                    )
                    psDialogMsg.show()
                    return@setOnClickListener
                }

                if (userPassword.length < 4) {
                    psDialogMsg.showWarningDialog(
                        getString(R.string.error_message_small_password),
                        getString(R.string.app_ok)
                    )
                    psDialogMsg.show()
                    return@setOnClickListener
                }

                doLogin(userEmail, userPassword)

            } else {
                psDialogMsg.showWarningDialog(
                    getString(R.string.no_internet_error),
                    getString(R.string.app_ok)
                )
                psDialogMsg.show()
            }
        }
        binding.signup.setOnClickListener {
            Utils.navigateAfterRegister(requireActivity(), navigationController)
        }
        binding.viewForgotPassword.setOnClickListener {
            Toast.makeText(requireContext(), "En desarrollo...", Toast.LENGTH_SHORT).show()
            //Utils.navigateAfterForgotPassword(requireActivity(), navigationController)
        }
        binding.btnGoogleSignIn.setOnClickListener { signInGoogle() }

        binding.btnFacebookSignIn.setOnClickListener { signInFacebook()}
    }

    private fun getTokenNotifications() {
        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                Timber.w("Fetching FCM registration token failed ${task.exception}")
                return@OnCompleteListener
            }
            val token = task.result
            Timber.d("TOKEN DESDE LOGIN: $token")
            token?.let { sessionManager.saveTokenFirebase(it) }
        })
    }

    private fun signInFacebook() {
        LoginManager.getInstance().logInWithReadPermissions(this, listOf("public_profile", "email"))
        LoginManager.getInstance().registerCallback(callbackManager, object : FacebookCallback<LoginResult?> {
                override fun onSuccess(result: LoginResult?) {
                    val request: GraphRequest = GraphRequest.newMeRequest(result?.accessToken) { `object`, response ->

                        try {
                            if(`object` != null && `object`.has("last_name")){

                                last_name = `object`.getString("last_name")
                            }
                        }catch (e: JSONException){
                            e.printStackTrace()
                        }
                        try {
                            if(`object` != null && `object`.has("email")){

                                email = `object`.getString("email")
                            }
                        }catch (e: JSONException){
                            e.printStackTrace()
                        }
                        try {
                            if(`object` != null && `object`.has("id")){

                                id = `object`.getString("id")
                            }
                        }catch (e: JSONException){
                            e.printStackTrace()
                        }
                        try {
                            if(`object` != null && `object`.has("first_name")){

                                first_name = `object`.getString("first_name")
                            }
                        }catch (e: JSONException){
                            e.printStackTrace()
                        }

                        // Facebook Profile Pic URL
                        if (`object`.has("picture")) {
                            val facebookPictureObject = `object`.getJSONObject("picture")
                            if (facebookPictureObject.has("data")) {
                                val facebookDataObject = facebookPictureObject.getJSONObject("data")
                                if (facebookDataObject.has("url")) {
                                    val facebookProfilePicURL = facebookDataObject.getString("url")
                                    Timber.i("Facebook Profile Pic URL: %s", facebookProfilePicURL)
                                    imageURL = facebookProfilePicURL
                                }
                            }
                        } else {
                            Timber.i("Facebook Profile Pic URL: %s", "Not exists")
                        }

                        if (id != "") {
                            userViewModel.registerFBUser(id, first_name, last_name, email, imageURL, fcmToken)
                        }
                    }
                    val parameters = Bundle()
                    parameters.putString("fields", "email,first_name, last_name,id")
                    request.parameters = parameters
                    request.executeAsync()

                }

                override fun onCancel() {
                    Timber.d("OnCancel")
                }

                override fun onError(error: FacebookException?) {
                    Timber.e("OnError $error")
                    psDialogMsg.showErrorDialog("No se pudo registrar su facebook", getString(R.string.app__ok))
                    psDialogMsg.show()
                }

            })

    }

    private fun doLogin(userEmail: String, userPassword: String) {
        userViewModel.setUserLogin(User(password = userPassword, email = userEmail, person = null, fcmKey = fcmToken))
    }

    private fun signInGoogle() {
        val signInIntent = mGoogleSignInClient.signInIntent
        startActivityForResult(signInIntent, GOOGLE_SIGN)
    }

    fun signOut() {
        mGoogleSignInClient.signOut()
    }

    override fun initAdapters() {}

    override fun initData() {
        userViewModel.getUserLoginStatus().observe(this, { listResource ->
                when(listResource.status){
                    Resource.Status.LOADING -> {
                        loadingMsg.showloaderDialog()
                        loadingMsg.show()
                    }
                    Resource.Status.SUCCESS -> {
                        loadingMsg.cancel()
                            try {
                                listResource.data?.authToken?.let { sessionManager.saveAuthToken(it) }
                                Utils.navigateAfterUserLogin(requireActivity(),navigationController)
                            }catch (ne: NullPointerException){
                                Timber.e("ERROR!!! $ne")
                            }
                    }
                    Resource.Status.ERROR ->{
                        loadingMsg.cancel()
                        Timber.e(listResource.message)
                        if(listResource.message == "Invalid credentials."){
                            psDialogMsg.showErrorDialog("Usuario o contraseña incorrectos", getString(R.string.app__ok))
                            psDialogMsg.show()
                        }else{
                            psDialogMsg.showErrorDialog("Debe validar su email para ingresar a su cuenta", getString(R.string.app__ok))
                            psDialogMsg.show()
                        }
                    }
                }
        })

        userViewModel.getGoogleLoginData()?.observe(this, { listResource ->
                when(listResource.status){
                    Resource.Status.LOADING ->{
                        loadingMsg.showloaderDialog()
                        loadingMsg.show()
                    }
                    Resource.Status.SUCCESS ->{
                        loadingMsg.cancel()
                        listResource.data?.authToken?.let { sessionManager.saveAuthToken(it) }
                        Utils.navigateAfterUserLogin(requireActivity(),navigationController)
                    }
                    Resource.Status.ERROR ->{
                        loadingMsg.cancel()
                        psDialogMsg.showErrorDialog(listResource.message, "OK")
                        psDialogMsg.show()
                        signOut()
                    }
            }
        })

        userViewModel.getRegisterFBUserData()?.observe(this, { listResource ->
                when (listResource.status){
                    Resource.Status.LOADING ->{
                        loadingMsg.showloaderDialog()
                        loadingMsg.show()
                    }
                    Resource.Status.SUCCESS -> {
                        loadingMsg.cancel()
                        listResource.data?.authToken?.let { sessionManager.saveAuthToken(it) }
                        Utils.navigateAfterUserLogin(requireActivity(), navigationController)
                    }
                    Resource.Status.ERROR -> {
                        loadingMsg.cancel()
                        psDialogMsg.showErrorDialog(listResource.message, getString(R.string.app__ok))
                        psDialogMsg.show()
                        LoginManager.getInstance().logOut()
                    }
                }
        })

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        callbackManager.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == GOOGLE_SIGN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                handleSignInResult(task)
            }catch (e: Exception){
                Timber.e("ERROR $e")
            }
        }
    }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>?) {
        try {
            val account = completedTask?.getResult(ApiException::class.java)
            userViewModel.setGoogleLoginUser(account?.id,
                account?.givenName,account?.familyName,
                account?.email, account?.photoUrl.toString(), fcmToken)

        }catch (e: ApiException) {
            Timber.e(e)
        }
    }
}
