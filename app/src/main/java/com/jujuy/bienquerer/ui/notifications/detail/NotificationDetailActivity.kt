package com.jujuy.bienquerer.ui.notifications.detail

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.base.BaseActivity
import com.jujuy.bienquerer.databinding.ActivityNotificationDetailBinding
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class NotificationDetailActivity : BaseActivity() {
    private lateinit var binding: ActivityNotificationDetailBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_notification_detail)
        initUI(binding)
    }

    private fun initUI(binding: ActivityNotificationDetailBinding) {
        initToolbar(binding.toolbar, "Detalles")
        setupFragment(NotificationDetailFragment())
    }

    fun changeTextToolbar(text: String?) {
        setToolbarText(binding.toolbar, text!!)
    }

    override fun onBackPressed() {
        finish()
    }
}
