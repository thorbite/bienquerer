package com.jujuy.bienquerer.ui.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.api.network.AppExecutors
import com.jujuy.bienquerer.databinding.ItemCategory2Binding
import com.jujuy.bienquerer.model.entity.Category
import com.jujuy.bienquerer.utils.DataBoundListAdapter

class CategoryHomeAdapter(appExecutors: AppExecutors, val callback: HomeCategoryClickCallback) :
    DataBoundListAdapter<Category, ItemCategory2Binding>(
        appExecutors= appExecutors,
        diffCallback = object : DiffUtil.ItemCallback<Category>() {
            override fun areItemsTheSame(oldItem: Category, newItem: Category): Boolean {
                return oldItem.idCategory == newItem.idCategory
                        && oldItem.nameCategory == newItem.nameCategory
            }

            override fun areContentsTheSame(oldItem: Category, newItem: Category): Boolean {
                return oldItem.idCategory == newItem.idCategory
                        && oldItem.nameCategory == newItem.nameCategory
            }
        }
    ) {
    private val lastPosition = -1

    interface HomeCategoryClickCallback {
        fun onClick(category: Category?)
    }

    override fun createBinding(parent: ViewGroup): ItemCategory2Binding {
        val binding: ItemCategory2Binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context),
            R.layout.item_category_2, parent, false)
        binding.root.setOnClickListener { v ->
            val category: Category? = binding.category
            callback.onClick(category)
        }
        return binding
    }

    override fun bind(binding: ItemCategory2Binding, item: Category) {
        binding.category = item
    }
}