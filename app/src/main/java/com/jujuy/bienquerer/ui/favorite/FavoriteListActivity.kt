package com.jujuy.bienquerer.ui.favorite

import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.base.BaseActivity
import com.jujuy.bienquerer.databinding.ActivityFavouriteListBinding
import com.jujuy.bienquerer.databinding.ActivityProfileBinding
import com.skydoves.whatif.whatIfNotNull
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class FavoriteListActivity : BaseActivity() {

    val binding : ActivityFavouriteListBinding by lazy {
        DataBindingUtil.setContentView<ActivityFavouriteListBinding>(this, R.layout.activity_favourite_list) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initUI(binding)
    }

    private fun initUI(binding: ActivityFavouriteListBinding) {
        initToolbar(binding.toolbar, "Favoritos")
        setupFragment(FavoriteFragment())
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val fragment = supportFragmentManager.findFragmentById(R.id.content_frame)
        fragment.whatIfNotNull {
            it.onActivityResult(requestCode,resultCode,data)
        }
    }
}