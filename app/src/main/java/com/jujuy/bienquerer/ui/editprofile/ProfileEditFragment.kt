package com.jujuy.bienquerer.ui.editprofile

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Toast
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.github.dhaval2404.imagepicker.ImagePicker
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.api.ApiConstants
import com.jujuy.bienquerer.api.network.Resource
import com.jujuy.bienquerer.base.BaseFragment
import com.jujuy.bienquerer.databinding.ProfileEditFragmentBinding
import com.jujuy.bienquerer.model.entity.Person
import com.jujuy.bienquerer.model.entity.Rol
import com.jujuy.bienquerer.model.entity.User
import com.jujuy.bienquerer.viewmodel.user.UserViewModel
import com.jujuy.bienquerer.utils.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import java.io.File
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class ProfileEditFragment : BaseFragment() {

    companion object {
        fun newInstance() = ProfileEditFragment()
    }

    private val userViewModel by viewModel<UserViewModel>()
    private val psDialogMsg: PSDialogMsg by lazy { PSDialogMsg(activity, false)}
    private val loadingMsg: LoadingMsg by lazy { LoadingMsg(requireActivity(), false) }
    private var binding by autoCleared<ProfileEditFragmentBinding>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = ProfileEditFragmentBinding.inflate(inflater, container, false)
        activity?.let {
            it.window?.setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN
            )
        }
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }


    override fun initAdapters() {}

    override fun initData() {
        userViewModel.getLoginUser(loginUserId).observe(this, { dataUser ->
            if (dataUser != null) {
                fadeIn(binding.root)
                binding.user = dataUser
                userViewModel.user = dataUser
            }
        })

        userViewModel.getUpdateUserData()?.observe(this, Observer { listResource ->
                when (listResource.status) {
                    Resource.Status.LOADING -> {
                        loadingMsg.showloaderDialog()
                        loadingMsg.show()
                    }
                    Resource.Status.SUCCESS -> {
                        loadingMsg.cancel()
                        if (listResource.data?.code == 200) {
                            psDialogMsg.showSuccessDialog(
                                listResource.data.message,
                                getString(R.string.app__ok)
                            )
                            psDialogMsg.show()
                        } else {
                            psDialogMsg.showErrorDialog(
                                listResource.data?.message,
                                getString(R.string.message_ok_close)
                            )
                            psDialogMsg.show()
                        }

                    }
                    Resource.Status.ERROR -> {
                        loadingMsg.cancel()
                        psDialogMsg.showErrorDialog(
                            listResource.message,
                            getString(R.string.app__ok)
                        )
                        psDialogMsg.show()
                    }
                }
        })

    }

    override fun initUIAndActions() {
        getDataFromIntent()
        if (context != null) {
            binding.userNameEditText.setHint(R.string.edit_profile__user_name)
            binding.userEmailEditText.setHint(R.string.edit_profile__email)
            binding.userPhoneEditText.setHint(R.string.edit_profile__phone)
            binding.usernameEditText.setHint(R.string.edit_profile__username)
            binding.userLastNameEditText.setHint(R.string.edit_profile__user_lastname)
            binding.nameTextView.text =
                requireContext().getString(R.string.edit_profile__user_name)
            binding.emailTextView.text =
                requireContext().getString(R.string.edit_profile__email)
            binding.phoneTextView.text =
                requireContext().getString(R.string.edit_profile__phone)
        }

        binding.profileImageView.setOnClickListener {
            if (connectivity.isConnected) {
                try {
                    if (Utils.isStoragePermissionGranted(requireActivity())) {
                        ImagePicker.with(this)
                            .cropSquare()                    //Crop image(Optional), Check Customization for more option
                            .compress(1024)            //Final image size will be less than 1 MB(Optional)
                            .galleryMimeTypes(  //Exclude gif images
                                mimeTypes = arrayOf(
                                    "image/png",
                                    "image/jpg",
                                    "image/jpeg"
                                )
                            )
                            .start(Constants.RESULT_LOAD_IMAGE_PROFILE)
                        //val i = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                        //startActivityForResult(i, Constants.RESULT_LOAD_IMAGE)
                    }
                } catch (e: Exception) {
                    Timber.e("ERROR $e")
                }
            } else {
                psDialogMsg.showWarningDialog(
                    getString(R.string.no_internet_error),
                    getString(R.string.app__ok)
                )
                psDialogMsg.show()
            }
        }

        binding.saveButton.setOnClickListener {
            userViewModel.setLoadingState(true)
            editProfileData()
        }

        binding.passwordChangeButton.setOnClickListener {
            //navigationController.navigateToPasswordChangeActivity(activity)
        }

        binding.userCoverBlurImageView.setOnClickListener {
            try {
                if (Utils.isStoragePermissionGranted(requireActivity())) {
                    ImagePicker.with(this)
                        .crop(16f, 9f)                   //Crop image(Optional), Check Customization for more option
                        .compress(1024)            //Final image size will be less than 1 MB(Optional)
                        .galleryMimeTypes(  //Exclude gif images
                            mimeTypes = arrayOf(
                                "image/png",
                                "image/jpg",
                                "image/jpeg"
                            )
                        )
                        .start(Constants.RESULT_LOAD_IMAGE_BANNER)
                    //val i = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                    //startActivityForResult(i, Constants.RESULT_LOAD_IMAGE)
                }
            } catch (e: Exception) {
                Timber.e("ERROR $e")
            }

        }
    }

    private fun getDataFromIntent() {
        if (userIsProfessional){
            binding.userPhoneEditText.isEnabled = false
            binding.userPhoneEditText.isClickable = false
            binding.userPhoneEditText.alpha = 0.5f
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        try {
            if (requestCode == Constants.RESULT_LOAD_IMAGE_PROFILE && resultCode == Constants.RESULT_OK) {
                //Image Uri will not be null for RESULT_OK
                val fileUri = data?.data
                binding.profileImageView.setImageURI(fileUri)

                //You can get File object from intent
                val file: File? = ImagePicker.getFile(data)

                //You can also get File Path from intent
                val filePath: String? = ImagePicker.getFilePath(data)
                userViewModel.profileImagePath = filePath.toString()
                uploadImage()
            } else if (resultCode == ImagePicker.RESULT_ERROR) {
                Toast.makeText(activity, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
            }

            if (requestCode == Constants.RESULT_LOAD_IMAGE_BANNER && resultCode == Constants.RESULT_OK) {
                val fileUri = data?.data
                binding.userCoverBlurImageView.setImageURI(fileUri)
                val file: File? = ImagePicker.getFile(data)
                val filePath: String? = ImagePicker.getFilePath(data)
                userViewModel.bannerImagePath = filePath.toString()
                uploadCoverImage()
            }else if (resultCode == ImagePicker.RESULT_ERROR) {
                Toast.makeText(activity, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
            }
            /*if (requestCode == Constants.RESULT_LOAD_IMAGE && resultCode == Constants.RESULT_OK && null != data) {
                val selectedImage = data.data
                binding.profileImageView.setImageURI(data.data)
                val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)
                if (activity != null && selectedImage != null) {
                    val cursor = activity!!.contentResolver.query(
                        selectedImage,
                        filePathColumn, null, null, null
                    )
                    if (cursor != null) {
                        cursor.moveToFirst()
                        val columnIndex = cursor.getColumnIndex(filePathColumn[0])
                        userViewModel.profileImagePath = cursor.getString(columnIndex)
                        cursor.close()
                        uploadImage()
                    }
                }
            }*/
        } catch (e: Exception) {
            Timber.e("ERROR $e")
        }
    }

    private fun uploadImage() {
        userViewModel.setLoadingState(true)
        userViewModel.uploadImage(userViewModel.profileImagePath).observe(this, Observer { listResource ->
                if (listResource?.data != null) {
                    Timber.d(listResource.data.toString())
                    userViewModel.setLoadingState(false)
                    Timber.d("Got Data " + listResource.data.person?.photoProfileUrl + listResource.toString())

                } else if (listResource?.message != null) {
                    Timber.d("Message from server.")
                    psDialogMsg.showInfoDialog(listResource.message, getString(R.string.app__ok))
                    psDialogMsg.show()
                    userViewModel.setLoadingState(false)
                } else {
                    userViewModel.setLoadingState(false)
                    Timber.e("Empty Data")
                }
            })
    }

    private fun uploadCoverImage() {
        userViewModel.setLoadingState(true)
        userViewModel.uploadCoverImage(userViewModel.bannerImagePath).observe(this, Observer { listResource ->
            if (listResource?.data != null) {
                userViewModel.setLoadingState(false)
                if (!listResource.data.person?.photoCoverUrl.isNullOrEmpty()) {
                    Timber.e("Image Url ${ listResource.data.person?.photoCoverUrl}")
                    //binding.userCoverBlurImageView.setImageURI(fileUri)
                }else{
                    Timber.e("Error es null lpm")
                }
            } else if (listResource?.message != null) {
                Timber.d("Message from server.")
                psDialogMsg.showInfoDialog(listResource.message, getString(R.string.app__ok))
                psDialogMsg.show()
                userViewModel.setLoadingState(false)
            } else {
                userViewModel.setLoadingState(false)
                Timber.e("Empty Data")
            }
        })
    }

    private fun editProfileData() {
        if (!connectivity.isConnected) {
            psDialogMsg.showWarningDialog(
                getString(R.string.no_internet_error),
                getString(R.string.app__ok)
            )
            psDialogMsg.show()
            return
        }

        /*val userName = binding.userNameEditText.text.toString()
        if (userName == "") {
            psDialogMsg.showWarningDialog(
                getString(R.string.error_message__blank_name),
                getString(R.string.app__ok)
            )
            psDialogMsg.show()
            return
        }
        val userLastName = binding.userLastNameEditText.text.toString()
        if (userLastName == "") {
            psDialogMsg.showWarningDialog(
                getString(R.string.error_message__blank_lastname),
                getString(R.string.app__ok)
            )
            psDialogMsg.show()
            return
        }
        val username = binding.usernameEditText.text.toString()
        if (username == "") {
            psDialogMsg.showWarningDialog(
                getString(R.string.error_message__blank_username),
                getString(R.string.app__ok)
            )
            psDialogMsg.show()
            return
        }
        val userEmail = binding.userEmailEditText.text.toString()
        if (userEmail == "") {
            psDialogMsg.showWarningDialog(
                getString(R.string.error_message__blank_email),
                getString(R.string.app__ok)
            )
            psDialogMsg.show()
            return
        }
        val userPhone = binding.userPhoneEditText.text.toString()
        if (userPhone == "") {
            psDialogMsg.showWarningDialog(
                getString(R.string.error_message__blank_phone),
                getString(R.string.app__ok)
            )
            psDialogMsg.show()
            return
        }
        val userAddress = binding.userAddressEditText.text.toString()
        if (userAddress == "") {
            psDialogMsg.showWarningDialog(
                getString(R.string.error_message__blank_address),
                getString(R.string.app__ok)
            )
            psDialogMsg.show()
            return
        }*/
        if (!checkToUpdateProfile()) {
            updateUserProfile()
        }
    }

    private fun checkToUpdateProfile(): Boolean {
        return binding.userNameEditText.text.toString() == userViewModel.user.person?.name &&
                binding.userLastNameEditText.text.toString() == userViewModel.user.person?.lastname &&
                binding.usernameEditText.text.toString() == userViewModel.user.username &&
                binding.userEmailEditText.text.toString() == userViewModel.user.email &&
                binding.userPhoneEditText.text.toString() == userViewModel.user.phone
                //binding.userAddressEditText.text.toString() == userViewModel.user.person?.address
    }

    private fun updateUserProfile() {
        val rol = Rol(userViewModel.user.rol?.get(0)?.id!!)
        val person = Person(
            name = binding.userNameEditText.text.toString(),
            lastname = binding.userLastNameEditText.text.toString()
            //address = binding.userAddressEditText.text.toString()
        )
        val user = User(
            id = userViewModel.user.id,
            username = binding.usernameEditText.text.toString(),
            person = person, rol = listOf(rol),
            email = binding.userEmailEditText.text.toString(),
            phone = binding.userPhoneEditText.text.toString()
        )
        userViewModel.setUpdateUserObj(user)
    }
}