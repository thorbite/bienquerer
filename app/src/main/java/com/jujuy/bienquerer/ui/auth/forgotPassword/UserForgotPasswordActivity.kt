package com.jujuy.bienquerer.ui.auth.forgotPassword

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.jujuy.bienquerer.R

class UserForgotPasswordActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.user_forgot_password_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, UserForgotPasswordFragment.newInstance())
                .commitNow()
        }
    }
}
