package com.jujuy.bienquerer.ui.chat.list

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.*
import androidx.activity.OnBackPressedCallback
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import com.jujuy.bienquerer.MainActivity

import com.jujuy.bienquerer.model.entity.Chat
import com.jujuy.bienquerer.base.BaseFragment
import com.jujuy.bienquerer.databinding.FragmentChatListBinding
import com.jujuy.bienquerer.extensions.gone
import com.jujuy.bienquerer.extensions.visible
import com.jujuy.bienquerer.ui.chat.detail.ChatDetailActivity
import com.jujuy.bienquerer.ui.chat.list.adapter.ChatListAdapter
import com.jujuy.bienquerer.utils.Constants
import com.jujuy.bienquerer.utils.autoCleared
import com.jujuy.bienquerer.viewmodel.chat.ChatViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class ChatListFragment : BaseFragment() {

    companion object {
        fun newInstance() = ChatListFragment()
    }

    private var valueEventListener: ValueEventListener? = null
    private var ref: DatabaseReference? = null

    // data and adapter
    private val chatViewModel by viewModel<ChatViewModel>()
    private var adapter by autoCleared<ChatListAdapter>()
    private var binding by autoCleared<FragmentChatListBinding>()
    private var firebaseUser: FirebaseUser? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentChatListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun initUIAndActions() {
        loginUserId = pref.getString(Constants.USER_ID, Constants.EMPTY_STRING).toString()
        val mLayoutManager = LinearLayoutManager(context)
        binding.chatListRecyclerView.layoutManager = mLayoutManager
        binding.chatListRecyclerView.itemAnimator = DefaultItemAnimator()
    }

    override fun initAdapters() {
        adapter = ChatListAdapter(appExecutors, true, loginUserId, object : ChatListAdapter.OnItemClickListener{
            override fun onItemClick(obj: Chat) {
                val intent = Intent(activity, ChatDetailActivity::class.java)
                Timber.e(obj.toString())
                val id = if (obj.senderId != loginUserId) obj.senderId else obj.receiverId
                Timber.e("DATOS ENVIADOS PARA EL CHAT: $id ${obj.name}")
                intent.putExtra(Constants.RECEIVE_USER_NAME, obj.name)
                intent.putExtra(Constants.RECEIVE_USER_ID, id)
                intent.putExtra(Constants.RECEIVE_USER_LAST_NAME, obj.receiverId)
                intent.putExtra(Constants.RECEIVE_USER_IMG_URL, obj.isSeen)
                startActivity(intent)
            }

        })
        binding.chatListRecyclerView.adapter = adapter
    }

    override fun initData() {
        val chatList= ArrayList<Chat>()
        firebaseUser = FirebaseAuth.getInstance().currentUser
        ref = FirebaseDatabase.getInstance().reference.child("userChatrooms").child(loginUserId)
        valueEventListener = object : ValueEventListener{
            override fun onCancelled(error: DatabaseError) {}

            override fun onDataChange(snapshot: DataSnapshot) {
                chatList.clear()
                Timber.e("Nuevo mensaje para mostrar en la lista...")
                for (data in snapshot.children){
                    val list = data.getValue(Chat::class.java)
                    chatList.add(list!!)
                }
                if (chatList.isEmpty()){
                    binding.loadMoreBar.gone()
                    binding.noItemConstraintLayout.visible()
                }else{
                    binding.loadMoreBar.gone()
                    binding.noItemConstraintLayout.gone()
                    Timber.e("Nueva lista ${chatList[chatList.lastIndex].lastMessage?.message}")
                    adapter.submitList(chatList.toMutableList().sortedByDescending { it.updateAt?.time })
                    binding.chatListRecyclerView.adapter?.notifyDataSetChanged()
                }
            }
        }

        valueEventListener?.let{ ref?.addValueEventListener(it)}

    }

    override fun onResume() {
        super.onResume()
        binding.chatListRecyclerView.adapter?.notifyDataSetChanged()
    }

    override fun onDetach() {
        super.onDetach()
        valueEventListener?.let { ref?.removeEventListener(it)}
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                (activity as MainActivity).setBottomNavigationMenu(0)
            }
        })
    }
}
