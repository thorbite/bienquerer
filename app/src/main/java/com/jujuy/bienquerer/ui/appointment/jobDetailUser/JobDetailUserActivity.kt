package com.jujuy.bienquerer.ui.appointment.jobDetailUser

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.base.BaseActivity
import com.jujuy.bienquerer.databinding.JobDetailUserActivityBinding
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class JobDetailUserActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: JobDetailUserActivityBinding = DataBindingUtil.setContentView(this,R.layout.job_detail_user_activity)
        initUI(binding)
    }


    private fun initUI(binding: JobDetailUserActivityBinding) {
        initToolbar(binding.toolbar, "Detalles")
        setupFragment(JobDetailUserFragment())
    }
}