package com.jujuy.bienquerer.ui.professionals.detail

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.ColorStateList
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.chip.Chip
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.api.network.Resource
import com.jujuy.bienquerer.base.BaseFragment
import com.jujuy.bienquerer.databinding.ItemProfessionalFragmentBinding
import com.jujuy.bienquerer.extensions.gone
import com.jujuy.bienquerer.extensions.showLongToast
import com.jujuy.bienquerer.model.entity.Professional
import com.jujuy.bienquerer.utils.*
import com.jujuy.bienquerer.viewmodel.favorite.FavoriteViewModel
import com.jujuy.bienquerer.viewmodel.professional.ProfessionalViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import java.net.URLEncoder
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class ItemProfessionalFragment : BaseFragment() {

    companion object {
        fun newInstance() = ItemProfessionalFragment()
        var FACEBOOK_URL = "https://www.facebook.com/diariotodojujuy"
        var FACEBOOK_PAGE_ID = "diariotodojujuy"
    }

    private var binding by autoCleared<ItemProfessionalFragmentBinding>()
    private val professionalViewModel by viewModel<ProfessionalViewModel>()
    private val favoriteViewModel by viewModel<FavoriteViewModel>()
    private val loading: LoadingMsg by lazy { LoadingMsg(requireActivity(), false) }
    private val psDialogMsg: PSDialogMsg by lazy {PSDialogMsg(activity, false)}
    private var map: GoogleMap? = null
    var bundle: Bundle? = null
    private var twist = false
    private var idReceived: String? = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = ItemProfessionalFragmentBinding.inflate(inflater, container, false)
        activity?.let { activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN) }
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun initUIAndActions() {
        hideFloatingButton()
        checkIfIsLoggin()

        binding.phoneTextView.setOnClickListener {
            val number = binding.phoneTextView.text.toString()
            if (!(number.trim { it <= ' ' }.isEmpty() || number.trim { it <= ' ' } == "-")) {
                Utils.callPhone(this, number)
            }
        }

        binding.emailTextView.setOnClickListener {
            val email = binding.emailTextView.text.toString()
            if (!(email.trim { it <= ' ' }.isEmpty() || email.trim { it <= ' ' } == "-")) {
                Utils.openEmail(this, email)
            }
        }

        binding.professionalFvrt.setOnClickListener {
            Utils.navigateOnUserVerificationActivity(userIdToVerify, loginUserId, psDialogMsg,
                requireActivity(),
                navigationController,
                object : Utils.NavigateOnUserVerificationActivityCallback {
                    override fun onSuccess() {
                        if (professionalViewModel.currentItem?.id == null) {
                            psDialogMsg.showWarningDialog(
                                getString(R.string.error_message__login_first),
                                getString(R.string.app__ok)
                            )
                            psDialogMsg.show()
                        } else {
                            if (professionalViewModel.currentItem?.isFav == false) addToFavorites() else removeFromFavorites()
                        }
                    }
                })
        }

        binding.ubicationProfessional.setOnClickListener {
            if (professionalViewModel.latValue.isNotEmpty()) {
                val intent = Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("http://maps.google.com/maps?daddr=${professionalViewModel.latValue},${professionalViewModel.lngValue}")
                )
                startActivity(intent)
            }
        }

        binding.textView31.setOnClickListener {
            val face = binding.textView31.text.toString()
            if (!(face.trim { it <= ' ' }.isEmpty() || face.trim { it <= ' ' } == "-")) {
                if (isAppInstalled()) {
                    Toast.makeText(
                        context,
                        "Facebook ya esta instalado",
                        Toast.LENGTH_SHORT
                    ).show()
                    val facebookIntent = Intent(Intent.ACTION_VIEW)
                    val facebookUrl: String? = context?.let { it1 -> getFacebookPageURL(it1) }
                    facebookIntent.data = Uri.parse(facebookUrl)
                    startActivity(facebookIntent)
                } else {
                    Toast.makeText(
                        context,
                        "Facebook no esta instalado",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
        psDialogMsg.showInfoDialog(
            getString(R.string.error_message__login_first),
            getString(R.string.app__ok)
        )
        binding.messengerFloatingActionButton.setOnClickListener {
            Utils.navigateOnUserVerificationActivity(userIdToVerify, loginUserId, psDialogMsg,
                requireActivity(),
                navigationController,
                object : Utils.NavigateOnUserVerificationActivityCallback {
                    override fun onSuccess() {
                        if (professionalViewModel.currentItem?.id == null) {
                            psDialogMsg.showWarningDialog(
                                getString(R.string.item_entry_user_not_exit),
                                getString(R.string.app__ok)
                            )
                            psDialogMsg.show()
                        } else {
                            navigationController.navigateToChatActivity(
                                activity!!,
                                professionalViewModel.currentItem?.id.toString(),
                                professionalViewModel.currentItem?.name,
                                professionalViewModel.currentItem?.lastname,
                                professionalViewModel.currentItem?.photo_profile_url,
                                0
                            )
                        }
                    }
                })
        }

        binding.mainFloatingActionButton.setOnClickListener {
            twist = Utils.twistFab(it, !twist)
            if (twist) {
                Utils.showFab(binding.messengerFloatingActionButton)
                Utils.showFab(binding.whatsappFloatingActionButton)
                Utils.showFab(binding.phoneFloatingActionButton)
                Utils.showFab(binding.messengerTextView)
                Utils.showFab(binding.whatsAppTextView)
                Utils.showFab(binding.phoneTv)
            } else {
                Utils.hideFab(binding.messengerFloatingActionButton)
                Utils.hideFab(binding.whatsappFloatingActionButton)
                Utils.hideFab(binding.phoneFloatingActionButton)
                Utils.hideFab(binding.messengerTextView)
                Utils.hideFab(binding.whatsAppTextView)
                Utils.hideFab(binding.phoneTv)
            }
        }

        binding.phoneFloatingActionButton.setOnClickListener {
            val number = binding.phoneTextView.text.toString()
            if (!(number.trim { it <= ' ' }.isEmpty() || number.trim { it <= ' ' } == "-")) {
                Utils.callPhone(this, number)
            }
        }

        binding.whatsappFloatingActionButton.setOnClickListener { v ->
            if(binding.phoneTextView.text.trim().length > 8 ){
                val message = "Hola! me comunico desde *Bien Querer*. Estoy interesado en tus servicios. Tenes unos minutos para coordinar un trabajo?"
                val phoneNumber = "549"+binding.phoneTextView.text.trim().toString()
                openWhatsApp(phoneNumber, message)
            }else{
                psDialogMsg.showWarningDialog("Este profesional aun no registra un número válido", getString(R.string.app__ok))
                psDialogMsg.show()
            }
        }

        binding.btnMakeAppointment.setOnClickListener {
            Utils.navigateOnUserVerificationActivity(userIdToVerify, loginUserId, psDialogMsg,
                requireActivity(),
                navigationController,
                object : Utils.NavigateOnUserVerificationActivityCallback {
                    override fun onSuccess() {
                        if (professionalViewModel.currentItem?.id == null) {
                            psDialogMsg.showWarningDialog(
                                getString(R.string.item_entry_user_not_exit),
                                getString(R.string.app__ok)
                            )
                            psDialogMsg.show()
                        } else {
                            navigationController.navigateToBookAppointmentDetailActivity(
                                activity!!,
                                professionalViewModel.currentItem,
                                loginUserName,
                                loginUserLastname,
                                loginUserEmail
                            )
                        }
                    }
                })
        }

        binding.commentsCardView.setOnClickListener {
            navigationController.navigateToCommentsProfessional(requireActivity(), professionalViewModel.currentItem?.id.toString())
        }
    }

    private fun openWhatsApp(numero: String, mensaje: String) {
        try {
            val packageManager = requireActivity().packageManager
            val i = Intent(Intent.ACTION_VIEW)
            val url =
                "whatsapp://send?phone=$numero&text=" + URLEncoder.encode(mensaje, "UTF-8")
            i.setPackage("com.whatsapp")
            i.data = Uri.parse(url)
            if (i.resolveActivity(packageManager) != null) {
                startActivity(i)
            } else {
                Timber.e("ERROR WHATSAPP NO ESTA INSTALADO")
            }
        } catch (e: java.lang.Exception) {
            Timber.e("ERROR WHATSAPP ${e.toString()}")
        }
    }

    private fun checkIfIsLoggin() {
        if (!sessionManager.isLoggedIn()){
            binding.professionalFvrt.gone()
            binding.emailTextView.gone()
            binding.phoneTextView.gone()
            binding.containerLocation.gone()
            binding.textView31.gone()
            binding.buttonsContainer.gone()
        }
    }

    override fun initData() {
        getIntentData()
        professionalViewModel.getProfessionalListDetailData()
            ?.observe(viewLifecycleOwner, Observer {
                when (it.status) {
                    Resource.Status.LOADING -> {
                        loading.showloaderDialog()
                        loading.show()
                    }
                    Resource.Status.SUCCESS -> {
                        loading.cancel()
                        professionalViewModel.currentItem = it.data
                        bindDataProfessional()
                    }
                    Resource.Status.ERROR -> {
                        loading.cancel()
                        Timber.e(it.message)
                    }
                }
            })

        favoriteViewModel.getFavouriteItemData()?.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Resource.Status.LOADING -> {
                    Timber.d("LOADING............")
                }
                Resource.Status.SUCCESS -> {
                    Timber.d("SUCCESS............")
                    if (it.data?.code == 200 && it.data.data == true) {
                        professionalViewModel.currentItem?.isFavorite = true
                        binding.professionalFvrt.background = (ContextCompat.getDrawable(
                            requireContext(),
                            R.drawable.ic_added_favorite
                        ))
                    }
                }
                Resource.Status.ERROR -> {
                    Timber.e(it.message)
                }
            }
        })
    }

    override fun initAdapters() {
    }

    private fun removeFromFavorites() {
        professionalViewModel.currentItem?.isFav = false
        favoriteViewModel.setDeleteFavoriteDataObj(professionalViewModel.currentItem?.id.toString())
        binding.professionalFvrt.background = (ContextCompat.getDrawable(
            requireContext(),
            R.drawable.ic_add_favorite
        ))
        favoriteViewModel.getDeleteFavouriteData()?.observe(this, Observer { response ->
            if (response != null) {
                when (response.status) {
                    Resource.Status.LOADING -> {
                    }
                    Resource.Status.SUCCESS -> {
                    }
                    Resource.Status.ERROR -> {
                        Utils.showSnackBarError(
                            requireActivity(),
                            "Error! No se puede actualizar!"
                        )
                    }
                }
            }
        })
    }

    private fun addToFavorites() {
        professionalViewModel.currentItem?.isFav = true
        favoriteViewModel.setFavouritePostDataObj(professionalViewModel.currentItem?.id.toString())
        binding.professionalFvrt.background = (ContextCompat.getDrawable(
            requireContext(),
            R.drawable.ic_added_favorite
        ))
        favoriteViewModel.getFavouritePostData()?.observe(this, Observer { response ->
            if (response != null) {
                when (response.status) {
                    Resource.Status.LOADING -> {
                        Timber.e("Thread Name : ${Thread.currentThread().name}")
                    }
                    Resource.Status.SUCCESS -> {
                        Utils.showSnackBarSuccess(
                            requireActivity(),
                            "Agregado a favoritos exitosamente!"
                        )
                    }
                    Resource.Status.ERROR -> {
                        Utils.showSnackBarError(requireActivity(), "Error! No se pudo actualizar!")
                        binding.professionalFvrt.background = (ContextCompat.getDrawable(
                            requireContext(),
                            R.drawable.ic_add_favorite
                        ))
                    }
                }
            }
        })
    }

    private fun getFacebookPageURL(context: Context): String {
        val packageManager: PackageManager = context.packageManager
        return try {
            val versionCode: Int = packageManager.getPackageInfo("com.facebook.orca", 0).versionCode
            if (versionCode >= 3002850) { //newer versions of fb app
                "fb://facewebmodal/f?href=$FACEBOOK_URL"
            } else { //older versions of fb app
                "fb://page/$FACEBOOK_PAGE_ID"
            }
        } catch (e: PackageManager.NameNotFoundException) {
            FACEBOOK_URL //normal web url
        }
    }

    private fun isAppInstalled(): Boolean {
        return try {
            context?.packageManager?.getApplicationInfo("com.facebook.katana", 0)
            true
        } catch (e: PackageManager.NameNotFoundException) {
            false
        }
    }

    private fun initializeMap(savedInstanceState: Bundle?) {
        try {
            MapsInitializer.initialize(requireContext())
        } catch (e: Exception) {
            e.printStackTrace()
        }

        binding.mapView.onCreate(savedInstanceState)
        bindMap(professionalViewModel.latValue, professionalViewModel.lngValue)

    }

    private fun bindMap(latValue: String, lngValue: String) {
        binding.mapView.onResume()

        binding.mapView.getMapAsync { googleMap ->
            map = googleMap
            try {
                map?.addMarker(
                    MarkerOptions()
                        .icon(BitmapDescriptorFactory.fromBitmap(generateSmallIcon(requireContext())))
                        .position(LatLng(latValue.toDouble(), lngValue.toDouble()))
                        .title("Ubicacion")
                )

                //zoom
                if (latValue.isNotEmpty() && lngValue.isNotEmpty()) {
                    val zoomlevel = 15
                    // Animating to the touched position
                    map?.moveCamera(
                        CameraUpdateFactory.newLatLngZoom(
                            LatLng(latValue.toDouble(), lngValue.toDouble()),
                            zoomlevel.toFloat()
                        )
                    )
                }
            } catch (e: java.lang.Exception) {
                Timber.e("$e")
            }
        }

    }

    private fun generateSmallIcon(context: Context): Bitmap {
        val height = 100
        val width = 100
        val bitmap = BitmapFactory.decodeResource(context.resources, R.drawable.finger_marker)
        return Bitmap.createScaledBitmap(bitmap, width, height, false)
    }

    private fun getIntentData() {
        if (activity != null) {
            Timber.e("Thread Name : ${Thread.currentThread().name}")
            requireActivity().intent?.extras?.let {
                idReceived = requireActivity().intent.getStringExtra(Constants.PROFESSIONAL)
                idReceived?.let {
                    professionalViewModel.setProfessionalListDetailObj(it)
                    checkIfFavorite(it)
                }
            }
        }

        if (idReceived == loginUserId) {
            binding.btnMakeAppointment.gone()
            binding.professionalFvrt.gone()
            binding.buttonsContainer.gone()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        val number = binding.phoneTextView.text.toString()
        if (!(number.trim { it <= ' ' }.isEmpty() || number.trim { it <= ' ' } == "-")) {
            Utils.phoneCallPermissionResult(requestCode, grantResults, this, number)
        }
    }

    private fun hideFloatingButton() {
        Utils.hideFirstFab(binding.messengerFloatingActionButton)
        Utils.hideFirstFab(binding.whatsappFloatingActionButton)
        Utils.hideFirstFab(binding.phoneFloatingActionButton)
        Utils.hideFab(binding.messengerTextView)
        Utils.hideFab(binding.whatsAppTextView)
        Utils.hideFab(binding.phoneTv)
    }

    private fun checkIfFavorite(id: String) {
        Timber.e("CheckFavoriteId : $id")
        favoriteViewModel.setFavoriteItemDataObj(id)
    }

    private fun bindDataProfessional() {
        binding.professional = professionalViewModel.currentItem
        professionalViewModel.latValue = professionalViewModel.currentItem?.latitude.toString()
        professionalViewModel.lngValue = professionalViewModel.currentItem?.longitude.toString()
        professionalViewModel.address = professionalViewModel.currentItem?.address.toString()
        professionalViewModel.currentItem?.let { bindTextView(it) }
        initializeMap(bundle)
    }

    private fun bindTextView(data: Professional) {
        val sb = StringBuilder()
        if (data.localities != null) {
            for (i in data.localities!!.indices) {
                if (data.localities!![i].departmentLocalities != null) {
                    sb.append("${data.localities!![i].nameLocalities} ${data.localities!![i].departmentLocalities!!.nameDepartment} ")
                } else {
                    sb.append("${data.localities!![i].nameLocalities} ")
                }
            }
        } else {
            sb.append("-")
        }

        val sb2 = StringBuilder()
        if (data.profession != null) {
            for (j in data.profession!!.indices) {
                sb2.append("| ${data.profession!![j].nameSubCategory} |")
            }
        }
        binding.ubicationProfessional.text = sb
        binding.phoneTextView.text = data.phone ?: "-"
        binding.tvProfessionsList.text = ""//sb2

        binding.textView27.removeAllViews()
        if (data.profession != null) {
            val array = data.profession
            for (i in array!!.indices) {
                val chip = Chip(binding.textView27.context)
                chip.text = array[i].nameSubCategory
                chip.chipBackgroundColor = ColorStateList.valueOf(
                    ContextCompat.getColor(
                        requireActivity(),
                        R.color.global__primary
                    )
                )
                chip.setTextColor(ContextCompat.getColor(requireActivity(), R.color.white))
                chip.isClickable = false
                chip.isCheckable = false
                binding.textView27.addView(chip)
            }
        }

    }
}