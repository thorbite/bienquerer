package com.jujuy.bienquerer.ui.chat.detail.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.AsyncDifferConfig
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.api.network.AppExecutors
import com.jujuy.bienquerer.binding.bindCircleImage
import com.jujuy.bienquerer.databinding.*
import com.jujuy.bienquerer.model.Message
import com.jujuy.bienquerer.utils.Constants
import com.jujuy.bienquerer.BR


class ChatAdapter(appExecutors: AppExecutors,
                  private val userId: String,
                  private val callback: clickCallBack,
                  private val itemId: String?,
                  private val otherUserProfileUrl: String?, private val msgAdapterListener: MsgAdapterListener) :
    ListAdapter<Message, RecyclerView.ViewHolder>(
    AsyncDifferConfig.Builder<Message>(
        object : DiffUtil.ItemCallback<Message>() {
            override fun areItemsTheSame(oldItem: Message, newItem: Message): Boolean {
                return oldItem.id == newItem.id
                        && oldItem.sendByUserId == newItem.sendByUserId
            }

            override fun areContentsTheSame(oldItem: Message, newItem: Message): Boolean {
                return oldItem.message == newItem.message
                        && oldItem.addedDate == newItem.addedDate
            }
        })
        .setBackgroundThreadExecutor(appExecutors.diskIO())
        .build()
)  {

    private var messageList: List<Message>? = null

    interface clickCallBack {
        fun onImageClicked(message: Message?)
        fun onProfileClicked()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when(viewType){
            R.layout.item_chat_list_time -> {
               ItemChatListTimeBinding.inflate(LayoutInflater.from(parent.context), parent,false)
            }
            R.layout.item_chat_list_sender_adapter -> {
                ItemChatListSenderAdapterBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false)
            }
            R.layout.item_chat_list_image_sender_adapter -> {
                ItemChatListImageSenderAdapterBinding.inflate(
                        LayoutInflater.from(parent.context), parent, false)
            }
            R.layout.item_chat_list_receiver_adapter -> {
                ItemChatListReceiverAdapterBinding.inflate(LayoutInflater.from(parent.context),
                        parent, false)
            }
            R.layout.item_chat_list_image_receiver_adapter -> {
                    ItemChatListImageReceiverAdapterBinding.inflate(LayoutInflater.from(parent.context),
                        parent,
                        false)
        }
            else -> {
                ItemChatListTimeBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false)
            }
        }.let { MsgViewHolder(it) }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val msg = getItem(position)
        (holder as MsgViewHolder<*>).bind(msgAdapterListener, msg)
    }

    override fun getItemViewType(position: Int): Int {
        val item = getItem(position)!!
        return if (item.sendByUserId == userId) {
            when (item.type) {
                Constants.CHAT_TYPE_DATE -> R.layout.item_chat_list_time
                Constants.CHAT_TYPE_TEXT -> R.layout.item_chat_list_sender_adapter
                else -> R.layout.item_chat_list_image_sender_adapter
            }
        } else {
            when(item.type) {
                Constants.CHAT_TYPE_DATE -> R.layout.item_chat_list_time
                Constants.CHAT_TYPE_TEXT -> R.layout.item_chat_list_receiver_adapter
                else -> R.layout.item_chat_list_image_receiver_adapter
            }
        }
    }

    class MsgViewHolder<T : ViewDataBinding> constructor(val binding: T) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(msgAdapterListener: MsgAdapterListener, item: Message) {
            binding.setVariable(BR.message, item)
            binding.setVariable(BR.msgAdapterInt, msgAdapterListener)
            binding.executePendingBindings()
        }
    }

    interface MsgAdapterListener {
        fun showPic(view: View, message: Message)
    }

    /*override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
            when (holder) {
                is TextSenderViewHolder -> {
                    holder.bind(messageList!![position])
                }
                is ImageSenderViewHolder -> {
                    holder.bind(messageList!![position])
                }
                is TextReceiverViewHolder -> {
                    holder.bind(messageList!![position])
                }
                is ImageReceiverViewHolder -> {
                    holder.bind(messageList!![position])
                }
                is TimeViewHolder -> {
                    holder.bind(messageList!![position])
                }
            }
    }*/

    /*override fun getItemViewType(position: Int): Int {
        Timber.e(messageList?.get(position)?.message)
        if (messageList != null && messageList!!.isNotEmpty()){
            if (position != messageList!!.size){
                if(messageList!![position].type == Constants.CHAT_TYPE_DATE){
                    return Constants.CHAT_DATE_UI
                } else if (messageList!![position].sendByUserId == userId){
                    if (messageList!![position].type == Constants.CHAT_TYPE_TEXT){
                        return Constants.CHAT_SENDER_UI
                    } else {
                        return Constants.CHAT_SENDER_IMAGE_UI
                    }
                } else if (messageList!![position].type == Constants.CHAT_TYPE_TEXT){
                    return Constants.CHAT_RECEIVER_UI
                } else {
                    return Constants.CHAT_RECEIVER_IMAGE_UI
                }
            }else{
                return -1
            }
        }else {
            return -1
        }
    }*/

    inner class TimeViewHolder (itemChatListTimeBinding: ItemChatListTimeBinding) :
        RecyclerView.ViewHolder(itemChatListTimeBinding.root) {
        private val itemTimeBinding: ItemChatListTimeBinding = itemChatListTimeBinding
        fun bind(message: Message?) {
            itemTimeBinding.message = message
            itemTimeBinding.executePendingBindings()
        }
    }

    inner class ImageSenderViewHolder(itemChatListImageAdapterBinding: ItemChatListImageSenderAdapterBinding)
        : RecyclerView.ViewHolder(itemChatListImageAdapterBinding.root) {

        private val imageAdapterBinding: ItemChatListImageSenderAdapterBinding? = itemChatListImageAdapterBinding
        fun bind(message: Message?) {
            imageAdapterBinding?.message = message
            imageAdapterBinding?.executePendingBindings()
            imageAdapterBinding?.chatImageView?.setOnClickListener { v ->
                val chatMessage = imageAdapterBinding.message
                callback.onImageClicked(chatMessage)
            }
        }
    }

    inner class TextSenderViewHolder (itemChatListAdapterBinding: ItemChatListSenderAdapterBinding) :
        RecyclerView.ViewHolder(itemChatListAdapterBinding.root) {
        private val textAdapterBinding: ItemChatListSenderAdapterBinding = itemChatListAdapterBinding
        fun bind(message: Message?) {
            textAdapterBinding.message = message
            textAdapterBinding.executePendingBindings()
        }
    }

    inner class ImageReceiverViewHolder(itemChatListImageAdapterBinding: ItemChatListImageReceiverAdapterBinding) :
        RecyclerView.ViewHolder(itemChatListImageAdapterBinding.root) {
        private val imageAdapterBinding: ItemChatListImageReceiverAdapterBinding = itemChatListImageAdapterBinding
        fun bind(message: Message?) {
            imageAdapterBinding.message = message
            imageAdapterBinding.profileImageView.setOnClickListener { v ->
                val chatMessage = imageAdapterBinding.message
                if (chatMessage != null && callback != null) {
                    callback.onProfileClicked()
                }
            }
            imageAdapterBinding.chatImageView.setOnClickListener { v ->
                val chatMessage = imageAdapterBinding.message
                callback.onImageClicked(chatMessage)
            }

            imageAdapterBinding.executePendingBindings()
        }
    }

    inner class TextReceiverViewHolder (itemChatListAdapterBinding: ItemChatListReceiverAdapterBinding) :
        RecyclerView.ViewHolder(itemChatListAdapterBinding.root) {
        private val textAdapterBinding: ItemChatListReceiverAdapterBinding = itemChatListAdapterBinding

        fun bind(message: Message?) {
            textAdapterBinding.message = message
            textAdapterBinding.profileImageView.setOnClickListener {
                val chatMessage: Message? = textAdapterBinding.message
                if (chatMessage != null && callback != null) {
                    callback.onProfileClicked()
                }
            }
            bindCircleImage(textAdapterBinding.profileImageView, otherUserProfileUrl)
            textAdapterBinding.executePendingBindings()
        }
    }

}
