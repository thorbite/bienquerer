package com.jujuy.bienquerer.ui.profile

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.AdapterView
import android.widget.Switch
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.core.view.children
import androidx.lifecycle.Observer
import com.google.android.flexbox.FlexboxLayout
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.model.*
import com.google.android.material.chip.Chip
import com.google.android.material.snackbar.Snackbar
import com.jujuy.bienquerer.MainActivity
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.api.network.Resource
import com.jujuy.bienquerer.base.BaseFragment
import com.jujuy.bienquerer.databinding.FragmentProfileBinding
import com.jujuy.bienquerer.extensions.onClick
import com.jujuy.bienquerer.extensions.visible
import com.jujuy.bienquerer.model.ItemParameterHolder
import com.jujuy.bienquerer.model.entity.SubCategory
import com.jujuy.bienquerer.model.entity.User
import com.jujuy.bienquerer.ui.profile.adapter.AutoCompleteProfessionAdapter
import com.jujuy.bienquerer.utils.*
import com.jujuy.bienquerer.viewmodel.professional.ProfessionalViewModel
import com.jujuy.bienquerer.viewmodel.subcategory.SubCategoryViewModel
import com.jujuy.bienquerer.viewmodel.user.UserViewModel
import com.sucho.placepicker.AddressData
import com.sucho.placepicker.MapType
import com.sucho.placepicker.PlacePicker
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import java.text.SimpleDateFormat
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class ProfileFragment : BaseFragment() {

    companion object {
        fun newInstance() = ProfileFragment()
    }

    private val userViewModel by viewModel<UserViewModel>()
    private val itemViewModel by viewModel<ProfessionalViewModel>()
    private val professionalViewModel by viewModel<SubCategoryViewModel>()
    private var binding by autoCleared<FragmentProfileBinding>()
    private val socialDialogMsg: PSDialogMsg by lazy { PSDialogMsg(activity, false) }
    private val psDialogMsg by lazy { PSDialogMsg(activity, false) }
    private val loading by lazy { LoadingMsg(requireActivity(), false) }

    private var professionList: List<SubCategory> = ArrayList()
    lateinit var adapter: AutoCompleteProfessionAdapter

    private var location: ItemParameterHolder = ItemParameterHolder()

    var bundle: Bundle? = null
    private var marker: Marker? = null
    private var user: User? = null

    private var map: GoogleMap? = null
    lateinit var alertDialog: AlertDialog.Builder
    private var fromPickMap = false


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentProfileBinding.inflate(inflater, container, false)
        activity?.let { it.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN) }
        setHasOptionsMenu(true)
        Timber.e("OnCreate........")
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        userViewModel.setLoadingState(true)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.addressEditText.isSelected = true
        binding.recipientInputET.visible()
    }

    private fun setupSearchAdapter(data: List<SubCategory>?) {
        professionList = data!!
        adapter =
            AutoCompleteProfessionAdapter(
                requireContext(),
                professionList
            )
        binding.recipientInputET.setAdapter(adapter)

        binding.recipientInputET.onItemClickListener =
            AdapterView.OnItemClickListener { adapterView, view, i, p3 ->
                val profession: SubCategory = adapterView.getItemAtPosition(i) as SubCategory
                addNewChip(profession, binding.profileChipsProfessions)
                binding.recipientInputET.setText("")
                saveProfession(profession)
            }
    }

    private fun saveProfession(profession: SubCategory) {
        itemViewModel.setProfessionSaveObj(loginUserId, profession.idSubCategory.toString())
    }

    private fun initializeMap(savedInstanceState: Bundle?, latValue: String, lngValue: String) {
        try {
            MapsInitializer.initialize(requireContext())
        } catch (e: Exception) {
            e.printStackTrace()
        }

        binding.mapView.onCreate(savedInstanceState)

        Timber.d("LATITUD $selectedLat")
        Timber.d("LONGITUD $selectedLng")

        bindMap(latValue, lngValue)

    }

    private fun bindMap(latValue: String, lngValue: String) {
        binding.mapView.onResume()
        binding.mapView.getMapAsync { googleMap ->
            map = googleMap
            try {
                map?.addMarker(
                    MarkerOptions()
                        .icon(BitmapDescriptorFactory.fromBitmap(generateSmallIcon(requireContext())))
                        .position(LatLng(latValue.toDouble(), lngValue.toDouble()))
                        .title("Ubicacion")
                )

                //zoom
                Timber.d("LATITUD RETURNED ON MAP ${latValue}")
                Timber.d("LONGITUD RETURNED ON MAP ${lngValue}")
                if (latValue.isNotEmpty() && lngValue.isNotEmpty()) {
                    val zoomlevel = 15
                    // Animating to the touched position
                    map?.moveCamera(
                        CameraUpdateFactory.newLatLngZoom(
                            LatLng(latValue.toDouble(), lngValue.toDouble()),
                            zoomlevel.toFloat()
                        )
                    )
                    itemViewModel.latValue = latValue
                    itemViewModel.lngValue = lngValue
                }
            } catch (e: Exception) {
                Timber.e("$e")
            }
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Constants.RESULT_CODE__TO_MAP_VIEW && resultCode == Constants.RESULT_CODE__FROM_MAP_VIEW) {
            //itemViewModel.latValue = data!!.getStringExtra(Constants.LAT)!!
            //itemViewModel.lngValue = data.getStringExtra(Constants.LNG)!!

            Timber.d("LATITUD 1 ${itemViewModel.latValue}")
            Timber.d("LONGITUD 1 ${itemViewModel.lngValue}")

            changeCamera(itemViewModel.latValue, itemViewModel.lngValue)
        }
        if (requestCode == Constants.PLACE_PICKER_REQUEST) {
            if (resultCode == Constants.RESULT_OK) {
                val addressData = data?.getParcelableExtra<AddressData>(Constants.ADDRESS_INTENT)
                itemViewModel.latValue = addressData?.latitude.toString()
                itemViewModel.lngValue = addressData?.longitude.toString()
                itemViewModel.address = addressData?.addressList?.get(0)?.getAddressLine(0) ?: "Desconocido"

                Timber.d("LATITUD RETURNED ${itemViewModel.latValue}")
                Timber.d("LONGITUD RETURNED ${itemViewModel.lngValue}")
                Timber.e(
                    "ADDRESS ${
                        addressData?.addressList?.get(0)?.getAddressLine(0) ?: "Desconocido"
                    }"
                )

                binding.addressEditText.text = itemViewModel.address
                fromPickMap = true
                changeCamera(itemViewModel.latValue, itemViewModel.lngValue)
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun changeCamera(latValue: String, lngValue: String) {
        if (marker != null) {
            marker!!.remove()
        }
        map!!.moveCamera(
            CameraUpdateFactory.newCameraPosition(
                CameraPosition.Builder().target(
                    LatLng(
                       latValue.toDouble(), lngValue.toDouble()
                    )
                ).zoom(15f).bearing(15f).tilt(15f).build()
            )
        )
        marker = map!!.addMarker(
            MarkerOptions()
                .position(
                    LatLng(latValue.toDouble(), lngValue.toDouble())
                )
                .title("Ubicacion")
                .icon(BitmapDescriptorFactory.fromBitmap(generateSmallIcon(requireContext())))
        )
    }

    override fun initUIAndActions() {

        itemViewModel.setProfessionalListDetailObj(loginUserId)

        binding.locationTitleDownImageView.setOnClickListener { v ->
            val show = Utils.toggleUpDownWithAnimation(v)
            if (show) {
                ViewAnimationUtil.expand(binding.locationMapConstraintLayout)
            } else {
                ViewAnimationUtil.collapse(binding.locationMapConstraintLayout)
            }
        }

        binding.locationTitleTextView.setOnClickListener { v ->
            val show = Utils.toggleUpDownWithAnimation(binding.locationTitleDownImageView)
            if (show) {
                ViewAnimationUtil.expand(binding.locationMapConstraintLayout)
            } else {
                ViewAnimationUtil.collapse(binding.locationMapConstraintLayout)
            }
        }

        binding.editTitleTextView.setOnClickListener {
            navigationController.navigateToProfileEditActivity(
                requireActivity(), user?.phoneVerified
                    ?: false
            )
        }

        binding.editTipsDownImageView.setOnClickListener {
            navigationController.navigateToProfileEditActivity(
                requireActivity(), user?.phoneVerified
                    ?: false
            )
        }


        if (activity is MainActivity) {
            (activity as MainActivity).setToolbarText(
                (activity as MainActivity).binding.toolbarMain,
                getString(R.string.menu__profile)
            )
            (this.activity as MainActivity).binding.toolbarMain.setBackgroundColor(
                ContextCompat.getColor(requireContext(), R.color.global__primary)
            )
        }

        binding.beProfesionalUserTextView.setOnClickListener {
            navigationController.navigateToRegisterProfessional(requireActivity())
        }

        binding.submitButton.setOnClickListener {
            location.userId = loginUserId
            location.lat = itemViewModel.latValue
            location.lng = itemViewModel.lngValue
            location.address = itemViewModel.address

            itemViewModel.setProfessionalUpdated(location)
        }

        binding.mapButton.setOnClickListener {
            map!!.clear()
            if (itemViewModel.itemId == Constants.ADD_NEW_ITEM) {
                navigationController.navigateToMapActivity(
                    requireActivity(),
                    itemViewModel.lngValue,
                    itemViewModel.latValue,
                    Constants.MAP_PICK
                )
            } else {
                val intent = PlacePicker.IntentBuilder()
                    .setLatLong(
                        itemViewModel.latValue.toDouble(),
                        itemViewModel.lngValue.toDouble()
                    )  // Initial Latitude and Longitude the Map will load into
                    .showLatLong(true)  // Show Coordinates in the Activity
                    .setMapZoom(12.0f)  // Map Zoom Level. Default: 14.0
                    .setAddressRequired(true) // Set If return only Coordinates if cannot fetch Address for the coordinates. Default: True
                    .setMarkerDrawable(R.drawable.ic_marker_finger3) // Change the default Marker Image
                    .setFabColor(R.color.global__accent)
                    .setMapType(MapType.NORMAL)
                    .setPlaceSearchBar(
                        true,
                        getString(R.string.google_map_api_key)
                    )
                    .build(requireActivity())
                startActivityForResult(intent, Constants.PLACE_PICKER_REQUEST)
                /*navigationController.navigateToMapActivity(
                    this@ProfileFragment.activity!!,
                    itemViewModel.lngValue,
                    itemViewModel.latValue,
                    Constants.MAP_PICK
                )*/
            }
        }

        binding.btnIsAvailable.setOnClickListener {
            onSwitchClicked(it)
        }
        binding.btnIsAvailable.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                binding.btnIsAvailable.text = "Disponible"
            } else {
                binding.btnIsAvailable.text = "No Disponible"
            }
        }

        binding.socialProfileUserTextView.setOnClickListener {
            if (itemViewModel.isSocial != null || itemViewModel.isSocial == true) {
                psDialogMsg.showConfirmDialog(
                    getString(R.string.profile__confirm_delete_social),
                    getString(R.string.app_ok),
                    getString(R.string.message__cancel_close)
                )
                psDialogMsg.show()

                psDialogMsg.okButton.setOnClickListener {
                    itemViewModel.setProfessionalSocial()
                    itemViewModel.isSocial = false
                    psDialogMsg.cancel()
                }
                psDialogMsg.cancelButton.setOnClickListener {
                    psDialogMsg.cancel()
                }
            } else {
                alertDialogSocialQuestion()
                /*psDialogMsg.showConfirmDialog(
                    getString(R.string.profile__confirm_social),
                    getString(R.string.app_ok),
                    getString(R.string.message__cancel_close)
                )*/
            }


        }
    }

    private fun alertDialogSocialQuestion() {
        psDialogMsg.showConfirmDialog("¿Sentis el llamado de ayudar?",
            "Ver más...",
            getString(R.string.message__cancel_close)
        )

        psDialogMsg.show()

        psDialogMsg.okButton.onClick {
            psDialogMsg.cancel()
            socialDialogMsg.showConfirmDialog("Te vas a convertir en un profesional social que ayuda a las personas que mas lo necesitan sin cobro", getString(R.string.app__ok), getString(R.string.message__cancel_close))
            socialDialogMsg.show()
            socialDialogMsg.okButton.onClick {
                itemViewModel.setProfessionalSocial()
                itemViewModel.isSocial = true
                socialDialogMsg.cancel()
            }
            socialDialogMsg.cancelButton.onClick { socialDialogMsg.cancel() }

            //professionalViewModel.isSocial = !itemViewModel.isSocial!!

        }
        psDialogMsg.cancelButton.onClick {
            psDialogMsg.cancel()
        }
    }

    //Response to the Switch button click event
    private fun onSwitchClicked(v: View) {
        val on: Boolean = (v as Switch).isChecked
        if (on) {
            itemViewModel.setProfessionalAvailable()
        } else {
            itemViewModel.setProfessionalAvailable()
        }
    }


    override fun initAdapters() {
        itemViewModel.mapLat = itemViewModel.latValue
        itemViewModel.mapLng = itemViewModel.lngValue
    }

    override fun initData() {
        itemViewModel.getProfessionSaveData()?.observe(this, Observer { response ->
            when (response.status) {
                Resource.Status.LOADING -> {
                    loading.showloaderDialog()
                    loading.show()
                }
                Resource.Status.SUCCESS -> {
                    loading.cancel()
                    psDialogMsg.showSuccessDialog(
                        "¡Nueva profesion agregada!",
                        getString(R.string.app__ok)
                    )
                    psDialogMsg.show()
                }
                Resource.Status.ERROR -> {
                    loading.cancel()
                    psDialogMsg.showErrorDialog(
                        "Ha ocurrido un error: ${response.message}",
                        getString(R.string.app__ok)
                    )
                    psDialogMsg.show()
                }
            }
        })

        itemViewModel.getProfessionDeleteData()?.observe(this, Observer { listResource ->
            when (listResource.status) {
                Resource.Status.LOADING -> {
                }
                Resource.Status.SUCCESS -> {
                    showSnackBarSuccess("Profesion eliminada correctamente")
                }
                Resource.Status.ERROR -> {
                    psDialogMsg.showErrorDialog("Ha ocurrido un error", getString(R.string.app__ok))
                    psDialogMsg.show()
                }
            }
        })

        //professionalViewModel.setProfessionListObj("")
        professionalViewModel.getProfessionListData()?.observe(this, Observer { listResources ->
            when (listResources.status) {
                Resource.Status.LOADING -> {
                }
                Resource.Status.SUCCESS -> {
                    if (listResources.data != null) {
                        setupSearchAdapter(listResources.data)
                    }
                }
                Resource.Status.ERROR -> {
                }
            }
        })


        Timber.e("ID en Profile : $loginUserId")
        userViewModel.getLoginUser(loginUserId).observe(this, { data ->


            if (fromPickMap == false ) {
                user = data
                binding.user = data
                Utils.updateUserLoginData(pref, user!!)
                if (data.person?.latitude != "" && data.person?.longitude != "") {
                    //itemViewModel.latValue = data.person?.latitude.toString()
                    //itemViewModel.lngValue = data.person?.longitude.toString()
                    itemViewModel.isSocial = data.person?.social
                    if (itemViewModel.isSocial == true) {
                        binding.socialProfileUserTextView.text =
                            "Ya eres un profesional social"
                    } else {
                        binding.socialProfileUserTextView.text = "Ser un profesional social"
                    }
                }
                replaceUserData(data)

                itemViewModel.address = data.person?.address.toString()//selectedLng
                initializeMap(bundle, data.person?.latitude.toString(), data.person?.longitude.toString())
            } else {
                binding.addressEditText.text = itemViewModel.address
            }

        })

        itemViewModel.getProfessionalUpdated()?.observe(this, Observer { professional ->
            Timber.e("Professional ${professional.data}")
            when (professional.status) {
                Resource.Status.LOADING -> {
                    loading.showloaderDialog()
                    loading.show()
                }
                Resource.Status.SUCCESS -> {
                    loading.cancel()
                    if (professional.data != null) {
                        binding.addressEditText.text = professional.data.address
                        showSnackBarSuccess("¡Guardado satisfactoriamente!")
                    }
                }
                Resource.Status.ERROR -> {
                    loading.cancel()
                    psDialogMsg.showErrorDialog(professional.message, getString(R.string.app__ok))
                    psDialogMsg.show()
                }
            }
        })

        itemViewModel.getProfessionalAvailable()?.observe(this, Observer { response ->
            when (response.status) {
                Resource.Status.LOADING -> {
                }
                Resource.Status.SUCCESS -> {
                    response.data?.let {
                        if (it.code == 200) {
                            Timber.e("DISPONIBLE  + ${it.message}")
                        }
                    }
                }
                Resource.Status.ERROR -> {
                    Timber.e("ERROR " + response.message)
                }
            }
        })

        itemViewModel.getProfessionalSocial()?.observe(this, Observer { response ->
            when (response.status) {
                Resource.Status.LOADING -> {
                }
                Resource.Status.SUCCESS -> {
                    if (response.data!!.code == 200) {
                        if (itemViewModel.isSocial == true) {
                            binding.socialProfileUserTextView.text =
                                "Ya eres un profesional social"
                        } else {
                            binding.socialProfileUserTextView.text =
                                "Ser un profesional social"
                        }
                    }
                }
                Resource.Status.ERROR -> {
                    Timber.e("ERROR " + response.message)
                }
            }
        })

        itemViewModel.getProfessionalListDetailData()?.observe(this, Observer { resource ->
            when (resource.status) {
                Resource.Status.LOADING -> {
                }
                Resource.Status.SUCCESS -> {
                    binding.btnIsAvailable.isChecked = resource.data?.available ?: false
                    //itemViewModel.isSocial = resource.data?.social ?: false
                    //if(resource.data?.social != null) {
                    if (itemViewModel.isSocial == true) {
                        binding.socialProfileUserTextView.text = "Ya eres un profesional social"
                    } else {
                        binding.socialProfileUserTextView.text = "Ser un profesional social"
                    }
                    // }

                    if (resource.data?.profession != null) {
                        val array = resource.data.profession
                        binding.profileChipsProfessions.removeAllViews()
                        for (i in array!!.indices) {
                            //val chip = Chip(chip_group.context)
                            addNewChip(array[i], binding.profileChipsProfessions)
                        }
                    }
                }
                Resource.Status.ERROR -> {
                }
            }
        })
    }

    @SuppressLint("SimpleDateFormat")
    private fun replaceUserData(user: User) {
        val parser = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        val formatter = SimpleDateFormat("dd.MM.yyyy HH:mm")
        var formattedDate = ""
        user.createdAt?.let {
            formattedDate = formatter.format(parser.parse(it))
        }

        binding.nameTextView.text = user.person?.name + " " + user.person?.lastname


        if (user.phoneVerified == true) {
            binding.infoProfessionalProfile.visible()
            binding.beProfesionalUserTextView.text = "Ya eres profesional"
            binding.beProfesionalUserTextView.setOnClickListener(null)
            binding.btnIsAvailable.visible()
            //binding.ratingBarInformation.visible()
        }
    }

    private fun addNewChip(person: SubCategory, chipGroup: FlexboxLayout) {
        val chip = Chip(chipGroup.context)
        Timber.e("NUMEROS DE CHIPS: ${chipGroup.childCount}")
        if (chipGroup.childCount > 0) {
            chip.isCloseIconVisible = true
            chipGroup.children.forEach {
                (it as Chip).isCloseIconVisible = true
            }
        } else {
            chip.isCloseIconVisible = false
        }
        chip.text = person.nameSubCategory
        chip.chipIcon = ContextCompat.getDrawable(requireContext(), R.mipmap.ic_launcher_round)
        chip.isClickable = true
        chip.isCheckable = false
        chip.chipBackgroundColor = ColorStateList.valueOf(
            ContextCompat.getColor(
                requireActivity(),
                R.color.global__primary
            )
        )
        chip.setTextColor(ContextCompat.getColor(requireActivity(), R.color.white))
        chipGroup.addView(chip as View, chipGroup.childCount)
        chip.setOnCloseIconClickListener {
            chipGroup.removeView(chip as View)
            if (chipGroup.childCount == 1) {
                chipGroup.children.forEach {
                    (it as Chip).isCloseIconVisible = false
                }
            }
            deleteProfession(person.idSubCategory.toString())
        }
    }

    private fun deleteProfession(idProfession: String) {
        Timber.e("DELETE: $loginUserId  $idProfession")
        itemViewModel.setProfessionDeleteObj(loginUserId, idProfession)
    }


    private fun showSnackBarSuccess(message: String) {
        val snack = Snackbar.make(
            requireActivity().findViewById(android.R.id.content),
            message,
            Snackbar.LENGTH_SHORT
        )
        snack.setAction("OK") {
            // executed when DISMISS is clicked
        }

        // change action button text color
        snack.setActionTextColor(Color.WHITE)

        // snackbar background color
        snack.view.setBackgroundColor(
            ContextCompat.getColor(
                requireContext(),
                R.color.md_green_800
            )
        )

        val textView = snack.view.findViewById(R.id.snackbar_text) as TextView
        // change Snackbar text color
        textView.setTextColor(Color.WHITE)

        snack.show()
    }

    private fun generateSmallIcon(context: Context): Bitmap {
        val height = 100
        val width = 100
        val bitmap = BitmapFactory.decodeResource(context.resources, R.drawable.finger_marker)
        return Bitmap.createScaledBitmap(bitmap, width, height, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                (activity as MainActivity).setBottomNavigationMenu(0)
            }
        })
    }
}
