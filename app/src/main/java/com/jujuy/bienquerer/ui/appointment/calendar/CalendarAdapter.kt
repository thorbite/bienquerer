package com.jujuy.bienquerer.ui.appointment.calendar

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.github.sundeepk.compactcalendarview.domain.Event
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.model.network.response.Appointment
import com.jujuy.bienquerer.utils.Utils
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class CalendarAdapter(private val jobList: List<Event>, activity: Activity): RecyclerView.Adapter<CalendarAdapter.MyViewHolder>() {


    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var name: TextView
        var phone: TextView
        var city: TextView
        var date: TextView

        init {
            name = view.findViewById(R.id.text_name)
            phone = view.findViewById(R.id.text_mobile)
            city = view.findViewById(R.id.text_city)
            date = view.findViewById(R.id.text_date)
        }
    }
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyViewHolder {
        val itemView: View = LayoutInflater.from(parent.context).inflate(R.layout.calendar_adapter_item, parent, false)
        return MyViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return jobList?.size ?: 0
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.name.setText((jobList?.get(position)?.data as Appointment).client.name + " "+ (jobList?.get(position)?.data as Appointment).client.lastname)
        holder.phone.setText((jobList?.get(position)?.data as Appointment).professional.phone)
        holder.city.setText((jobList?.get(position)?.data as Appointment).comment)
        holder.date.setText(Utils.parseDateCorrectForm((jobList?.get(position)?.data as Appointment).requestDate))
    }
}