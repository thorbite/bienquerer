package com.jujuy.bienquerer.ui.category.categoryfilter

import com.jujuy.bienquerer.model.entity.Category
import com.jujuy.bienquerer.model.entity.SubCategory
import com.jujuy.bienquerer.utils.Constants
import java.util.*
import kotlin.collections.LinkedHashMap
import kotlin.collections.List
import kotlin.collections.indices

class Grouping {
    var catId : String =""
    var subCatId : String =""
    var map: LinkedHashMap<Category, List<SubCategory>> =
        LinkedHashMap<Category, List<SubCategory>>()

    fun group(categoryList:List<Category>, subCategoryList: List<SubCategory>): LinkedHashMap<Category,List<SubCategory>> {
        map.clear()
        for (i in categoryList.indices){
            val subCategories: MutableList<SubCategory> = ArrayList<SubCategory>()
            val subCategory: SubCategory = SubCategory(Constants.ZERO.toInt(), Constants.CATEGORY_ALL,
                "", "",null)
            subCategories.add(0, subCategory)
            catId = categoryList[i].idCategory.toString()
            for (j in subCategoryList.indices){
                subCatId = subCategoryList[j].categorySubCategory?.idCategory.toString()
                if (catId == subCatId){
                    subCategories.add(subCategoryList[j])
                }
            }
            if(subCategories.isNotEmpty()){
                map[categoryList[i]] = subCategories
            }
        }
        return map
    }
}