package com.jujuy.bienquerer.ui.search

import com.jujuy.bienquerer.model.SearchRecent
import com.jujuy.bienquerer.room.SearchDao
import timber.log.Timber
import java.io.Serializable

class DataProvider(private val searchDao: SearchDao) : Serializable {
    private var initialSearchQueries: MutableList<String> = mutableListOf()

    fun getInitialSearchQueries(): List<String> {
        return getDataFromRoom()
    }

    fun getSuggestionsForQuery(query: String): List<String> {
        val pickedSuggestions = mutableListOf<String>()
        if (query.isEmpty()) {
            pickedSuggestions.addAll(initialSearchQueries)
        } else {
            initialSearchQueries.forEach {
                if (it.toLowerCase().startsWith(query.toLowerCase())) {
                    pickedSuggestions.add(it)
                }
            }
        }
        return pickedSuggestions
    }


    fun saveSearchQuery(searchQuery: String) {
        with(initialSearchQueries) {
            remove(searchQuery)
            add(0, searchQuery)
            saveInRoom(this)
        }
    }


    fun removeSearchQuery(searchQuery: String) {
        initialSearchQueries.remove(searchQuery)
        searchDao.deleteSearched(searchQuery)
    }

    private fun saveInRoom(list: MutableList<String>) {
        val room: List<SearchRecent> = list.map { SearchRecent(it) }
        searchDao.insertAll(room)
    }

    private fun getDataFromRoom(): MutableList<String> {
        val room = searchDao.getAllSearched()
        initialSearchQueries.clear()
        initialSearchQueries = room.map { it.searchText }.toMutableList()
        return initialSearchQueries
    }
}