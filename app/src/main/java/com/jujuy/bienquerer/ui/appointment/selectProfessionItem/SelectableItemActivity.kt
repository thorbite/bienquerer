package com.jujuy.bienquerer.ui.appointment.selectProfessionItem

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.base.BaseActivity
import com.jujuy.bienquerer.ui.appointment.adapter.SelectableRecyclerViewAdapter
import com.jujuy.bienquerer.utils.Constants
import kotlinx.android.synthetic.main.activity_selectable_item.*
import java.util.*

class SelectableItemActivity : BaseActivity(), SelectableViewHolder.OnItemSelectedListener {

    var recyclerView: RecyclerView? = null
    var adapter: SelectableRecyclerViewAdapter? = null
    var clearSelection: TextView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_selectable_item)
        initViews()
        setAdapter()

        val title = intent.getStringExtra(Constants.TITLE_PROFESSION)
        if (title != null) {
            initToolbar(toolbar, title)
        }
        //supportActionBar!!.title = if (title != null && title.isNotEmpty()) title else "Refine Search"
    }

    private fun initViews() {
        val layoutManager = LinearLayoutManager(this)
        selection_list.layoutManager = layoutManager

        selection_clear.setOnClickListener(View.OnClickListener {
            adapter?.clearSelections()
        })
    }

    private fun setAdapter() {
        val selectableItems: List<String> = intent.getStringArrayListExtra(Constants.DATA_PROFESSION) as List<String>
        val selectedItem = intent.getStringExtra(Constants.SELECTED_ITEM)
        adapter = selectedItem?.let {
            SelectableRecyclerViewAdapter(this, selectableItems, false,
                it
            )
        }
        selection_list!!.adapter = adapter
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.action_done, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.action_done -> {
                setResult(RESULT_OK, getResultIntent())
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun getResultIntent(): Intent? {
        val intent = Intent()
        val selectedItems: ArrayList<String>? = adapter?.getSelectedItems()
        intent.putStringArrayListExtra(Constants.DATA_PROFESSION, selectedItems)
        return intent
    }

    override fun onBackPressed() {
        setResult(RESULT_OK, getResultIntent())
        finish()
    }

    override fun onItemSelected(item: SelectableItem?) {

    }
}