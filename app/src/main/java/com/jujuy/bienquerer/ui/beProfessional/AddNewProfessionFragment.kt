package com.jujuy.bienquerer.ui.beProfessional

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.model.entity.Category
import com.jujuy.bienquerer.model.entity.SubCategory
import com.jujuy.bienquerer.viewmodel.category.CategoryViewModel
import com.jujuy.bienquerer.viewmodel.subcategory.SubCategoryViewModel
import kotlinx.android.synthetic.main.fragment_add_new_profession.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import timber.log.Timber
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class AddNewProfessionFragment : DialogFragment() {

    private val categoryViewModel by sharedViewModel<CategoryViewModel>()
    private val subCategoryViewModel by sharedViewModel<SubCategoryViewModel>()

    companion object {
        fun newInstance() = AddNewProfessionFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_add_new_profession, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        loadCategoryList()

        //initObservers()
    }

    /*@ExperimentalCoroutinesApi
    private fun initObservers() {
        categoryViewModel.getCategoryListData()?.observe(this, Observer { listResource ->
            if (listResource != null){
                when(listResource.status){
                    Status.LOADING ->{ }
                    Status.SUCCESS ->{
                        if (listResource.data != null){
                            setDataToCategorySpinner(listResource.data)
                            Timber.e("DATOS: "+ listResource.data.toString())
                        }
                    }
                    Status.ERROR ->{ }
                }
            }
        })

        subCategoryViewModel.getSubCategoryListData()?.observe(this, Observer { listResource ->
            if (listResource != null){
                setDataToProfessionSpinner(listResource)
            }
        })
    }*/

    private fun loadCategoryList() {
        categoryViewModel.setCategoryListObj("","")
    }

    private fun loadProfessionsList(idCategory: String){
        Timber.e(idCategory)
        //subCategoryViewModel.setSubCategoryListObj(idCategory)
    }

    private fun setDataToCategorySpinner(listCategory: List<Category>){
        spinnerCategory.apply {
            setSpinnerAdapter(CategoryItemAdapter(this))
            setItems(listCategory)
            getSpinnerRecyclerView().layoutManager = LinearLayoutManager(context)
            //selectItemByIndex(0)
            lifecycleOwner = this@AddNewProfessionFragment
            setOnSpinnerItemSelectedListener<Category> { index, item ->
                Timber.e(item.idCategory.toString())
                loadProfessionsList(item.idCategory.toString())
            }
        }
    }

    private fun setDataToProfessionSpinner(listCategory: List<SubCategory>){
        Timber.e(listCategory.toString())
        spinnerProfession.apply {
            setSpinnerAdapter(ProfessionItemAdapter(this))
            setItems(listCategory)
            getSpinnerRecyclerView().layoutManager = LinearLayoutManager(context)
            selectItemByIndex(0)
            lifecycleOwner = this@AddNewProfessionFragment
        }
    }


    override fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
    }
}