package com.jujuy.bienquerer.ui.appointment.jobDetailProfessional

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.api.network.Resource
import com.jujuy.bienquerer.base.BaseFragment
import com.jujuy.bienquerer.databinding.FragmentJobDetailProfessionalBinding
import com.jujuy.bienquerer.extensions.gone
import com.jujuy.bienquerer.utils.Constants
import com.jujuy.bienquerer.utils.LoadingMsg
import com.jujuy.bienquerer.utils.autoCleared
import com.jujuy.bienquerer.viewmodel.jobs.AppointmentViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import kotlin.time.ExperimentalTime


@ExperimentalTime
@ExperimentalCoroutinesApi
class JobDetailProfessionalFragment : BaseFragment() {

    private lateinit var jobId: String
    private var binding by autoCleared<FragmentJobDetailProfessionalBinding>()
    private val jobViewModel by viewModel<AppointmentViewModel>()
    private val loadingMsg by lazy { LoadingMsg(requireActivity(), false) }

    companion object {
        fun newInstance() = JobDetailProfessionalFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentJobDetailProfessionalBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun initUIAndActions() {
        getIntentData()
        binding.acceptJob.setOnClickListener {
            if (jobViewModel.jobContainer?.status?.id == 1){
                jobViewModel.setChangeStatusJob(null, jobId, "2")
            }else{
                jobViewModel.setChangeStatusJob(null, jobId, "4")
            }
        }
        binding.rejectJob.setOnClickListener {
            if (jobViewModel.jobContainer?.status?.id == 1) {
                jobViewModel.setChangeStatusJob(null, jobId, "3")
            }
        }
        binding.messageWithUser.setOnClickListener {
            navigationController.navigateToChatActivity(
                requireActivity(),
                jobViewModel.jobContainer?.client?.id.toString(),
                jobViewModel.jobContainer?.client?.name,
                jobViewModel.jobContainer?.client?.lastname,
                jobViewModel.jobContainer?.client?.photoProfileUrl,
                0
            )
        }
    }

    private fun getIntentData() {
        if (requireActivity().intent.extras != null) {
            jobId = requireActivity().intent.extras!!.getString(Constants.JOB)!!
        }
    }

    private fun checkforButtons() {
        Timber.e(jobViewModel.jobContainer?.status?.id.toString())
        when(jobViewModel.jobContainer?.status?.id){
            /*1 ->{
                binding.acceptJob.isEnabled = true
                binding.acceptJob.isClickable = true
                binding.rejectJob.isEnabled = true
                binding.rejectJob.isClickable = true
            }*/
            2 ->{
                Timber.e("2 OCultar boton rechazar y cambiar texto")
                binding.rejectJob.gone()
                binding.acceptJob.text = "Marcar como completado"
            }
            3 ->{
                Timber.e("3 Ocultar botones")
                binding.buttonsJob.gone()
            }
            4 ->{
                Timber.e("4 Ocultar botones")
                binding.buttonsJob.gone()
            }
        }
    }

    override fun initAdapters() {

    }

    override fun initData() {
        getResultsData()
        loadDataJob()
    }

    private fun loadDataJob() {
        jobViewModel.getJobData(jobId)?.observe(this, Observer { job ->
            job?.let {
                Timber.d("JOB: $it")
                binding.job = it
                jobViewModel.jobContainer = it
                getJobStatus(it.status.id.toString())
                checkforButtons()
            }
        })
    }

    private fun getResultsData() {
        jobViewModel.getChangeStatusJob().observe(this, Observer {
            when(it.status){
                Resource.Status.LOADING ->{
                    loadingMsg.showloaderDialog()
                    loadingMsg.show()
                }
                Resource.Status.SUCCESS -> {
                    //loadingMsg.cancel()
                    getListUpdated()
                    Timber.d(it.data.toString())

                }
                Resource.Status.ERROR ->{
                    loadingMsg.cancel()
                    Timber.d(it.message)
                }
            }
        })
    }

    private fun getListUpdated() {
        jobViewModel.setListJobObj()
        jobViewModel.getListJobData().observe(this, Observer {
            when(it.status){
                Resource.Status.LOADING ->{}
                Resource.Status.SUCCESS ->{
                    loadingMsg.cancel()
                    loadDataJob()
                }
                Resource.Status.ERROR ->{}
            }
        })
    }

    private fun getJobStatus(jobStatus: String) {
        val alfa = 0.5f
        when (jobStatus) {
            "1" -> {
                setStatusPending(alfa)
            }
            "2" -> {
                setStatusAcepted(alfa)
            }
            "4" -> {
                setStatusComplete(alfa)
            }
            "3" -> {
                setStatusReject(alfa)
            }
        }
    }

    private fun setStatusPending(alfa: Float) {
        binding.viewOrderPlaced.background = ContextCompat.getDrawable(requireContext(), R.drawable.shape_status_completed)
        binding.viewOrderConfirmed.background = ContextCompat.getDrawable(requireContext(), R.drawable.shape_status_current)
        binding.viewOrderProcessed.background = ContextCompat.getDrawable(requireContext(), R.drawable.shape_status_current)
        binding.conDivider.background = ContextCompat.getDrawable(requireContext(), R.drawable.shape_status_current)
        binding.placedDivider.background = ContextCompat.getDrawable(requireContext(), R.drawable.shape_status_current)
        binding.placedDivider.alpha = alfa
        binding.conDivider.alpha = alfa
        binding.viewOrderConfirmed.alpha = alfa
        binding.viewOrderProcessed.alpha = alfa
        binding.imgJobAcepted.alpha = alfa
        binding.textJobAcepted.alpha = alfa
        binding.descJobAcepted.alpha = alfa
        binding.imgJobComplete.alpha = alfa
        binding.textJobComplete.alpha = alfa
        binding.descJobComplete.alpha = alfa
    }

    private fun setStatusAcepted(alfa: Float) {
        binding.viewOrderPlaced.background = ContextCompat.getDrawable(requireContext(), R.drawable.shape_status_completed)
        binding.viewOrderConfirmed.background = ContextCompat.getDrawable(requireContext(), R.drawable.shape_status_completed)
        binding.viewOrderProcessed.background = ContextCompat.getDrawable(requireContext(), R.drawable.shape_status_current)
        binding.conDivider.background = ContextCompat.getDrawable(requireContext(), R.drawable.shape_status_current)
        binding.placedDivider.background = ContextCompat.getDrawable(requireContext(), R.drawable.shape_status_completed)
        binding.placedDivider.alpha = 1f
        binding.viewOrderConfirmed.alpha = 1f
        binding.imgJobAcepted.alpha = 1f
        binding.textJobAcepted.alpha = 1f
        binding.descJobAcepted.alpha = 1f
        binding.conDivider.alpha = alfa
        binding.imgJobComplete.alpha = alfa
        binding.textJobComplete.alpha = alfa
        binding.descJobComplete.alpha = alfa
    }

    private fun setStatusComplete(alfa: Float) {
        binding.viewOrderPlaced.background = ContextCompat.getDrawable(requireContext(), R.drawable.shape_status_completed)
        binding.viewOrderConfirmed.background = ContextCompat.getDrawable(requireContext(), R.drawable.shape_status_completed)
        binding.viewOrderProcessed.background = ContextCompat.getDrawable(requireContext(), R.drawable.shape_status_completed)
        binding.conDivider.background = ContextCompat.getDrawable(requireContext(), R.drawable.shape_status_completed)
        binding.placedDivider.background = ContextCompat.getDrawable(requireContext(), R.drawable.shape_status_completed)
        binding.placedDivider.alpha = 1f
        binding.viewOrderConfirmed.alpha = 1f
        binding.imgJobAcepted.alpha = 1f
        binding.textJobAcepted.alpha = 1f
        binding.descJobAcepted.alpha = 1f
        binding.conDivider.alpha = 1f
        binding.imgJobComplete.alpha = 1f
        binding.textJobComplete.alpha = 1f
        binding.descJobComplete.alpha = 1f
    }

    private fun setStatusReject(alfa: Float){
        binding.imgJobPending.setImageResource(R.drawable.ic_icon_reject)
        binding.textJobPending.text = "Rechazado"

        binding.viewOrderPlaced.background = ContextCompat.getDrawable(requireContext(), R.drawable.shape_status_completed)
        binding.viewOrderConfirmed.background = ContextCompat.getDrawable(requireContext(), R.drawable.shape_status_current)
        binding.viewOrderProcessed.background = ContextCompat.getDrawable(requireContext(), R.drawable.shape_status_current)
        binding.conDivider.background = ContextCompat.getDrawable(requireContext(), R.drawable.shape_status_current)
        binding.placedDivider.background = ContextCompat.getDrawable(requireContext(), R.drawable.shape_status_current)
        binding.placedDivider.alpha = alfa
        binding.conDivider.alpha = alfa
        binding.viewOrderConfirmed.alpha = alfa
        binding.viewOrderProcessed.alpha = alfa
        binding.imgJobAcepted.alpha = alfa
        binding.descJobAcepted.alpha = alfa
        binding.textJobAcepted.alpha = alfa
        binding.imgJobComplete.alpha = alfa
        binding.textJobComplete.alpha = alfa
        binding.descJobComplete.alpha = alfa
    }
}