package com.jujuy.bienquerer.ui.appointment.user

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Toast
import androidx.core.util.Pair
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.datepicker.MaterialDatePicker
import com.jujuy.bienquerer.api.network.Resource
import com.jujuy.bienquerer.base.BaseFragment
import com.jujuy.bienquerer.databinding.FragmentDashboardBinding
import com.jujuy.bienquerer.extensions.gone
import com.jujuy.bienquerer.extensions.visible
import com.jujuy.bienquerer.model.network.response.Appointment
import com.jujuy.bienquerer.ui.appointment.adapter.MyAppointmentsRecyclerViewAdapter
import com.jujuy.bienquerer.ui.appointment.selectProfessionItem.OnListInteractionListener
import com.jujuy.bienquerer.ui.notifications.LoadingAdapter
import com.jujuy.bienquerer.utils.autoCleared
import com.jujuy.bienquerer.viewmodel.jobs.AppointmentViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class AppointmentFragmentUser : BaseFragment(),
    OnListInteractionListener, MyAppointmentsRecyclerViewAdapter.AppoinmentClickCallback {

    private var appointmentUserList: Job? = null
    private val appointmentViewModel by viewModel<AppointmentViewModel>()
    private var binding by autoCleared<FragmentDashboardBinding>()
    private var adapter = MyAppointmentsRecyclerViewAdapter(this)

    private var filteredData: List<Appointment>? = ArrayList()
    private var data: List<Appointment>? = null

    companion object {
        fun newInstance() =
            AppointmentFragmentUser()
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        binding = FragmentDashboardBinding.inflate(inflater, container, false)
        setHasOptionsMenu(true)
        return binding.root
    }


    override fun initUIAndActions() {
        appointmentViewModel.setListAppointmentObj()

        binding.appointmentSort.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(parentView: AdapterView<*>?, selectedItemView: View?, position: Int, id: Long) {
                sortByStatus(position)
            }
        }

        binding.aptFilterDate.setOnClickListener {
            val builder: MaterialDatePicker.Builder<Pair<Long, Long>> = MaterialDatePicker.Builder.dateRangePicker()
            builder.setTitleText("Elige un rango de fechas")
            val picker = builder.build()
            picker.show(childFragmentManager, "PICKER")
            picker.addOnPositiveButtonClickListener { pair ->
                lifecycleScope.launch{
                    Timber.e("RANGO DE FECHAS: ${pair.first}, ${pair.second}")
                    appointmentViewModel.getFilteredByDate(pair.first!!, pair.second!!).collectLatest {
                        adapter.submitData(it)
                    }

                }
            }
            picker.addOnNegativeButtonClickListener {
                Timber.e("Salir del picker")
            }
        }

    }

    override fun initAdapters() {
        binding.listAppointmentRv.layoutManager = LinearLayoutManager(requireContext())
        binding.listAppointmentRv.adapter = adapter.withLoadStateHeaderAndFooter(
            header = LoadingAdapter { adapter.retry() },
            footer = LoadingAdapter { adapter.retry() }
        )
        adapter.addLoadStateListener { loadState ->
            binding.progressBar.isVisible = loadState.source.refresh is LoadState.Loading
            binding.retryButton.isVisible = loadState.source.refresh is LoadState.Error

            // Toast on any error, regardless of whether it came from RemoteMediator or PagingSource
            val errorState = loadState.source.append as? LoadState.Error
                ?: loadState.source.prepend as? LoadState.Error
                ?: loadState.append as? LoadState.Error
                ?: loadState.prepend as? LoadState.Error
            errorState?.let {
                Toast.makeText(
                    requireContext(),
                    "\uD83D\uDE28 Wooops ${it.error}",
                    Toast.LENGTH_LONG
                ).show()
            }

            if (loadState.source.refresh is LoadState.NotLoading && adapter.itemCount < 1) {
                Timber.e("Ocultar lista ${adapter.itemCount < 1} ${loadState.refresh.endOfPaginationReached} ${loadState.source.refresh is LoadState.NotLoading}")
                binding.listAppointmentRv.isVisible = false
                binding.noItemConstraintLayout.isVisible = true
            } else {
                Timber.e("Mostrar lista ${adapter.itemCount < 1} ${loadState.refresh.endOfPaginationReached} ${loadState.source.refresh is LoadState.NotLoading}")
                binding.listAppointmentRv.isVisible = true
                binding.noItemConstraintLayout.isVisible = false
            }
        }
    }

    override fun initData() {
        appointmentUserList?.cancel()
        appointmentUserList = lifecycleScope.launch {
            appointmentViewModel.getListAppointmentUserData().collectLatest {
                Timber.e("Fragment list dataaaa")
                adapter.submitData(it)
            }
        }

       /* appointmentViewModel.getListAppoinmentData().observe(this, Observer { response ->
                when(response.status){
                    Resource.Status.LOADING ->{}
                    Resource.Status.SUCCESS -> {
                        if (response.data?.isNotEmpty()!!) {
                            binding.noItemConstraintLayout.gone()
                            binding.containerAppointments.visible()
                            data = response.data
                            Timber.e(data.toString())
                            adapter.submitList(data!!.reversed())
                        }else {
                            binding.noItemConstraintLayout.visible()
                            binding.containerAppointments.gone()
                        }
                    }
                    Resource.Status.ERROR -> {}
                }
        })*/
    }

    override fun onAppointmentInteraction(item: Appointment?, pos: Int) {
        if (item != null) {
            //Utils.showDialog(requireContext(), item.toListString(), null)

        }
    }

    fun sortByStatus(id: Int) {
        Timber.e("Buscar: $id")
        if (id == 0){
            initData()
        }else{
            lifecycleScope.launch {
                appointmentViewModel.getFilteredByStatus(id).collectLatest {
                    adapter.submitData(it)
                }
            }
        }
    }

    override fun onClick(app: Appointment) {
        navigationController.navigateToDetailAppoinment(requireActivity(), app)
    }
}
