package com.jujuy.bienquerer.ui.home.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import coil.api.load
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.model.ItemImageSlider
import com.smarteist.autoimageslider.SliderViewAdapter
import kotlinx.android.synthetic.main.image_slider_layout_item.view.*


class SliderImageAdapter(val mSliderItems : ArrayList<ItemImageSlider>, val ctx: Context) : SliderViewAdapter<SliderImageAdapter.SliderAdapterVH>() {

    class SliderAdapterVH(itemView : View) : SliderViewAdapter.ViewHolder(itemView) {

        fun bindItems(image : ItemImageSlider){
            itemView.iv_auto_image_slider.load(image.imageUrl){
                crossfade(true)
                placeholder(R.drawable.default_image) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup?): SliderAdapterVH {
        val inflate: View = LayoutInflater.from(parent!!.context).inflate(R.layout.image_slider_layout_item, null)
        return SliderAdapterVH(inflate)
    }

    override fun getCount(): Int {
        return mSliderItems.size
    }

    override fun onBindViewHolder(viewHolder: SliderAdapterVH, position: Int) {
        viewHolder.bindItems(mSliderItems[position])
    }


}