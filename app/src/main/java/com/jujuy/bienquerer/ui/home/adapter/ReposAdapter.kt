package com.jujuy.bienquerer.ui.home.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import coil.api.load
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.api.ApiConstants
import com.jujuy.bienquerer.model.entity.Professional
import kotlinx.coroutines.ExperimentalCoroutinesApi
import timber.log.Timber
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class ReposAdapter(val handleClick: ListenerClick) : PagingDataAdapter<Professional, ReposAdapter.ProfessionalHomeViewHolder>(UIMODEL_COMPARATOR) {

    interface ListenerClick{
        fun onItemClick(prof: Professional)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProfessionalHomeViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_professional_list, parent, false)
        return ProfessionalHomeViewHolder(view)
    }

    override fun onBindViewHolder(holder: ProfessionalHomeViewHolder, position: Int) {
        getItem(position).let {
            holder.bind(it)
        }
    }

    companion object {
        private val UIMODEL_COMPARATOR = object : DiffUtil.ItemCallback<Professional>() {
            override fun areItemsTheSame(oldItem: Professional, newItem: Professional): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Professional, newItem: Professional): Boolean =
                oldItem == newItem
        }
    }


    inner class ProfessionalHomeViewHolder(view: View): ViewHolder(view){
        private val name: TextView = view.findViewById(R.id.professional_name)
        private val image: ImageView = view.findViewById(R.id.professional_thumbail)
        private val professions: TextView = view.findViewById(R.id.professional_category)
        private val availableDot: ImageView = view.findViewById(R.id.onlineDotHome)
        private val availableTxt: TextView = view.findViewById(R.id.availableProf)
        private var professional: Professional? = null
        init {
            view.setOnClickListener {
                Timber.e("Click en el item ${professional.toString()}")
                val position = bindingAdapterPosition
                if (position != RecyclerView.NO_POSITION){
                    val item = getItem(position)
                    if (item != null) {
                        handleClick.onItemClick(item)
                    }
                }
            }
        }

        fun bind(prof: Professional?){
            Timber.e("PROFESSIONAL: ${prof ?: ""}")
            if (prof == null){
                name.text = "Loading"
            } else {
                showProfessionalData(prof)
            }
        }

        private fun showProfessionalData(prof: Professional) {
            val sb = StringBuilder()
            this.professional = prof
            name.text = prof.name + " " + prof.lastname

            prof.available?.let {
                availableDot.setColorFilter(
                    if (it) ContextCompat.getColor(availableDot.context, R.color.onlineOn)
                    else ContextCompat.getColor(availableDot.context, R.color.onlineOff)
                )
                availableTxt.text = if (it) "Disponible" else "Ocupado"
            }

            if (prof.photo_profile_url.isNullOrEmpty()){
                image.load(R.drawable.default_image)
            }else{
                val urlComplete = ApiConstants.APP_PROFILE_IMAGES + prof.photo_profile_url
                image.load(urlComplete) {
                    crossfade(true)
                    placeholder(R.drawable.default_image)
                }
            }


            for (i in prof.profession?.indices!!){
                sb.append(prof.profession!![i].nameSubCategory + "| ")
            }
            professions.text = sb
        }

    }

}
