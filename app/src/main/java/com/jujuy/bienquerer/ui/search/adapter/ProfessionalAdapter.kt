package com.jujuy.bienquerer.ui.search.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.api.ApiConstants
import com.jujuy.bienquerer.api.network.AppExecutors
import com.jujuy.bienquerer.databinding.ItemProfessionalSearchBinding
import com.jujuy.bienquerer.model.entity.ItemSearch
import com.jujuy.bienquerer.ui.common.custom.DataBoundViewHolder
import com.jujuy.bienquerer.utils.DataBoundListAdapter


class ProfessionalAdapter( appExecutors: AppExecutors,
    var callback: ProfClickCallback
): DataBoundListAdapter<ItemSearch, ItemProfessionalSearchBinding>(
    appExecutors = appExecutors,
    diffCallback = object : DiffUtil.ItemCallback<ItemSearch>() {
        override fun areItemsTheSame(oldItem: ItemSearch, newItem: ItemSearch): Boolean {
            return oldItem.id == newItem.id
                    && oldItem.name == newItem.name
        }

        override fun areContentsTheSame(oldItem: ItemSearch, newItem: ItemSearch): Boolean {
            return oldItem.id == newItem.id
                    && oldItem.name == newItem.name
        }
    }
){
    private var lastPosition = -1

    interface ProfClickCallback {
        fun onClick(professional: ItemSearch)
    }

    override fun createBinding(parent: ViewGroup): ItemProfessionalSearchBinding {
        val binding = DataBindingUtil.inflate<ItemProfessionalSearchBinding>(LayoutInflater.from(
            parent.context
        ), R.layout.item_professional_search,parent,false)
        binding.root.setOnClickListener {
            val professional = binding.professional
            if (professional != null){
                callback.onClick(professional)
            }
        }
        return binding
    }


    override fun bind(binding: ItemProfessionalSearchBinding, item: ItemSearch) {
        binding.professional = item
        binding.professionalName.text = "${item.name?.capitalize()}"

        when(item.entity){
            "professional" -> {
                if (item.icon != null){
                        Glide.with(binding.root.context)
                            .load(ApiConstants.APP_PROFILE_IMAGES + item.icon)
                            .circleCrop()
                            .placeholder(R.drawable.circle_default_image)
                            .apply(RequestOptions.skipMemoryCacheOf(true))
                            .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                            .into(binding.professionalThumbail)
                }
                binding.professionalCategory.text = "Profesional"}
            "profession" -> {
                if (item.icon != null){
                    Glide.with(binding.root.context)
                        .load(ApiConstants.APP_IMAGES_URL + "professions/" + item.icon)
                        .circleCrop()
                        .placeholder(R.drawable.circle_default_image)
                        .apply(RequestOptions.skipMemoryCacheOf(true))
                        .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                        .into(binding.professionalThumbail)
                }
                binding.professionalCategory.text = "Profesión"}
        }
    }

    override fun bindView(
        holder: DataBoundViewHolder<ItemProfessionalSearchBinding>,
        position: Int
    ) {
        super.bindView(holder, position)
        setAnimation(holder.itemView, position)
    }

    private fun setAnimation(viewToAnimate: View, position: Int) {
        lastPosition = if (position > lastPosition) {
            val animation = AnimationUtils.loadAnimation(
                viewToAnimate.context,
                R.anim.slide_in_bottom
            )
            viewToAnimate.startAnimation(animation)
            position
        } else {
            position
        }
    }
}
