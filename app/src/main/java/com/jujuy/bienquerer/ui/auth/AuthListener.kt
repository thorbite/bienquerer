package com.jujuy.bienquerer.ui.auth

interface AuthListener {
    fun onStarted()
    fun onSuccess(token: String)
    fun onFailure(message: String)
}