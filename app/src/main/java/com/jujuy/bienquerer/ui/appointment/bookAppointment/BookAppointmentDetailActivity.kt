package com.jujuy.bienquerer.ui.appointment.bookAppointment

import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.base.BaseActivity
import com.jujuy.bienquerer.databinding.ActivityBookAppointmentDetailBinding
import com.skydoves.whatif.whatIfNotNull
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class BookAppointmentDetailActivity : BaseActivity() {
    private lateinit var binding: ActivityBookAppointmentDetailBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding  = DataBindingUtil.setContentView(this,R.layout.activity_book_appointment_detail)
        initUI(binding)
    }

    private fun initUI(binding: ActivityBookAppointmentDetailBinding) {
        initToolbar(binding.toolbar, "Crear una agenda")
        setupFragment(BookAppointmentDetailFragment())
    }

    fun changeTextToolbar(text: String?) {
        setToolbarText(binding.toolbar, text!!)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val fragment = supportFragmentManager.findFragmentById(R.id.content_frame)
        fragment.whatIfNotNull {
            it.onActivityResult(requestCode,resultCode,data)
        }
    }

    override fun onBackPressed() {
        finish()
    }
}