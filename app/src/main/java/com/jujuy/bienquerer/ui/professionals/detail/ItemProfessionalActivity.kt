package com.jujuy.bienquerer.ui.professionals.detail

import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.base.BaseActivity
import com.jujuy.bienquerer.databinding.ItemProfessionalActivityBinding
import com.jujuy.bienquerer.utils.Constants
import com.skydoves.whatif.whatIfNotNull
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class ItemProfessionalActivity : BaseActivity() {
    val binding : ItemProfessionalActivityBinding by lazy {
        DataBindingUtil.setContentView<ItemProfessionalActivityBinding>(this, R.layout.item_professional_activity) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initUI(binding)
    }


    private fun initUI(binding: ItemProfessionalActivityBinding) {
        initToolbar(binding.toolbar, intent.getStringExtra(Constants.ITEM_NAME)!!)
        setupFragment(ItemProfessionalFragment())
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val fragment = supportFragmentManager.findFragmentById(R.id.content_frame)
        fragment.whatIfNotNull {
            it.onActivityResult(requestCode,resultCode,data)
        }
    }
}