package com.jujuy.bienquerer.ui.appointment.adapter

import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.ui.appointment.selectProfessionItem.SelectableItem
import com.jujuy.bienquerer.ui.appointment.selectProfessionItem.SelectableViewHolder
import java.util.*

class SelectableRecyclerViewAdapter(listener: SelectableViewHolder.OnItemSelectedListener,
                                    items: List<String>,
                                    isMultiSelectionEnabled: Boolean,
                                    seletecItem: String): RecyclerView.Adapter<RecyclerView.ViewHolder>(),
    SelectableViewHolder.OnItemSelectedListener {

    private var mValues: List<SelectableItem>? = null
    private var isMultiSelectionEnabled = false
    var listener: SelectableViewHolder.OnItemSelectedListener? = null

    init {
        this.listener = listener
        this.isMultiSelectionEnabled = isMultiSelectionEnabled

        mValues = ArrayList()
        for (item in items) {
            if (item == seletecItem) {
                (mValues as ArrayList<SelectableItem>).add(SelectableItem(name = item, isSelected = true))
            } else {
                (mValues as ArrayList<SelectableItem>).add(SelectableItem(name = item, isSelected = false))
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val itemView: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.checked_item, parent, false)

        return SelectableViewHolder(itemView, this)
    }

    override fun getItemCount(): Int {
        return mValues?.size ?: 0
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as SelectableViewHolder
        val selectableItem = mValues!![position]
        val name: String? = selectableItem.name
        holder.textView.text = name
        if (isMultiSelectionEnabled) {
            val value = TypedValue()
            holder.textView.context.theme
                .resolveAttribute(android.R.attr.listChoiceIndicatorMultiple, value, true)
            val checkMarkDrawableResId = value.resourceId
            holder.textView.setCheckMarkDrawable(checkMarkDrawableResId)
        } else {
            val value = TypedValue()
            holder.textView.context.theme
                .resolveAttribute(android.R.attr.listChoiceIndicatorSingle, value, true)
            val checkMarkDrawableResId = value.resourceId
            holder.textView.setCheckMarkDrawable(checkMarkDrawableResId)
        }

        holder.mItem = selectableItem
        holder.setChecked(holder.mItem!!.isSelected!!)
    }

    override fun onItemSelected(item: SelectableItem?) {
        if (!isMultiSelectionEnabled) {
            for (selectableItem in mValues!!) {
                if (selectableItem != item && selectableItem.isSelected!!) {
                    selectableItem.isSelected = false
                } else if (selectableItem == item && item.isSelected!!) {
                    selectableItem.isSelected = true
                }
            }
            notifyDataSetChanged()
        }
        listener!!.onItemSelected(item)
    }

    override fun getItemViewType(position: Int): Int {
        return if (isMultiSelectionEnabled) {
            SelectableViewHolder.MULTI_SELECTION
        } else {
            SelectableViewHolder.SINGLE_SELECTION
        }
    }

    fun getSelectedItems(): ArrayList<String>? {
        val selectedItems = ArrayList<String>()
        for (item in mValues!!) {
            if (item.isSelected!!) {
                item.name?.let { selectedItems.add(it) }
            }
        }
        return selectedItems
    }

    fun clearSelections() {
        for (item in mValues!!) {
            item.isSelected = false
        }
        notifyDataSetChanged()
    }
}