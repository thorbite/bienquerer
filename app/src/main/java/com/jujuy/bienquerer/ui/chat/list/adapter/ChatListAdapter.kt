package com.jujuy.bienquerer.ui.chat.list.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import coil.api.load
import com.bumptech.glide.Glide
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.api.ApiConstants
import com.jujuy.bienquerer.api.ApiConstants.APP_PROFILE_IMAGES
import com.jujuy.bienquerer.api.network.AppExecutors
import com.jujuy.bienquerer.binding.isUrlValid
import com.jujuy.bienquerer.databinding.ChatListItemBinding
import com.jujuy.bienquerer.extensions.inVisible
import com.jujuy.bienquerer.extensions.visible
import com.jujuy.bienquerer.model.entity.Chat
import com.jujuy.bienquerer.model.entity.UserChat
import com.jujuy.bienquerer.ui.common.custom.DataBoundViewHolder
import com.jujuy.bienquerer.utils.DataBoundListAdapter
import com.jujuy.bienquerer.utils.Utils
import kotlinx.coroutines.ExperimentalCoroutinesApi
import timber.log.Timber
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class ChatListAdapter(appExecutors: AppExecutors,
                      private val isChatCheck: Boolean,
                      private val idUser: String,
                      val itemClickListener: OnItemClickListener) :
    DataBoundListAdapter<Chat, ChatListItemBinding>(
        appExecutors = appExecutors,
        diffCallback = object : DiffUtil.ItemCallback<Chat>() {
            override fun areItemsTheSame(oldItem: Chat, newItem: Chat): Boolean {
                return oldItem.id == newItem.id
                        && oldItem.receiverId == newItem.receiverId
            }

            override fun areContentsTheSame(oldItem: Chat, newItem: Chat): Boolean {
                return oldItem == newItem
            }
        }
    ) {

    interface OnItemClickListener {
        fun onItemClick(obj: Chat)
    }

    override fun createBinding(parent: ViewGroup): ChatListItemBinding {
        val binding: ChatListItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context)
            ,R.layout.chat_list_item,parent,false)
        binding.root.setOnClickListener{
            val chat: Chat = binding.chat!!
            itemClickListener.onItemClick(chat)
        }
        return binding
    }


    override fun bindView(holder: DataBoundViewHolder<ChatListItemBinding>, position: Int) {
        val chat = getItem(position)
        Timber.e("ADAPTER bindview ${chat.lastMessage?.message}")
        val context = holder.binding.root.context
        holder.binding.cardView.setCardBackgroundColor(ContextCompat.getColor(context, R.color.md_orange_A700))
        holder.binding.cardView.inVisible()

        val id = if (idUser != chat.senderId) chat.senderId else chat.receiverId

        val dbRef = FirebaseDatabase.getInstance().reference.child("users").child(id!!)
        dbRef.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {

            }

            override fun onDataChange(snapshot: DataSnapshot) {
                Timber.e(idUser)
                if (snapshot.exists()) {
                    val userChat = snapshot.getValue(UserChat::class.java)
                    holder.binding.userNameTextView.text = userChat?.userName
                    when {
                        userChat?.imageProfile.isNullOrEmpty() -> {
                            holder.binding.userImageView.setImageResource(R.drawable.circle_default_image)
                        }
                        isUrlValid(userChat?.imageProfile) -> {
                            Glide.with(holder.binding.userImageView.context)
                                .load(userChat?.imageProfile)
                                .circleCrop()
                                .placeholder(R.drawable.circle_default_image)
                                .into(holder.binding.userImageView)
                        }
                        else -> {
                            val urlComplete = APP_PROFILE_IMAGES + userChat?.imageProfile
                            Glide.with(holder.binding.userImageView.context)
                                .load(urlComplete)
                                .circleCrop()
                                .placeholder(R.drawable.circle_default_image)
                                .into(holder.binding.userImageView);
                        }
                    }
                }
            }
        })

        val ref = FirebaseDatabase.getInstance().reference.child("unseenMsgCountData")
            .child(Utils.generateKeyForChatHeadId(chat.senderId!!, chat.receiverId!!))
            .child(idUser)
        ref.addListenerForSingleValueEvent(object : ValueEventListener{
            override fun onCancelled(error: DatabaseError) {

            }

            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.child("unseenMsgCount").exists()){
                    val count: Long = snapshot.child("unseenMsgCount").value as Long
                    if (count.toInt() > 0){
                        holder.binding.messagecount.text = count.toString()
                        holder.binding.timeTextView.setTextColor(ContextCompat.getColor(context, R.color.md_orange_A700))
                        holder.binding.messagecount.visible()
                        holder.binding.cardView.visible()
                        if (count.toInt() > 9)
                            holder.binding.messagecount.text = "9+"
                    }else {
                        holder.binding.timeTextView.setTextColor(ContextCompat.getColor(context, R.color.md_grey_600))
                    }
                }
            }
        })
    }


    override fun bind(binding: ChatListItemBinding, item: Chat) {
        Timber.e("ADAPTER bind ${item.lastMessage?.message}")
        binding.chat = item
    }
}
