package com.jujuy.bienquerer.ui.appointment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.RadioButton
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import com.jujuy.bienquerer.MainActivity
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.base.BaseFragment
import com.jujuy.bienquerer.databinding.FragmentJobBinding
import com.jujuy.bienquerer.extensions.gone
import com.jujuy.bienquerer.extensions.visible
import com.jujuy.bienquerer.ui.appointment.professional.AppointmentProfessionalFragment
import com.jujuy.bienquerer.ui.appointment.user.AppointmentFragmentUser
import com.jujuy.bienquerer.utils.autoCleared
import kotlinx.coroutines.ExperimentalCoroutinesApi
import timber.log.Timber
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class JobFragment : BaseFragment() {

    companion object {
        fun newInstance() = JobFragment()
    }

    private var binding by autoCleared<FragmentJobBinding>()
    private val userFragment: AppointmentFragmentUser? by lazy { AppointmentFragmentUser() }
    private val professionalFragment: AppointmentProfessionalFragment? by lazy { AppointmentProfessionalFragment() }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentJobBinding.inflate(inflater, container, false)
        activity?.let { it.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN) }
        checkUser()
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }


    private fun checkUser() {
        Timber.e("es profesional: $userIsProfessional")
        if (!userIsProfessional){
            binding.radioGroup.gone()
            setupFragment(userFragment!!, R.id.frame)
        }else{
            setupFragment(professionalFragment!!, R.id.frame)
            binding.radioGroup.visible()
        }
    }


    override fun initUIAndActions() {
        binding.radioGroup.setOnCheckedChangeListener { radioGroup, i ->
            val rd: RadioButton = radioGroup.findViewById(i)
            when(rd){
                binding.option1 -> {
                    Timber.e("1")
                    setupFragment(professionalFragment!!, R.id.frame)
                }
                binding.option2 -> {
                    Timber.e("2")
                    setupFragment(userFragment!!, R.id.frame)
                }
            }
        }
    }

    fun setupFragment(fragment: Fragment, frameId: Int){
        try {
            requireActivity().supportFragmentManager.beginTransaction()
                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                .replace(frameId, fragment)
                .commitAllowingStateLoss()
        }catch (e:Exception){
            Timber.e(e)
        }
    }

    override fun initAdapters() {}

    override fun initData() {}

    override fun onAttach(context: Context) {
        super.onAttach(context)
        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                (activity as MainActivity).setBottomNavigationMenu(0)
            }
        })
    }
}