package com.jujuy.bienquerer.ui.scoringJob

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.api.network.Resource
import com.jujuy.bienquerer.base.BaseFragment
import com.jujuy.bienquerer.databinding.ScoringFragmentBinding
import com.jujuy.bienquerer.extensions.showLongToast
import com.jujuy.bienquerer.model.network.response.Client
import com.jujuy.bienquerer.ui.appointment.bookAppointment.BookAppointmentDetailActivity
import com.jujuy.bienquerer.utils.PSDialogMsg
import com.jujuy.bienquerer.utils.autoCleared
import com.jujuy.bienquerer.viewmodel.jobs.AppointmentViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import kotlin.time.ExperimentalTime

@ExperimentalTime
@ExperimentalCoroutinesApi
class ScoringFragment : BaseFragment() {

    private var binding by autoCleared<ScoringFragmentBinding>()
    private val scoreViewModel by viewModel<ScoringViewModel>()
    private val psDialogMsg: PSDialogMsg by lazy { PSDialogMsg(activity, false) }

    companion object {
        fun newInstance() = ScoringFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = ScoringFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun initUIAndActions() {
        scoreViewModel.professionalContainer = requireActivity().intent.getParcelableExtra<Client>("professionalToScore") as Client
        scoreViewModel.professionOfProfessional = requireActivity().intent.getStringExtra("professionToScore").toString()
        scoreViewModel.jobId = requireActivity().intent.getStringExtra("jobId").toString()

        binding.professional = scoreViewModel.professionalContainer
        binding.profession = scoreViewModel.professionOfProfessional

        binding.ratingBar.setOnRatingBarChangeListener { ratingBar, float, bool ->
            binding.tvDescriptionRating.text = float.toString()
            when(binding.ratingBar.rating.toInt()){
                1 -> binding.tvDescriptionRating.text = "Muy Mal!"
                2 -> binding.tvDescriptionRating.text = "Necesita mejorar"
                3 -> binding.tvDescriptionRating.text = "Bien"
                4 -> binding.tvDescriptionRating.text = "Muy Bien!"
                5 -> binding.tvDescriptionRating.text = "Excelente!"
                else -> binding.tvDescriptionRating.text = ""
            }
        }

        binding.confirmFeedback.setOnClickListener {
            scoreViewModel.setSaveScoreObj(
                scoreViewModel.jobId, binding.ratingBarPuntuality.rating.toString(),
                binding.ratingBarResult.rating.toString(), binding.ratingBarPrice.rating.toString(),
                binding.edtCommentScore.text.toString()
            )
//            if (binding.edtCommentScore.text.isEmpty()){
//                showLongToast("Por favor complete el espacio disponible con un comentario")
//            }else {
//                binding.edtCommentScore.setText("")
//                //binding.ratingBar.rating = 0f
//
//                showLongToast(binding.ratingBarPuntuality.rating.toString() + " " + binding.ratingBarResult.rating.toString() + " " + binding.ratingBarPrice.rating.toString())
//            }
        }
    }

    override fun initAdapters() {
    }

    override fun initData() {
        scoreViewModel.getSaveScoreData()?.observe(this, { response ->
            when(response.status){
                Resource.Status.LOADING ->{
                }
                Resource.Status.SUCCESS -> {
                    if (response.data?.code == 200){
                        psDialogMsg.showSuccessDialog(response.data.message, getString(R.string.app__ok))
                        psDialogMsg.okButton.setOnClickListener {
                            psDialogMsg.cancel()
                            activity?.finish()
                        }
                        psDialogMsg.show()
                    }else{
                        psDialogMsg.showErrorDialog(response.data?.message, getString(R.string.app__ok))
                        psDialogMsg.show()
                    }
                }
                Resource.Status.ERROR -> {
                    psDialogMsg.showErrorDialog(response.message, getString(R.string.app__ok))
                    psDialogMsg.show()
                }
            }
        })
    }

}