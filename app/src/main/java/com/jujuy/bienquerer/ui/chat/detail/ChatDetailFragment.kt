package com.jujuy.bienquerer.ui.chat.detail

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.Task
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageTask
import com.google.firebase.storage.UploadTask
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.api.network.Resource
import com.jujuy.bienquerer.base.BaseFragment
import com.jujuy.bienquerer.databinding.FragmentChatDetailBinding
import com.jujuy.bienquerer.extensions.gone
import com.jujuy.bienquerer.model.Message
import com.jujuy.bienquerer.model.entity.Chat
import com.jujuy.bienquerer.model.entity.UserChat
import com.jujuy.bienquerer.ui.chat.detail.adapter.ChatAdapter
import com.jujuy.bienquerer.utils.*
import com.jujuy.bienquerer.viewmodel.chat.ChatViewModel
import com.jujuy.bienquerer.viewmodel.user.UserViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class ChatDetailFragment : BaseFragment() {

    companion object {
        fun newInstance() = ChatDetailFragment()
    }

    private val chatViewModel: ChatViewModel by viewModel()
    private val userlViewModel by viewModel<UserViewModel>()
    private var binding by autoCleared<FragmentChatDetailBinding>()
    private var chatsAdapter by autoCleared<ChatAdapter>()
    private val loadingMsg: LoadingMsg by lazy { LoadingMsg(requireActivity(), false) }
    private val psDialogMsg: PSDialogMsg by lazy { PSDialogMsg(activity, false) }

    private var layoutManager1: LinearLayoutManager? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentChatDetailBinding.inflate(inflater, container, false)
        activity?.let { it.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN) }
        setHasOptionsMenu(true)
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun initUIAndActions() {
        if (!connectivity.isConnected) {
            Toast.makeText(context, R.string.no_internet_error, Toast.LENGTH_SHORT).show()
        }

        loginUserId = pref.getString(Constants.USER_ID, Constants.EMPTY_STRING).toString()

        val layoutManager = binding.rvChatDetails.layoutManager as LinearLayoutManager?

        if (layoutManager != null) {
            layoutManager.reverseLayout = true
        }

        if (loginUserEmail.isEmpty() && loginUserPwd.isEmpty() || loginUserPwd == null) {
            if (userIdToVerify.isEmpty()) {
                if (loginUserId == "") {
                    navigationController.navigateToUserLoginActivity(requireActivity())
                }
            }
        }

        binding.sendMessage.setOnClickListener {
            if (!connectivity.isConnected) {
                Toast.makeText(context, R.string.no_internet_error, Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (binding.inputEditText.text.toString().isNotEmpty()) {
                sendMessageToUser(binding.inputEditText.text.toString().trim())

                binding.inputEditText.setText("")
            }
        }

        binding.addImageView.setOnClickListener {
            if (connectivity.isConnected) {
                try {
                    if (Utils.isStoragePermissionGranted(requireActivity())) {
                        ImagePicker.with(this)
                            .crop()
                            .compress(512)
                            .start(Constants.RESULT_LOAD_IMAGE_PROFILE)

                    }
                } catch (e: Exception) {
                    Timber.e("ERROR $e")
                }
            } else {
                psDialogMsg.showWarningDialog(
                    getString(R.string.no_internet_error),
                    getString(R.string.app__ok)
                )
                psDialogMsg.show()
            }
        }

        binding.rvChatDetails.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                layoutManager1 = recyclerView.layoutManager as LinearLayoutManager?

                if (layoutManager1 != null) {
                    chatViewModel.loadingDirection = Utils.LoadingDirection.bottom
                    val firstPosition = layoutManager1!!
                        .findLastVisibleItemPosition()
                    if (firstPosition == chatsAdapter.itemCount - 1) {
                        if (!binding.loadingMore && !chatViewModel.forceEndLoading) {
                            chatViewModel.setFetchMessagesFromConversationObj(
                                loginUserId,
                                chatViewModel.receiverId
                            )
                        }
                    }
                }
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Constants.RESULT_LOAD_IMAGE_PROFILE && resultCode == Constants.RESULT_OK && null != data) {
            loadingMsg.showloaderDialog()
            loadingMsg.show()
            val fileUri = data.data

            val storageReference = FirebaseStorage.getInstance().reference.child("Chat Images")
            val ref = FirebaseDatabase.getInstance().reference
            val messageId = ref.push().key
            val filePath = storageReference.child("$messageId.jpg")

            val uploadTask: StorageTask<*>
            uploadTask = filePath.putFile(fileUri!!)
            uploadTask.continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
                if (!task.isSuccessful) {
                    task.exception?.let {
                        throw  it
                    }
                }
                return@Continuation filePath.downloadUrl
            }).addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val key =
                        Utils.generateKeyForChatHeadId(loginUserId, chatViewModel.receiverId!!)
                    val date = Calendar.getInstance().time
                    val dowloadUrl = task.result
                    val url = dowloadUrl.toString()
                    val message = Message(
                        Utils.generateKeyForChatHeadId(loginUserId, chatViewModel.receiverId!!),
                        chatViewModel.receiverId, url, Constants.CHAT_TYPE_IMAGE, loginUserId
                    )
                    val lastMessage = Message(
                        Utils.generateKeyForChatHeadId(loginUserId, chatViewModel.receiverId!!),
                        chatViewModel.receiverId, "Imagen", Constants.CHAT_TYPE_IMAGE, loginUserId
                    )
                    chatViewModel.setSaveMessagesToFirebaseObj(
                        message,
                        loginUserId,
                        chatViewModel.receiverId
                    )
                    chatViewModel.setSaveChatRoomToFirebaseObj(
                        Chat(
                            key,
                            date,
                            loginUserId,
                            chatViewModel.receiverId,
                            false,
                            lastMessage,
                            imageProfile = chatViewModel.itemImagePath,
                            name = chatViewModel.itemName
                        ),
                        loginUserId, chatViewModel.receiverId
                    )
                    loadingMsg.cancel()
                }
            }

        }

    }

    private fun sendMessageToUser(message: String) {
        val dateFormat: DateFormat = SimpleDateFormat("dd MM yy hh:mm a")
        val date = Date()
        val sentDate: String = dateFormat.format(date)
        val key = Utils.generateKeyForChatHeadId(loginUserId, chatViewModel.receiverId!!)

        val lastMessage =
            Message(key, chatViewModel.receiverId!!, message, Constants.CHAT_TYPE_TEXT, loginUserId)

        chatViewModel.lastMessage = lastMessage

        chatViewModel.setSaveMessagesToFirebaseObj(
            lastMessage,
            loginUserId,
            chatViewModel.receiverId
        )
        chatViewModel.setSaveChatRoomToFirebaseObj(
            Chat(
                key, date, loginUserId, chatViewModel.receiverId, false, lastMessage,
                imageProfile = chatViewModel.itemImagePath, name = chatViewModel.itemName
            ),
            loginUserId, chatViewModel.receiverId
        )

        chatViewModel.setUpdateUnseenMessagesObj(key, chatViewModel.receiverId!!)


    }

    override fun initAdapters() {
        if (activity != null) {
            chatViewModel.receiverId =
                requireActivity().intent.getStringExtra(Constants.RECEIVE_USER_ID)
            chatViewModel.receiverName =
                requireActivity().intent.getStringExtra(Constants.RECEIVE_USER_NAME)
            chatViewModel.receiverLastName =
                requireActivity().intent.getStringExtra(Constants.RECEIVE_USER_LAST_NAME)
            chatViewModel.receiverUserImgUrl =
                requireActivity().intent.getStringExtra(Constants.RECEIVE_USER_IMG_URL)

            Timber.e("IMAGE URL: ${chatViewModel.receiverUserImgUrl}")
        }
        getItemDetailData()

        val chatListAdapter =
            ChatAdapter(appExecutors, loginUserId, object : ChatAdapter.clickCallBack {
                override fun onImageClicked(message: Message?) {

                }

                override fun onProfileClicked() {

                }

            }, chatViewModel.receiverId, chatViewModel.receiverUserImgUrl, getMsgAdapterListener())

        chatsAdapter = chatListAdapter
        binding.rvChatDetails.adapter = chatListAdapter

    }

    private fun getMsgAdapterListener(): ChatAdapter.MsgAdapterListener {
        return object : ChatAdapter.MsgAdapterListener {
            override fun showPic(view: View, message: Message) {
                Timber.e(message.id)
            }
        }
    }

    private fun getItemDetailData() {
        userlViewModel.setDetailUser(chatViewModel.receiverId.toString())
        userlViewModel.getDetailUser()?.observe(this, Observer { resource ->
            when (resource.status) {
                Resource.Status.LOADING -> {
                }
                Resource.Status.SUCCESS -> {
                    resource.data?.let {
                        chatViewModel.itemImagePath = it.person?.photoProfileUrl.toString()
                        chatViewModel.itemName = "${it.person?.name} ${it.person?.lastname}"
                        (activity as ChatDetailActivity?)?.changeText(chatViewModel.itemName.capitalize())
                    }
                }
                Resource.Status.ERROR -> {
                    // Error State

                }
            }
        })
    }

    override fun initData() {
        chatViewModel.setRegisterUserToFirebaseObj(loginUserEmail, sessionManager.fetchUserCode()!!)
        chatViewModel.setFetchMessagesFromConversationObj(loginUserId, chatViewModel.receiverId)
        chatViewModel.setResetUnseenMessagesObj(
            Utils.generateKeyForChatHeadId(
                loginUserId,
                chatViewModel.receiverId!!
            ), loginUserId
        )

        Timber.e("DATOS PARA ENVIAR: $loginUserId , ${chatViewModel.receiverId}")
        chatViewModel.getRegisterUserToFirebaseData()?.observe(this, Observer { result ->
            if (result != null) {
                when (result.status) {
                    Resource.Status.SUCCESS -> {
                        chatViewModel.setLoginUserToFirebaseObj(
                            loginUserEmail,
                            sessionManager.fetchUserCode()!!
                        )
                    }
                    Resource.Status.ERROR -> chatViewModel.setLoginUserToFirebaseObj(
                        loginUserEmail,
                        sessionManager.fetchUserCode()!!
                    )
                    else -> Timber.e("ERROR firebase else $result")
                }
            }
        })

        chatViewModel.getLoginUserToFirebaseData()?.observe(this, Observer { booleanResource ->
            if (booleanResource != null) {
                when (booleanResource.status) {
                    Resource.Status.SUCCESS -> {
                        chatViewModel.setSaveUserChatObj(
                            UserChat(
                                loginUserId,
                                "$loginUserName $loginUserLastname",
                                0,
                                profileImageUrl,
                                fcmToken = sessionManager.fetchTokenFirebase()
                            )
                        )
                    }
                    Resource.Status.ERROR -> {
                    }
                }
            }
        })

        chatViewModel.getFetchMessagesFromConversationData()?.observe(this, Observer { resource ->
            if (resource != null) {
                when (resource.status) {
                    Resource.Status.SUCCESS -> {
                        chatViewModel.setGetMessagesFromDatabaseObj(
                            loginUserId,
                            chatViewModel.receiverId
                        )
                        chatViewModel.setLoadingState(false)
                    }
                    Resource.Status.ERROR -> {
                        chatViewModel.setGetMessagesFromDatabaseObj(
                            loginUserId,
                            chatViewModel.receiverId
                        )
                        chatViewModel.setLoadingState(false)
                        chatViewModel.forceEndLoading = true
                    }

                }
            }
        })

        chatViewModel.getSaveMessagesToFirebaseData()?.observe(this, Observer { booleanResource ->
            if (booleanResource != null) {
                when (booleanResource.status) {
                    Resource.Status.SUCCESS -> {
                        binding.inputEditText.text.clear()
                        binding.progressBar3.gone()
                        binding.sendMessage.isClickable = true
                        binding.addImageView.isClickable = true
                        Timber.e("Mensajes guardados-......")
                        chatViewModel.setFetchMessagesFromConversationObj(
                            loginUserId,
                            chatViewModel.receiverId
                        )
                        if (binding.rvChatDetails != null) {
                            val layoutManager =
                                binding.rvChatDetails.layoutManager as LinearLayoutManager
                            layoutManager.scrollToPosition(0)
                        }
                        //sendNotification(chatViewModel.lastMessage)
                    }
                    Resource.Status.ERROR -> {
                        binding.progressBar3.gone()
                        binding.sendMessage.isClickable = true
                        binding.addImageView.isClickable = true
                    }
                }
            }

        })

        chatViewModel.getGetMessagesFromDatabaseData()!!.observe(this, Observer { listresource ->
            if (listresource != null) {
                if (listresource.isNotEmpty()) {
                    for (i in listresource.indices) {
                        Timber.e("mensajes desde room: " + listresource[i].message)
                    }
                    chatViewModel.setResetUnseenMessagesObj(
                        Utils.generateKeyForChatHeadId(
                            loginUserId,
                            chatViewModel.receiverId!!
                        ), loginUserId
                    )
                    replaceData(listresource)
                }
            }
        })

        chatViewModel.getLoadingState()!!.observe(this, Observer { loadingState ->
            binding.loadingMore = chatViewModel.isLoading
        })

        chatViewModel.getSaveChatRoomToFirebaseData()?.observe(this, Observer { booleanResource ->
            if (booleanResource != null) {
                when (booleanResource.status) {
                    Resource.Status.SUCCESS -> {
                    }
                    Resource.Status.ERROR -> {
                    }
                }
            }
        })

        chatViewModel.getSaveUserChatData()?.observe(this, Observer { booleanResource ->
            if (booleanResource != null) {
                when (booleanResource.status) {
                    Resource.Status.SUCCESS -> {
                    }
                    Resource.Status.ERROR -> {
                    }
                }
            }
        })

        chatViewModel.getUpdateUnSeenMessagesData()?.observe(this, Observer { booleanResource ->
            if (booleanResource != null) {
                when (booleanResource.status) {
                    Resource.Status.SUCCESS -> {
                    }
                    Resource.Status.ERROR -> {
                    }
                }
            }
        })

        chatViewModel.getResetUnSeenMessagesData()?.observe(this, Observer { booleanResource ->
            if (booleanResource != null) {
                when (booleanResource.status) {
                    Resource.Status.SUCCESS -> {
                    }
                    Resource.Status.ERROR -> {
                    }
                }
            }
        })

    }

    /*private fun sendNotification(lastMessage: Message?) {
        var userChat: UserChat? = null
        val dbRef = FirebaseDatabase.getInstance().reference.child("users")
        val query = dbRef.orderByKey().equalTo(lastMessage?.receiverByUserId)
        query.addListenerForSingleValueEvent(object : ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {
                for (snap in snapshot.children){
                    userChat = snap.getValue(UserChat::class.java)!!
                }

                val messageBody = MessageBody(
                    chatViewModel.senderName,
                    userChat?.fcmToken.toString(), lastMessage)

                network.send(messageBody, object : NetworkMessage.MessageCallback {
                    override fun onSuccess() {
                        Timber.e("SUCCESSS")
                    }

                    override fun onFailed(message: String?) {
                        Timber.e("$message")
                    }
                })
            }

            override fun onCancelled(error: DatabaseError) {

            }

        })


    }*/

    //var seenListener : ValueEventListener? = null
    /*private fun seenMessage(userId: String){
        refer = FirebaseDatabase.getInstance().reference.child("Chats")
        seenListener = refer!!.addValueEventListener(object : ValueEventListener{
            override fun onCancelled(error: DatabaseError) {

            }

            override fun onDataChange(snapshot: DataSnapshot) {
                for (data in snapshot.children){
                    val chat = data.getValue(Chat::class.java)
                    if (chat!!.receiverId.equals(loginUserId) && chat.senderId.equals(userId)){
                        val hashMap = HashMap<String, Any>()
                        hashMap["seen"] = true
                        data.ref.updateChildren(hashMap)
                    }
                }
            }

        })

    }*/

    private fun replaceData(messageList: List<Message>) {
        messageList.toMutableList().reverse()
        val updatedMessages: List<Message> = regenerateMessageList(messageList)
        Timber.e("Replace data ${updatedMessages.toString()}")
        chatsAdapter.submitList(updatedMessages.reversed())
        binding.executePendingBindings()
    }

    private fun regenerateMessageList(messageList: List<Message>): List<Message> {
        var currentDate = ""
        for (i in messageList.indices) {
            val date: Date? = Utils.getDateCurrentTimeZone(messageList[i].addedDate)
            val dateString: String = Utils.getDateString(date, "yyyy.MM.dd")
            val timeString: String = Utils.getDateString(date, "hh:mm a")
            messageList[i].date = date
            messageList[i].dateString = dateString
            messageList[i].time = timeString
            if (currentDate.isNotEmpty() && currentDate != dateString) {
                val obj = messageList[i]

                // add new message
                messageList.toMutableList().add(
                    i, Message(
                        "", obj.receiverByUserId, currentDate,
                        Constants.CHAT_TYPE_DATE, obj.sendByUserId
                    )
                )
                messageList[i].date = date
                messageList[i].dateString = dateString
                currentDate = dateString
            }
            if (currentDate.isEmpty()) {
                currentDate = dateString
            }

        }

        return messageList
    }
}
