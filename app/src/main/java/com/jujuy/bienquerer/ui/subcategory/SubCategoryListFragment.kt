package com.jujuy.bienquerer.ui.subcategory

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.lifecycle.Observer
import com.jujuy.bienquerer.api.network.Resource
import com.jujuy.bienquerer.base.BaseFragment
import com.jujuy.bienquerer.databinding.SubCategoryListFragmentBinding
import com.jujuy.bienquerer.model.ItemParameterHolder
import com.jujuy.bienquerer.model.entity.SubCategory
import com.jujuy.bienquerer.ui.subcategory.adapter.SubCategoryAdapter
import com.jujuy.bienquerer.viewmodel.subcategory.SubCategoryViewModel
import com.jujuy.bienquerer.utils.Constants
import com.jujuy.bienquerer.utils.autoCleared
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class SubCategoryListFragment : BaseFragment(){

    companion object {
        fun newInstance() = SubCategoryListFragment()
    }

    private var binding by autoCleared<SubCategoryListFragmentBinding>()
    private val subCategoryViewModel by viewModel<SubCategoryViewModel>()
    private var itemParameterHolder = ItemParameterHolder()
    var adapter by autoCleared<SubCategoryAdapter>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = SubCategoryListFragmentBinding.inflate(inflater, container, false)
        activity?.let {
            requireActivity().window.setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN
            )
        }
        binding.loadingMore = connectivity.isConnected
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun initUIAndActions() {
        binding.subcategoryList.setHasFixedSize(true)
        binding.subcategoryList.setItemViewCacheSize(25)
    }

    override fun initAdapters() {
        val nvAdapter: SubCategoryAdapter =
            SubCategoryAdapter(appExecutors, object : SubCategoryAdapter.SubCategoryClickCallback {
                override fun onClick(subCategory: SubCategory) {
                    itemParameterHolder.sub_cat_id = subCategory.idSubCategory.toString()
                    navigationController.navigateToHomeFilteringActivity(
                        activity!!,
                        itemParameterHolder,
                        subCategory.nameSubCategory,
                        Constants.MAP_MILES
                    )
                }
            })
        binding.subcategoryList.adapter = nvAdapter
        adapter = nvAdapter
    }

    override fun initData() {
        getIntentData()
        loadNews()
    }

    private fun getIntentData() {
        try {
            if (activity != null) {
                if (requireActivity().intent.extras != null) {
                    Timber.e(requireActivity().intent.extras!!.getString(Constants.CATEGORY_ID)!!)
                    subCategoryViewModel.catId =
                        requireActivity().intent.extras!!.getString(Constants.CATEGORY_ID)!!
                    itemParameterHolder.cat_id = subCategoryViewModel.catId
                }
            } else {
                Timber.e("Activity es null")
            }
        } catch (e: Exception) {
            Timber.e(e)
        }
    }


    private fun loadAllProfessions() {
        subCategoryViewModel.getProfessionListData()?.observe(this, Observer {
            when (it.status) {
                Resource.Status.LOADING -> {
                    Timber.e("loading..... ${it.data}")
                }
                Resource.Status.SUCCESS -> {
                    binding.loadingMore = false
                    Timber.e("datos cargados y listos..... ${it.data}")
                    loadNews()
                }
                Resource.Status.ERROR -> {
                    binding.loadingMore = false
                }
            }
        })
    }

    private fun loadNews() {
        subCategoryViewModel.getSubCategoryListData(subCategoryViewModel.catId)?.observe(this, Observer { listResource ->
            Timber.e("Datos de la BD: $listResource")
            if (listResource.isNotEmpty()) {
                binding.loadingMore = false
                replaceData(listResource)
            }else{
                loadAllProfessions()
            }
        })
    }

    private fun replaceData(newsList: List<SubCategory>) {
        Timber.d(newsList.toString())
        adapter.submitList(newsList)
        binding.executePendingBindings()
    }
}
