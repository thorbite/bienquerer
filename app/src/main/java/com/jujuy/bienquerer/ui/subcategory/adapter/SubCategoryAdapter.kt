package com.jujuy.bienquerer.ui.subcategory.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.api.network.AppExecutors
import com.jujuy.bienquerer.databinding.ItemSubCategoryAdapterBinding
import com.jujuy.bienquerer.model.entity.SubCategory
import com.jujuy.bienquerer.ui.common.custom.DataBoundViewHolder
import com.jujuy.bienquerer.utils.DataBoundListAdapter


class SubCategoryAdapter(appExecutors: AppExecutors,
    var callback: SubCategoryClickCallback
): DataBoundListAdapter<SubCategory, ItemSubCategoryAdapterBinding>(
    appExecutors= appExecutors,
    diffCallback = object : DiffUtil.ItemCallback<SubCategory>() {
        override fun areItemsTheSame(oldItem: SubCategory, newItem: SubCategory): Boolean {
            return oldItem.idSubCategory == newItem.idSubCategory
                    && oldItem.nameSubCategory == newItem.nameSubCategory
        }

        override fun areContentsTheSame(oldItem: SubCategory, newItem: SubCategory): Boolean {
            return oldItem.idSubCategory == newItem.idSubCategory
                    && oldItem.nameSubCategory == newItem.nameSubCategory
        }
    }
) {
    private var lastPosition = -1

    interface SubCategoryClickCallback {
        fun onClick(subCategory: SubCategory)
    }

    override fun createBinding(parent: ViewGroup): ItemSubCategoryAdapterBinding {
        val binding = DataBindingUtil.inflate<ItemSubCategoryAdapterBinding>(LayoutInflater.from(
            parent.context
        ), R.layout.item_sub_category_adapter,parent,false)
        binding.root.setOnClickListener {
            val subCategory = binding.subCategory
            if (subCategory != null && callback != null){
                callback.onClick(subCategory)
            }
        }
        return binding
    }

    override fun bindView(
        holder: DataBoundViewHolder<ItemSubCategoryAdapterBinding>,
        position: Int
    ) {
        super.bindView(holder, position)
        setAnimation(holder.itemView, position)
    }

    private fun setAnimation(viewToAnimate: View, position: Int) {
        lastPosition = if (position > lastPosition) {
            val animation = AnimationUtils.loadAnimation(
                viewToAnimate.context,
                R.anim.slide_in_bottom
            )
            viewToAnimate.startAnimation(animation)
            position
        } else {
            position
        }
    }

    override fun bind(binding: ItemSubCategoryAdapterBinding, item: SubCategory) {
        binding.subCategory = item
    }

}