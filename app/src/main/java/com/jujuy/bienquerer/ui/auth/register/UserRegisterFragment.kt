package com.jujuy.bienquerer.ui.auth.register

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.CheckBox
import androidx.appcompat.widget.AppCompatCheckBox
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.api.network.Resource
import com.jujuy.bienquerer.base.BaseFragment
import com.jujuy.bienquerer.databinding.RegisterFragmentBinding
import com.jujuy.bienquerer.extensions.disable
import com.jujuy.bienquerer.extensions.enable
import com.jujuy.bienquerer.extensions.showLongToast
import com.jujuy.bienquerer.model.entity.Person
import com.jujuy.bienquerer.model.entity.User
import com.jujuy.bienquerer.utils.*
import com.jujuy.bienquerer.viewmodel.user.UserViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.regex.Matcher
import java.util.regex.Pattern
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class UserRegisterFragment() : BaseFragment() {
    private val userViewModel by viewModel<UserViewModel>()
    private val psDialogMsg: PSDialogMsg by lazy { PSDialogMsg(activity, false) }
    private val loadingMsg: LoadingMsg by lazy { LoadingMsg(requireActivity(), false) }
    private var binding by autoCleared<RegisterFragmentBinding>()
    lateinit var alertDialog: AlertDialog.Builder;

    companion object {
        fun newInstance() =
            UserRegisterFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = RegisterFragmentBinding.inflate(inflater, container, false)
        activity?.let {
            it.window?.setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN
            )
        }
        return binding.root
    }

    override fun initUIAndActions() {
        fadeIn(binding.root)
        binding.backToLogin.setOnClickListener {
            if (connectivity.isConnected) {
                Utils.navigateToLogin(requireActivity(), navigationController)
            } else {
                psDialogMsg.showWarningDialog(
                    getString(R.string.no_internet_error),
                    getString(R.string.message_ok_close)
                )
            }
        }

        binding.registerSubmit.setOnClickListener {
            registerUser()
        }


        checkTerminosyCondiciones()
        initializeAlertDialog()

        binding.txtTerminosCondiciones.setOnClickListener {
            alertDialog.show()
        }
    }

    private fun initializeAlertDialog() {
        alertDialog = AlertDialog.Builder(requireContext())
        alertDialog.setTitle("Términos y Condiciones")
        alertDialog.setMessage(getString(R.string.condiciones))

        alertDialog.setPositiveButton("Aceptar") { dialog, which ->

        }
    }

    private fun checkTerminosyCondiciones(){
        binding.checkBoxTermCond.setOnCheckedChangeListener { compoundButton, isChecked ->
            if (isChecked) binding.registerSubmit.enable() else binding.registerSubmit.disable()
        }
    }

    private fun registerUser() {
        Utils.hideKeyboard(requireActivity())
        val id = null
        val name = binding.registerName.text.toString().trim()
        val lastname = binding.registerLastname.text.toString().trim()
        val username = binding.registerUsername.text.toString().trim()
        val email = binding.registerEmail.text.toString().trim()
        val phone = binding.codeEtRegister.text.toString().trim() + binding.numberEtRegister.text.toString().trim()
        val address = binding.registerAddress.text.toString().trim()
        val password = binding.registerPassword.text.toString().trim()

        if (name == "") {
            psDialogMsg.showWarningDialog(
                "El campo nombre esta vacio",
                getString(R.string.app_ok)
            )
            psDialogMsg.show()
            return
        }
        if (username == "") {
            psDialogMsg.showWarningDialog(
                "El campo usuario esta vacio",
                getString(R.string.app_ok)
            )
            psDialogMsg.show()
            return
        }
        if (lastname == "") {
            psDialogMsg.showWarningDialog(
                "El campo apellido esta vacio",
                getString(R.string.app_ok)
            )
            psDialogMsg.show()
            return
        }
        if (email == "") {
            psDialogMsg.showWarningDialog(
                "El campo email esta vacio",
                getString(R.string.app_ok)
            )
            psDialogMsg.show()
            return
        }

        if (!isValidPassword(password)) {
            psDialogMsg.showWarningDialog(
                "La contraseña debe contener una combinación de letras mayúsculas y minúsculas (4-20)",
                getString(R.string.app_ok)
            )
            psDialogMsg.show()
            return
        }

        val person = Person(name = name, lastname = lastname, address = address)
        val fcmKey = sessionManager.fetchTokenFirebase()
        userViewModel.setRegisterUser(
            User(
                username = username,
                password = password,
                email = email,
                phone = phone,
                emailVerified = false,
                phoneVerified = false,
                person = person,
                fcmKey = fcmKey
            )
        )
    }

    private fun isValidPassword(password: String): Boolean {
        val matcher: Matcher =
            Pattern.compile("((?=.*[a-z])(?=.*\\d)(?=.*[A-Z]).{4,20})")
                .matcher(password)
        return matcher.matches()
    }

    override fun initAdapters() {}

    override fun initData() {
        userViewModel.getRegisterUser().observe(this, Observer { listResource ->
                when (listResource.status) {
                    Resource.Status.LOADING -> {
                        loadingMsg.showloaderDialog()
                        loadingMsg.show()
                    }
                    Resource.Status.SUCCESS -> {
                        loadingMsg.cancel()
                        listResource.data?.let {
                            if (it.statusCode == 200) {
                                Toast.makeText(activity, it.message, Toast.LENGTH_LONG).show()
                                psDialogMsg.showInfoDialog(it.message, getString(R.string.app__ok))
                                psDialogMsg.show()
                                psDialogMsg.okButton.setOnClickListener {
                                    Utils.navigateAfterUserRegister(
                                        requireActivity(),
                                        navigationController
                                    )
                                }
                            } else {
                                psDialogMsg.showErrorDialog(
                                    it.message,
                                    getString(R.string.message_ok_close)
                                )
                                psDialogMsg.show()
                            }
                        }
                    }
                    Resource.Status.ERROR -> {
                        loadingMsg.cancel()
                        psDialogMsg.showErrorDialog(
                            listResource.message,
                            getString(R.string.message_ok_close)
                        )
                        psDialogMsg.show()
                    }
                }
        })
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {

            }
        })
    }

}
