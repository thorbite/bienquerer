package com.jujuy.bienquerer.viewmodel.subcategory

import androidx.lifecycle.*
import com.jujuy.bienquerer.api.network.AbsentLiveData
import com.jujuy.bienquerer.api.network.Resource
import com.jujuy.bienquerer.base.BaseViewModel
import com.jujuy.bienquerer.model.entity.SubCategory
import com.jujuy.bienquerer.repository.CategoryRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.map
import kotlin.time.ExperimentalTime
@ExperimentalCoroutinesApi
@ExperimentalTime
class SubCategoryViewModel(private val categoryRepository: CategoryRepository) : BaseViewModel() {

    private var allSubCategoryListData: LiveData<Resource<List<SubCategory>>>? = null
    private var allSubCategoryListObj: MutableLiveData<TmpDataHolder> =
        MutableLiveData<TmpDataHolder>()

    private var professionListObj = MutableLiveData<String>()
    private var professionListData: LiveData<Resource<List<SubCategory>>>? = null

    var catId = ""

    init {
        allSubCategoryListData = allSubCategoryListObj.switchMap { obj ->
            launchOnViewModelScope {
                if (obj == null) {
                    AbsentLiveData.create()
                } else {
                    categoryRepository.getAllItemSubCategoryList()
                }
            }
        }
        professionListObj.value = ""

    }

    fun getProfessionListData(): LiveData<Resource<List<SubCategory>>>? = professionListObj.switchMap {
        categoryRepository.getAllProfessions().map {
            when (it.status) {
                Resource.Status.LOADING -> {
                    Resource.loading()
                }
                Resource.Status.SUCCESS -> {
                    val list = it.data
                    Resource.success(list)
                }
                Resource.Status.ERROR -> {
                    Resource.error(it.message!!)
                }
            }
        }.asLiveData(viewModelScope.coroutineContext)
    }


    fun getSubCategoryListData(catId: String): LiveData<List<SubCategory>>? {
        return categoryRepository.getSubCategoryList(catId).asLiveData()
    }

    //endregion
    fun setAllSubCategoryListObj() {
        if (!isLoading) {
            val tmpDataHolder: TmpDataHolder =
                TmpDataHolder()
            allSubCategoryListObj.value = tmpDataHolder

            // start loading
            setLoadingState(true)
        }
    }

    fun getAllSubCategoryListData(): LiveData<Resource<List<SubCategory>>>? {
        return allSubCategoryListData
    }

    class TmpDataHolder {
        var catId = ""
        var name = ""
    }
}