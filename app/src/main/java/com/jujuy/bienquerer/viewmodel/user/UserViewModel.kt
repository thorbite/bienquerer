package com.jujuy.bienquerer.viewmodel.user

import androidx.lifecycle.*
import com.jujuy.bienquerer.api.network.AbsentLiveData
import com.jujuy.bienquerer.api.network.Resource
import com.jujuy.bienquerer.model.entity.User
import com.jujuy.bienquerer.model.entity.UserLogin
import com.jujuy.bienquerer.model.network.*
import com.jujuy.bienquerer.model.network.request.FacebookRequest
import com.jujuy.bienquerer.model.network.request.GoogleRequest
import com.jujuy.bienquerer.model.network.request.LoginRequest
import com.jujuy.bienquerer.model.network.request.RegisterRequest
import com.jujuy.bienquerer.model.network.response.LoginResponse
import com.jujuy.bienquerer.repository.UserRepository
import com.jujuy.bienquerer.viewmodel.common.DispatchViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.map
import kotlin.time.ExperimentalTime


@ExperimentalCoroutinesApi
class UserViewModel(private val userRepository: UserRepository) : DispatchViewModel() {
    private val loadingState = MutableLiveData<Boolean>()
    var isLoading = false
    var profileImagePath = ""
    var bannerImagePath = ""
    var user: User = User()

    //for login
    private val doUserLoginData: LiveData<Resource<LoginResponse>>
    private val doUserLoginObj = MutableLiveData<User>()

    //for register
    private val registerUserData: LiveData<Resource<LoginResponse>>
    private val registerUserObj: MutableLiveData<UserTmpDataHolder> = MutableLiveData()

    //for google login
    private var googleLoginData: LiveData<Resource<LoginResponse>>? = null
    private val googleLoginObj: MutableLiveData<TmpDataHolder> = MutableLiveData<TmpDataHolder>()

    // for register FB
    private var registerFBUserData: LiveData<Resource<LoginResponse>>? = null
    private var registerFBUserObj = MutableLiveData<TmpDataHolder>()

    // for get User
    private val userObj: MutableLiveData<DeleteUserTmpDataHolder> = MutableLiveData()
    private var userData: LiveData<Resource<User>>? = null

    // for phone login
    private var phoneLoginData: LiveData<Resource<LoginResponse>>? = null
    private val phoneLoginObj: MutableLiveData<TmpDataHolder> = MutableLiveData<TmpDataHolder>()

    // for update user
    private var updateUserData: LiveData<Resource<BaseResponse<Any>>>? = null
    private val updateUserObj = MutableLiveData<User>()

    // for getting login user from db
    private val userLoginData: LiveData<List<UserLogin>>
    private val userLoginObj = MutableLiveData<String>()

    // for image upload
    private val imgObj = MutableLiveData<String>()

    // for get User detail
    private var userDetailData: LiveData<Resource<User>>? = null
    private val userDetailObj: MutableLiveData<DeleteUserTmpDataHolder> = MutableLiveData()

    //send sms phone
    private var userPhoneData: LiveData<Resource<BaseResponse<Any>>>? = null
    private val userPhoneObj = MutableLiveData<String>()

    internal class UserTmpDataHolder(var user: User)

    init {
        doUserLoginData = doUserLoginObj.switchMap { obj ->
            userRepository.doLogin(LoginRequest(obj.email!!, obj.password!!, obj.fcmKey!!))
                .asLiveData(viewModelScope.coroutineContext)
        }

        registerUserData = registerUserObj.switchMap { obj ->
            userRepository.registerUser(
                RegisterRequest(
                    obj.user.id,
                    obj.user.username!!,
                    obj.user.person?.name!!,
                    obj.user.person!!.lastname!!,
                    obj.user.password!!,
                    obj.user.email!!,
                    obj.user.phone,
                    obj.user.person!!.address,
                    obj.user.fcmKey
                )
            ).map {
                when(it.status){
                    Resource.Status.LOADING -> {
                        Resource.loading()
                    }
                    Resource.Status.SUCCESS -> {
                        Resource.success(it.data)
                    }
                    Resource.Status.ERROR -> {
                        Resource.error(it.message!!)
                    }
                }
            }.asLiveData(viewModelScope.coroutineContext)
        }

        //get User data
        userLoginData = userLoginObj.switchMap { obj ->
            launchOnViewModelScope {
                if (obj == null) {
                    AbsentLiveData.create()
                } else {
                    userRepository.getLoginUser()
                }
            }
        }

        googleLoginData = googleLoginObj.switchMap { obj ->
            userRepository.postGoogleLogin(
                GoogleRequest(
                    obj.googleId,
                    obj.email,
                    obj.name,
                    obj.familyname,
                    obj.imageUrl,
                    obj.deviceToken
                )
            ).asLiveData(viewModelScope.coroutineContext)
        }

        registerFBUserData = registerFBUserObj.switchMap { obj ->
            userRepository.postFacebookLogin(
                FacebookRequest(
                    obj.fbId,
                    obj.name,
                    obj.lastname,
                    obj.email,
                    obj.imageUrl,
                    obj.deviceToken
                )
            ).asLiveData(viewModelScope.coroutineContext)
        }

        phoneLoginData = phoneLoginObj.switchMap { obj ->
            userRepository.postSendSms(obj.loginUserId).map {
                when(it.status){
                    Resource.Status.LOADING -> {
                        Resource.loading()
                    }
                    Resource.Status.SUCCESS -> {
                        Resource.success(it.data)
                    }
                    Resource.Status.ERROR -> {
                        Resource.error(it.message!!)
                    }
                }
            }.asLiveData(viewModelScope.coroutineContext)
        }

        updateUserData = updateUserObj.switchMap { obj ->
            userRepository.updateUser(obj).asLiveData(viewModelScope.coroutineContext)
        }

        userDetailData = userDetailObj.switchMap { obj ->
            userRepository.getDetailUser(obj.userId!!).map {
                when(it.status){
                    Resource.Status.LOADING -> {
                        Resource.loading()
                    }
                    Resource.Status.SUCCESS -> {
                        val user = it.data?.data
                        Resource.success(user)
                    }
                    Resource.Status.ERROR -> {
                        Resource.error(it.message!!)
                    }
                }
            }.asLiveData(viewModelScope.coroutineContext)
        }

        userPhoneData = userPhoneObj.switchMap {
            userRepository.sendSmsPhone(it).map{ response ->
                when(response.status){
                    Resource.Status.LOADING -> {
                        Resource.loading()
                    }
                    Resource.Status.SUCCESS -> {
                        Resource.success(response.data)
                    }
                    Resource.Status.ERROR -> {
                        Resource.error(response.message!!)
                    }
                }
            }.asLiveData(viewModelScope.coroutineContext)
        }
    }

    // For loading status
    fun setLoadingState(state: Boolean) {
        isLoading = state
        loadingState.value = state
    }
    fun getLoadingState(): MutableLiveData<Boolean> = loadingState

    // For Login User
    fun setUserLogin(obj: User?) {
        doUserLoginObj.value = obj
    }
    fun getUserLoginStatus(): LiveData<Resource<LoginResponse>> = doUserLoginData

    //for register user
    fun setRegisterUser(user: User) {
        val tmpDataHolder =
            UserTmpDataHolder(
                user
            )
        registerUserObj.value = tmpDataHolder
    }
    fun getRegisterUser(): LiveData<Resource<LoginResponse>> = registerUserData


    // For Login User Data
    fun setUserObj(userId: String?) {
        val deleteUserTmpDataHolder =
            DeleteUserTmpDataHolder(
                userId
            )
        userObj.value = deleteUserTmpDataHolder
    }

    @ExperimentalCoroutinesApi
    @ExperimentalTime
    fun getUserData(): LiveData<Resource<User>>? {
        return userRepository.getLoginUserRemote().asLiveData(viewModelScope.coroutineContext)
    }


    //get detail user
    fun setDetailUser(userId: String?){
        val deleteUserTmpDataHolder =
            DeleteUserTmpDataHolder(
                userId
            )
        userDetailObj.value = deleteUserTmpDataHolder
    }
    fun getDetailUser(): LiveData<Resource<User>>? = userDetailData

    // Login validate Phone
    fun setPhoneLoginUser(phoneId: String?) {
        val tmpDataHolder: TmpDataHolder =
            TmpDataHolder()
        if (phoneId != null) {
            tmpDataHolder.loginUserId = phoneId
        }
        phoneLoginObj.value = tmpDataHolder
    }
    fun getPhoneLoginData(): LiveData<Resource<LoginResponse>>? = phoneLoginData

    // Update User
    fun setUpdateUserObj(user: User?) {
        updateUserObj.value = user
    }
    fun getUpdateUserData(): LiveData<Resource<BaseResponse<Any>>>? = updateUserData

    //send phone data
    fun setUserPhoneObj(phone: String) {
        userPhoneObj.value = phone
    }
    fun getUserPhoneData(): LiveData<Resource<BaseResponse<Any>>>? = userPhoneData

    // google login
    fun setGoogleLoginUser(googleId: String?, name: String?
                           ,familyname: String?, email: String?
                           , imageUrl: String?, fcmToken: String?) {
        val tmpDataHolder =
            TmpDataHolder()
        if (googleId != null) {
            tmpDataHolder.googleId = googleId
        }
        if (name != null) {
            tmpDataHolder.name = name
        }
        if (familyname != null) {
            tmpDataHolder.familyname = familyname
        }
        if (email != null) {
            tmpDataHolder.email = email
        }
        if (imageUrl != null) {
            tmpDataHolder.imageUrl = imageUrl
        }
        fcmToken?.let { tmpDataHolder.deviceToken = it }
        googleLoginObj.value = tmpDataHolder
    }
    fun getGoogleLoginData(): LiveData<Resource<LoginResponse>>? = googleLoginData

    // Facebook Register User
    fun registerFBUser(
        fbId: String?,
        firstname: String?,
        lastname: String?,
        email: String?,
        imageUrl: String?,
        fcmToken: String?
    ) {
        val tmpDataHolder = TmpDataHolder()
        if (fbId != null) {
            tmpDataHolder.fbId = fbId
        }
        if (firstname != null) {
            tmpDataHolder.name = firstname
        }
        if (lastname != null) {
            tmpDataHolder.lastname = lastname
        }
        if (email != null) {
            tmpDataHolder.email = email
        }
        if (imageUrl != null) {
            tmpDataHolder.imageUrl = imageUrl
        }
        fcmToken?.let { tmpDataHolder.deviceToken = it }
        registerFBUserObj.value = tmpDataHolder
    }

    fun getRegisterFBUserData(): LiveData<Resource<LoginResponse>>? = registerFBUserData

    // Upload Image
    fun uploadImage(filePath: String): LiveData<Resource<User>>{
        imgObj.value = "PS"
        return imgObj.switchMap{ obj ->
            userRepository.uploadImage(filePath).map {
                when(it.status){
                    Resource.Status.LOADING -> {
                        Resource.loading()
                    }
                    Resource.Status.SUCCESS -> {
                        val user = it.data
                        Resource.success(user)
                    }
                    Resource.Status.ERROR -> {
                        Resource.error(it.message!!)
                    }
                }
            }.asLiveData(viewModelScope.coroutineContext)
        }

    }

    fun uploadCoverImage(filePath: String): LiveData<Resource<User>>{
        imgObj.value = "PS"
        return imgObj.switchMap{ obj ->
            userRepository.uploadCoverImage(filePath).map {
                when(it.status){
                    Resource.Status.LOADING -> {
                        Resource.loading()
                    }
                    Resource.Status.SUCCESS -> {
                        val user = it.data
                        Resource.success(user)
                    }
                    Resource.Status.ERROR -> {
                        Resource.error(it.message!!)
                    }
                }
            }.asLiveData(viewModelScope.coroutineContext)
        }

    }

    // For Getting Login User Data
    fun getLoginUser(id: String): LiveData<User> {
        return userRepository.getUserLocal(id).asLiveData()
    }

    fun setLoginUser() {
        userLoginObj.value = "load"
    }

    fun deleteUserLogin(user: User?): LiveData<Resource<Boolean>>? {
        return if (user == null) {
            AbsentLiveData.create()
        } else userRepository.delete(user)
    }


    fun logoutUser(fcmToken: String): LiveData<Resource<BaseResponse<Any>>> {
        return userRepository.logoutUser(fcmToken).asLiveData()
    }
    //region Tmp Holder
    internal class TmpDataHolder {
        var loginUserId = ""
        var password = ""
        var fbId = ""
        var name = ""
        var lastname = ""
        var familyname = ""
        var email = ""
        var imageUrl = ""
        var googleId = ""
        var code = ""
        var deviceToken = ""
        var address = ""
        var phone = ""
        var username = ""
    }

    class DeleteUserTmpDataHolder constructor(var userId: String?)
}
