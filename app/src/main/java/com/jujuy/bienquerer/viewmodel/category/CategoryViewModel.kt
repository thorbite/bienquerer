package com.jujuy.bienquerer.viewmodel.category

import androidx.lifecycle.*
import com.jujuy.bienquerer.api.network.Resource
import com.jujuy.bienquerer.base.BaseViewModel
import com.jujuy.bienquerer.model.entity.Category
import com.jujuy.bienquerer.repository.CategoryRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.map
import kotlin.time.ExperimentalTime
@ExperimentalCoroutinesApi
@ExperimentalTime
class CategoryViewModel(private val categoryRepository: CategoryRepository) : BaseViewModel() {

    //region Variables
    private var categoryListData: LiveData<Resource<List<Category>>>? = null
    private var categoryListObj: MutableLiveData<TmpDataHolder> = MutableLiveData<TmpDataHolder>()


    // category list
    fun setCategoryListObj(limit: String, offset: String) {
        if (!isLoading) {
            val tmpDataHolder =
                TmpDataHolder()
            tmpDataHolder.offset = offset
            tmpDataHolder.limit = limit
            categoryListObj.value = tmpDataHolder

            // start loading
            setLoadingState(true)
        }
    }
    @ExperimentalTime
    @ExperimentalCoroutinesApi
    fun getCategoryListData(): LiveData<Resource<List<Category>>>? = categoryListObj.switchMap { obj ->
        categoryRepository.loadCategoryList().map {
            when(it.status){
                Resource.Status.LOADING -> {
                    Resource.loading()
                }
                Resource.Status.SUCCESS -> {
                    val user = it.data
                    Resource.success(user)
                }
                Resource.Status.ERROR -> {
                    Resource.error(it.message!!)
                }
            }
        }.asLiveData(viewModelScope.coroutineContext)
    }

    internal class TmpDataHolder {
        var limit = ""
        var offset = ""
        var cityId = ""
    }
}
