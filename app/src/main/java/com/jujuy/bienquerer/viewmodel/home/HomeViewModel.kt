package com.jujuy.bienquerer.viewmodel.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.switchMap
import com.jujuy.bienquerer.viewmodel.common.DispatchViewModel
import com.jujuy.bienquerer.model.entity.Provider
import com.jujuy.bienquerer.repository.HomeRepository

class HomeViewModel constructor(private val homeRepository: HomeRepository) : DispatchViewModel() {

    private var categoryPageLiveData: MutableLiveData<Int> = MutableLiveData()
    //val categoryListLiveData: LiveData<List<Category>>

    private var recentProviderPageLiveData: MutableLiveData<Int> = MutableLiveData()
    val recentProviderListLiveData: LiveData<List<Provider>>


    val toastLiveData: MutableLiveData<String> = MutableLiveData()


    init {
        /*this.categoryListLiveData = categoryPageLiveData.switchMap { page ->
            launchOnViewModelScope{
                homeRepository.loadCategory(page){ toastLiveData.postValue(it) }
            }
        }*/

        this.recentProviderListLiveData = recentProviderPageLiveData.switchMap { page ->
            launchOnViewModelScope{
                homeRepository.loadRecentProvider(page){toastLiveData.postValue(it)}
            }
        }
    }

    fun postCategoryPage(page: Int) = categoryPageLiveData.postValue(page)
    fun postRecentProviderPage(page: Int) = recentProviderPageLiveData.postValue(page)

}