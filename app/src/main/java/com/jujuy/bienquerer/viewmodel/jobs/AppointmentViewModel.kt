package com.jujuy.bienquerer.viewmodel.jobs

import android.annotation.SuppressLint
import androidx.lifecycle.*
import androidx.paging.PagingData
import androidx.paging.cachedIn
import androidx.paging.filter
import com.jujuy.bienquerer.api.network.Resource
import com.jujuy.bienquerer.base.BaseViewModel
import com.jujuy.bienquerer.model.entity.Professional
import com.jujuy.bienquerer.model.network.BaseResponse
import com.jujuy.bienquerer.model.network.request.AppointmentRequest
import com.jujuy.bienquerer.model.network.response.Job
import com.jujuy.bienquerer.repository.JobRepository
import com.jujuy.bienquerer.model.network.response.Appointment
import com.jujuy.bienquerer.room.AppoinmentDao
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class AppointmentViewModel(private val jobRepository: JobRepository): BaseViewModel() {
    var name: String? = ""
    var lastname: String? = ""
    var professionalContainer: Professional? = null
    var phone: String? = ""
    var email: String? = ""
    var jobContainer: Job? = null
    var appointmentContainer: Appointment? = null

    private var saveAppointmentData: LiveData<Resource<BaseResponse<Boolean>>>? = null
    private var saveAppointmentObj = MutableLiveData<TmpDataHolder>()

    private var listAppointmentData: LiveData<Resource<List<Appointment>>>
    private var listAppointmentObj = MutableLiveData<TmpDataHolder>()

    private var listJobData: LiveData<Resource<List<Job>>>
    private var listJobObj = MutableLiveData<TmpDataHolder>()

    private var statusJobData: LiveData<Resource<BaseResponse<Any>>>
    private var statusJobObj = MutableLiveData<TmpDataHolder>()

    init {
        saveAppointmentData = saveAppointmentObj.switchMap { obj ->
            jobRepository.sendRequestAppointent(
                AppointmentRequest(
                    obj.professionalId,
                    obj.professionId,
                    obj.requestDate,
                    obj.comment
                )
            ).map {
                when(it.status){
                    Resource.Status.LOADING ->{
                        Resource.loading()
                    }
                    Resource.Status.SUCCESS ->{
                        Resource.success(it.data)
                    }
                    Resource.Status.ERROR -> {
                        Resource.error(it.message!!)
                    }
                }
            }.asLiveData(viewModelScope.coroutineContext)
        }

        listAppointmentData = listAppointmentObj.switchMap { obj ->
            jobRepository.getListAppointent(obj.statusId).map {
                when(it.status){
                    Resource.Status.LOADING ->{
                        Resource.loading()
                    }
                    Resource.Status.SUCCESS ->{
                        Resource.success(it.data)
                    }
                    Resource.Status.ERROR -> {
                        Resource.error(it.message!!)
                    }
                }
            }.asLiveData(viewModelScope.coroutineContext)
        }

        listJobData = listJobObj.switchMap { obj ->
            jobRepository.getListJob(obj.statusId).map {
                when(it.status){
                    Resource.Status.LOADING ->{
                        Resource.loading()
                    }
                    Resource.Status.SUCCESS ->{
                        Resource.success(it.data)
                    }
                    Resource.Status.ERROR -> {
                        Resource.error(it.message!!)
                    }
                }
            }.asLiveData(viewModelScope.coroutineContext)
        }

        statusJobData = statusJobObj.switchMap {
            jobRepository.changeStatusJob(it.comment, it.jobId, it.statusId).map { resp ->
                when(resp.status){
                    Resource.Status.LOADING ->{
                        Resource.loading()
                    }
                    Resource.Status.SUCCESS ->{
                        Resource.success(resp.data)
                    }
                    Resource.Status.ERROR -> {
                        Resource.error(resp.message!!)
                    }
                }
            }.asLiveData(viewModelScope.coroutineContext)
        }
    }

    fun setSaveAppointmentObj(professionalId: String, professionId: String?, requestDate: String?, comment: String?) {
        if (!isLoading) {
            val tmpDataHolder = TmpDataHolder()
            tmpDataHolder.professionalId = professionalId
            if (professionId != null) {
                tmpDataHolder.professionId = professionId
            }
            if (requestDate != null) {
                tmpDataHolder.requestDate = requestDate
            }
            if (comment != null) {
                tmpDataHolder.comment = comment
            }
            saveAppointmentObj.value = tmpDataHolder
            setLoadingState(true)
        }
    }
    fun getSaveAppointmentData(): LiveData<Resource<BaseResponse<Boolean>>>? = saveAppointmentData

    fun setListAppointmentObj(){
        val tmpDataHolder = TmpDataHolder()
        tmpDataHolder.statusId = "1"
        listAppointmentObj.value = tmpDataHolder
    }

    fun getListAppoinmentData() = listAppointmentData

    fun setListJobObj(){
        val tmpDataHolder = TmpDataHolder()
        tmpDataHolder.statusId = "1"
        listJobObj.value = tmpDataHolder
    }

    fun getListJobData() = listJobData

    fun getJobData(jobId: String): LiveData<Job>? {
        return jobRepository.getJobDataById(jobId).asLiveData()
    }

    fun getAppointmentData(appointmentId: String): LiveData<Appointment>? {
        return jobRepository.getAppointmentDataById(appointmentId).asLiveData()
    }

    //change status job
    fun setChangeStatusJob(comment: String?, jobId: String, statusId: String){
        val tmp = TmpDataHolder()
        tmp.comment = comment
        tmp.jobId = jobId
        tmp.statusId = statusId
        statusJobObj.value = tmp
    }
    fun getChangeStatusJob(): LiveData<Resource<BaseResponse<Any>>> = statusJobData

    fun getListAppointmentUserData(): Flow<PagingData<Appointment>> {
        return jobRepository.getListAppointmentPaged().cachedIn(viewModelScope)
    }

    fun getJobListData(): Flow<PagingData<Job>> {
        return jobRepository.getListjobPaged().cachedIn(viewModelScope)
    }

    fun getFilteredByDate(first: Long, second: Long): Flow<PagingData<Appointment>>{
        return jobRepository.getListAppointmentPaged().map { paged ->
            paged.filter { milliseconds(it.createdAt) in first..second }
        }.cachedIn(viewModelScope)
    }

    fun getJobFilteredByDate(first: Long, second: Long): Flow<PagingData<Job>>{
        return jobRepository.getListjobPaged().map { paged ->
            paged.filter { milliseconds(it.createdAt) in first..second }
        }.cachedIn(viewModelScope)
    }

    @SuppressLint("CheckResult")
    fun getFilteredByStatus(id: Int): Flow<PagingData<Appointment>>{
        return jobRepository.getListAppointmentPaged().map { paged ->
            paged.filter { it.status.id == id }
        }.cachedIn(viewModelScope)
    }

    @SuppressLint("CheckResult")
    fun getJobFilteredByStatus(id: Int): Flow<PagingData<Job>>{
        return jobRepository.getListjobPaged().map { paged -> paged.filter { it.status.id == id }
        }.cachedIn(viewModelScope)
    }

    internal class TmpDataHolder {
        var professionalId = ""
        var professionId = ""
        var requestDate = ""
        var comment: String? = ""
        var statusId = ""
        var jobId = ""
    }

    @SuppressLint("SimpleDateFormat")
    fun milliseconds(date: String): Long {
        val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
        try {
            val mDate: Date = sdf.parse(date)
            val timeInMilliseconds: Long = mDate.time
            println("Date in milli :: $timeInMilliseconds")
            return timeInMilliseconds
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return 0
    }
}