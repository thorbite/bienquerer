package com.jujuy.bienquerer.viewmodel.favorite

import androidx.lifecycle.*
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.jujuy.bienquerer.api.network.Resource
import com.jujuy.bienquerer.base.BaseViewModel
import com.jujuy.bienquerer.model.entity.Professional
import com.jujuy.bienquerer.model.entity.ProfessionalFav
import com.jujuy.bienquerer.model.network.BaseResponse
import com.jujuy.bienquerer.repository.ProfessionalRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class FavoriteViewModel (private val professionalRepository: ProfessionalRepository) : BaseViewModel() {
    private var sendFavouritePostData: LiveData<Resource<BaseResponse<Boolean>>>? = null
    private var sendFavouriteDataPostObj = MutableLiveData<TmpDataHolder>()

    private var favoriteItemData: LiveData<Resource<BaseResponse<Boolean>>>? = null
    private var favoriteItemObj = MutableLiveData<TmpDataHolder>()

    private var deleteFavoriteItemData: LiveData<Resource<BaseResponse<Boolean>>>? = null
    private var deleteFavoriteItemObj = MutableLiveData<TmpDataHolder>()

    private var listFavoriteData: LiveData<Resource<List<Professional>>>? = null
    private var listFavoriteListObj = MutableLiveData<TmpDataHolder>()

    init {
        sendFavouritePostData = sendFavouriteDataPostObj.switchMap { obj ->
            professionalRepository.uploadFavouritePostToServer(obj.itemId).map {
                when(it.status){
                    Resource.Status.LOADING ->{
                        Resource.loading()
                    }
                    Resource.Status.SUCCESS ->{
                        Resource.success(it.data)
                    }
                    Resource.Status.ERROR ->{
                        Resource.error(it.message!!)
                    }
                }
            }.asLiveData(viewModelScope.coroutineContext)
        }

        favoriteItemData = favoriteItemObj.switchMap { obj ->
            professionalRepository.verifyFavoriteProfessional(obj.itemId).map {
                when(it.status){
                    Resource.Status.LOADING ->{
                        Resource.loading()
                    }
                    Resource.Status.SUCCESS ->{
                        Resource.success(it.data)
                    }
                    Resource.Status.ERROR ->{
                        Resource.error(it.message!!)
                    }
                }
            }.asLiveData(viewModelScope.coroutineContext)
        }

        deleteFavoriteItemData = deleteFavoriteItemObj.switchMap { obj ->
            professionalRepository.removeFavoriteProfessional(obj.itemId).map {
                when(it.status){
                    Resource.Status.LOADING -> {
                        Resource.loading()
                    }
                    Resource.Status.SUCCESS -> {
                        Resource.success(it.data)
                    }
                    Resource.Status.ERROR -> {
                        Resource.error(it.message!!)
                    }
                }
            }.asLiveData(viewModelScope.coroutineContext)
        }
    }

    //set favorite item
    fun setFavouritePostDataObj(itemId: String) {
            val tmpDataHolder = TmpDataHolder()
            tmpDataHolder.itemId = itemId
            sendFavouriteDataPostObj.value = tmpDataHolder
            setLoadingState(true)
    }
    fun getFavouritePostData(): LiveData<Resource<BaseResponse<Boolean>>>? = sendFavouritePostData

    //get if item is favorite
    fun setFavoriteItemDataObj(itemId: String) {
            val tmpDataHolder = TmpDataHolder()
            tmpDataHolder.itemId = itemId
            favoriteItemObj.value = tmpDataHolder
    }
    fun getFavouriteItemData(): LiveData<Resource<BaseResponse<Boolean>>>? = favoriteItemData

    //delete favorite
    fun setDeleteFavoriteDataObj(itemId: String) {
        val tmpDataHolder = TmpDataHolder()
        tmpDataHolder.itemId = itemId
        deleteFavoriteItemObj.value = tmpDataHolder
    }
    fun getDeleteFavouriteData(): LiveData<Resource<BaseResponse<Boolean>>>? = deleteFavoriteItemData

    // get list favorites
    fun setListFavoritesObj(){
        val tmpDataHolder = TmpDataHolder()
        listFavoriteListObj.value = tmpDataHolder
    }

    fun getListFavoritesData(): Flow<PagingData<ProfessionalFav>>{
        return professionalRepository.getListFavoriteProfessional().cachedIn(viewModelScope)
    }

    internal class TmpDataHolder {
        var itemId = ""
        var userId = ""
    }
}
