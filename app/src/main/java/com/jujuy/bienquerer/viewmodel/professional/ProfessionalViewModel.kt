package com.jujuy.bienquerer.viewmodel.professional

import androidx.lifecycle.*
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.jujuy.bienquerer.api.network.Resource
import com.jujuy.bienquerer.base.BaseViewModel
import com.jujuy.bienquerer.model.ItemParameterHolder
import com.jujuy.bienquerer.model.RegisterProfessionalRequest
import com.jujuy.bienquerer.model.entity.ItemSearch
import com.jujuy.bienquerer.model.entity.Professional
import com.jujuy.bienquerer.model.network.BaseResponse
import com.jujuy.bienquerer.model.network.request.MapListRequest
import com.jujuy.bienquerer.repository.ProfessionalRepository
import com.jujuy.bienquerer.utils.Constants
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import timber.log.Timber
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class ProfessionalViewModel(private val professionalRepository: ProfessionalRepository) : BaseViewModel() {
    var holder = ItemParameterHolder()
    var latValue = "-24.1893621"
    var lngValue = "-65.2962097"
    var newLatValue = "-24.1893621"
    var newLngValue = "-65.2962097"
    var itemContainer: Professional? = null
    var address= ""

    var itemDescription = ""
    var itemId = ""
    var cityId = ""
    var historyFlag = ""
    var customImageUri: String? = null
    var locationId = ""
    var locationName = ""
    var otherUserId = ""
    var otherUserName = ""
    var is_sold_out = ""
    var mapLat = ""
    var mapLng = ""

    var currentItem: Professional? = null
    var listItem: List<Professional> = listOf(Professional())
    var itemSearch: ItemSearch? = null

    var profileImagePath = Constants.EMPTY_STRING
    var bannerImagePath = Constants.EMPTY_STRING

    var userId = ""
    var isSocial: Boolean? = false


    /*private var itemListByKeyData: LiveData<Resource<List<Professional>>>? = null
    private var itemListByKeyObj: MutableLiveData<ItemTmpDataHolder> =
        MutableLiveData<ItemTmpDataHolder>()*/

    private var professionalListByKeyData: LiveData<Resource<BaseResponse<List<ItemSearch>>>>? = null
    private var professionalListByKeyObj = MutableLiveData<ItemTmpDataHolder>()

    //get professional detail
    private var professionalListDetailData: LiveData<Resource<Professional>>? = null
    private var professionalListDetailObj: MutableLiveData<TmpDataHolder> = MutableLiveData<TmpDataHolder>()

    private var professionalUpdatedData: LiveData<Resource<Professional>>? = null
    private var professionalUpdatedObj = MutableLiveData<ItemTmpDataHolder>()

    private var professionSaveData: LiveData<Resource<Boolean>>? = null
    private var professionSaveObj: MutableLiveData<TmpDataHolder> = MutableLiveData<TmpDataHolder>()

    private var professionDeleteData: LiveData<Resource<Boolean>>? = null
    private var professionDeleteObj = MutableLiveData<TmpDataHolder>()

    private var professionAvailableData: LiveData<Resource<BaseResponse<Boolean>>>? = null
    private var professionAvailableObj = MutableLiveData<TmpDataHolder>()

    private var professionalSocialData: LiveData<Resource<BaseResponse<Boolean>>>? = null
    private var professionalSocialObj = MutableLiveData<TmpDataHolder>()

    private var professionalRegisterData: LiveData<Resource<BaseResponse<Boolean>>>? = null
    private var professionalRegisterObj = MutableLiveData<TmpDataRegisterProf>()

    private var professionalMapData: LiveData<Resource<BaseResponse<List<Professional>>>>? = null
    private var professionalMapObj = MutableLiveData<TmpDataMapHolder>()

    init {

        /*itemListByKeyData = itemListByKeyObj.switchMap { obj ->
            professionalRepository.getItemListByKey(obj.itemParameterHolder).map {
                when(it.status){
                    Resource.Status.LOADING ->{
                        Resource.loading()
                    }
                    Resource.Status.SUCCESS ->{
                        val list = it.data
                        Resource.success(list)
                    }
                    Resource.Status.ERROR -> {
                        Resource.error(it.message!!)
                    }
                }
            }.asLiveData(viewModelScope.coroutineContext)
        }*/
        professionalMapData = professionalMapObj.switchMap { obj ->
            professionalRepository.getMapListFilter(
                obj.lat,
                obj.lng,
                obj.categoryId,
                obj.professionId,
                obj.radio).map{
                when(it.status){
                    Resource.Status.LOADING ->{
                        Resource.loading()
                    }
                    Resource.Status.SUCCESS -> {
                        Resource.success(it.data)
                    }
                    Resource.Status.ERROR -> {
                        Resource.error(it.message!!)
                    }
                }
            }.asLiveData(viewModelScope.coroutineContext)
        }

        professionalListByKeyData = professionalListByKeyObj.switchMap { obj ->
            professionalRepository.getItemSearch(obj.itemParameterHolder).map {
                when(it.status){
                    Resource.Status.LOADING ->{
                        Resource.loading()
                    }
                    Resource.Status.SUCCESS -> {
                        Resource.success(it.data)
                    }
                    Resource.Status.ERROR -> {
                        Resource.error(it.message!!)
                    }
                }
            }.asLiveData(viewModelScope.coroutineContext)
        }

        professionalListDetailData = professionalListDetailObj.switchMap { obj ->
            professionalRepository.getProfessionalDetail(obj.professionalId).map {
                when(it.status){
                    Resource.Status.LOADING ->{
                        Resource.loading()
                    }
                    Resource.Status.SUCCESS -> {
                        Resource.success(it.data?.data)
                    }
                    Resource.Status.ERROR -> {
                        Resource.error(it.message!!)
                    }
                }
            }.asLiveData(viewModelScope.coroutineContext)
        }

        professionalUpdatedData = professionalUpdatedObj.switchMap { obj ->
            professionalRepository.updateUserLocation(obj.itemParameterHolder).map {
                when(it.status){
                    Resource.Status.LOADING ->{
                        Resource.loading()
                    }
                    Resource.Status.SUCCESS ->{
                        Resource.success(it.data)
                    }
                    Resource.Status.ERROR -> {
                        Resource.error(it.message!!)
                    }
                }
            }.asLiveData(viewModelScope.coroutineContext)
        }

        professionSaveData = professionSaveObj.switchMap { obj ->
            professionalRepository.postSaveProfession(obj.userId, obj.professionId).map {
                when(it.status){
                    Resource.Status.LOADING ->{
                        Resource.loading()
                    }
                    Resource.Status.SUCCESS ->{
                        Resource.success(it.data?.data)
                    }
                    Resource.Status.ERROR -> {
                        Resource.error(it.message!!)
                    }
                }
            }.asLiveData(viewModelScope.coroutineContext)

        }

        professionDeleteData = professionDeleteObj.switchMap { obj ->
            professionalRepository.deleteProfession(obj.userId, obj.professionId).map{
                when(it.status){
                    Resource.Status.LOADING ->{
                        Resource.loading()
                    }
                    Resource.Status.SUCCESS ->{
                        Resource.success(it.data?.data)
                    }
                    Resource.Status.ERROR -> {
                        Resource.error(it.message!!)
                    }
                }
            }.asLiveData(viewModelScope.coroutineContext)
        }

        professionAvailableData = professionAvailableObj.switchMap {
            professionalRepository.putAvailableProfessional().map {
                when(it.status){
                    Resource.Status.LOADING ->{
                        Resource.loading()
                    }
                    Resource.Status.SUCCESS ->{
                        Resource.success(it.data)
                    }
                    Resource.Status.ERROR -> {
                        Resource.error(it.message!!)
                    }
                }
            }.asLiveData(viewModelScope.coroutineContext)

        }

        professionalSocialData = professionalSocialObj.switchMap {
            professionalRepository.putSocialProfessional().map {
                when(it.status){
                    Resource.Status.LOADING ->{
                        Resource.loading()
                    }
                    Resource.Status.SUCCESS ->{
                        Resource.success(it.data)
                    }
                    Resource.Status.ERROR -> {
                        Resource.error(it.message!!)
                    }
                }
            }.asLiveData(viewModelScope.coroutineContext)

        }

        professionalRegisterData = professionalRegisterObj.switchMap { obj ->
            professionalRepository.registerProfessional(obj.profilePath, obj.bannerPath, obj.registerProf!!).map {
                when(it.status){
                    Resource.Status.LOADING ->{
                        Resource.loading()
                    }
                    Resource.Status.SUCCESS ->{
                        Resource.success(it.data)
                    }
                    Resource.Status.ERROR -> {
                        Resource.error(it.message!!)
                    }
                }
            }.asLiveData(viewModelScope.coroutineContext)
        }

    }

    //save profession data
    fun setProfessionSaveObj(userId: String, professionId: String){
            val tmpDataHolder =
                TmpDataHolder()
            tmpDataHolder.userId = userId
            tmpDataHolder.professionId = professionId
            this.professionSaveObj.value = tmpDataHolder
            //setLoadingState(true)
    }
    fun getProfessionSaveData(): LiveData<Resource<Boolean>>? = professionSaveData

    //delete profession
    fun setProfessionDeleteObj(userId: String, professionId: String){
        val tmpDataHolder = TmpDataHolder()
        tmpDataHolder.userId = userId
        tmpDataHolder.professionId = professionId
        professionDeleteObj.value = tmpDataHolder
        Timber.e("DELETE: ${tmpDataHolder.professionId} ${tmpDataHolder.userId}")
        //setLoadingState(true)
    }
    fun getProfessionDeleteData(): LiveData<Resource<Boolean>>? = professionDeleteData

    //


    fun setProfessionalListDetailObj(professionalId: String){
            val tmpDataHolder = TmpDataHolder()
            tmpDataHolder.professionalId = professionalId
            this.professionalListDetailObj.value = tmpDataHolder
    }
    fun getProfessionalListDetailData(): LiveData<Resource<Professional>>? = professionalListDetailData

    /*fun setItemListByKeyObj(parameterHolder: ItemParameterHolder) {
        val tmpHolder = ItemTmpDataHolder(parameterHolder)
        this.itemListByKeyObj.value = tmpHolder
    }

    fun getItemListByKeyData(): LiveData<Resource<List<Professional>>>? {
        return itemListByKeyData
    }*/

    fun getListProfessionals(parameterHolder: ItemParameterHolder): Flow<PagingData<Professional>> {
        return professionalRepository.getItemListByKey(parameterHolder).cachedIn(viewModelScope)
    }



    fun setProfessionalListByKey(parameterHolder: ItemParameterHolder) {
        val tmpHolder =
            ItemTmpDataHolder(
                parameterHolder
            )
        this.professionalListByKeyObj.value = tmpHolder
    }

    fun getProfessionalListByKeyData(): LiveData<Resource<BaseResponse<List<ItemSearch>>>>? {
        return professionalListByKeyData
    }

    fun getProfessionalUpdated(): LiveData<Resource<Professional>>?{
        return professionalUpdatedData
    }
    fun setProfessionalUpdated(parameterHolder: ItemParameterHolder ){
        val tmpHolder = ItemTmpDataHolder(parameterHolder)
        this.professionalUpdatedObj.value = tmpHolder
    }

    fun setProfessionalAvailable(){
        val tmpHolder = TmpDataHolder()
        professionAvailableObj.value = tmpHolder
    }
    fun getProfessionalAvailable(): LiveData<Resource<BaseResponse<Boolean>>>? = professionAvailableData

    fun setProfessionalSocial(){
        val tmpHolder = TmpDataHolder()
        professionalSocialObj.value = tmpHolder
    }
    fun getProfessionalSocial(): LiveData<Resource<BaseResponse<Boolean>>>? = professionalSocialData


    //register professional
    fun setRegisterProfessional(profile: String, banner: String, data: RegisterProfessionalRequest){
        val tmp = TmpDataRegisterProf()
        tmp.bannerPath = banner
        tmp.profilePath = profile
        tmp.registerProf = data
        professionalRegisterObj.value = tmp
    }
    fun getRegisterProfessional(): LiveData<Resource<BaseResponse<Boolean>>>? = professionalRegisterData

    //get data map list
    fun setGetMapList(lat: String, lng: String, categoryId: String, professionId: String?, radio:String){
        val map = TmpDataMapHolder()
        map.lat = lat
        map.lng = lng
        map.categoryId = categoryId
        map.professionId = professionId ?: ""
        map.radio = radio
        professionalMapObj.value = map
    }
    fun getMapList(): LiveData<Resource<BaseResponse<List<Professional>>>>? = professionalMapData

    fun getListProfessionalsMap(parameterHolder: ItemParameterHolder): Flow<PagingData<Professional>> {
        return professionalRepository.getMapListPage(parameterHolder).cachedIn(viewModelScope)
    }


}
class TmpDataHolder {
    var professionalId = ""
    var userId = ""
    var professionId = ""
}

class TmpDataMapHolder {
    var lat = ""
    var lng = ""
    var categoryId = ""
    var professionId = ""
    var radio = ""
}

internal class TmpDataRegisterProf{
    var profilePath = ""
    var bannerPath = ""
    var registerProf : RegisterProfessionalRequest? = null
}
//endregion
data class ItemTmpDataHolder(
    var itemParameterHolder: ItemParameterHolder
)

sealed class UiModel {
    data class ProfessionalItem(val repo: Professional) : UiModel()
}