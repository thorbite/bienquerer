package com.jujuy.bienquerer.viewmodel.notifications

import androidx.lifecycle.*
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.jujuy.bienquerer.api.network.Resource
import com.jujuy.bienquerer.base.BaseViewModel
import com.jujuy.bienquerer.model.network.BaseResponse
import com.jujuy.bienquerer.model.network.response.ListNotifications
import com.jujuy.bienquerer.repository.NotificationRepository
import com.jujuy.bienquerer.ui.auth.AuthListener
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import timber.log.Timber
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class NotificationsViewModel (private val notificationRepository: NotificationRepository) : BaseViewModel() {

    var authListener: AuthListener? = null
    private var deleteNotificationData: LiveData<Resource<Boolean>>? = null
    private val deleteNotificationObj = MutableLiveData<TmpDataHolder>()

    private var psCountData: LiveData<Resource<BaseResponse<Int>>>? = null
    private val psCountObj =  MutableLiveData<TmpDataHolder>()

    private var itemNotificationData: LiveData<Resource<ListNotifications>>? = null
    private val itemNotificationObj =  MutableLiveData<TmpDataHolder>()

    var token = ""
    var notiId: String? = ""

    init {
        deleteNotificationData = deleteNotificationObj.switchMap { obj ->
            notificationRepository.deleteNotification(obj.notificationId).map {
                when(it.status){
                    Resource.Status.LOADING -> {
                        Resource.loading()
                    }
                    Resource.Status.SUCCESS ->{
                        Resource.success(it.data?.data)
                    }
                    Resource.Status.ERROR ->{
                        Resource.error(it.message!!)
                    }
                }
            }.asLiveData(viewModelScope.coroutineContext)
        }

        psCountData = psCountObj.switchMap { obj ->
            notificationRepository.getNotificationCount().map {
                when(it.status){
                    Resource.Status.LOADING -> {
                        Resource.loading()
                    }
                    Resource.Status.SUCCESS ->{
                        Resource.success(it.data)
                    }
                    Resource.Status.ERROR ->{
                        Resource.error(it.message!!)
                    }
                }
            }.asLiveData(viewModelScope.coroutineContext)
        }

        itemNotificationData = itemNotificationObj.switchMap { obj ->
            notificationRepository.getNotificationItem(obj.userId!!).map {
                when(it.status){
                    Resource.Status.LOADING -> {
                        Resource.loading()
                    }
                    Resource.Status.SUCCESS ->{
                        Resource.success(it.data)
                    }
                    Resource.Status.ERROR ->{
                        Resource.error(it.message!!)
                    }
                }
            }.asLiveData(viewModelScope.coroutineContext)
        }
    }

    fun setNotificationCount(){
        val temp = TmpDataHolder()
        temp.userId = ""
        psCountObj.value = temp
    }
    fun getNotificationCount() = notificationRepository.getNotificationCount().asLiveData()

    ///////////////////////
    fun setNotificationItem(id: String){
        val temp = TmpDataHolder()
        temp.userId = id
        itemNotificationObj.value = temp
    }
    fun getNotificationItem() = itemNotificationData

    fun setDeleteNotificationItem(id: String){
        val temp = TmpDataHolder()
        temp.notificationId = id
        deleteNotificationObj.value = temp
    }
    fun getDeleteNotificationItem() = deleteNotificationData

    fun getListNotification(): Flow<PagingData<ListNotifications>> {
        Timber.e("Notification view model list")
        return notificationRepository.getNotificationList().cachedIn(viewModelScope)
    }

    internal class TmpDataHolder {
        var limit = ""
        var offset = ""
        var isConnected = false
        var notificationId = ""
        var userId = ""
        var deviceToken = ""
    }

}