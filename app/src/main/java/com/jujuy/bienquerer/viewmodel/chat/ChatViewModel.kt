package com.jujuy.bienquerer.viewmodel.chat

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.switchMap
import com.jujuy.bienquerer.api.network.AbsentLiveData
import com.jujuy.bienquerer.api.network.Resource
import com.jujuy.bienquerer.base.BaseViewModel
import com.jujuy.bienquerer.model.Message
import com.jujuy.bienquerer.model.entity.Chat
import com.jujuy.bienquerer.model.entity.UserChat
import com.jujuy.bienquerer.repository.ChatRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class ChatViewModel(var chatRepository: ChatRepository) : BaseViewModel() {

    private var registerUserToFirebaseData: LiveData<Resource<Boolean>>? = null
    private val registerUserToFirebaseObj = MutableLiveData<TmpDataHolder>()

    private var loginUserToFirebaseData: LiveData<Resource<Boolean>>? = null
    private var loginUserToFirebaseObj = MutableLiveData<TmpDataHolder>()

    private var saveMessagesToFirebaseData: LiveData<Resource<Boolean>>? = null
    private var saveMessagesToFirebaseObj = MutableLiveData<saveMessageTmpDataHolder>()

    private var fetchMessagesFromConversationData: LiveData<Resource<Boolean>>? = null
    private var fetchMessagesFromConversationObj = MutableLiveData<fetchMessagesTmpDataHolder>()

    private var getMessagesFromDatabaseData: LiveData<List<Message>>? = null
    private val getMessagesFromDatabaseObj = MutableLiveData<fetchMessagesTmpDataHolder>()

    private var fetchChatRoomListData: LiveData<List<Chat>>? = null
    private val fetchChatRoomsListObj = MutableLiveData<fetchChatRoomsTmpDataHolder>()

    private var saveChatRoomToFirebaseData: LiveData<Resource<Boolean>>? = null
    private var saveChatRoomToFirebaseObj = MutableLiveData<saveChatRoomTmpDataHolder>()

    private var saveUserChatToFirebaseData: LiveData<Resource<Boolean>>? = null
    private var saveUserChatToFirebaseObj = MutableLiveData<saveUserChatTmpDataHolder>()

    private var saveUnSeenCountFirebaseData: LiveData<Resource<Boolean>>? = null
    private var saveUnSeenCountFirebaseObj = MutableLiveData<TmpCountUnseenHolder>()

    private var resetUnSeenCountFirebaseData: LiveData<Resource<Boolean>>? = null
    private var resetUnSeenCountFirebaseObj = MutableLiveData<TmpCountUnseenHolder>()



    var receiverId: String? = ""
    var receiverName: String? = ""
    var receiverUserImgUrl: String? = ""
    var receiverLastName : String?= ""
    var lastMessage : Message? = null

    var itemImagePath = ""
    var itemName = ""
    var senderName = ""
    var offerItemPrice = "0"
    var itemCurrency = ""
    var itemConditionName = ""
    var chatFlag = ""
    var itemId = ""

    init {
        registerUserToFirebaseData = registerUserToFirebaseObj.switchMap { obj ->
            if (obj == null){
                AbsentLiveData.create()
            }else{
                chatRepository.registerUserToFireBase(obj.email, obj.password)
            }
        }

        loginUserToFirebaseData = loginUserToFirebaseObj.switchMap { obj ->
            if (obj == null){
                AbsentLiveData.create()
            }else{
                chatRepository.loginUserToFireBase(obj.email, obj.password)
            }
        }

        saveMessagesToFirebaseData = saveMessagesToFirebaseObj.switchMap { obj ->
            launchOnViewModelScope {
                if (obj == null){
                    AbsentLiveData.create()
                }else{
                    chatRepository.saveMessagesToFirebase(obj.message)
                }
            }
        }

        fetchMessagesFromConversationData = fetchMessagesFromConversationObj.switchMap { obj ->
            if (obj == null){
                AbsentLiveData.create()
            }else {
                chatRepository.getMessagesFromSpecificNode(obj.senderId, obj.receiverId)
            }
        }

        getMessagesFromDatabaseData = getMessagesFromDatabaseObj.switchMap { obj ->
            if (obj == null) {
                AbsentLiveData.create()
            } else {
                chatRepository.getMessagesFromDatabase(obj.senderId, obj.receiverId)
            }
        }

        saveChatRoomToFirebaseData = saveChatRoomToFirebaseObj.switchMap { obj ->
            launchOnViewModelScope {
                if (obj == null) {
                    AbsentLiveData.create()
                } else {
                    chatRepository.saveUserChatRoomToFirebase(obj.chat, obj.senderId, obj.receiverId)
                }
            }
        }

        fetchChatRoomListData = fetchChatRoomsListObj.switchMap { obj ->
            if (obj == null) {
                AbsentLiveData.create()
            } else {
                chatRepository.getUserChatrooms(obj.senderId)
            }
        }

        saveUserChatToFirebaseData = saveUserChatToFirebaseObj.switchMap { obj ->
            if (obj == null) {
                AbsentLiveData.create()
            } else {
                chatRepository.saveUserToFirebase(obj.chat)
            }
        }

        saveUnSeenCountFirebaseData = saveUnSeenCountFirebaseObj.switchMap { obj ->
            if (obj == null) {
                AbsentLiveData.create()
            } else {
                chatRepository.saveUnSeenCountToFirebase(obj.key, obj.receiver)
            }
        }

        resetUnSeenCountFirebaseData = resetUnSeenCountFirebaseObj.switchMap { obj ->
            if (obj == null) {
                AbsentLiveData.create()
            } else {
                chatRepository.resetUnSeenCountToFirebase(obj.key, obj.receiver)
            }
        }

    }
    //register
    fun setRegisterUserToFirebaseObj(email: String, password: String) {
        val tmpDataHolder = TmpDataHolder(email = email, password = password)
        registerUserToFirebaseObj.value = tmpDataHolder
    }
    fun getRegisterUserToFirebaseData(): LiveData<Resource<Boolean>>? {
        return registerUserToFirebaseData
    }

    //login
    fun setLoginUserToFirebaseObj(email: String, password: String) {
        val tmpDataHolder =
            TmpDataHolder(
               email =  email,
                password = password
            )
        loginUserToFirebaseObj.value = tmpDataHolder
    }

    fun getLoginUserToFirebaseData(): LiveData<Resource<Boolean>>? {
        return loginUserToFirebaseData
    }

    //save messages
    fun setSaveMessagesToFirebaseObj(messages: Message?, senderId: String?, receiverId: String?){
        val tmpDataHolder =
            saveMessageTmpDataHolder(
                messages!!,
                senderId!!,
                receiverId!!
            )
        saveMessagesToFirebaseObj.value = tmpDataHolder
    }

    fun getSaveMessagesToFirebaseData(): LiveData<Resource<Boolean>>? {
        return saveMessagesToFirebaseData
    }

    //fetch messages conversation
    fun setFetchMessagesFromConversationObj(senderId: String?, receiverId: String?){
        if (!isLoading) {
            val tmpDataHolder = fetchMessagesTmpDataHolder(senderId!!, receiverId!!)
            fetchMessagesFromConversationObj.value = tmpDataHolder
            setLoadingState(true)
        }
    }

    fun getFetchMessagesFromConversationData(): LiveData<Resource<Boolean>>? {
        return fetchMessagesFromConversationData
    }

    //data from database
    fun setGetMessagesFromDatabaseObj(senderId: String?, receiverId: String?) {
        val tmpDataHolder = fetchMessagesTmpDataHolder(senderId!!, receiverId!!)
        this.getMessagesFromDatabaseObj.value = tmpDataHolder
    }

    fun getGetMessagesFromDatabaseData(): LiveData<List<Message>>? {
        return getMessagesFromDatabaseData
    }

    //get data from chat rooms
    fun setGetChatRoomsObj(userId: String?) {
        val tmpDataHolder = fetchChatRoomsTmpDataHolder(userId!!)
        this.fetchChatRoomsListObj.value = tmpDataHolder
    }

    fun getGetChatRoomsData(): LiveData<List<Chat>>? {
        return fetchChatRoomListData
    }

    //save chat room by user id
    fun setSaveChatRoomToFirebaseObj(chat: Chat, senderId: String?, receiverId: String?){
        val tmpDataHolder = saveChatRoomTmpDataHolder(chat,senderId!!, receiverId!!)
        saveChatRoomToFirebaseObj.value = tmpDataHolder
    }

    fun getSaveChatRoomToFirebaseData(): LiveData<Resource<Boolean>>? {
        return saveChatRoomToFirebaseData
    }

    //save user in firebase
    fun setSaveUserChatObj(chat: UserChat){
        val tmp = saveUserChatTmpDataHolder(chat)
        saveUserChatToFirebaseObj.value = tmp
    }

    fun getSaveUserChatData(): LiveData<Resource<Boolean>>?{
        return saveUserChatToFirebaseData
    }

    //add count unsee messages
    fun setUpdateUnseenMessagesObj(key: String, receiverId: String) {
        val tmp = TmpCountUnseenHolder(key, receiverId)
        saveUnSeenCountFirebaseObj.value = tmp
    }

    fun getUpdateUnSeenMessagesData(): LiveData<Resource<Boolean>>?{
        return saveUnSeenCountFirebaseData
    }

    //reset unsee messages
    fun setResetUnseenMessagesObj(key: String, receiverId: String) {
        val tmp = TmpCountUnseenHolder(key, receiverId)
        resetUnSeenCountFirebaseObj.value = tmp
    }

    fun getResetUnSeenMessagesData(): LiveData<Resource<Boolean>>?{
        return resetUnSeenCountFirebaseData
    }

    internal class TmpDataHolder(var email: String, var password: String)
    internal class TmpCountUnseenHolder(var key: String, var receiver: String)
    internal class saveMessageTmpDataHolder(var message: Message,
                                            var senderId: String,
                                            var receiverId: String)

    internal class fetchMessagesTmpDataHolder(
        var senderId: String,
        var receiverId: String
    )

    internal class fetchChatRoomsTmpDataHolder(var senderId: String)

    internal class imageUploadTmpDataHolder(
        var path: String,
        var senderId: String,
        var receiverId: String
    )

    internal class saveChatRoomTmpDataHolder(var chat: Chat, var senderId: String, var receiverId: String)

    internal class saveUserChatTmpDataHolder(var chat: UserChat)
}
