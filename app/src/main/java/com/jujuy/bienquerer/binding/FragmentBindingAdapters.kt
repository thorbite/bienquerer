package com.jujuy.bienquerer.binding

import android.util.Patterns
import android.widget.ImageView
import androidx.core.net.toUri
import androidx.databinding.BindingAdapter
import coil.api.load
import coil.transform.CircleCropTransformation
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.api.ApiConstants
import timber.log.Timber

@BindingAdapter("imageCircleUrl")
fun bindCircleImage(imageView: ImageView?, url: String?) {
    var urlComplete = ""
    if (isValid(imageView, url)) {
        urlComplete = ApiConstants.APP_IMAGES_URL + url
        imageView?.load(urlComplete) {
            crossfade(true)
            placeholder(R.drawable.circle_default_image)
            transformations(CircleCropTransformation())
        }
    } else {
        imageView?.setImageResource(R.drawable.circle_default_image)
    }
}


@BindingAdapter("loadImageUrl")
fun bindUrlImage(imageView: ImageView?, url: String?) {
    if (isValid(imageView, url)) {
        Glide.with(imageView!!.context)
            .load(ApiConstants.APP_IMAGES_URL + url)
            .placeholder(R.drawable.default_image)
            .into(imageView);
    } else {
        Timber.e("HUBO ERROR EN LA IMAGEN...$url")
        imageView?.setImageResource(R.drawable.default_image)
    }
}

@BindingAdapter("imageCoverUrl")
fun bindCoverImage(imageView: ImageView?, url: String?) {
    var urlComplete = ""
    if (isValid(imageView, url)) {
        urlComplete = ApiConstants.APP_COVER_IMAGES + url
        //imageView?.setImageURI(urlComplete.toUri())
        Glide.with(imageView!!.context)
            .load(urlComplete)
            .diskCacheStrategy(DiskCacheStrategy.NONE)
            .skipMemoryCache(true)
            .placeholder(R.drawable.default_image)
            .into(imageView)
    } else {
        imageView?.setImageResource(R.drawable.default_image)
    }
}

@BindingAdapter("loadImage")
fun setImage(imageView: ImageView?, url: String?) {
    var urlComplete = ""
    if (!url.isNullOrEmpty()){
        if (isUrlValid(url)) {
            Glide.with(imageView!!.context)
                .load(url)
                .circleCrop()
                .placeholder(R.drawable.circle_default_image)
                .into(imageView)
        }else{
            urlComplete = ApiConstants.APP_PROFILE_IMAGES + url
            Glide.with(imageView!!.context)
                .load(urlComplete)
                .circleCrop()
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .placeholder(R.drawable.circle_default_image)
                .into(imageView);
        }
    }else{
        imageView?.setImageResource(R.drawable.circle_default_image)
    }
}

fun isUrlValid(url: String?): Boolean{
    return Patterns.WEB_URL.matcher(url.toString()).matches()
}

fun isValid(imageView: ImageView?, url: String?): Boolean {
    return !(url == null || imageView == null || url == "")
}