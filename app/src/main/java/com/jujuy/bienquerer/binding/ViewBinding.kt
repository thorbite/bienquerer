package com.jujuy.bienquerer.binding

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import androidx.lifecycle.LiveData
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.extensions.visible
import com.jujuy.bienquerer.utils.Utils
import com.perfomer.blitz.setTimeAgo
import com.skydoves.whatif.whatIfNotNull
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlin.time.ExperimentalTime

@BindingAdapter("toast")
fun bindToast(view: View, text: LiveData<String>) {
  text.value.whatIfNotNull {
    Toast.makeText(view.context, it, Toast.LENGTH_SHORT).show()
  }
}

@BindingAdapter("visibilityByResource")
fun bindVisibilityByResource(view: View, anyList: List<Any>?) {
  anyList.whatIfNotNull {
    view.visible()
  }
}

@BindingAdapter("visibleGone")
fun showHide(view: View, show: Boolean) {
  view.visibility = if (show) View.VISIBLE else View.GONE
}

@BindingAdapter("onlineTint")
fun bindingOnlineTint(view: ImageView, isOnline: Boolean?) {
  isOnline?.let {
    view.setColorFilter(
      if (isOnline) ContextCompat.getColor(view.context, R.color.onlineOn)
      else ContextCompat.getColor(view.context, R.color.onlineOff)
    )
  }
}

@BindingAdapter("hourTime")
fun bindingTextHourTime(textView: TextView, time: Long?) {
    /*textView.text = if (time == null ) { "" } else {
    DateUtils.getRelativeTimeSpanString(time).toString()
  }*/

  textView.text = if (time == null) { "" } else {
    textView.setTimeAgo(time, showSeconds = true, autoUpdate = true).toString()
  }
}

@ExperimentalCoroutinesApi
@ExperimentalTime
@BindingAdapter("dateTime")
fun bindingDateTime(textView: TextView, date: String?){
  textView.text = if (date.isNullOrEmpty()){ "" } else {
    Utils.parseDateCorrectForm(date)
  }
}