package com.jujuy.bienquerer.di


import com.jujuy.bienquerer.api.ApiConstants
import com.jujuy.bienquerer.api.network.FlowResourceCallAdapterFactory
import com.jujuy.bienquerer.api.network.RequestInterceptor
import com.jujuy.bienquerer.api.network.LiveDataCallAdapterFactory
import com.jujuy.bienquerer.api.service.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
val networkModule = module {
  single {
    HttpLoggingInterceptor().apply {
      level = HttpLoggingInterceptor.Level.BODY
    }
  }

  single {
    OkHttpClient.Builder()
      .addInterceptor(
        RequestInterceptor(
          androidContext()
        )
      )
      .addInterceptor(get<HttpLoggingInterceptor>())
      .build()
  }

  single {
    Retrofit.Builder()
      .client(get<OkHttpClient>())
      .baseUrl(ApiConstants.BASE_URL)
      .addConverterFactory(GsonConverterFactory.create())
      .addCallAdapterFactory(LiveDataCallAdapterFactory())
      .addCallAdapterFactory(FlowResourceCallAdapterFactory())
      .build()
  }

  single {
    get<Retrofit>().create(AuthService::class.java)
  }
  single {
    get<Retrofit>().create(CategoryService::class.java)
  }
  single {
    get<Retrofit>().create(SubCategoryService::class.java)
  }
  single {
    get<Retrofit>().create(ProfessionalService::class.java)
  }
  single {
    get<Retrofit>().create(AppointmentService::class.java)
  }
  single {
    get<Retrofit>().create(NotificationService::class.java)
  }
}
