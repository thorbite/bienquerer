package com.jujuy.bienquerer.di

import com.jujuy.bienquerer.base.NavigationController
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.dsl.module
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
val NavigationModule = module {
    single { NavigationController() }

}