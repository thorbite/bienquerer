package com.jujuy.bienquerer.di

import android.app.Application
import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import com.jujuy.bienquerer.api.network.AppExecutors
import com.jujuy.bienquerer.utils.AppPreferences
import com.jujuy.bienquerer.utils.Connectivity
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

val SharedPreferencesModule = module {

    single{ sharedPrefs(androidApplication()) }

    single { Connectivity(androidApplication()) }

    single { AppExecutors() }

    single { AppPreferences(androidApplication()) }
}

fun sharedPrefs(androidApplication: Application): SharedPreferences {
    return  PreferenceManager.getDefaultSharedPreferences(androidApplication.applicationContext)
}