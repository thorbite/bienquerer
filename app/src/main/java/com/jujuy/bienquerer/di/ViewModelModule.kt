package com.jujuy.bienquerer.di

import com.jujuy.bienquerer.ui.beProfessional.SocialMediaViewModel
import com.jujuy.bienquerer.ui.professionals.comments.CommentsViewModel
import com.jujuy.bienquerer.ui.scoringJob.ScoringViewModel
import com.jujuy.bienquerer.viewmodel.user.UserViewModel
import com.jujuy.bienquerer.viewmodel.category.CategoryViewModel
import com.jujuy.bienquerer.viewmodel.chat.ChatViewModel
import com.jujuy.bienquerer.viewmodel.home.HomeViewModel
import com.jujuy.bienquerer.viewmodel.notifications.NotificationsViewModel
import com.jujuy.bienquerer.viewmodel.favorite.FavoriteViewModel
import com.jujuy.bienquerer.viewmodel.jobs.AppointmentViewModel
import com.jujuy.bienquerer.viewmodel.professional.ProfessionalViewModel
import com.jujuy.bienquerer.viewmodel.subcategory.SubCategoryViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
val viewModelModule = module {
  viewModel { NotificationsViewModel(get()) }
  viewModel { HomeViewModel(get()) }
  viewModel { SubCategoryViewModel(get()) }
  viewModel { UserViewModel(get()) }
  viewModel { CategoryViewModel(get()) }
  viewModel { ProfessionalViewModel(get()) }
  viewModel { ChatViewModel(get()) }
  viewModel { FavoriteViewModel(get()) }
  viewModel { AppointmentViewModel(get()) }
  viewModel { SocialMediaViewModel(get()) }
  viewModel { ScoringViewModel(get()) }
  viewModel { CommentsViewModel(get()) }

}
