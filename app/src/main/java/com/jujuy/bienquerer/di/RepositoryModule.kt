package com.jujuy.bienquerer.di


import com.jujuy.bienquerer.repository.*
import com.jujuy.bienquerer.ui.search.DataProvider
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.dsl.module
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
val repositoryModule = module {
  single { NotificationRepository(get(), get(), get(), get(), get()) }
  single { HomeRepository(get(),get(),get()) }
  single { CategoryRepository(get(),get(), get(),get(),get(),get(), get()) }
  single { UserRepository(connectivity = get(),appExecutors = get(),authService = get(),userDao = get()) }
  single { ProfessionalRepository(get(),get(),get(),get()) }
  single { ChatRepository(get()) }
  single { JobRepository(get(), get(), get(), get(),get()) }
  single { DataProvider(get())}

}
