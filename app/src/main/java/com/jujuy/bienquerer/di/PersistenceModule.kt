package com.jujuy.bienquerer.di

import com.jujuy.bienquerer.room.AppDatabase
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.dsl.module
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
val persistenceModule = module {
  single {  AppDatabase.getInstance(get())}
  single { get<AppDatabase>().notificationDao() }
  single { get<AppDatabase>().categoryDao() }
  single { get<AppDatabase>().providerDao() }
  single { get<AppDatabase>().subCategoryDao()}
  single { get<AppDatabase>().userDao() }
  single { get<AppDatabase>().professionalDao() }
  single { get<AppDatabase>().itemMapDao() }
  single { get<AppDatabase>().messageDao() }
  single { get<AppDatabase>().appointmentDao() }
  single { get<AppDatabase>().jobDao() }
  single { get<AppDatabase>().remoteKeysDao() }
  single { get<AppDatabase>().notificationKeysDao() }
  single { get<AppDatabase>().favoriteKeysDao() }
  single { get<AppDatabase>().appointmentUserKeyDao() }
  single { get<AppDatabase>().jobProfessionalKeyDao() }
  single { get<AppDatabase>().searchDao() }

}
