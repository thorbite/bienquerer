package com.jujuy.bienquerer.api.service

import com.jujuy.bienquerer.api.ApiConstants
import com.jujuy.bienquerer.api.network.Resource
import com.jujuy.bienquerer.model.network.BaseResponse
import com.jujuy.bienquerer.model.network.request.AppointmentRequest
import com.jujuy.bienquerer.model.network.response.JobsListResponse
import com.jujuy.bienquerer.model.network.response.ListAppoinments
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import retrofit2.Call
import retrofit2.http.*
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
interface AppointmentService {
    @POST(ApiConstants.POST_ADD_APPOINTMENT)
    fun addNewAppointment(@Body appointmentRequest: AppointmentRequest) : Flow<Resource<BaseResponse<Boolean>>>

    @GET(ApiConstants.GET_APPOINTMENT)
    fun getAppointment(@Body appointmentRequest: AppointmentRequest) : Call<BaseResponse<Boolean>>

    @POST(ApiConstants.POST_APPOINTMENT_USER)
    fun getListAppointments() : Flow<Resource<BaseResponse<ListAppoinments>>>

    @POST(ApiConstants.POST_APPOINTMENT_USER)
    suspend fun getListAppointments(@Query("currentPage") currentPage: Int) : BaseResponse<ListAppoinments>

    @POST(ApiConstants.POST_APPOINTMENT_PROFESSIONAL)
    fun getListJobs() : Flow<Resource<BaseResponse<JobsListResponse>>>

    @POST(ApiConstants.POST_APPOINTMENT_PROFESSIONAL)
    suspend fun getListJobs(@Query("currentPage") currentPage: Int) : BaseResponse<JobsListResponse>

    @FormUrlEncoded
    @PUT(ApiConstants.PUT_JOB_STATUS)
    fun changeStatusJob(@Field("comment") comment: String?,
                        @Field("jobId") jobId: String,
                        @Field("statusId") statusId: String): Flow<Resource<BaseResponse<Any>>>
}