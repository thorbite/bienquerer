package com.jujuy.bienquerer.api.service

import com.jujuy.bienquerer.api.ApiConstants
import com.jujuy.bienquerer.api.network.Resource
import com.jujuy.bienquerer.model.network.BaseResponse
import com.jujuy.bienquerer.model.network.response.ListNotifications
import com.jujuy.bienquerer.model.network.response.DataResponse
import kotlinx.coroutines.flow.Flow
import retrofit2.http.*

interface NotificationService {
    @GET(ApiConstants.GET_NOTIFICATION_COUNT)
    fun getNotificationsCount(): Flow<Resource<BaseResponse<Int>>>

    @POST(ApiConstants.GET_NOTIFICATION_LIST)
    suspend fun getNotificationsList(@Query("currentPage") page: Int): BaseResponse<DataResponse>

    @GET(ApiConstants.GET_NOTIFICATION_ITEM)
    fun getNotificationsItem(@Path("id") notificationId: String): Flow<Resource<BaseResponse<ListNotifications>>>

    //@FormUrlEncoded
    //@HTTP(method = "DELETE", path = ApiConstants.GET_NOTIFICATION_ITEM, hasBody = false)
    @DELETE(ApiConstants.GET_NOTIFICATION_ITEM)
    fun deleteNotificationbyId(@Path("id") notificationId: String): Flow<Resource<BaseResponse<Boolean>>>
}