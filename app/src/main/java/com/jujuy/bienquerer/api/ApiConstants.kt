package com.jujuy.bienquerer.api

object ApiConstants {
    // Endpoints
    const val BASE_URL = "http://3.128.95.194/bienquerer-server/public/index.php/"
    const val LOGIN_URL = "api/login"
    const val ALL_CATEGORIES_URL = "public/categories/all"
    const val ALL_SUBCATEGORY_BY_CAT_ID_URL = "public/professions/all"
    const val SUBCATEGORIES_URL = "api/admin/professions/search?_format=json&currentPage=1&sortField=id"
    const val SUBCATEGORIES_BY_ID_URL = "public/professionals/search?_format=json&currentPage=1&sortField=id"
    const val REGISTER_GOOGLE_URL = "api/register/google"
    const val REGISTER_URL = "api/register"
    const val SEND_SMS_URL = "api/users/send/sms" //{userId: 9}
    const val VALIDATE_CODE_SMS_URL = "api/users/validate/code/sms" //{userId: 9, code: "14254"}
    const val USER_URL = "api/users"
    const val USER_DETAIL = "api/admin/{userId}/user"
    const val SEARCH_PROFESSIONAL_URL = "public/professionals/search?_format=json&sortDirection=DESC&sortField=id"
    const val PROFESSIONAL_DETAIL = "public/{professionalId}/professionals "
    const val UPDATE_USER_LOCATION = "api/users/location"
    const val SAVE_PROFESSION_URL = "api/users/professional/profession"
    const val DELETE_PROFESSION = "api/users/professional/profession"
    const val SAVE_PROFESSIONAL_FAVORITE = "api/users/professional/favorite"
    const val GET_PROFESSIONAL_FAVORITE = "api/users/professional/favorite/verify"
    const val DELETE_PROFESSIONAL_FAVORITE = "api/users/professional/favorite"
    const val LIST_PROFESSIONAL_FAVORITE = "api/users/professional/favorites/search?_format=json&sortDirection=DESC&sortField=id"
    const val POST_REGISTER_FACEBOOK = "api/register/facebook"
    const val PUT_PROFESSIONAL_AVAILABLE = "api/users/professional/available"
    const val PUT_PROFESSIONAL_SOCIAL = "api/users/professional/social"
    const val POST_ADD_APPOINTMENT = "api/jobs"
    const val GET_APPOINTMENT = "api/jobs/status/all"
    const val POST_APPOINTMENT_PROFESSIONAL = "api/jobs/professional/search?_format=json&sortDirection=DESC&sortField=id"
    const val POST_APPOINTMENT_USER = "api/jobs/user/search?_format=json&sortDirection=DESC&sortField=id"

    const val SEARCH_PROFESSIONAL_BY_KEY = "public/professionals/professions/filter"
    const val GET_MAP_LIST = "api/users/professional/map"

    const val GET_NOTIFICATION_COUNT = "api/notifications/news/user/count"
    const val GET_NOTIFICATION_LIST = "api/notifications/search?_format=json&sortDirection=DESC&sortField=id"
    const val GET_NOTIFICATION_ITEM = "api/notifications/{id}"
    const val POST_REGISTER_PROFESSIONAL = "api/users/professional/register/complete"
    const val POST_SOCIAL_NETWORK = "api/admin/socialnetworks"

    const val PUT_JOB_STATUS = "api/jobs/status"

    const val PUT_SMS_CODE = "api/users/phone"

    const val PUT_RATING_PROFESSIONAL = "api/jobs/rating"

    const val GET_LIST_COMMENTS = "public/{professionalId}/professionals/comments"

    const val COVER_PROFILE_IMAGE = "api/users/photo/cover"

    const val APP_IMAGES_URL = "http://3.128.95.194/bienquerer-server/public/uploads/"
    const val APP_PROFILE_IMAGES = "http://3.128.95.194/bienquerer-server/public/uploads/users/profile/"
    const val APP_COVER_IMAGES = "http://3.128.95.194/bienquerer-server/public/uploads/users/cover/"
    const val LOGOUT_URL= "api/users/logout"
}