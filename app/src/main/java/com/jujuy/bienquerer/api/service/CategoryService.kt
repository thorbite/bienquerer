package com.jujuy.bienquerer.api.service

import com.jujuy.bienquerer.api.ApiConstants
import com.jujuy.bienquerer.api.network.Resource
import com.jujuy.bienquerer.model.entity.Category
import com.jujuy.bienquerer.model.network.BaseResponse
import kotlinx.coroutines.flow.Flow
import retrofit2.http.GET


interface CategoryService {

    @GET(ApiConstants.ALL_CATEGORIES_URL)
    fun getAllCategories(): Flow<Resource<BaseResponse<List<Category>>>>

}