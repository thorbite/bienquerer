package com.jujuy.bienquerer.api.service

import com.jujuy.bienquerer.api.ApiConstants
import com.jujuy.bienquerer.api.network.Resource
import com.jujuy.bienquerer.model.entity.Professional
import com.jujuy.bienquerer.model.entity.User
import com.jujuy.bienquerer.model.network.*
import com.jujuy.bienquerer.model.network.request.FacebookRequest
import com.jujuy.bienquerer.model.network.request.GoogleRequest
import com.jujuy.bienquerer.model.network.request.LoginRequest
import com.jujuy.bienquerer.model.network.request.RegisterRequest
import com.jujuy.bienquerer.model.network.response.LoginResponse
import kotlinx.coroutines.flow.Flow
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface AuthService {

    @POST(ApiConstants.LOGIN_URL)
    fun postUserLogin(@Body request: LoginRequest): Flow<Resource<LoginResponse>>

    @POST(ApiConstants.REGISTER_URL)
    fun postUser(@Body requestRegister: RegisterRequest): Flow<Resource<LoginResponse>>

    @POST(ApiConstants.REGISTER_GOOGLE_URL)
    fun postUserGoogle(@Body requestGoogle: GoogleRequest) : Flow<Resource<LoginResponse>>

    @GET(ApiConstants.USER_URL)
    fun getUserDetail(): Flow<Resource<BaseResponse<User>>>

    @GET(ApiConstants.USER_DETAIL)
    fun getUser(@Path("userId") userId: String): Flow<Resource<BaseResponse<User>>>

    @FormUrlEncoded
    @POST(ApiConstants.SEND_SMS_URL)
    fun sendSmsforCode(@Field("userId") userId: String): Flow<Resource<LoginResponse>>

    @FormUrlEncoded
    @PUT(ApiConstants.PUT_SMS_CODE)
    fun sendSms(@Field("phone") phone: String): Flow<Resource<BaseResponse<Any>>>

    @FormUrlEncoded
    @POST(ApiConstants.VALIDATE_CODE_SMS_URL)
    fun validateCode(@Field("userId") userId: String,
                     @Field("code") code: String): Call<LoginResponse>

    @FormUrlEncoded
    @PUT(ApiConstants.USER_URL)
    fun putUser(@Field("id") id: String,
                @Field("name") name: String?,
                @Field("lastname") lastname: String?,
                @Field("username") username: String,
                @Field("email") email: String,
                @Field("phone") phone: String?,
                @Field("address") address: String?,
                @Field("password") password:String?,
                @Field("passwordRepeat") passwordRepeat: String?,
                @Field("rolId") rolId: Int?): Flow<Resource<BaseResponse<Any>>>

    @Multipart
    @POST("api/users/photo/profile")
    fun doUploadImage(@Part("file")name: RequestBody,
                      @Part file: MultipartBody.Part): Flow<Resource<BaseResponse<Professional>>>

    @Multipart
    @POST(ApiConstants.COVER_PROFILE_IMAGE)
    fun doUploadCoverImage(@Part("file")name: RequestBody,
                      @Part file: MultipartBody.Part): Flow<Resource<BaseResponse<Professional>>>

    @POST(ApiConstants.POST_REGISTER_FACEBOOK)
    fun postRegisterFacebook(@Body facebookRequest: FacebookRequest): Flow<Resource<LoginResponse>>

    @FormUrlEncoded
    @PUT(ApiConstants.LOGOUT_URL)
    fun logoutUser(@Field("fcmKey") fcmKey: String): Flow<Resource<BaseResponse<Any>>>

}