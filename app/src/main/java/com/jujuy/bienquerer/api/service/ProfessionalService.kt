package com.jujuy.bienquerer.api.service

import com.jujuy.bienquerer.api.ApiConstants
import com.jujuy.bienquerer.api.network.Resource
import com.jujuy.bienquerer.model.SocialNetworks
import com.jujuy.bienquerer.model.entity.Comment
import com.jujuy.bienquerer.model.entity.ItemSearch
import com.jujuy.bienquerer.model.entity.Professional
import com.jujuy.bienquerer.model.network.*
import com.jujuy.bienquerer.model.network.request.ListProfessionalRequest
import com.jujuy.bienquerer.model.network.request.MapListRequest
import com.jujuy.bienquerer.model.network.request.ScoreRequest
import com.jujuy.bienquerer.model.network.response.FavoriteListResponse
import com.jujuy.bienquerer.model.network.response.ListProfessional
import kotlinx.coroutines.flow.Flow
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*
import java.util.stream.BaseStream


interface ProfessionalService {
    @POST(ApiConstants.SEARCH_PROFESSIONAL_URL)
    suspend fun getProfessionalsBySubCatId(@Body request: ListProfessionalRequest,
                                   @Query("currentPage") currentPage: Int): BaseResponse<ListProfessional>

    @PUT(ApiConstants.PUT_RATING_PROFESSIONAL)
    fun putProfessionalScore(@Body request: ScoreRequest): Flow<Resource<BaseResponse<Boolean>>>

    @GET(ApiConstants.GET_LIST_COMMENTS)
    fun getListComments(@Path("professionalId") professionId: String) : Flow<Resource<BaseResponse<List<Comment>>>>

    @GET(ApiConstants.POST_SOCIAL_NETWORK)
    fun getAllSocialNetworks(): Flow<Resource<BaseResponse<List<SocialNetworks>>>>

    @GET(ApiConstants.PROFESSIONAL_DETAIL)
    fun getProfessionalDetail(@Path("professionalId") professionalId: String): Flow<Resource<BaseResponse<Professional>>>

    @FormUrlEncoded
    @PUT(ApiConstants.UPDATE_USER_LOCATION)
    fun updateUserLocation(@Field("userId") id: String?,
                           @Field("address") address: String?,
                           @Field("lat") lat: String?,
                           @Field("lng") lng: String?): Flow<Resource<BaseResponse<Professional>>>


    @FormUrlEncoded
    @POST(ApiConstants.SAVE_PROFESSION_URL)
    fun saveProfessionById(@Field("userId") id: String?,
                           @Field("professionId") professionId: String?): Flow<Resource<BaseResponse<Boolean>>>

    @FormUrlEncoded
    @HTTP(method = "DELETE", path = ApiConstants.DELETE_PROFESSION, hasBody = true)
    fun deleteProfessionById(@Field("userId") id: String?,
                             @Field("professionId") professionId: String?): Flow<Resource<BaseResponse<Boolean>>>

    @FormUrlEncoded
    @POST(ApiConstants.SAVE_PROFESSIONAL_FAVORITE)
    fun setPostFavourite(@Field("professionalId") professionId: String?): Flow<Resource<BaseResponse<Boolean>>>

    @GET(ApiConstants.GET_PROFESSIONAL_FAVORITE)
    fun getItemFavorite(@Query("professionalId") professionId: String?): Flow<Resource<BaseResponse<Boolean>>>

    @FormUrlEncoded
    @HTTP(method = "DELETE", path = ApiConstants.DELETE_PROFESSIONAL_FAVORITE, hasBody = true)
    fun deleteFavoriteById(@Field("professionalId") professionId: String?): Flow<Resource<BaseResponse<Boolean>>>

    @POST(ApiConstants.LIST_PROFESSIONAL_FAVORITE)
    suspend fun getListFavorite(@Query("currentPage") currentPage: Int): BaseResponse<FavoriteListResponse>

    @PUT(ApiConstants.PUT_PROFESSIONAL_AVAILABLE)
    fun putProfessionalAvailable(): Flow<Resource<BaseResponse<Boolean>>>

    @PUT(ApiConstants.PUT_PROFESSIONAL_SOCIAL)
    fun putProfessionalSocial(): Flow<Resource<BaseResponse<Boolean>>>

    @Multipart
    @POST(ApiConstants.POST_REGISTER_PROFESSIONAL)
    fun registerProfessional(
        @Part profilePhoto: MultipartBody.Part?,
        @Part coverPhoto: MultipartBody.Part?,
        @Part("data") data: RequestBody?
    ): Flow<Resource<BaseResponse<Boolean>>>


    @GET(ApiConstants.SEARCH_PROFESSIONAL_BY_KEY)
    fun getItemSearch(@Query("term") key: String?): Flow<Resource<BaseResponse<List<ItemSearch>>>>

    @POST(ApiConstants.GET_MAP_LIST)
    suspend fun getMapList(@Body requestMap: MapListRequest): BaseResponse<List<Professional>>

    @POST(ApiConstants.GET_MAP_LIST)
    fun getMapListFilter(@Body requestMap: MapListRequest): Flow<Resource<BaseResponse<List<Professional>>>>

}