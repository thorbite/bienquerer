package com.jujuy.bienquerer.api.service

import androidx.lifecycle.LiveData
import com.jujuy.bienquerer.api.ApiConstants
import com.jujuy.bienquerer.api.network.ApiResponse
import com.jujuy.bienquerer.api.network.Resource
import com.jujuy.bienquerer.model.entity.SubCategory
import com.jujuy.bienquerer.model.network.BaseResponse
import kotlinx.coroutines.flow.Flow
import retrofit2.Call
import retrofit2.http.*

interface SubCategoryService {

    @GET(ApiConstants.ALL_SUBCATEGORY_BY_CAT_ID_URL)
    fun getSubCategories(): Call<BaseResponse<List<SubCategory>>>

    @GET(ApiConstants.ALL_SUBCATEGORY_BY_CAT_ID_URL)
    fun getAllProfessions(): Flow<Resource<BaseResponse<List<SubCategory>>>>

    /*@FormUrlEncoded
    @POST(ApiConstants.SUBCATEGORIES_URL)
    fun getSubCategoriesByKey(@Field("name") name: String, @Field("categoryId") categoryId: String?): Call<BaseResponse<ProfessionList>>
*/
    @GET(ApiConstants.ALL_SUBCATEGORY_BY_CAT_ID_URL)
    fun getAllSubCategoriesByCatId(@Query("categoryId") catId: String) : LiveData<ApiResponse<BaseResponse<List<SubCategory>>>>

    @GET(ApiConstants.ALL_SUBCATEGORY_BY_CAT_ID_URL)
    fun getAllSubCategories() : LiveData<ApiResponse<BaseResponse<List<SubCategory>>>>
}