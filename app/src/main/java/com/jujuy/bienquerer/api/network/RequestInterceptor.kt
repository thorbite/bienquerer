package com.jujuy.bienquerer.api.network

import android.content.Context
import com.jujuy.bienquerer.utils.AppPreferences
import okhttp3.Interceptor
import okhttp3.Response

internal class RequestInterceptor(context: Context) : Interceptor {
  private val sessionManager = AppPreferences(context)

  override fun intercept(chain: Interceptor.Chain): Response {
    val requestBuilder = chain.request().newBuilder()

    sessionManager.fetchAuthToken()?.let {
      requestBuilder.addHeader("Authorization", "Bearer $it")
    }

    return chain.proceed(requestBuilder.build())
  }
}
