/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jujuy.bienquerer.api.network

import com.jujuy.bienquerer.model.network.BaseResponse
import org.json.JSONObject
import retrofit2.Response
import timber.log.Timber


/**
 * A generic class that holds a value with its loading status.
 * @param <T>
</T> */
class Resource<out T> private constructor(
    val status: Status, val data: T?,
    val message: String?
) {

    enum class Status {
        SUCCESS, ERROR, LOADING
    }

    companion object {

        fun <T> success(data: T?): Resource<T> {
            return Resource(
                Status.SUCCESS,
                data,
                null
            )
        }

        fun <T> error(msg: String, data: T? = null): Resource<T> {
            return Resource(
                Status.ERROR,
                data,
                msg
            )
        }


        fun <T> loading(data: T? = null): Resource<T> {
            return Resource(
                Status.LOADING,
                data,
                null
            )
        }

        fun <T> create(status: Status, data: T?, message: String?): Resource<T> {
            return Resource(status, data, message)
        }

        /**
         * Create error resource
         */
        fun <T> create(throwable: Throwable): Resource<T> {
            return Resource(
                Status.ERROR,
                null,
                throwable.message ?: "Error desconocido"
            )
        }

        fun <T> create(response: Response<T>, isNeedDeepCheck: Boolean = false): Resource<T> {

            return if (response.isSuccessful) {
                val body = response.body()
                if (body == null || response.code() == 204) {
                    Resource<T>(
                        Status.SUCCESS,
                        null,
                        "Sin contenido"
                    )
                } else {
                    if (isNeedDeepCheck) {
                        if (body is BaseResponse<*>) {
                            val baseApiResponse = body as BaseResponse<*>
                            if (baseApiResponse.code != 200) {
                                Resource<T>(
                                    Status.ERROR,
                                    body,
                                    baseApiResponse.message
                                )
                            } else {
                                Resource<T>(
                                    Status.SUCCESS,
                                    body,
                                    "success"
                                )
                            }
                        } else {
                            Resource<T>(
                                Status.ERROR,
                                body,
                                "Body doesn't follow BaseApiResponse standard. DeepCheck not possible"
                            )
                        }

                    } else {
                        Resource(
                            Status.SUCCESS,
                            body,
                            "success"
                        )
                    }
                }
            } else {

                val msg = response.errorBody()?.string()
                val errorMsg = if (msg.isNullOrEmpty()) {
                    response.message()
                } else {
                    // Return error string
                    JSONObject(msg).getString("message")
                }
                error(errorMsg ?: "Error desconocido")
            }
        }
    }
}
