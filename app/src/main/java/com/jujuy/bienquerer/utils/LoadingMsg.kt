package com.jujuy.bienquerer.utils

import android.app.Activity
import android.app.Dialog
import android.view.Window
import android.view.WindowManager
import com.jujuy.bienquerer.R

class LoadingMsg(val activity: Activity, val cancelable: Boolean) {

    var dialog: Dialog = Dialog(activity)
    init {
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    }

    private fun getLayoutParams(dialog: Dialog): WindowManager.LayoutParams? {
        val layoutParams = WindowManager.LayoutParams()
        if (dialog.window != null) {
            layoutParams.copyFrom(dialog.window!!.attributes)
        }
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT
        return layoutParams
    }

    fun showloaderDialog() {
        this.dialog.setContentView(R.layout.dialog_loading)
        if (dialog.window != null) {
            dialog.window!!.attributes = getLayoutParams(dialog)
            dialog.window!!.setBackgroundDrawableResource(R.color.transparent)
            dialog.setCancelable(false)
        }
    }

    fun show() {
        if (dialog != null) {
            dialog.show()
        }
    }
    fun cancel() {
        if (dialog != null) {
            dialog.dismiss()
        }
    }

}