package com.jujuy.bienquerer.utils

import android.content.Context
import android.content.SharedPreferences
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.di.sharedPrefs
import com.jujuy.bienquerer.extensions.putAny
import com.jujuy.bienquerer.extensions.remove
import com.jujuy.bienquerer.model.entity.User
import timber.log.Timber

class AppPreferences(context: Context) {
    private var prefs: SharedPreferences = context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE)

    companion object {
        const val USER_TOKEN = "user_token"
        const val USER_CODE = "user_code"
        const val IS_LOGIN = "isLogin"
        const val IS_PROFESSIONAL = "isProfessional"
        const val TOKEN_FIREBASE = "token_firebase"
    }

    /**
     * Function to save auth token
     */
    fun saveAuthToken(token: String) {
        Timber.e("Token $token")
        prefs.putAny(IS_LOGIN, true)
        prefs.putAny(USER_TOKEN, token)
    }

    fun signOut(){
        prefs.remove(IS_LOGIN)
        prefs.remove(USER_TOKEN)
        prefs.remove(USER_CODE)
    }

    /**
     * Function to fetch auth token
     */
    fun fetchAuthToken(): String? = prefs.getString(USER_TOKEN, null)

    fun isLoggedIn(): Boolean = prefs.getBoolean(IS_LOGIN, false)

    fun fetchTokenFirebase(): String? = prefs.getString(TOKEN_FIREBASE, null)

    fun saveTokenFirebase(token: String){
        prefs.putAny(TOKEN_FIREBASE, token)
    }

    /***************************************/
    fun saveUserCode(code: String) {
        prefs.putAny(USER_CODE, code)
    }

    fun fetchUserCode(): String? = prefs.getString(USER_CODE, null)

    /***************************/
    fun setIsProfessional(boolean: Boolean){
        prefs.putAny(IS_PROFESSIONAL, boolean)
    }

    fun getIsProfessional() = prefs.getBoolean(IS_PROFESSIONAL, false)

    /***************************/

    fun addUserLoginData(user: User) {
        prefs.putAny(Constants.USER_PHONE, user.phone)
        prefs.putAny(Constants.USER_ID, user.id)
        prefs.putAny(Constants.USER_NAME, user.person?.name)
        prefs.putAny(Constants.USER_USERNAME, user.username)
        prefs.putAny(Constants.USER_EMAIL, user.email)
        prefs.putAny(Constants.USER_LASTNAME, user.person?.lastname)
        prefs.putAny(Constants.USER_ADDRESS, user.person?.address)
        prefs.putAny(Constants.USER_CODE, user.userCode)
        prefs.putAny(Constants.PROFILE_IMAGE_URL, user.person?.photoProfileUrl)
    }

    fun deleteUserLoginData(){
        prefs.putAny(Constants.USER_PHONE, Constants.EMPTY_STRING)
        prefs.putAny(Constants.USER_ID, Constants.EMPTY_STRING)
        prefs.putAny(Constants.USER_NAME, Constants.EMPTY_STRING)
        prefs.putAny(Constants.USER_USERNAME, Constants.EMPTY_STRING)
        prefs.putAny(Constants.USER_EMAIL, Constants.EMPTY_STRING)
        prefs.putAny(Constants.USER_LASTNAME, Constants.EMPTY_STRING)
        prefs.putAny(Constants.USER_ADDRESS, Constants.EMPTY_STRING)
        prefs.putAny(Constants.USER_CODE, Constants.EMPTY_STRING)
        prefs.putAny(Constants.PROFILE_IMAGE_URL, Constants.EMPTY_STRING)
    }
}