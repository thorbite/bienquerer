package com.jujuy.bienquerer.utils

import androidx.annotation.MainThread
import androidx.annotation.WorkerThread
import com.jujuy.bienquerer.api.network.Resource
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.*
import timber.log.Timber

abstract class NetworkBoundResource<DB, REMOTE> {

    @MainThread
    abstract fun fetchFromLocal(): Flow<DB>

    @MainThread
    abstract fun fetchFromRemote(): Flow<Resource<REMOTE>>

    @WorkerThread
    abstract fun saveRemoteData(data: REMOTE)

    @MainThread
    abstract fun shouldFetchFromRemote(data: DB): Boolean

    @ExperimentalCoroutinesApi
    fun asFlow() = flow<Resource<DB>> {

        Timber.d("----------------------")
        Timber.d("Starting...")


        val localData = fetchFromLocal().first()

        // checking if local data is staled
        if (shouldFetchFromRemote(localData)) {

            Timber.d("Fetching from remote")
            // need remote data
            fetchFromRemote()
                .collect { response ->
                    when (response.status) {

                        Resource.Status.LOADING -> {
                            Timber.d("Remote is loading")
                            emit(Resource.loading())
                        }

                        Resource.Status.SUCCESS -> {
                            Timber.d("Remote got data")
                            val data = response.data!!
                            saveRemoteData(data)

                            // start watching it
                            emitLocalDbData()
                        }


                        Resource.Status.ERROR -> {
                            Timber.e("Remote met with an error")
                            emit(Resource.error(response.message!!))
                        }
                    }
                }

        } else {
            Timber.d("Fetching from local")
            // valid cache, no need to fetch from remote.
            emitLocalDbData()
        }
    }

    @ExperimentalCoroutinesApi
    private suspend fun FlowCollector<Resource<DB>>.emitLocalDbData() {
        Timber.d("Sending local data to UI")
        // sending loading status
        emit(Resource.loading())

        emitAll(fetchFromLocal().map { dbData ->
            Timber.d("Sending local...")
            Resource.success(dbData)
        })
    }
}