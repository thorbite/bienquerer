package com.jujuy.bienquerer.utils

import android.content.Context
import androidx.core.content.ContextCompat
import androidx.lifecycle.LifecycleOwner
import com.jujuy.bienquerer.R
import com.skydoves.balloon.ArrowConstraints
import com.skydoves.balloon.ArrowOrientation
import com.skydoves.balloon.Balloon
import com.skydoves.balloon.BalloonAnimation

object BalloonUtils {

    fun getPhoneBalloon(context: Context, lifecycleOwner: LifecycleOwner, text: String): Balloon {
        return Balloon.Builder(context)
            .setArrowSize(10)
            .setArrowOrientation(ArrowOrientation.BOTTOM)
            .setArrowConstraints(ArrowConstraints.ALIGN_BALLOON)
            .setArrowPosition(0.5f)
            .setArrowVisible(true)
            .setWidthRatio(0.5f)
            .setHeight(65)
            .setTextSize(11f)
            .setCornerRadius(4f)
            .setAlpha(0.9f)
            .setText(text)
            .setTextColor(ContextCompat.getColor(context, R.color.white))
            .setDismissWhenClicked(true)
            .setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary))
            .setBalloonAnimation(BalloonAnimation.FADE)
            .setAutoDismissDuration(3000L)
            .setLifecycleOwner(lifecycleOwner)
            .build()
    }

    fun getDescriptionBalloon(context: Context, lifecycleOwner: LifecycleOwner): Balloon {
        return Balloon.Builder(context)
            .setArrowSize(10)
            .setArrowOrientation(ArrowOrientation.BOTTOM)
            .setArrowConstraints(ArrowConstraints.ALIGN_BALLOON)
            .setArrowPosition(0.5f)
            .setArrowVisible(true)
            .setWidthRatio(0.5f)
            .setHeight(65)
            .setTextSize(15f)
            .setCornerRadius(4f)
            .setAlpha(0.9f)
            .setText("Escribe aqui tu informacion adicional")
            .setTextColor(ContextCompat.getColor(context, R.color.white))
            .setDismissWhenClicked(true)
            .setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary))
            .setBalloonAnimation(BalloonAnimation.FADE)
            .setAutoDismissDuration(2500L)
            .setLifecycleOwner(lifecycleOwner)
            .build()
    }
}