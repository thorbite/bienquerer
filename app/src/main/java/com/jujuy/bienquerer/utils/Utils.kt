@file:Suppress("NAME_SHADOWING")

package com.jujuy.bienquerer.utils

import android.Manifest
import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.snackbar.Snackbar
import com.jujuy.bienquerer.MainActivity
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.base.NavigationController
import com.jujuy.bienquerer.extensions.putAny
import com.jujuy.bienquerer.extensions.remove
import com.jujuy.bienquerer.model.entity.User
import com.skydoves.whatif.whatIfNotNull
import kotlinx.coroutines.ExperimentalCoroutinesApi
import timber.log.Timber
import java.math.BigDecimal
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
object Utils {

    fun hideFirstFab(v: View) {
        v.visibility = View.GONE
        v.translationY = v.height.toFloat()
        v.alpha = 0f
    }

    fun twistFab(v: View, rotate: Boolean): Boolean {
        v.animate().setDuration(300)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                }
            })
            .rotation(if (rotate) 165f else 0f)
        return rotate
    }

    fun showFab(v: View) {
        v.visibility = View.VISIBLE
        v.alpha = 0f
        v.translationY = v.height.toFloat()
        v.animate()
            .setDuration(300)
            .translationY(0f)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                }
            })
            .alpha(1f)
            .start()
    }

    fun hideFab(v: View) {
        v.visibility = View.VISIBLE
        v.alpha = 1f
        v.translationY = 0f
        v.animate()
            .setDuration(300)
            .translationY(v.height.toFloat())
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    v.visibility = View.GONE
                    super.onAnimationEnd(animation)
                }
            }).alpha(0f)
            .start()
    }

    fun isStoragePermissionGranted(activity: Activity): Boolean {
        return if (Build.VERSION.SDK_INT >= 23) {
            if (activity.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED
            ) {
                Timber.d("Permission is granted")
                true
            } else {
                Timber.d("Permission is revoked")
                ActivityCompat.requestPermissions(
                    activity,
                    arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    1
                )
                false
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Timber.d("Permission is granted")
            true
        }
    }

    fun toggleUpDownWithAnimation(view: View): Boolean {
        return if (view.rotation == 0f) {
            view.animate().setDuration(150).rotation(180f)
            true
        } else {
            view.animate().setDuration(150).rotation(0f)
            false
        }
    }

    fun toggleUpDownWithAnimation(view: View, duration: Int, degree: Int) {

        view.animate().setDuration(duration.toLong()).rotation(degree.toFloat())

    }

    /**
     * Function to convert milliseconds time to
     * Timer Format
     * Hours:Minutes:Seconds
     */
    fun milliSecondsToTimer(milliseconds: Long): String {
        var finalTimerString = ""
        val secondsString: String

        // Convert total duration into time
        val hours = (milliseconds / (1000 * 60 * 60)).toInt()
        val minutes = (milliseconds % (1000 * 60 * 60)).toInt() / (1000 * 60)
        val seconds = (milliseconds % (1000 * 60 * 60) % (1000 * 60) / 1000).toInt()
        // Add hours if there
        if (hours > 0) {
            finalTimerString = "$hours:"
        }

        // Prepending 0 to seconds if it is one digit
        if (seconds < 10) {
            secondsString = "0$seconds"
        } else {
            secondsString = "" + seconds
        }

        finalTimerString = "$finalTimerString$minutes:$secondsString"

        // return timer string
        return finalTimerString
    }

    /**
     * Function to get Progress percentage
     */
    fun getProgressPercentage(currentDuration: Long, totalDuration: Long): Int {
        val percentage: Double?

        val currentSeconds = (currentDuration / 1000).toInt().toLong()
        val totalSeconds = (totalDuration / 1000).toInt().toLong()

        // calculating percentage
        percentage = currentSeconds.toDouble() / totalSeconds * 100

        // return percentage
        return percentage.toInt()
    }

    /**
     * Function to change progress to timer
     *
     * @param progress      -
     * @param totalDuration returns current duration in milliseconds
     */
    fun progressToTimer(progress: Int, totalDuration: Int): Int {
        var totalDuration = totalDuration
        val currentDuration: Int
        totalDuration = totalDuration / 1000
        currentDuration = (progress.toDouble() / 100 * totalDuration).toInt()

        // return current duration in milliseconds
        return currentDuration * 1000
    }

    fun getDrawableInt(context: Context?, name: String?): Int {
        return context?.resources!!.getIdentifier(name, "drawable", context.packageName)
    }

    fun getResourceInt(context: Context?, defType: String, name: String): Int {
        return context?.resources!!.getIdentifier(name, defType, context.packageName)
    }

    fun setImageToImageView(context: Context, imageView: ImageView, drawable: Int) {
        val requestOptions = RequestOptions()
            .diskCacheStrategy(DiskCacheStrategy.NONE) // because file name is always same
            .skipMemoryCache(true)

        Glide.with(context)
            .load(drawable)
            .apply(requestOptions)
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(imageView)
    }

    fun roundDouble(d: Double, decimalPlace: Int): Float {
        var bd = BigDecimal(d.toString())
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP)
        return bd.toFloat()
    }

    fun hideKeyboard(activity: Activity) {
        try {
            val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            if (imm != null) {
                if (activity.currentFocus != null) {
                    imm.hideSoftInputFromWindow(activity.currentFocus!!.windowToken, 0)
                }
            }
        } catch (e: java.lang.Exception) {
            TODO()
        }
    }

    fun addToolbarScrollFlag(toolbar: Toolbar) {
        val params = toolbar.layoutParams as AppBarLayout.LayoutParams
        params.scrollFlags = (AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL
                or AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS)
    }

    fun removeToolbarScrollFlag(toolbar: Toolbar) {
        val params = toolbar.layoutParams as AppBarLayout.LayoutParams
        params.scrollFlags = 0
    }
    interface DialogInteractionListener {
        fun onPositiveClick(msg: String?)
    }

    fun showDialog(context: Context?, msg: String?, listener: DialogInteractionListener?) {
        val builder: AlertDialog.Builder = AlertDialog.Builder(context)
        builder.setMessage(msg).setPositiveButton(android.R.string.yes) { dialog, _ ->
                listener?.onPositiveClick(null)
                dialog.dismiss()
            }
            .setIcon(android.R.drawable.ic_dialog_alert)
            .setCancelable(false)
            .show()
    }

    fun navigateAfterRegister(activity: Activity, navigationController: NavigationController) {
        if (activity is MainActivity) {
            navigationController.navigateToUserRegister(activity as MainActivity)
        } else {
            navigationController.navigateToUserRegisterActivity(activity)
            /*try {
                activity.finish()
            } catch (e: Exception) {
                Timber.e(e)
            }*/
        }
    }

    fun navigateAfterForgotPassword(activity: Activity, navigationController: NavigationController) {
        if (activity is MainActivity) {
            navigationController.navigateToUserForgotPassword(activity as MainActivity)
        } else {
            navigationController.navigateToUserForgotPasswordActivity(activity)
            try {
                activity.whatIfNotNull { activity.finish() }
            } catch (e: Exception) {
                Timber.e(e)
            }
        }
    }

    fun navigateAfterUserLogin(activity: Activity, navigationController: NavigationController) {
        if (activity is MainActivity) {
            activity.setToolbarText(
                activity.binding.toolbarMain,
                activity.getString(R.string.profile_title)
            )
            activity.refreshUserData()
            navigationController.navigateToUserProfile(activity)
        } else {
            activity.let {
                navigationController.navigateToMainActivity(it)
                it.finish()
            }
        }
    }

    fun navigateToLogin(activity: Activity, navigationController: NavigationController) {
        if (activity is MainActivity) {
            navigationController.navigateToUserLogin(activity)
        } else {
            activity.whatIfNotNull {
                navigationController.navigateToUserLoginActivity(it)
                it.finish()
            }
        }
    }

    fun navigateAfterUserRegister(activity: Activity, navigationController: NavigationController) {
        if (activity is MainActivity) {
            activity.setToolbarText(
                activity.binding.toolbarMain,
                activity.getString(R.string.verify_email)
            )
            //navigationController.navigateToVerifyEmail(activity)
            navigationController.navigateToUserLogin(activity)
        } else {
            //navigationController.navigateToVerifyEmailActivity(activity)
            navigationController.navigateToUserLoginActivity(activity)
            activity.finish()
        }
    }

    fun navigateOnUserVerificationActivity(userIdToVerify: String, loginUserId: String, psDialogMsg: PSDialogMsg, activity: Activity,
        navigationController: NavigationController, callback: NavigateOnUserVerificationActivityCallback
    ) {
        if (userIdToVerify.isEmpty()) {
            if (loginUserId == "") {
                psDialogMsg.showInfoDialog(
                    activity.getString(R.string.error_message__login_first),
                    activity.getString(R.string.app__ok)
                )
                psDialogMsg.show()
                psDialogMsg.okButton.setOnClickListener {
                    psDialogMsg.cancel()
                    navigationController.navigateToUserLoginActivity(activity)
                }
            } else {
                callback.onSuccess()
            }
        }
    }

    interface NavigateOnUserVerificationActivityCallback {
        fun onSuccess()
    }

    fun registerUserLoginData(pref: SharedPreferences, user: User, password: String) {
        addUserLoginData(pref, user, password)
    }

    fun updateUserLoginData(pref: SharedPreferences, user: User) {
        addUserLoginData(pref, user, user.password)
    }

    private fun addUserLoginData(pref: SharedPreferences, user: User, password: String?) {
        pref.putAny(Constants.USER_PHONE, user.phone)
        pref.putAny(Constants.USER_ID, user.id)
        pref.putAny(Constants.USER_PASSWORD, user.userCode)
        pref.putAny(Constants.USER_NAME, user.person?.name)
        pref.putAny(Constants.USER_USERNAME, user.username)
        pref.putAny(Constants.USER_EMAIL, user.email)
        pref.putAny(Constants.USER_LASTNAME, user.person?.lastname)
        pref.putAny(Constants.USER_ADDRESS, user.person?.address)
        pref.putAny(Constants.USER_PASSWORD, password)
        pref.putAny(Constants.USER_CODE, user.userCode)
        pref.putAny(Constants.IS_PROFESSIONAL, user.phoneVerified ?: false)
        pref.putAny(Constants.PROFILE_IMAGE_URL, user.person?.photoProfileUrl)
    }

    fun deleteUserLoginData(prefs: SharedPreferences){
        prefs.remove(Constants.USER_PHONE)
        prefs.remove(Constants.USER_ID)
        prefs.remove(Constants.USER_NAME)
        prefs.remove(Constants.USER_USERNAME)
        prefs.remove(Constants.USER_EMAIL)
        prefs.remove(Constants.USER_LASTNAME)
        prefs.remove(Constants.USER_ADDRESS)
        prefs.remove(Constants.USER_CODE)
        prefs.remove(Constants.PROFILE_IMAGE_URL)
    }


    fun toggleUporDown(v: View): Boolean {
        return if (v.rotation == 0f) {
            v.animate().setDuration(150).rotation(180f)
            true
        } else {
            v.animate().setDuration(150).rotation(0f)
            false
        }
    }

    enum class LoadingDirection {
        top, bottom, none
    }

    fun callPhone(fragment: Fragment, phoneNo: String) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            // Older Version No need to request Permission
            val dial = "tel:$phoneNo"
            fragment.startActivity(Intent(Intent.ACTION_CALL, Uri.parse(dial)))
        } else {
            // Need to request Permission
            if (fragment.activity != null) {
                if (ContextCompat.checkSelfPermission(
                        fragment.requireActivity(),
                        Manifest.permission.CALL_PHONE
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    fragment.requestPermissions(
                        arrayOf(
                            Manifest.permission.CALL_PHONE
                        ), Constants.REQUEST_CODE__PHONE_CALL_PERMISSION
                    )
                } else {
                    val dial = "tel:$phoneNo"
                    fragment.startActivity(Intent(Intent.ACTION_CALL, Uri.parse(dial)))
                }
            }
        }
    }

    fun phoneCallPermissionResult(
        requestCode: Int, grantResults: IntArray,
        fragment: Fragment, phoneNo: String) {
        if (requestCode == Constants.REQUEST_CODE__PHONE_CALL_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                callPhone(fragment, phoneNo)
            } else {
                Timber.e("Permission not Granted")
            }
        }
    }

    fun openEmail(fragment: Fragment, email: String) {
        val intent = Intent(Intent.ACTION_SENDTO)
        intent.data = Uri.parse("mailto:")
        intent.putExtra(Intent.EXTRA_EMAIL, arrayOf(email))
        if (intent.resolveActivity(fragment.requireActivity().packageManager) != null) {
            fragment.startActivity(intent)
        }
    }

    fun generateKeyForChatHeadId(senderId: String, receiverId: String): String {
        return if (senderId < receiverId) {
            senderId + "_" + receiverId
        } else  {
            receiverId + "_" + senderId
        }
    }

    fun showSnackBarSuccess(context: FragmentActivity, message: String){
        val snack = Snackbar.make(context.findViewById(android.R.id.content), message, Snackbar.LENGTH_SHORT)
        snack.setAction("OK", View.OnClickListener {
            // executed when DISMISS is clicked
        })

        // change action button text color
        snack.setActionTextColor(Color.WHITE)

        // snackbar background color
        snack.view.setBackgroundColor(ContextCompat.getColor(context,R.color.md_green_800))

        val textView = snack.view.findViewById(R.id.snackbar_text) as TextView
        // change Snackbar text color
        textView.setTextColor(Color.WHITE)

        snack.show()
    }
    fun showSnackBarError(context: FragmentActivity, message: String){
        val snack = Snackbar.make(context.findViewById(android.R.id.content), message, Snackbar.LENGTH_SHORT)
        snack.setAction("OK", View.OnClickListener {
            // executed when DISMISS is clicked
        })

        // change action button text color
        snack.setActionTextColor(Color.WHITE)

        // snackbar background color
        snack.view.setBackgroundColor(ContextCompat.getColor(context,R.color.md_red_900))

        val textView = snack.view.findViewById(R.id.snackbar_text) as TextView
        // change Snackbar text color
        textView.setTextColor(Color.WHITE)

        snack.show()
    }

    @SuppressLint("SimpleDateFormat")
    fun toMilliseconds(date: String): Long {
        val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
        try {
            val mDate: Date = sdf.parse(date)
            val timeInMilliseconds: Long = mDate.time
            println("Date in milli :: $timeInMilliseconds")
            return timeInMilliseconds
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return 0
    }

    fun ParseDate(fecha: String): String {
        val actual = Calendar.getInstance().time
        var texto = ""
        try {
            if (fecha != null) {
                val obtenida = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").parse(fecha)

                texto = SimpleDateFormat("dd-MM-yyyy").format(obtenida).toString()

                return texto
            }
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return texto
    }

    fun parseDateCorrectForm(fecha: String): String {
        val actual = Calendar.getInstance().time
        var texto = ""
        try {
            if (fecha != null) {
                val obtenida = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").parse(fecha)

                texto = SimpleDateFormat("dd-MM-yyyy").format(obtenida).toString()

                return texto
            }
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return texto
    }

    fun messageSentDateProper(sentDate: String): String? {
        var properDate = ""
        val cal = Calendar.getInstance()
        val todayDate = Date()
        cal.time = todayDate
        val date = sentDate.split(" ".toRegex()).toTypedArray()
        val todayMonth = cal[Calendar.MONTH] + 1
        val todayDay = cal[Calendar.DAY_OF_MONTH]
        properDate = if (todayMonth == date[1].toInt() && todayDay == date[0].toInt()) {
            "Hoy" + " " + date[3] + " " + date[4]
            // 06 11 17 12:28 AM
        } else if (todayMonth == date[1].toInt() && todayDay - 1 == date[0].toInt()) {
            "Ayer" + " " + date[3] + " " + date[4]
        } else {
            date[0] + " " + toCharacterMonth(
                date[1].toInt()
            ) + " " + date[2] + " " + date[3] + " " + date[4]
        }
        return properDate
    }

    fun toCharacterMonth(month: Int): String? {
        return if (month == 1) "Ene" else if (month == 2) "Feb" else if (month == 3) "Mar" else if (month == 4) "Abr" else if (month == 5) "May" else if (month == 6) "Jun" else if (month == 7) "Jul" else if (month == 8) "Ago" else if (month == 9) "Sep" else if (month == 10) "Oct" else if (month == 11) "Nov" else "Dic"
    }

    fun getDateCurrentTimeZone(timestamp: Long?): Date? {
        try {
            val cal = Calendar.getInstance()
            if (timestamp != null) {
                cal.timeInMillis = timestamp
            }
            return cal.time as Date
        } catch (e: java.lang.Exception) {
        }
        return null
    }

    fun getDateString(netDate: Date?, pattern: String?): String {
        return try {
            val sdf = SimpleDateFormat(pattern)
            sdf.format(netDate)
        } catch (ex: java.lang.Exception) {
            "error"
        }
    }

}
