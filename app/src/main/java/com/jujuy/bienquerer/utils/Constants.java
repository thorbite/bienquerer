package com.jujuy.bienquerer.utils;

public interface Constants {

    //region General

    String EMPTY_STRING = "";
    String SPACE_STRING = " ";
    String LAT_STRING = "-24.1896462";
    String LNG_STRING = "-65.3050643";
    String ZERO = "0";
    String ONE = "1";
    String TWO = "2";
    String THREE = "3";
    String FOUR = "4";
    String FIVE = "5";

    String NO_DATA = "NO_DATA";
    String DASH = "-";

    //endregion

    //chat

    String CHAT_TO_SELLER = "to_seller";//don't change
    String CHAT_TO_BUYER = "to_buyer";//don't change
    String CHAT_TYPE_SELLER = "seller";//don't change
    String CHAT_TYPE_BUYER = "buyer";//don't change
    String CHAT_OFFER_STATUS = "offerStatus";//don't change
    String CHAT_IS_SOLD = "isSold";//don't change
    String CHAT_FROM_BUYER = "CHAT_FROM_BUYER";
    String CHAT_FROM_SELLER = "CHAT_FROM_SELLER";
    String CHAT_FLAG = "CHAT_FLAG";

    int CHAT_DATE_UI = 0; //don't change

    int CHAT_SENDER_UI = 1; //don't change
    int CHAT_SENDER_IMAGE_UI = 2; //don't change

    int CHAT_RECEIVER_UI = 3; //don't change
    int CHAT_RECEIVER_IMAGE_UI = 4; //don't change

    int CHAT_TYPE_TEXT = 0;
    int CHAT_TYPE_IMAGE = 1;
    int CHAT_TYPE_DATE = 3;

    // region City
    String CITY_START_DATE = "CITY_START_DATE";
    String CITY_END_DATE = "CITY_END_DATE";


    //region REQUEST CODE AND RESULT CODE

     int PLACE_PICKER_REQUEST = 100;
     String ADDRESS_INTENT = "ADDRESS_INTENT";
    int REQUEST_CODE__NOTIFICATION_LIST_FRAGMENT = 1002;
    int REQUEST_CODE__ITEM_LIST_FRAGMENT = 1003;
    int REQUEST_CODE__MAP_FILTERING = 1007;
    int REQUEST_CODE__SELECTED_CITY_FRAGMENT = 1008;

    int REQUEST_CODE__PHONE_CALL_PERMISSION = 2030;

    int RESULT_LOAD_IMAGE_PROFILE = 1;
    int RESULT_LOAD_IMAGE_BANNER = 2;
    int RESULT_OK = -1;

    int RESULT_CODE__REFRESH_NOTIFICATION = 2003;
    int RESULT_CODE__SPECIAL_FILTER = 2004;
    int RESULT_CODE__CATEGORY_FILTER = 2005;
    int RESULT_CODE__MAP_FILTERING = 2008;
    int RESULT_CODE__IMAGE_CATEGORY = 2013;
    int RESULT_CODE__FROM_MAP_VIEW = 2016;
    int RESULT_CODE__TO_MAP_VIEW = 2017;

    //endregion

    //region Platform

    String PLATFORM = "android"; // Please don't change!

    //map
    String MAP_MILES = "8";//cannot change
    String MAP_PICK = "MAP_PICK";
    String MAP = "MAP";
    String MAP_FLAG = "MAP_FLAG";

    //endregion

    //region User
    String FACEBOOK_ID = "FACEBOOK_ID";
    String PHONE_ID = "PHONE_ID";
    String GOOGLE_ID = "GOOGLE_ID";
    String USER_ID = "USER_ID";
    String OTHER_USER_ID = "OTHER_USER_ID";
    String OTHER_USER_NAME = "OTHER_USER_NAME";
    String USER_NAME = "USER_NAME";
    String USER_USERNAME = "USER_USERNAME";
    String USER_PHONE = "USER_PHONE";
    String USER_EMAIL = "USER_EMAIL";
    String USER_LASTNAME = "USER_LASTNAME";
    String USER_ADDRESS = "USER_ADDRESS";
    String TOKEN_SERVER = "TOKEN_SERVER";
    String USER_NO_USER = "nologinuser"; // Don't Change
    String USER_NO_DEVICE_TOKEN = "nodevicetoken"; // Don't Change
    String USER_PASSWORD = "password";
    String RECEIVE_USER_ID = "RECEIVE_USER_ID";
    String RECEIVE_USER_NAME = "RECEIVE_USER_NAME";
    String RECEIVE_USER_LAST_NAME = "RECEIVE_USER_LAST_NAME";
    String RECEIVE_USER_IMG_URL = "RECEIVE_USER_IMG_URL";
    String USER_EMAIL_TO_VERIFY = "USER_EMAIL_TO_VERIFY";
    String USER_PASSWORD_TO_VERIFY = "USER_PASSWORD_TO_VERIFY";
    String USER_NAME_TO_VERIFY = "USER_NAME_TO_VERIFY";
    String USER_ID_TO_VERIFY = "USER_ID_TO_VERIFY";
    String USER_LASTNAME_TO_VERIFY = "USER_LASTNAME_TO_VERIFY";
    String USER_USERNAME_TO_VERIFY = "USER_USERNAME_TO_VERIFY";
    String USER_PHONE_TO_VERIFY = "USER_PHONE_TO_VERIFY";
    String USER_ADDRESS_TO_VERIFY = "USER_ADDRESS_TO_VERIFY";

    String USER_PARAM_HOLDER_KEY = "USER_PARAM_HOLDER_KEY";

    String IS_PROFESSIONAL = "IS_PROFESSIONAL";


    //endregion

    String ITEM_PARAM_HOLDER_KEY = "ITEM_PARAM_HOLDER_KEY";

    String ITEM_NAME = "ITEM_NAME";
    String ITEM_COUNT = "ITEM_COUNT";
    String ITEM_TAG = "ITEM_TAG";
    String ITEM_ID = "ITEM_id";
    String ITEM_HOLDER = "ITEM_HOLDER";
    String ITEM_USER_ID = "ITEM_USER_ID";
    String ADD_NEW_ITEM = "ADD_NEW_ITEM";

    //region Filtering Don't Change

    String FILTERING_FILTER_NAME = "name"; // Don't Change
    String FILTERING_TYPE_FILTER = "tf"; // Don't Change
    String FILTERING_SPECIAL_FILTER = "sf"; // Don't Change
    String FILTERING_TYPE_NAME = "item"; // Don't Change
    String FILTERING_INACTIVE = ""; // Don't Change
    String FILTERING_TRENDING = "touch_count"; // Don't Change
    String FILTERING_FEATURE = "featured_date"; // Don't Change
    String FILTERING_ASC = "asc"; // Don't Change
    String FILTERING_DESC = "desc"; // Don't Change
    String FILTERING_ADDED_DATE = "added_date"; // Don't Change
    String FILTERING_HOLDER = "filter_holder"; // Don't Change
    String FILTERING_NAME = "title"; // Don't Change

    //endregion

    //region Category

    String CATEGORY_NAME = "CATEGORY_NAME";
    String CATEGORY_ID = "CATEGORY_ID";
    String CATEGORY = "CATEGORY";
    String CATEGORY_ALL = "TODO";
    String CATEGORY_FLAG = "CATEGORY_FLAG";

    //endregion

    //region SubCategory
    String SUBCATEGORY_ID = "SUBCATEGORY_ID";
    String SUBCATEGORY = "SUBCATEGORY";
    String SUBCATEGORY_NAME = "SUBCATEGORY_NAME";

    String SELECTED_LOCATION_ID = "SELECTED_LOCATION_ID";
    String SELECTED_LOCATION_NAME = "SELECTED_LOCATION_NAME";

    //region Language

    String LANGUAGE_CODE = "Language";
    String LANGUAGE_COUNTRY_CODE ="Language_Country_Code";

    //endregion

    //regionHistory

    String HISTORY_FLAG = "history_flag";

    //endregion


    //region Noti

    String NOTI_NEW_ID = "NOTI_NEW_ID";
    String NOTI_ID = "NOTI_ID";
    String NOTI_TOKEN = "NOTI_TOKEN";
    String NOTI_EXISTS_TO_SHOW = "IS_NOTI_EXISTS_TO_SHOW";
    String NOTI_MSG = "NOTI_MSG";
    String NOTI_SETTING = "NOTI_SETTING";
    String NOTI_HEADER_ID = "NOTI_HEADER_ID";
    String NOTI_ITEM_ID = "NOTI_ITEM_ID";
    String NOTI_BUYER_ID = "NOTI_BUYER_ID";
    String NOTI_SELLER_ID = "NOTI_SELLER_ID";
    String NOTI_SENDER_NAME = "NOTI_SENDER_NAME";
    String NOTI_SENDER_URL = "NOTI_SENDER_URL";
    String C_NOTI_ID = "C_NOTI_ID";


    //endregion


    //region FB Register

    String FB_FIELDS = "fields"; // Don't Change
    String FB_EMAILNAMEID = "email,name,id"; // Don't Change
    String FB_NAME_KEY = "name"; // Don't Change
    String FB_EMAIL_KEK = "email"; // Don't Change
    String FB_ID_KEY = "id"; // Don't Change

    //endregion

    //region Email Type

    String EMAIL_TYPE = "plain/text"; // Don't Change
    String HTTP = "http://"; // Don't Change

    //endregion

    String LAT = "lat";

    String LNG = "lng";

    String PROFESSIONAL = "professional";
    String DATA_PROFESSION = "data";
    String TITLE_PROFESSION = "title";
    String SELECTED_ITEM = "selected";
    String PROFILE_IMAGE_URL = "PROFILE_IMAGE_URL";
    String USER_CODE= "USER_CODE";
    String JOB = "JOB";
    String APPOINTMENT_ID = "APPOINTMENT_ID";
}

