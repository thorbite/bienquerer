package com.jujuy.bienquerer.model.network.request

import com.google.gson.annotations.SerializedName

data class GoogleRequest(
    @SerializedName("googleId")
    var id: String?,

    @SerializedName("googleEmail")
    var email: String?,

    @SerializedName("googleFirstName")
    var name: String?,

    @SerializedName("googleLastNameName")
    var lastName: String?,

    @SerializedName("photoUrl")
    var photoUrl: String?,

    @SerializedName("fcmKey")
    var fcmKey: String?
)