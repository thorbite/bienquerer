package com.jujuy.bienquerer.model.entity

import android.os.Parcelable
import androidx.room.Entity
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(primaryKeys = [("id")])
data class Provider(
    //val catId: String,
    val id: String,
    /*val latitud: String,
    val longitud: String,
    val phone: String,
    val username: String,
    val email: String,
    val website: String,
    val avatar: String,
    val fullName: String,
    val nickname:String,
    val subCatId: List<String>,
    val companyName: String,
    val facebook: String,
    val address: String,
    val country: String,
    val city: String,
    val description: String,*/
    val firstName: String,
    val lastName: String,
    val qualification: String,
    val profilePhoto: String
    //val profileBanner: String
): Parcelable
