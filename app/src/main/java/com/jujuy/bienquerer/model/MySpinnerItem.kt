package com.jujuy.bienquerer.model

import android.graphics.drawable.Drawable

class MySpinnerItem (
    val id: String,
    val icon: Drawable?,
    val text: CharSequence,
    var url: String? = ""
)