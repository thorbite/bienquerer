package com.jujuy.bienquerer.model.entity

import android.os.Parcelable
import androidx.room.Entity
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(primaryKeys = ["id"])
data class Notification(
    val id: String,
    val name: String? ="",
    val title: String? ="",
    val body: String? ="",
    val icon: String? ="",
    val createAt: String? ="",
    var type: String? ="",
    val isRead: String? = ""
): Parcelable