package com.jujuy.bienquerer.model.entity

import androidx.room.Entity

@Entity(primaryKeys = ["id"])
data class ItemMap (
    val id: String,
    val mapkey: String,
    val itemId: String,
    val sorting: Int,
    val addedDate: String?
)