package com.jujuy.bienquerer.model.network.request

import com.google.gson.annotations.SerializedName

data class FacebookRequest (
    @SerializedName("id") val id : String? = "",
    @SerializedName("first_name") val first_name : String? = "",
    @SerializedName("last_name") val last_name : String? = "",
    @SerializedName("email") val email : String? = "",
    @SerializedName("photoUrl") val photoUrl : String? = "",
    @SerializedName("fcmKey") val fcmKey : String? = ""
)