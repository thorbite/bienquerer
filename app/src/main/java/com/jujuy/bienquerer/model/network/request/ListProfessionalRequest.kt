package com.jujuy.bienquerer.model.network.request

import com.google.gson.annotations.SerializedName

data class ListProfessionalRequest (
    @SerializedName("categoryId")
    var categoryId: String?,

    @SerializedName("professionId")
    var professionId: String?,

    @SerializedName("name")
    var name: String? = null,

    @SerializedName("lastname")
    var lastname: String? = null



//{"name":"hector","lastname":null,"categoryId":1,"professionId":null}
)