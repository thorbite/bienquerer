package com.jujuy.bienquerer.model.network.response

import com.google.gson.annotations.SerializedName

data class LoginResponse (
    @SerializedName("code")
    var statusCode: Int? = null,

    @SerializedName("token")
    var authToken: String?,

    @SerializedName("message")
    var message: String? = null
)