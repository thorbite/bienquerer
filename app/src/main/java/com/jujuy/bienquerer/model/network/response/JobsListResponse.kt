package com.jujuy.bienquerer.model.network.response

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import com.jujuy.bienquerer.model.entity.SubCategory
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
data class JobsListResponse (
    @SerializedName("list") val list : List<Job>,
    @SerializedName("current_page") val current_page : Int,
    @SerializedName("page_size") val page_size : Int,
    @SerializedName("total_results") val total_results : Int,
    @SerializedName("sort_field") val sort_field : String,
    @SerializedName("sort_direction") val sort_direction : String
)
@ExperimentalCoroutinesApi
@ExperimentalTime
@Entity
data class Job (
    @PrimaryKey @SerializedName("id") val id : Int,
    @SerializedName("requestDate") val requestDate : String,
    @SerializedName("createdAt") val createdAt : String,
    @SerializedName("updatedAt") val updatedAt : String,
    @SerializedName("comment") val comment : String? = "",
    @SerializedName("commentCacelled") val commentCacelled : String? ="",
    @SerializedName("commentRejected") val commentRejected : String? = "",
    @SerializedName("client") val client : Client = Client(),
    @SerializedName("professional") val professional : Client = Client(),
    @SerializedName("status") val status : Status = Status(),
    @SerializedName("profession") val profession : SubCategory? = null
){
    /*fun toListString(): String {
        val title = "<h2><b>Informacion del trabajo</b></h2> <br></br>"
        val data =
            " <strong>Profesional : </strong> <span>" + professional?.name + " " + professional?.lastname + "</span> <br></br><br></br>" +
                    " <strong>Fecha : </strong> <span>" + Utils.ParseDataTime(requestDate) + "</span><br></br> <br></br>" +
                    " <strong>Servicio : </strong> <span>${profession?.nameSubCategory ?: "No especificado" }</span><br></br> <br></br>" +
                    " <strong>Estado : </strong> <span>" + status?.name + "</span> <br></br><br></br>" +
                    //" <strong>Reason : </strong> <span>" + status + "</span> <br></br><br></br>" +
                    " <strong>Description : </strong> <span>${comment ?: "No especificado"}</span> <br></br>"
        return Html.fromHtml(title + data).toString()
    }*/
}
