package com.jujuy.bienquerer.model.network.response

import com.google.gson.annotations.SerializedName
import com.jujuy.bienquerer.model.entity.Professional

data class ListProfessional (
    @SerializedName("list") val list : List<Professional>? = emptyList(),
    @SerializedName("current_page") val current_page : Int,
    @SerializedName("page_size") val page_size : Int,
    @SerializedName("total_results") val total_results : Int
)