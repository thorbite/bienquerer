package com.jujuy.bienquerer.model.entity

import android.os.Parcelable
import androidx.room.Embedded
import com.google.gson.annotations.SerializedName

import kotlinx.android.parcel.Parcelize

@Parcelize
data class Department (
    @SerializedName("id")
    val idDepartment : Int,

    @SerializedName("name")
    val nameDepartment : String,

    @SerializedName("province")
    @Embedded
    var provinceDepartment : Province? = null
): Parcelable