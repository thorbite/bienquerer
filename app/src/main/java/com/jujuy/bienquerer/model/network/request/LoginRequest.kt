package com.jujuy.bienquerer.model.network.request

import com.google.gson.annotations.SerializedName

data class LoginRequest (
    @SerializedName("username")
    var email: String,

    @SerializedName("password")
    var password: String,

    @SerializedName("fcmKey")
    var fcmKey: String
)