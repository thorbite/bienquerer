package com.jujuy.bienquerer.model.network.response

import com.google.gson.annotations.SerializedName

data class ResponseNotificationCount(
    @SerializedName("code") val code : Int?,
    @SerializedName("status") val status : String?,
    @SerializedName("message") val message : String?,
    @SerializedName("data") val data : String? = ""
)