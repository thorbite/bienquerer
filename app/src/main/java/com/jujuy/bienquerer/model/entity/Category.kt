package com.jujuy.bienquerer.model.entity

import android.os.Parcelable
import androidx.room.Entity
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(primaryKeys = [("idCategory")])
data class Category(

    val sorting : Int?,

    @SerializedName("id")
    val idCategory: Int = 0,

    @SerializedName("name")
    val nameCategory: String,

    @SerializedName("created_at")
    val created_atCategory: String?,

    @SerializedName("updated_at")
    var updated_atCategory: String?,

    @SerializedName("image_url")
    val imageUrl: String?,

    @SerializedName("image_background_url")
    val imageBackgroundUrl: String?


) : Parcelable/*{
    constructor() : this(0, "", "","","","",0)
}*/