package com.jujuy.bienquerer.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "searchRecents")
data class SearchRecent (
   @PrimaryKey val searchText: String
)
