package com.jujuy.bienquerer.model.entity

import android.os.Parcelable
import androidx.room.Embedded
import androidx.room.Entity
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(primaryKeys = ["userId"])
data class UserLogin(
    val userId: String= "",
    val login: Boolean = false,
    @Embedded(prefix = "user_")
    val user: User = User(),
    val message: String? = ""
): Parcelable