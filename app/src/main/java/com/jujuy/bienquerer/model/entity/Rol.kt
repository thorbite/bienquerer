package com.jujuy.bienquerer.model.entity

import android.os.Parcelable
import androidx.room.Entity
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(primaryKeys = ["id"])
data class Rol (
    @SerializedName("id") val id : Int = 0,
    @SerializedName("name") val name : String? ="",
    @SerializedName("description") val description : String? =""
): Parcelable