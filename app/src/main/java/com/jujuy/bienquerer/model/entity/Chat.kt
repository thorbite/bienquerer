package com.jujuy.bienquerer.model.entity

import com.jujuy.bienquerer.model.Message
import java.util.*

data class Chat (
    var id: String? = "",
    var updateAt: Date? = null,
    var senderId: String? = "",
    var receiverId: String? = "",
    var isSeen: Boolean = false,
    var lastMessage: Message? = Message(),
    var dateString: String? = "",
    var time: String? = "",
    var imageProfile: String? = null,
    var name: String? = null
)