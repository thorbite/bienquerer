package com.jujuy.bienquerer.model.network.request

data class MapListRequest(
    var lat: String ="",
    var long: String ="",
    var distance: String ="",
    var categoryId: String ="",
    var professionId: String? = null
)
