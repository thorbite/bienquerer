package com.jujuy.bienquerer.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class ItemFavorite (
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    val professionalId: String,
    val sorting: Int
)