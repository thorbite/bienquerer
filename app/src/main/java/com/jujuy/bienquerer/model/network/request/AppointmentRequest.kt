package com.jujuy.bienquerer.model.network.request

data class AppointmentRequest (
    var professionalId: String? ="",
    var professionId: String? ="",
    var requestedDate: String? ="",
    var comment: String? =""
)