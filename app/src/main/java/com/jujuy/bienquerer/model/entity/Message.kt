package com.jujuy.bienquerer.model.entity

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.Ignore
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
@Entity(primaryKeys = ["senderId"])
data class Message (
    var addedDate: Long? = null,
    var id: String = "",
    var sessionId: String = "",
    var itemId: String = "",
    var type : Int = 0,
    var sendByUserId: String = "",

    var senderId: String = "",
    var receiverId: String? = "",
    var message: String? ="",
    var isSeen: Boolean = false,
    var messageId: String? = "",
    var url: String? = "",
    @Ignore
    var date: Date? = null,
    @Ignore
    var dateString: String? = "",
    @Ignore
    var time: String? = ""
): Parcelable
