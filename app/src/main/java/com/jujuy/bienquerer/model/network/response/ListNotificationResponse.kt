package com.jujuy.bienquerer.model.network.response

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import com.jujuy.bienquerer.model.entity.Notification

data class DataResponse (
    @SerializedName("list") val list : List<ListNotifications>?,
    @SerializedName("current_page") val current_page : Int,
    @SerializedName("page_size") val page_size : Int,
    @SerializedName("total_results") val total_results : Int,
    @SerializedName("sort_field") val sort_field : String,
    @SerializedName("sort_direction") val sort_direction : String
)
@Entity
data class ListNotifications (
    @PrimaryKey @field:SerializedName("id") val id : Int,
    @field:SerializedName("nroGroup") val nroGroup : String? = "",
    @field:SerializedName("isNew") val isNew : Boolean? = false,
    @field:SerializedName("createdAt") val createdAt : String? = "",
    @field:SerializedName("title") val title : String? = "",
    @field:SerializedName("body") val body : String? = "",
    @field:SerializedName("icon") val icon : String? = "",
    @field:SerializedName("type") val type : String? = "",

)
