package com.jujuy.bienquerer.model.network.response

import com.google.gson.annotations.SerializedName
import com.jujuy.bienquerer.model.entity.SubCategory

data class ProfessionListAllResponse(
    @SerializedName("data") val data : ProfessionList?,
    @SerializedName("code") val code : Int?,
    @SerializedName("status") val status : String?,
    @SerializedName("message") val message : String?
)

data class ProfessionList (

    @SerializedName("list") val list : List<SubCategory>,
    @SerializedName("current_page") val current_page : Int,
    @SerializedName("page_size") val page_size : Int,
    @SerializedName("total_results") val total_results : Int,
    @SerializedName("sort_field") val sort_field : String,
    @SerializedName("sort_direction") val sort_direction : String
)