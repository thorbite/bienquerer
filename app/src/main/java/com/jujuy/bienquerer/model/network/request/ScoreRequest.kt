package com.jujuy.bienquerer.model.network.request

import com.google.gson.annotations.SerializedName

data class ScoreRequest (
    @SerializedName("jobId") val jobId: String,
    @SerializedName("ratings") val ratings: Ratings,
    @SerializedName("comment") val comment: String
)

data class Ratings (
    @SerializedName("puntuality_code") val puntualityCode: String,
    @SerializedName("result_code") val resultCode: String,
    @SerializedName("price_code") val priceCode: String
)
