package com.jujuy.bienquerer.model.network.response

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import com.jujuy.bienquerer.model.entity.SubCategory
import kotlinx.android.parcel.Parcelize
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
data class ListAppoinments (
    @SerializedName("list") val list : List<Appointment>,
    @SerializedName("current_page") val current_page : Int,
    @SerializedName("page_size") val page_size : Int,
    @SerializedName("total_results") val total_results : Int,
    @SerializedName("sort_field") val sort_field : String,
    @SerializedName("sort_direction") val sort_direction : String
)
@ExperimentalCoroutinesApi
@ExperimentalTime
@Entity
data class Appointment (
    @PrimaryKey @SerializedName("id") val id : Int,
    @SerializedName("requestDate") val requestDate : String,
    @SerializedName("createdAt") val createdAt : String,
    @SerializedName("updatedAt") val updatedAt : String,
    @SerializedName("comment") val comment : String? = "",
    @SerializedName("commentCacelled") val commentCacelled : String? ="",
    @SerializedName("commentRejected") val commentRejected : String? = "",
    @SerializedName("client") val client : Client = Client(),
    @SerializedName("professional") val professional : Client = Client(),
    @SerializedName("status") val status : Status = Status(),
    @SerializedName("profession") val profession : SubCategory? = null
)

@Parcelize
data class Client (
    @SerializedName("id") val id : Int? = 0,
    @SerializedName("name") val name : String? = "",
    @SerializedName("lastname") val lastname : String? = "",
    @SerializedName("email") val email : String? = "",
    @SerializedName("document") val document : String? = "",
    @SerializedName("photoProfileUrl") val photoProfileUrl : String? = "",
    @SerializedName("photoCoverUrl") val photoCoverUrl : String? = "",
    @SerializedName("latitude") val latitude : String? = "",
    @SerializedName("longitude") val longitude : String? = "",
    @SerializedName("address") val address : String? = "",
    @SerializedName("createdAt") val createdAt : String? = "",
    @SerializedName("updatedAt") val updatedAt : String? = "",
    @SerializedName("phone") val phone : String? = "",
    @SerializedName("available") val available : Boolean? = false,
    @SerializedName("social") val social : Boolean? = false
):Parcelable

@Entity
data class Status (
    @PrimaryKey @SerializedName("id") val id : Int? = 0,
    @SerializedName("name") val name : String? = "",
    @SerializedName("codeName") val codeName : String? = "",
    @SerializedName("description") val description : String? = ""
)