package com.jujuy.bienquerer.model.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize

data class Country (
    @SerializedName("id")
    val idCountry : Int,

    @SerializedName("name")
    val nameCountry : String
): Parcelable