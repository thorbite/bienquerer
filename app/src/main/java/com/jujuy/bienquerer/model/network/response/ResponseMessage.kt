package com.jujuy.bienquerer.model.network.response

data class ResponseMessage(
    val multicastId: Long,
    val success: Int,
    val failure: Int
)