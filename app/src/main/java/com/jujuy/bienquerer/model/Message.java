package com.jujuy.bienquerer.model;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Ignore;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.ServerValue;

import java.util.Date;

@Entity(primaryKeys = {"id"})
public class Message {

    public Long addedDate;

    @NonNull
    public String id;

    public String sessionId;

    public String receiverByUserId;

    public String message;

    public int type;

    public String sendByUserId;

    @Ignore
    public Date date;

    @Ignore
    public String dateString;

    @Ignore
    public String time;

    public java.util.Map<String, String> getAddedDate() {
        return ServerValue.TIMESTAMP;
    }

    @Exclude
    public Long getAddedDateLong() {
        return addedDate;
    }

    public void setAddedDate(Long addedDate) {
        this.addedDate = addedDate;
    }

    public Message(String sessionId, String receiverByUserId, String message, int type, String sendByUserId) {
        this.sessionId = sessionId;
        this.receiverByUserId = receiverByUserId;
        this.message = message;
        this.type = type;
        this.sendByUserId = sendByUserId;
    }

    @Ignore
    public Message() {

    }

}
