package com.jujuy.bienquerer.model

data class SocialNetworks (
    val id: Int = 0,
    val name: String = "",
    val description: String? = null,
    val base_url: String? = ""
)