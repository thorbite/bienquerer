package com.jujuy.bienquerer.model.entity

import android.os.Parcelable
import androidx.room.Entity
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(primaryKeys = ["id"])
data class Person (
    @SerializedName("id") val id : String = "",
    @SerializedName("name") val name : String? = "",
    @SerializedName("lastname") val lastname : String? = "",
    @SerializedName("email") val email : String? = "",
    @SerializedName("document") val document : String? = "",
    @SerializedName("photoProfileUrl") var photoProfileUrl : String? = "",
    @SerializedName("photoCoverUrl") val photoCoverUrl : String? = "",
    @SerializedName("latitude") val latitude : String? = "",
    @SerializedName("longitude") val longitude : String? = "",
    @SerializedName("address") val address : String? = "",
    @SerializedName("createdAt") val createdAt : String? = "",
    @SerializedName("available") val available : Boolean? = false,
    @SerializedName("updatedAt") val updatedAt : String? = "",
    @SerializedName("social") val social : Boolean? = false
): Parcelable