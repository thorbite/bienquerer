package com.jujuy.bienquerer.model.entity

import android.os.Parcelable
import androidx.room.Embedded
import androidx.room.Entity
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(primaryKeys = [("idSubCategory")])
data class SubCategory(
    @SerializedName("id")
    val idSubCategory : Int = 0,

    @SerializedName("name")
    val nameSubCategory : String,

    @SerializedName("created_at")
    val createdAtSubCategory : String?,

    @SerializedName("updated_at")
    val updatedAtSubCategory : String?,

    @SerializedName("category")
    @Embedded(prefix = "category_")
    val categorySubCategory : Category?,

    @SerializedName("image_background_url")
    val imageBackgroundUrl : String? = null



): Parcelable