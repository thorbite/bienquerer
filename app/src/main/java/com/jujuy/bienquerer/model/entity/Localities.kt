package com.jujuy.bienquerer.model.entity

import android.os.Parcelable
import androidx.room.Embedded
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Localities (
    @SerializedName("id")
    val idLocalities : Int,

    @SerializedName("name")
    val nameLocalities : String,

    @SerializedName("department")
    @Embedded
    var departmentLocalities : Department?
): Parcelable{
    /*constructor() : this(0, "", null)*/
}