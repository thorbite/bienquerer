package com.jujuy.bienquerer.model.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Social_network (
    @SerializedName("id")
    val idSocial_network : Int,

    @SerializedName("profileUrl")
    val nameSocial_network : String

):Parcelable{
    constructor() : this(0, "")
}
