package com.jujuy.bienquerer.model
import com.google.gson.annotations.SerializedName
import com.jujuy.bienquerer.model.entity.Social_network

data class RegisterProfessionalRequest(
    @SerializedName("address") val address : String? = "",
    @SerializedName("lat") val lat : String? = "",
    @SerializedName("lng") val lng : String? = "",
    @SerializedName("phone") val phone : String? = "",
    @SerializedName("description") val description : String? = "",
    @SerializedName("codeSms") val codeSms : String? = "",
    @SerializedName("professions") val professions : IntArray?,
    @SerializedName("socialNetworks") val socialNetworks : List<Social_network>?
)