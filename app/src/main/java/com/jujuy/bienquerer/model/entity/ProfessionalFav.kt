package com.jujuy.bienquerer.model.entity

import androidx.room.Entity
import com.google.gson.annotations.SerializedName

@Entity(primaryKeys = [("id")])
data class ProfessionalFav (
    @SerializedName("id") val id : Int,
    @SerializedName("favorite") val favorite : Professional = Professional(),
    @SerializedName("comment") val comment : String? = ""
)