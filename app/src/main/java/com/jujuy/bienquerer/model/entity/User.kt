package com.jujuy.bienquerer.model.entity

import android.os.Parcelable
import androidx.room.Embedded
import androidx.room.Entity
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
@Parcelize
@Entity (primaryKeys = ["id"])
data class User(
    @SerializedName("id") val id : String = "",
    @SerializedName("username") val username : String? = "",
    @SerializedName("password") val password : String? = "",
    @SerializedName("active") val active : Boolean? = false,
    @SerializedName("phone") val phone : String? = "",
    @SerializedName("email") val email : String? = "",
    @SerializedName("googleId") val googleId : String? = "",
    @SerializedName("photoGoogleUrl") val photoGoogleUrl : String? = "",
    @SerializedName("facebookId") val facebookId : String? = "",
    @SerializedName("phoneVerified") val phoneVerified : Boolean? = false,
    @SerializedName("emailVerified") val emailVerified : Boolean? = false,
    @SerializedName("available") val available : String? = "",
    @SerializedName("createdAt") val createdAt : String? = "",
    @SerializedName("updatedAt") val updatedAt : String? = "",
    @SerializedName("codeVerification") val codeVerification : String? = "",
    @SerializedName("userCode") val userCode : String? = "",
    @SerializedName("tokenEmail") val tokenEmail : String? = "",
    @SerializedName("rol") val rol : List<Rol>? = null,
    @Embedded(prefix = "person_")
    @SerializedName("person") val person : Person? = Person(),
    val fcmKey: String? = null
): Parcelable

