package com.jujuy.bienquerer.model.entity

import com.jujuy.bienquerer.model.network.response.Client

data class Comment(
        val id: Int,
        val requestDate: String,
        val createdAt: String,
        val updatedAt: String,
        val comment: String,
        val commentCacelled: Any? = null,
        val commentRejected: Any? = null,
        val jobComment: String,
        val averageScore: String,
        val client: Client
){
        val avgScore: Float
        get() = averageScore.toFloat()
}