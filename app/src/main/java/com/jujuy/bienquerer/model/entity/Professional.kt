package com.jujuy.bienquerer.model.entity

import android.os.Parcelable
import androidx.room.Entity
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(primaryKeys = [("id")])
data class Professional(

    val id: Int = 0,
    val name: String? = "",
    val lastname: String? = "",
    val email: String? = "",
    val document: String? = "",
    var photo_profile_url: String? = "",
    var photo_cover_url: String? = "",
    var photoProfileUrl: String? = "",
    var photoCoverUrl: String? = "",
    var latitude: String? = "",
    var longitude: String? = "",
    var address: String? = "",
    val created_at: String? = "",
    val updated_at: String? = "",
    val phone: String? = "",
    val available: Boolean? = false,
    val social: Boolean? = false,
    var localities: List<Localities>? = ArrayList(),
    var social_network: List<Social_network>? = ArrayList(),
    var profession: List<SubCategory>? = ArrayList(),
    var isFavorite: Boolean? = false,
    var comments: List<String>? = ArrayList(),
    var avgScore: String? = ""
) : Parcelable {
    @IgnoredOnParcel
    var isFav: Boolean? = false
    val score: Float
    get() = avgScore?.toFloat() ?: 0f
}







