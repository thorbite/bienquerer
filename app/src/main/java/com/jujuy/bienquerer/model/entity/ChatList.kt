package com.jujuy.bienquerer.model.entity

data class ChatList (
    var id: String? = "",
    var senderId: String? = "",
    var receiverId: String? = ""
)