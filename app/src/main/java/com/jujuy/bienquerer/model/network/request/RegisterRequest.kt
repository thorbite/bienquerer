package com.jujuy.bienquerer.model.network.request

import com.google.gson.annotations.SerializedName

class RegisterRequest(
    @SerializedName("id")
    var id: String?,

    @SerializedName("username")
    var username: String,

    @SerializedName("name")
    var name: String,

    @SerializedName("lastname")
    var lastname: String,

    @SerializedName("password")
    var password: String,

    @SerializedName("email")
    var email: String,

    @SerializedName("phone")
    var phone: String?,

    @SerializedName("address")
    var address: String?,

    @SerializedName("fcmKey")
    var fcmKey: String?
)