package com.jujuy.bienquerer.model.entity

import android.os.Parcelable
import androidx.room.Embedded
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize

data class Province (
    @SerializedName("id")
    val idProvince : Int,

    @SerializedName("name")
    val nameProvince : String ,

    @SerializedName("country")
    @Embedded
    var countryProvince : Country? = null
): Parcelable