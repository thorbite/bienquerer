package com.jujuy.bienquerer.model.network

data class BaseResponse<T>(
    var code: Int = 0,
    var message: String?,
    var status : String? = null,
    var data: T? = null
)