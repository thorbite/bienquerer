package com.jujuy.bienquerer.model.entity

import java.util.*

data class UserChat (
    var id: String = "",
    var userName: String? = "",
    var status: Int = 0,
    var imageProfile: String? = "",
    var fcmToken: String? = "",
    var updateAt: Date? = null
)