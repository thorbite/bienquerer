package com.jujuy.bienquerer.model.entity

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ItemSearch (
    var id: String? = "",
    var icon: String? = "",
    var name: String? = "",
    var moreDetails: String? = "",
    var entity: String? = ""
):Parcelable