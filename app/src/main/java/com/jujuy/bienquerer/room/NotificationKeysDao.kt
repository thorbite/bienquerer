package com.jujuy.bienquerer.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.jujuy.bienquerer.ui.notifications.NotificationKeys

@Dao
interface NotificationKeysDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(remoteKey: List<NotificationKeys>)

    @Query("SELECT * FROM notification_keys WHERE repoId = :repoId")
    suspend fun remoteKeysRepoId(repoId: Int): NotificationKeys?

    @Query("DELETE FROM notification_keys")
    suspend fun clearRemoteKeys()

    @Query("SELECT * FROM notification_keys")
    suspend fun getAllKeys(): List<NotificationKeys>
}