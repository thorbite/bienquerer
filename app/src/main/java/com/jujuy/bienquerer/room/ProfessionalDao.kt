package com.jujuy.bienquerer.room

import androidx.paging.PagingSource
import androidx.room.*
import com.jujuy.bienquerer.model.ItemFavorite
import com.jujuy.bienquerer.model.entity.Professional
import com.jujuy.bienquerer.model.entity.ProfessionalFav
import kotlinx.coroutines.flow.Flow

@Dao
interface ProfessionalDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(professional: Professional)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(professional: Professional)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAllProfessionalList(professionalList: List<Professional>)

    @Query("DELETE FROM Professional")
    fun deleteAllProfessionalList()

    /*@Query("SELECT * FROM Professional ORDER BY lastName")
    fun getAllProfessionalList(): List<Professional>*/
    @Query("SELECT * FROM Professional ORDER BY created_at")
    fun getAllProfessionalList(): PagingSource<Int, Professional>

    @Query("DELETE FROM Professional WHERE id = :id")
    fun deleteProfessionalbyId(id: String)

    @Query("SELECT * FROM Professional WHERE id = :id")
    fun getProfessionalById(id: String) : Professional

    @Query("SELECT isFavorite FROM Professional WHERE id = :id")
    fun getIsFavProfessional(id: String) : Flow<Boolean>

    @Query("UPDATE Professional SET isFavorite =:isFav WHERE id =:itemId")
    fun updateProfessionalFavById(itemId: String, isFav: Boolean)

    @Query("SELECT i.* FROM Professional i, ItemMap im WHERE i.id = im.itemId AND im.mapKey = :value ORDER BY im.sorting asc")
    fun getItemByKey(value: String?): Flow<List<Professional>>?



    //professional favorite
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertFavourite(itemFavourite: ItemFavorite)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllFavourites(itemFavourite: List<ProfessionalFav>)

    @Query("SELECT * FROM ProfessionalFav ORDER BY id Desc")
    fun getListFavourite(): PagingSource<Int, ProfessionalFav>

    @Query("DELETE FROM ProfessionalFav")
    fun deleteAllFavouriteItems()

    @Query("SELECT * FROM ProfessionalFav ORDER BY id Desc")
    fun getListFav(): List<ProfessionalFav>

    @Query("UPDATE Professional SET isFav =:is_favourited WHERE id =:itemId")
    fun updateProfessionalForFavById(itemId: String, is_favourited: String)

    @Query("DELETE FROM ItemFavorite where id = :itemId")
    fun deleteFavouriteItemByItemId(itemId: String)

    @Query("SELECT prd.* FROM Professional prd, ItemFavorite fp WHERE prd.id = fp.professionalId order by fp.sorting ")
    fun getAllFavouriteProfessional(): Flow<List<Professional>>
}