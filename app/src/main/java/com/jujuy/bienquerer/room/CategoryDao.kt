package com.jujuy.bienquerer.room

import androidx.room.*
import com.jujuy.bienquerer.model.entity.Category
import kotlinx.coroutines.flow.Flow

@Dao
interface CategoryDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(category: Category)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(category: Category)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(listCategories: List<Category>)

    @Query("DELETE FROM Category")
    fun deleteAllCategoryList()

    @Query("SELECT * FROM category")
    fun getAllCategoryList(): List<Category>

    @Query("DELETE FROM category WHERE idCategory = :id")
    fun deleteCategorybyId(id:String)

    @Query("SELECT * FROM category ORDER BY sorting")
    fun getAllCategoryById() : Flow<List<Category>>

}