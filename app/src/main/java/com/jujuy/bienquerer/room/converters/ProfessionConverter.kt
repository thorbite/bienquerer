package com.jujuy.bienquerer.room.converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.jujuy.bienquerer.model.entity.SubCategory

open class ProfessionConverter {
    @TypeConverter
    fun fromString(value: String): SubCategory? {
        val listType = object : TypeToken<SubCategory>() {}.type
        return Gson().fromJson<SubCategory>(value, listType)
    }

    @TypeConverter
    fun fromList(list: SubCategory?): String {
        val gson = Gson()
        return gson.toJson(list)
    }
}