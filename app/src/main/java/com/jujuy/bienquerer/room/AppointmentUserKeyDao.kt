package com.jujuy.bienquerer.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.jujuy.bienquerer.ui.appointment.user.AppointmentUserKeys

@Dao
interface AppointmentUserKeyDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(remoteKey: List<AppointmentUserKeys>)

    @Query("SELECT * FROM appointment_user_keys WHERE jobUserId = :repoId")
    suspend fun remoteKeysRepoId(repoId: Int): AppointmentUserKeys?

    @Query("DELETE FROM appointment_user_keys")
    suspend fun clearRemoteKeys()

    @Query("SELECT * FROM appointment_user_keys")
    suspend fun getAllKeys(): List<AppointmentUserKeys>
}