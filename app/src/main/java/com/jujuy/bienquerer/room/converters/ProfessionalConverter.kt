package com.jujuy.bienquerer.room.converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.jujuy.bienquerer.model.entity.Professional

open class ProfessionalConverter {
    @TypeConverter
    fun fromString(value: String): Professional? {
        val listType = object : TypeToken<Professional>() {}.type
        return Gson().fromJson<Professional>(value, listType)
    }

    @TypeConverter
    fun fromList(list: Professional?): String {
        val gson = Gson()
        return gson.toJson(list)
    }
}