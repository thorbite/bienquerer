package com.jujuy.bienquerer.room.converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.jujuy.bienquerer.model.entity.SubCategory

open class ProfessionListConverter {
    @TypeConverter
    fun fromString(value: String): List<SubCategory>? {
        val listType = object : TypeToken<List<SubCategory>>() {}.type
        return Gson().fromJson<List<SubCategory>>(value, listType)
    }

    @TypeConverter
    fun fromList(list: List<SubCategory>?): String {
        val gson = Gson()
        return gson.toJson(list)
    }
}