package com.jujuy.bienquerer.room.converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.jujuy.bienquerer.model.entity.Social_network

open class SocialNetworkListConverter {
    @TypeConverter
    fun fromString(value: String): List<Social_network>? {
        val listType = object : TypeToken<List<Social_network>>() {}.type
        return Gson().fromJson<List<Social_network>>(value, listType)
    }

    @TypeConverter
    fun fromList(list: List<Social_network>?): String {
        val gson = Gson()
        return gson.toJson(list)
    }
}