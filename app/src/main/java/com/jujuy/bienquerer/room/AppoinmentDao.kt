package com.jujuy.bienquerer.room

import androidx.paging.PagingSource
import androidx.room.*
import com.jujuy.bienquerer.model.entity.ProfessionalFav
import com.jujuy.bienquerer.model.network.response.Appointment
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlin.time.ExperimentalTime

@ExperimentalTime
@ExperimentalCoroutinesApi
@Dao
interface AppoinmentDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(appointment: Appointment)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(appointment: Appointment)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllAppointmentList(NotiList: List<Appointment>)

    @Query("DELETE FROM Appointment")
    fun deleteAllAppointmentList()

    @Query("SELECT * FROM Appointment ORDER BY id Desc")
    fun getAllAppointmentList(): Flow<List<Appointment>>

    @Query("SELECT * FROM Appointment ORDER BY id DESC")
    fun getAppointmentListPaged(): PagingSource<Int, Appointment>

    @Query("DELETE FROM Appointment WHERE id = :id")
    fun deleteAppointmentbyId(id:String)

    @Query("SELECT * FROM Appointment WHERE id = :id")
    fun getAppointmentById(id: String) : Flow<Appointment>
}