package com.jujuy.bienquerer.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.jujuy.bienquerer.model.SearchRecent

@Dao
interface SearchDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(search: SearchRecent)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(searchList: List<SearchRecent>)

    @Query("SELECT * FROM searchRecents")
    fun getAllSearched(): List<SearchRecent>

    @Query("DELETE FROM searchRecents where searchText == :query")
    fun deleteSearched(query: String)
}