package com.jujuy.bienquerer.room.converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.jujuy.bienquerer.model.entity.Person

open class PersonConverter {
    @TypeConverter
    fun fromString(value: String): Person? {
        val listType = object : TypeToken<Person>() {}.type
        return Gson().fromJson<Person>(value, listType)
    }

    @TypeConverter
    fun fromList(list: Person?): String {
        val gson = Gson()
        return gson.toJson(list)
    }
}