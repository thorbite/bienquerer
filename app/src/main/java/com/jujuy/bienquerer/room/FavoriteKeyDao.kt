package com.jujuy.bienquerer.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.jujuy.bienquerer.ui.favorite.FavoriteKeys

@Dao
interface FavoriteKeyDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(remoteKey: List<FavoriteKeys>)

    @Query("SELECT * FROM favorite_keys WHERE favId = :repoId")
    suspend fun remoteKeysRepoId(repoId: Int): FavoriteKeys?

    @Query("DELETE FROM favorite_keys")
    suspend fun clearRemoteKeys()

    @Query("SELECT * FROM favorite_keys")
    suspend fun getAllKeys(): List<FavoriteKeys>
}