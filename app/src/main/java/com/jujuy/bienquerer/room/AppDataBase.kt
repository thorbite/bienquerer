package com.jujuy.bienquerer.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.jujuy.bienquerer.model.ItemFavorite
import com.jujuy.bienquerer.model.Message
import com.jujuy.bienquerer.model.SearchRecent
import com.jujuy.bienquerer.model.entity.*
import com.jujuy.bienquerer.model.network.response.Job
import com.jujuy.bienquerer.model.network.response.ListNotifications
import com.jujuy.bienquerer.room.AppDatabase.Companion.DB_VERSION
import com.jujuy.bienquerer.room.converters.*
import com.jujuy.bienquerer.model.network.response.Appointment
import com.jujuy.bienquerer.model.network.response.Status
import com.jujuy.bienquerer.ui.appointment.professional.JobProfessionalKey
import com.jujuy.bienquerer.ui.appointment.user.AppointmentUserKeys
import com.jujuy.bienquerer.ui.favorite.FavoriteKeys
import com.jujuy.bienquerer.ui.notifications.NotificationKeys
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
@Database(entities = [(Notification::class),
    (Category::class),
    (Provider::class),
    (SubCategory::class),
    (User::class),
    (Professional::class),
    (UserLogin::class),
    (ItemMap::class),
    (Rol::class),
    (Person::class),
    (Message::class),
    (ListNotifications::class),
    (ItemFavorite::class), (ProfessionalFav::class), (Appointment::class),
    (Job::class), (Status::class), (RemoteKeys::class),
    (NotificationKeys::class), (FavoriteKeys::class), (AppointmentUserKeys::class), (JobProfessionalKey::class), (SearchRecent::class)],
    version = DB_VERSION, exportSchema = false)
@TypeConverters(value = [(LocalitiesListConverter::class), (ProfessionListConverter::class),
    (SocialNetworkListConverter::class),(RolListConverter::class),(PersonConverter::class),
    (NotificationConverter::class), (ProfessionalConverter::class),
    (ProfessionalFavConverter::class), (ClientConverter::class), (StatusJobConverter::class), (ProfessionConverter::class), (CommentsListConverter::class) ])
abstract class AppDatabase : RoomDatabase() {
    abstract fun notificationDao(): NotificationDao
    abstract fun categoryDao(): CategoryDao
    abstract fun providerDao(): ProviderDao
    abstract fun subCategoryDao(): SubCategoryDao
    abstract fun userDao(): UserDao
    abstract fun professionalDao(): ProfessionalDao
    abstract fun itemMapDao(): ItemMapDao
    abstract fun messageDao(): MessageDao
    abstract fun appointmentDao(): AppoinmentDao
    abstract fun jobDao(): JobDao
    abstract fun remoteKeysDao(): RemoteKeysDao
    abstract fun notificationKeysDao(): NotificationKeysDao
    abstract fun favoriteKeysDao(): FavoriteKeyDao
    abstract fun appointmentUserKeyDao(): AppointmentUserKeyDao
    abstract fun jobProfessionalKeyDao(): JobProfessionalKeyDao
    abstract fun searchDao(): SearchDao


    companion object {
        const val DB_VERSION = 1
        private const val DB_NAME = "YoNecesito.db"
        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: build(context).also { INSTANCE = it }
            }

        private fun build(context: Context) =
            Room.databaseBuilder(context.applicationContext, AppDatabase::class.java, DB_NAME)
                .addMigrations(MIGRATION_1_TO_2)
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build()

        //In case app has to migrate to a new DBDao
        private val MIGRATION_1_TO_2 = object : Migration(1, 2) {
            override fun migrate(database: SupportSQLiteDatabase) {

            }
        }
    }
}