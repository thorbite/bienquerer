package com.jujuy.bienquerer.room.converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.jujuy.bienquerer.model.network.response.Client

open class ClientConverter {
    @TypeConverter
    fun fromString(value: String): Client? {
        val listType = object : TypeToken<Client>() {}.type
        return Gson().fromJson<Client>(value, listType)
    }

    @TypeConverter
    fun fromList(list: Client?): String {
        val gson = Gson()
        return gson.toJson(list)
    }
}