package com.jujuy.bienquerer.room

import androidx.lifecycle.LiveData
import androidx.room.*
import com.jujuy.bienquerer.model.entity.SubCategory
import kotlinx.coroutines.flow.Flow

@Dao
interface SubCategoryDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(subCategory: SubCategory)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(subCategory: SubCategory)

    @Query("DELETE FROM SubCategory")
    fun deleteAllSubCategory()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(subCategories: List<SubCategory>)

    @Query("SELECT * FROM SubCategory ORDER BY createdAtSubCategory DESC")
    fun getAllSubCategory(): LiveData<List<SubCategory>>

    @Query("SELECT * FROM SubCategory ORDER BY createdAtSubCategory DESC")
    fun getAllProfessions(): Flow<List<SubCategory>>


    @Query("SELECT * FROM SubCategory WHERE subCategory.category_idCategory=:catId")
    fun getSubCategoryList(catId: Int): Flow<List<SubCategory>>

}