package com.jujuy.bienquerer.room

import androidx.room.*
import com.jujuy.bienquerer.model.entity.Provider

@Dao
interface ProviderDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(noti: Provider)
    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(noti: Provider)
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllProviderList(NotiList: List<Provider>)
    @Query("DELETE FROM provider")
    fun deleteAllProviderList()
    @Query("SELECT * FROM provider ORDER BY lastName")
    fun getAllProviderList(): List<Provider>
    @Query("DELETE FROM Provider WHERE id = :id")
    fun deleteProviderbyId(id:String)
    @Query("SELECT * FROM provider WHERE id = :id")
    fun getProviderById(id: String) : Provider
}