package com.jujuy.bienquerer.room

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.jujuy.bienquerer.model.Message

@Dao
interface MessageDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(message: Message)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(messageList: MutableList<Message?>)

    @Query("DELETE FROM Message WHERE sessionId = :value")
    fun deleteAll(value: String)

    @Query("DELETE FROM Message WHERE id = :id")
    fun deleteMessageById(id: String?)

    @Query("DELETE FROM Message")
    fun deleteAllMessage()

    @Query("SELECT * FROM MESSAGE WHERE sessionId=:key Order by addedDate")
    fun getMessages(key: String): LiveData<List<Message>>

    @Query("SELECT * FROM Message")
    fun getAllMessages(): LiveData<List<Message>>
}
