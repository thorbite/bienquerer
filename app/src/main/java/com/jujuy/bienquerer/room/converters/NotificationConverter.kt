package com.jujuy.bienquerer.room.converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.jujuy.bienquerer.model.entity.Notification

open class NotificationConverter {
    @TypeConverter
    fun fromString(value: String): Notification? {
        val listType = object : TypeToken<Notification>() {}.type
        return Gson().fromJson<Notification>(value, listType)
    }

    @TypeConverter
    fun fromList(list: Notification?): String {
        val gson = Gson()
        return gson.toJson(list)
    }
}