package com.jujuy.bienquerer.room.converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.jujuy.bienquerer.model.entity.Rol

open class RolListConverter {
    @TypeConverter
    fun fromString(value: String): List<Rol>? {
        val listType = object : TypeToken<List<Rol>>() {}.type
        return Gson().fromJson<List<Rol>>(value, listType)
    }

    @TypeConverter
    fun fromList(list: List<Rol>?): String {
        val gson = Gson()
        return gson.toJson(list)
    }
}