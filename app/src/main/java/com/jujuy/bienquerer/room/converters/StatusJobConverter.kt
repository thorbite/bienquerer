package com.jujuy.bienquerer.room.converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.jujuy.bienquerer.model.network.response.Status

open class StatusJobConverter {
    @TypeConverter
    fun fromString(value: String): Status? {
        val listType = object : TypeToken<Status>() {}.type
        return Gson().fromJson<Status>(value, listType)
    }

    @TypeConverter
    fun fromList(list: Status?): String {
        val gson = Gson()
        return gson.toJson(list)
    }
}