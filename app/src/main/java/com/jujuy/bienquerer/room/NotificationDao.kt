package com.jujuy.bienquerer.room

import androidx.paging.PagingSource
import androidx.room.*
import com.jujuy.bienquerer.model.network.response.ListNotifications
import kotlinx.coroutines.flow.Flow

@Dao
interface NotificationDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(noti: ListNotifications)
    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(noti: ListNotifications)
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAllNotificationList(noti: List<ListNotifications>)
    @Query("DELETE FROM ListNotifications")
    suspend fun deleteAllNotificationList()

    @Query("SELECT * FROM ListNotifications order by createdAt Desc")
    fun getAllNotificationList(): PagingSource<Int, ListNotifications>

    @Query("SELECT * FROM ListNotifications")
    fun getAllNotification(): List<ListNotifications>
    @Query("DELETE FROM ListNotifications WHERE id = :id")
    fun deleteNotificationbyId(id:String)
    @Query("SELECT * FROM ListNotifications WHERE id = :id")
    fun getNotificationById(id: String) : Flow<ListNotifications>
}