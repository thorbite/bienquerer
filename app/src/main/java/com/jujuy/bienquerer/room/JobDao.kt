package com.jujuy.bienquerer.room

import androidx.paging.PagingSource
import androidx.room.*
import com.jujuy.bienquerer.model.network.response.Appointment
import com.jujuy.bienquerer.model.network.response.Job
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlin.time.ExperimentalTime
@ExperimentalTime
@ExperimentalCoroutinesApi
@Dao
interface JobDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(noti: Job)
    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(noti: Job)
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllJobList(NotiList: List<Job>)
    @Query("DELETE FROM Job")
    fun deleteAllJobList()
    @Query("SELECT * FROM Job ORDER BY createdAt")
    fun getAllJobList(): Flow<List<Job>>
    @Query("DELETE FROM Job WHERE id = :id")
    fun deleteJobbyId(id:String)
    @Query("SELECT * FROM Job WHERE id = :id")
    fun getJobById(id: String) : Flow<Job>
    @Query("SELECT * FROM Job WHERE id = :id")
    fun getJob(id: String) : Job

    @Query("SELECT * FROM Job ORDER BY id DESC")
    fun getJobListPaged(): PagingSource<Int, Job>
}