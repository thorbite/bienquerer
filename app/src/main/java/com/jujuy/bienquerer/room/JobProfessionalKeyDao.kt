package com.jujuy.bienquerer.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.jujuy.bienquerer.ui.appointment.professional.JobProfessionalKey

@Dao
interface JobProfessionalKeyDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(remoteKey: List<JobProfessionalKey>)

    @Query("SELECT * FROM job_professional_keys WHERE jobProfessionalId = :repoId")
    suspend fun remoteKeysRepoId(repoId: Int): JobProfessionalKey?

    @Query("DELETE FROM job_professional_keys")
    suspend fun clearRemoteKeys()

    @Query("SELECT * FROM job_professional_keys")
    suspend fun getAllKeys(): List<JobProfessionalKey>
}