package com.jujuy.bienquerer.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.jujuy.bienquerer.model.entity.ItemMap

@Dao
interface ItemMapDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(itemMap: ItemMap?)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(itemMap: List<ItemMap>?)

    @Query("DELETE FROM ItemMap WHERE mapKey = :key")
    fun deleteByMapKey(key: String?)

    @Query("DELETE FROM ItemMap WHERE mapKey = :key AND itemId = :itemId")
    fun deleteByMapKeyAndItemId(key: String?, itemId: String?)

    @Query("SELECT max(sorting) from ItemMap WHERE mapKey = :value ")
    fun getMaxSortingByValue(value: String?): Int

    @Query("SELECT * FROM ItemMap")
    fun getAll(): List<ItemMap?>?

    @Query("SELECT * FROM ItemMap WHERE itemId = :id")
    fun getItemListByItemId(id: String?): List<ItemMap>?

    @Query("DELETE FROM ItemMap")
    fun deleteAll()
}