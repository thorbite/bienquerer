package com.jujuy.bienquerer.room.converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.jujuy.bienquerer.model.entity.ProfessionalFav

open class ProfessionalFavConverter {
    @TypeConverter
    fun fromString(value: String): List<ProfessionalFav>? {
        val listType = object : TypeToken<List<ProfessionalFav>>() {}.type
        return Gson().fromJson<List<ProfessionalFav>>(value, listType)
    }

    @TypeConverter
    fun fromList(list: List<ProfessionalFav>?): String {
        val gson = Gson()
        return gson.toJson(list)
    }
}