package com.jujuy.bienquerer.room.converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.jujuy.bienquerer.model.entity.Localities

open class LocalitiesListConverter {
    @TypeConverter
    fun fromString(value: String): List<Localities>? {
        val listType = object : TypeToken<List<Localities>>() {}.type
        return Gson().fromJson<List<Localities>>(value, listType)
    }

    @TypeConverter
    fun fromList(list: List<Localities>?): String {
        val gson = Gson()
        return gson.toJson(list)
    }
}
