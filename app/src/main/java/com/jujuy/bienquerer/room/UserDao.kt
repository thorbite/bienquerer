package com.jujuy.bienquerer.room

import androidx.lifecycle.LiveData
import androidx.room.*
import com.jujuy.bienquerer.model.entity.User
import com.jujuy.bienquerer.model.entity.UserLogin
import kotlinx.coroutines.flow.Flow

@Dao
interface UserDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(user: User)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(user: User)

    @Query("UPDATE User SET person_photoProfileUrl = :photo WHERE id = :userId")
    fun updateImageProfile(userId: String, photo: String)

    @Query("UPDATE User SET person_photoCoverUrl = :photo WHERE id = :userId")
    fun updateImageCoverProfile(userId: String, photo: String)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(userList: List<User?>)

    @Query("SELECT * FROM User")
    fun getAll(): User

    @Query("SELECT * FROM User WHERE id = :userId")
    fun getUserData(userId: String): Flow<User>

    @Query("SELECT * FROM User WHERE id = :userId")
    fun getUser(userId: String): User



    //region User Login Related
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(userLogin: UserLogin)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(userLogin: UserLogin)

    @Query("SELECT * FROM UserLogin WHERE userId = :userId")
    fun getUserLoginData(userId: String): Flow<UserLogin>

    @Query("UPDATE UserLogin SET user_person_photoProfileUrl = :photo WHERE user_id = :userId")
    fun updateImageProfileUserLogin(userId: String, photo: String)

    @Query("UPDATE UserLogin SET user_person_photoCoverUrl = :photo WHERE user_id = :userId")
    fun updateImageCoverProfileUserLogin(userId: String, photo: String)

    @Query("SELECT * FROM UserLogin")
    fun getUserLoginData(): LiveData<List<UserLogin>>

    @Query("DELETE FROM UserLogin")
    fun deleteUserLogin()

    //endregion
}