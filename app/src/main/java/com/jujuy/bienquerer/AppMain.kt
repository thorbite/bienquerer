package com.jujuy.bienquerer

import android.app.Application
import com.jujuy.bienquerer.di.*
import kotlinx.coroutines.ExperimentalCoroutinesApi

import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class AppMain: Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@AppMain)
            modules(networkModule)
            modules(persistenceModule)
            modules(repositoryModule)
            modules(NavigationModule)
            modules(viewModelModule)
            modules(SharedPreferencesModule)
        }

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

        //Stetho.initializeWithDefaults(this);
    }
}