package com.jujuy.bienquerer.base

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import androidx.fragment.app.FragmentActivity
import com.jujuy.bienquerer.MainActivity
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.extensions.launchActivity
import com.jujuy.bienquerer.extensions.replace
import com.jujuy.bienquerer.model.ItemParameterHolder
import com.jujuy.bienquerer.model.entity.Professional
import com.jujuy.bienquerer.model.network.response.Job
import com.jujuy.bienquerer.model.network.response.ListNotifications
import com.jujuy.bienquerer.model.network.response.Appointment
import com.jujuy.bienquerer.model.network.response.Client
import com.jujuy.bienquerer.ui.appointment.JobFragment
import com.jujuy.bienquerer.ui.appointment.bookAppointment.BookAppointmentDetailActivity
import com.jujuy.bienquerer.ui.appointment.jobDetailProfessional.JobDetailProfessionalActivity
import com.jujuy.bienquerer.ui.appointment.jobDetailUser.JobDetailUserActivity
import com.jujuy.bienquerer.ui.auth.forgotPassword.UserForgotPasswordActivity
import com.jujuy.bienquerer.ui.auth.forgotPassword.UserForgotPasswordFragment
import com.jujuy.bienquerer.ui.auth.login.UserLoginActivity
import com.jujuy.bienquerer.ui.auth.login.UserLoginFragment
import com.jujuy.bienquerer.ui.auth.register.UserRegisterActivity
import com.jujuy.bienquerer.ui.auth.register.UserRegisterFragment
import com.jujuy.bienquerer.ui.beProfessional.RegisterProfessionalActivity
import com.jujuy.bienquerer.ui.category.list.CategoryFragment
import com.jujuy.bienquerer.ui.chat.detail.ChatDetailActivity
import com.jujuy.bienquerer.ui.chat.list.ChatListFragment
import com.jujuy.bienquerer.ui.favorite.FavoriteFragment
import com.jujuy.bienquerer.ui.home.HomeFragment
import com.jujuy.bienquerer.ui.notifications.detail.NotificationDetailActivity
import com.jujuy.bienquerer.ui.professionals.detail.ItemProfessionalActivity
import com.jujuy.bienquerer.ui.professionals.filter.FilteringActivity
import com.jujuy.bienquerer.ui.professionals.map.MapActivity
import com.jujuy.bienquerer.ui.professionals.map.MapFilteringActivity
import com.jujuy.bienquerer.ui.professionals.search.SearchListActivity
import com.jujuy.bienquerer.ui.profile.ProfileFragment
import com.jujuy.bienquerer.ui.subcategory.SubCategoryListActivity
import com.jujuy.bienquerer.ui.editprofile.ProfileEditActivity
import com.jujuy.bienquerer.ui.professionals.comments.CommentsActivity
import com.jujuy.bienquerer.ui.scoringJob.ScoringActivity
import com.jujuy.bienquerer.utils.Constants
import kotlinx.coroutines.ExperimentalCoroutinesApi
import timber.log.Timber
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class NavigationController {
    private var containerId: Int = 0
    private var currentFragment: RegFragments = RegFragments.HOME_FRAGMENT

    init {
        this.containerId = R.id.content_frame
    }

    fun navigateToUserLogin(mainActivity: MainActivity) {
        if (checkFragmentChange(RegFragments.HOME_USER_LOGIN)) {
            try {
                mainActivity.replace(UserLoginFragment(), containerId)
            } catch (e: Exception) {
                Timber.e(e)
            }
        }
    }

    fun navigateToHome(mainActivity: MainActivity) {
        Timber.e(checkFragmentChange(RegFragments.HOME_FRAGMENT).toString())
        try {
            mainActivity.replace(HomeFragment(), containerId)
        } catch (e: Exception) {
            Timber.e(e)
        }
    }


    fun navigateToUserRegister(mainActivity: MainActivity) {
        if (checkFragmentChange(RegFragments.HOME_USER_REGISTER)) {
            try {
                mainActivity.replace(UserRegisterFragment(), containerId)
            } catch (e: Exception) {
                Timber.e(e)
            }
        }
    }

    fun navigateToJobs(mainActivity: MainActivity) {
        if (checkFragmentChange(RegFragments.HOME_JOB)) {
            try {
                mainActivity.replace(JobFragment(),containerId)
            } catch (e: Exception) {
                Timber.e(e)
            }
        }
    }

    fun navigateToMessage(mainActivity: MainActivity) {
        if (checkFragmentChange(RegFragments.HOME_MESSAGE)) {
            try {
                mainActivity.replace(ChatListFragment(), containerId)
            } catch (e: Exception) {
                Timber.e(e)
            }
        }
    }

    fun navigateToFavorite(mainActivity: MainActivity) {
        if (checkFragmentChange(RegFragments.HOME_FAVOURITE)) {
            try {
                mainActivity.replace(FavoriteFragment(),containerId)
            } catch (e: Exception) {
                Timber.e(e)
            }
        }
    }

    fun navigateToUserProfile(mainActivity: MainActivity) {
        if (checkFragmentChange(RegFragments.HOME_USER_PROFILE)) {
            try {
                mainActivity.replace(ProfileFragment(), containerId)
            } catch (e: Exception) {
                Timber.e(e)
            }
        }
    }

    fun navigateToUserForgotPassword(mainActivity: MainActivity) {
        if (checkFragmentChange(RegFragments.HOME_USER_FOGOT_PASSWORD)) {
            try {
                mainActivity.replace(UserForgotPasswordFragment(), containerId)
            } catch (e: Exception) {
                Timber.e(e)
            }
        }
    }


    fun navigateToJobList(activity: FragmentActivity) {
        try {
            val fragment = JobFragment()
            activity.supportFragmentManager.beginTransaction()
                .replace(containerId, fragment)
                .commitAllowingStateLoss()
        } catch (e: Exception) {
            Timber.e(e)
        }
    }


    private fun checkFragmentChange(regFragments: RegFragments): Boolean {
        if (currentFragment != regFragments) {
            currentFragment = regFragments
            return true
        }
        return false
    }


    fun navigateToMainActivity(activity: Activity) {
        activity.launchActivity<MainActivity>()
    }

    fun navigateToCategory(mainActivity: MainActivity) {
        if (checkFragmentChange(RegFragments.HOME_CATEGORY)) {
            try {
                mainActivity.replace(CategoryFragment(), containerId)
            } catch (e: java.lang.Exception) {
                Timber.e("Error! Can't replace fragment. $e")
            }
        }
    }

    fun navigateToMainActivity(
        activity: Activity,
        selectedLocationId: String?,
        selectedLocationName: String?,
        lat: String?,
        lng: String?
    ) {
        activity.launchActivity<MainActivity>(requestCode = Constants.REQUEST_CODE__SELECTED_CITY_FRAGMENT) {
            putExtra(Constants.SELECTED_LOCATION_ID, selectedLocationId)
            putExtra(Constants.SELECTED_LOCATION_NAME, selectedLocationName)
            putExtra(Constants.LAT, lat)
            putExtra(Constants.LNG, lng)
        }
    }

    fun navigateToUserRegisterActivity(activity: Activity) {
        activity.launchActivity<UserRegisterActivity>()
    }

    fun navigateToUserForgotPasswordActivity(activity: Activity) {
        activity.launchActivity<UserForgotPasswordActivity>()
    }

    fun navigateToUserLoginActivity(activity: Activity) {
        activity.launchActivity<UserLoginActivity> ()
    }

    fun navigateToSubCategoryActivity(
        activity: Activity,
        catId: Int?,
        catName: String?
    ) {
        activity.launchActivity<SubCategoryListActivity> {
            putExtra(Constants.CATEGORY_ID, catId.toString())
            putExtra(Constants.CATEGORY_NAME, catName)
        }
    }

    fun navigateToHomeFilteringActivity(
        mainActivity: FragmentActivity,
        itemParameterHolder: ItemParameterHolder,
        titleName: String,
        mapMiles: String
    ) {
        itemParameterHolder.mapMiles = mapMiles
        mainActivity.launchActivity<SearchListActivity> {
            putExtra(Constants.ITEM_NAME, titleName)
            putExtra(Constants.ITEM_PARAM_HOLDER_KEY, itemParameterHolder)
        }
    }


    @ExperimentalCoroutinesApi
    fun navigateToItemDetailActivity(
        fragmentActivity: FragmentActivity,
        professional: String,
        itemName: String
    ) {
        fragmentActivity.launchActivity<ItemProfessionalActivity> {
            putExtra(Constants.PROFESSIONAL, professional)
            //intent.putExtra("bundle", b)
            putExtra(Constants.HISTORY_FLAG, Constants.ONE)
            //intent.putExtra(Constants.PROFESSIONAL, professional)
            putExtra(Constants.ITEM_NAME, itemName)
        }
    }


    fun navigateBackFromNotiList(activity: Activity) {
        val intent = Intent()
        activity.setResult(Constants.RESULT_CODE__REFRESH_NOTIFICATION, intent)
    }

    fun navigateToMapActivity(activity: Activity, LNG: String?, LAT: String?, flag: String?) {
        activity.launchActivity<MapActivity>(requestCode = Constants.RESULT_CODE__TO_MAP_VIEW) {
            putExtra(Constants.LNG, LNG)
            putExtra(Constants.LAT, LAT)
            putExtra(Constants.MAP_FLAG, flag)
        }
    }

    fun navigateToTypeFilterFragment(
        mainActivity: FragmentActivity, catId: String?,
        subCatId: String?,
        itemParameterHolder: ItemParameterHolder?,
        name: String
    ) {
        var id = subCatId
        if (name == Constants.FILTERING_TYPE_FILTER) {
            mainActivity.launchActivity<FilteringActivity>(requestCode = Constants.REQUEST_CODE__ITEM_LIST_FRAGMENT) {
                putExtra(Constants.CATEGORY_ID, catId)
                if (subCatId == null || subCatId == "") {
                    id = Constants.ZERO
                }
                putExtra(Constants.SUBCATEGORY_ID, id)
                putExtra(Constants.FILTERING_FILTER_NAME, name)
            }
        }
    }

    fun navigateBackFromMapView(activity: Activity, lat: String?, lng: String?) {
        val intent = Intent()
        intent.putExtra(Constants.LAT, lat)
        intent.putExtra(Constants.LNG, lng)
        activity.setResult(Constants.RESULT_CODE__FROM_MAP_VIEW, intent)
    }


    fun navigateToChatActivity(
        activity: FragmentActivity,
        receivedUserId: String?,
        receiverName: String?,
        receiverLastName: String?,
        itemImagePath: String?,
        request_code: Int
    ) {
        activity.launchActivity<ChatDetailActivity>(requestCode = request_code) {
            putExtra(Constants.RECEIVE_USER_ID, receivedUserId)
            putExtra(Constants.RECEIVE_USER_NAME, receiverName)
            putExtra(Constants.RECEIVE_USER_LAST_NAME, receiverLastName)
            putExtra(Constants.RECEIVE_USER_IMG_URL, itemImagePath)
        }
    }

    fun navigateToBookAppointmentDetailActivity(
        activity: FragmentActivity,
        professional: Professional?,
        name: String?,
        lastname: String?,
        email: String?
    ) {
        activity.launchActivity<BookAppointmentDetailActivity> {
            putExtra(Constants.PROFESSIONAL, professional)
            putExtra(Constants.USER_NAME, name)
            putExtra(Constants.USER_LASTNAME, lastname)
            putExtra(Constants.USER_EMAIL, email)
        }
    }

    fun navigateToNotificationDetail(
        activity: Activity, noti: ListNotifications, token: String?
    ) {
        activity.launchActivity<NotificationDetailActivity>(requestCode = Constants.REQUEST_CODE__NOTIFICATION_LIST_FRAGMENT) {
            putExtra(Constants.NOTI_ID, noti.id.toString())
            putExtra(Constants.NOTI_TOKEN, token)
        }
    }

    fun navigateBackToHomeFeaturedFragment(
        mainActivity: FragmentActivity,
        catId: String?,
        subCatId: String?,
        nameSubcategory: String?
    ) {
        val intent = Intent()
        intent.putExtra(Constants.CATEGORY_ID, catId)
        intent.putExtra(Constants.SUBCATEGORY_ID, subCatId)
        intent.putExtra(Constants.SUBCATEGORY_NAME, nameSubcategory)
        mainActivity.setResult(Constants.RESULT_CODE__CATEGORY_FILTER, intent)
    }

    fun navigateToProfileEditActivity(activity: Activity, isProfessional: Boolean) {
        activity.launchActivity<ProfileEditActivity> {
            putExtra("isProfessional", isProfessional)
        }
    }


    fun navigateToRegisterProfessional(activity: Activity) {
        activity.launchActivity<RegisterProfessionalActivity>()
    }

    fun navigateToScoringJob(activity: Activity, professional: Client?, profession: String?, jobId: String){
        activity.launchActivity<ScoringActivity>(){
            putExtra("professionalToScore", professional)
            putExtra("professionToScore", profession)
            putExtra("jobId", jobId)
        }
    }

    fun navigateBackToSearchFromMapFiltering(
        activity: FragmentActivity,
        itemParameterHolder: ItemParameterHolder?
    ) {

        val intent = Intent()
        intent.putExtra(Constants.ITEM_HOLDER, itemParameterHolder)
        activity.setResult(Constants.RESULT_CODE__MAP_FILTERING, intent)
        activity.finish()
    }

    fun navigateBackToFiltering(
        activity: FragmentActivity,
        catId: String?,
        subcatId: String?
    ) {

        val intent = Intent()
        intent.putExtra(Constants.CATEGORY_ID, catId)
        intent.putExtra(Constants.SUBCATEGORY_ID, subcatId)
        activity.setResult(Constants.RESULT_CODE__FROM_MAP_VIEW, intent)
        activity.finish()
    }


    fun navigateToMapFiltering(
        activity: FragmentActivity,
        itemParameterHolder: ItemParameterHolder?
    ) {
        activity.launchActivity<MapFilteringActivity>(requestCode = Constants.REQUEST_CODE__MAP_FILTERING) {
            putExtra(Constants.ITEM_HOLDER, itemParameterHolder)
        }
    }


    fun navigateToDetailJob(activity: FragmentActivity, item: Job) {
        activity.launchActivity<JobDetailProfessionalActivity>{
            putExtra(Constants.JOB, item.id.toString())
        }
    }

    fun navigateToDetailAppoinment(requireActivity: FragmentActivity, item: Appointment) {
        requireActivity.launchActivity<JobDetailUserActivity> {
            putExtra(Constants.APPOINTMENT_ID, item.id.toString())
        }
    }

    fun navigateToCommentsProfessional(activity: FragmentActivity, id: String) {
        activity.launchActivity<CommentsActivity> {
            putExtra("idProf", id)
        }
    }


    private enum class RegFragments {
        HOME_FRAGMENT,
        HOME_USER_LOGIN,
        HOME_USER_PROFILE,
        HOME_USER_REGISTER,
        HOME_USER_FOGOT_PASSWORD,
        HOME_CATEGORY,
        HOME_MESSAGE,
        HOME_HOME,
        HOME_JOB,
        HOME_FAVOURITE
    }

}