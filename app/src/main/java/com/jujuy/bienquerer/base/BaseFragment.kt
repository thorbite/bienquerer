package com.jujuy.bienquerer.base

import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import com.jujuy.bienquerer.R
import com.jujuy.bienquerer.api.network.AppExecutors
import com.jujuy.bienquerer.utils.AppPreferences
import com.jujuy.bienquerer.utils.Connectivity
import com.jujuy.bienquerer.utils.Constants
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.android.ext.android.inject
import timber.log.Timber
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
abstract class BaseFragment: Fragment() {
    val navigationController: NavigationController by inject()
    val connectivity: Connectivity by inject()
    val pref: SharedPreferences by inject()
    val appExecutors: AppExecutors by inject()
    val sessionManager: AppPreferences by inject()
    lateinit var loginUserId : String
    lateinit var facebookId : String
    lateinit var googleId : String
    lateinit var loginUserEmail: String
    lateinit var loginUserPwd: String
    lateinit var loginUserCode: String
    lateinit var loginUserName: String
    lateinit var loginUserLastname: String
    lateinit var loginUsername: String
    lateinit var profileImageUrl: String
    lateinit var userPhone: String
    protected var force_update: Boolean = false
    var isProfessional: Boolean? = false
    val userIsProfessional: Boolean by lazy { pref.getBoolean(Constants.IS_PROFESSIONAL, false) }

    var selectedLat: String = ""
    var selectedLng: String = ""
    var selected_location_id: String? = ""
    var selected_location_name: String? = ""

    var isFadeIn : Boolean = false
    lateinit var userEmailToVerify : String
    lateinit var userPasswordToVerify : String
    lateinit var userNameToVerify : String
    lateinit var userIdToVerify : String

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        loadLoginUserId()
        initUIAndActions()
        initAdapters()
        initData()
    }


    private fun loadLoginUserId() {
        try {
            if(activity != null && activity?.baseContext != null){
                loginUserId = pref.getString(Constants.USER_ID, Constants.EMPTY_STRING).toString()
                facebookId = pref.getString(Constants.FACEBOOK_ID, Constants.EMPTY_STRING).toString()
                googleId = pref.getString(Constants.GOOGLE_ID, Constants.EMPTY_STRING).toString()
                loginUserEmail = pref.getString(Constants.USER_EMAIL, Constants.EMPTY_STRING).toString()
                loginUserPwd = pref.getString(Constants.USER_PASSWORD, Constants.EMPTY_STRING).toString()
                loginUserCode = pref.getString(Constants.USER_CODE, Constants.EMPTY_STRING).toString()
                loginUserName = pref.getString(Constants.USER_NAME, Constants.EMPTY_STRING).toString()
                loginUserLastname = pref.getString(Constants.USER_LASTNAME, Constants.EMPTY_STRING).toString()
                selectedLat = pref.getString(Constants.LAT, Constants.LAT_STRING).toString()
                selectedLng = pref.getString(Constants.LNG, Constants.LNG_STRING).toString()
                selected_location_id = pref.getString(Constants.SELECTED_LOCATION_ID, Constants.EMPTY_STRING).toString()
                selected_location_name = pref.getString(Constants.SELECTED_LOCATION_NAME, Constants.EMPTY_STRING).toString()
                profileImageUrl = pref.getString(Constants.PROFILE_IMAGE_URL, Constants.EMPTY_STRING).toString()
                userEmailToVerify = pref.getString(Constants.USER_EMAIL_TO_VERIFY, Constants.EMPTY_STRING).toString()
                userPasswordToVerify = pref.getString(Constants.USER_PASSWORD_TO_VERIFY, Constants.EMPTY_STRING).toString()
                userNameToVerify = pref.getString(Constants.USER_NAME_TO_VERIFY, Constants.EMPTY_STRING).toString()
                userIdToVerify = pref.getString(Constants.USER_ID_TO_VERIFY, Constants.EMPTY_STRING).toString()
                isProfessional = pref.getBoolean(Constants.IS_PROFESSIONAL, false)
                userPhone = pref.getString(Constants.USER_PHONE, Constants.EMPTY_STRING).toString()
            }
        }catch (ne: NullPointerException){
            Timber.e(ne)
        }catch (e:Exception){
            Timber.e(e)
        }
    }

    abstract fun initUIAndActions()

    abstract fun initAdapters()

    abstract fun initData()

    protected inline fun <reified T : ViewDataBinding> binding(
        inflater: LayoutInflater,
        resId: Int,
        container: ViewGroup?
    ): T = DataBindingUtil.inflate(inflater, resId, container, false)

    fun fadeIn(view: View){
        if(!isFadeIn){
            view.startAnimation((AnimationUtils.loadAnimation(context, R.anim.fade_in)))
            isFadeIn = true
        }
    }
}