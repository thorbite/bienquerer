package com.jujuy.bienquerer.base

import android.os.Bundle
import android.os.PersistableBundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.jujuy.bienquerer.R
import com.skydoves.whatif.whatIfNotNull
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.android.ext.android.inject
import timber.log.Timber
import kotlin.time.ExperimentalTime

abstract class BaseActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
    }

    fun initToolbar(toolbar: Toolbar, title: String): Toolbar {
        toolbar.whatIfNotNull {
            it.title = title
            try {
                it.setTitleTextColor(ContextCompat.getColor(this, R.color.white))
            }catch (e:Exception){
                Timber.e(e)
            }

            try {
                setSupportActionBar(it)
                supportActionBar?.setDisplayHomeAsUpEnabled(true)
            }catch (e:Exception){
                Timber.e(e)
            }

            title.whatIfNotNull { setToolbarText(toolbar,title)}
        }
        return toolbar
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        item.whatIfNotNull {
            when(item.itemId){
                android.R.id.home -> {onBackPressed(); return true}
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun setToolbarText(toolbar: Toolbar, text:String) {
        this.supportActionBar?.title = text
    }

    fun setupFragment(fragment: Fragment){
        try {
            this.supportFragmentManager.beginTransaction()
                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                .replace(R.id.content_frame, fragment)
                .commitAllowingStateLoss()
        }catch (e:Exception){
            Timber.e(e)
        }
    }

    fun setupFragment(fragment: Fragment, frameId: Int){
        try {
            this.supportFragmentManager.beginTransaction()
                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                .replace(frameId, fragment)
                .commitAllowingStateLoss()
        }catch (e:Exception){
            Timber.e(e)
        }
    }
}
