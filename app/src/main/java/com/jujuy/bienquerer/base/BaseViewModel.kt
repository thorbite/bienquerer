package com.jujuy.bienquerer.base

import androidx.lifecycle.*
import com.jujuy.bienquerer.utils.Utils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
open class BaseViewModel: ViewModel() {
    var loadingDirection: Utils.LoadingDirection = Utils.LoadingDirection.none
    private val loadingState = MutableLiveData<Boolean>()

    var offset = 0

    var forceEndLoading = false
    var isLoading = false

    internal fun <T> launchOnViewModelScope(block: suspend () -> LiveData<T>): LiveData<T> {
        return liveData(viewModelScope.coroutineContext + Dispatchers.IO) {
            emitSource(block())
        }
    }

    //region For loading status
    public fun setLoadingState(state: Boolean) {
        isLoading = state
        loadingState.value = state
    }

    fun getLoadingState(): MutableLiveData<Boolean>? {
        return loadingState
    }

    //endregion
}